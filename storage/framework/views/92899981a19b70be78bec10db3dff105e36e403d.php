    
    <?php $__env->startSection('content'); ?>
    <!-- Start Content-->
    <div class="container-fluid">
        <input type="hidden" id="lista_contas" value="<?php echo e($lista_contas); ?>"/>
        <input type="hidden" id="contas_bancaria" value="<?php echo e($contas_bancaria); ?>"/>
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <form class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control form-control-light" id="dash-daterange">
                                    <div class="input-group-append">
                                        <span class="input-group-text bg-primary border-primary text-white">
                                            <i class="mdi mdi-calendar-range font-13"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript: void(0);" class="btn btn-primary ml-2">
                                <i class="mdi mdi-autorenew"></i>
                            </a>
                            <a href="javascript: void(0);" class="btn btn-primary ml-1">
                                <i class="mdi mdi-filter-variant"></i>
                            </a>
                        </form>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-5 col-lg-6">

                <div class="row">
                    <div class="col-sm-6">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-end">
                                    <i class="mdi mdi-account-multiple widget-icon"></i>
                                </div>
                                <h5 class="text-muted fw-normal mt-0" title="Numeros de Clientes">Clientes</h5>
                                <h3 class="mt-3 mb-3"><?php echo e($dados['clientes']); ?></h3>
                                <p class="mb-0 text-muted">
                                    <span class="<?php echo e($dados['clientes_percentual'] < 0 ? 'text-danger' : 'text-success'); ?> me-2"><i class="mdi <?php echo e($dados['clientes_percentual'] < 0 ? 'mdi-arrow-down-bold' : 'mdi-arrow-up-bold'); ?> "></i> <?php echo e(abs($dados['clientes_percentual'])); ?>%</span>
                                    <span class="text-nowrap">Desde o último mês</span>  
                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->

                    <div class="col-sm-6">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-end">
                                    <i class="mdi mdi-cart-plus widget-icon"></i>
                                </div>
                                <h5 class="text-muted fw-normal mt-0" title="Numero de Propostas">Propostas</h5>
                                <h3 class="mt-3 mb-3"><?php echo e($dados['propostas_percentual']); ?></h3>
                                <p class="mb-0 text-muted">
                                    <span class="<?php echo e($dados['propostas_percentual'] < 0 ? 'text-danger' : 'text-success'); ?> me-2"><i class="mdi <?php echo e($dados['propostas_percentual'] < 0 ? 'mdi-arrow-down-bold' : 'mdi-arrow-up-bold'); ?> "></i> <?php echo e(abs($dados['propostas_percentual'])); ?>%</span>
                                    <span class="text-nowrap">Desde o último mês</span>
                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-sm-6">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-end">
                                    <i class="mdi mdi-currency-usd widget-icon"></i>
                                </div>
                                <h5 class="text-muted fw-normal mt-0" title="Receita Média">Receitas</h5>
                                <h3 class="mt-3 mb-3">$6,254</h3>
                                <p class="mb-0 text-muted">
                                    <span class="text-danger me-2"><i class="mdi mdi-arrow-down-bold"></i> 7.00%</span>
                                    <span class="text-nowrap">Desde o último mês</span>
                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->

                    <div class="col-sm-6">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-end">
                                    <i class="mdi mdi-pulse widget-icon"></i>
                                </div>
                                <h5 class="text-muted fw-normal mt-0" title="Crescimento">Crescimento</h5>
                                <h3 class="mt-3 mb-3">+ 30.56%</h3>
                                <p class="mb-0 text-muted">
                                    <span class="text-success me-2"><i class="mdi mdi-arrow-up-bold"></i> 4.87%</span>
                                    <span class="text-nowrap">Desde o último mês</span>
                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->
                </div> <!-- end row -->

            </div> <!-- end col -->

            <div class="col-xl-7 col-lg-6">
                <div class="card card-h-100">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-2">
                            <h4 class="header-title">Projeções Vs Reais</h4>
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle arrow-none card-drop" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="mdi mdi-dots-vertical"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-end">
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item">Sales Report</a>
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item">Export Report</a>
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item">Profit</a>
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item">Action</a>
                                </div>
                            </div>
                        </div>

                        <div dir="ltr">
                            <div id="high-performing-product" class="apex-charts" data-colors="#727cf5,#e3eaef"></div>
                            <canvas id="myChart" width="400" height="250"></canvas>
                        </div>
                            
                    </div> <!-- end card-body-->
                </div> <!-- end card-->

            </div> <!-- end col -->
        </div>
        <!-- end row -->
        
        <!-- end row -->
       
        <!-- end row -->


        
        <!-- end row -->

    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

    <script>
        const labels = ['JAN','FEV','MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'];
      
    const data = {
        labels: labels,
        datasets: [
            {
            label: 'Propostas',
            data: [10, 16, 18, 10, 15, 18, 12],
            backgroundColor: 'rgba(255, 190, 132, 0.2)' ,
            
            stack: 'Stack 0',
        },
        {
            label: 'Vendas',
            data: [2, 4, 4, 3, 6, 11, 2],
            backgroundColor: 'rgba(255, 99, 132, 0.2)' ,
            stack: 'Stack 0',
        }
        ]
    };
      
    const config = {
        type: 'bar',
        data: data,
        options: {
            plugins: {
                title: {
                    display: true,
                    text: 'Chart.js Bar Chart - Stacked'
                },
            },
            responsive: true,
            interaction: {
                intersect: false,
            },
            scales: {
                x: {
                    stacked: true,
                },
                y: {
                    stacked: true
                }
            }
        }
    };
    </script>
    <script>
        const myChart = new Chart(
            document.getElementById('myChart'),
            config
        );
    </script>
    <script>
        let lista_contas = JSON.parse(document.getElementById('lista_contas').value);
        let contas_bancaria = JSON.parse(document.getElementById('contas_bancaria').value);

        const date = moment().format('YYYY-MM-DD');
        let data_final = ''

        // Status = recebido - A Receber - Pago - A Pagar
        const lista_contas_all = filtroAvancado(date, data_final, 'A Receber');
        selecionaPeriodoPagar(7);
        selecionaPeriodoAPagar(7);


        function filtroAvancado(data_inicial, data_final,  status ) {

            return   lista_contas.filter((item) => {
                if (status == 'Pago' || status == 'Recebido') {
                return  item.status == status && item.data_pagto <= data_inicial && item.data_pagto >= data_final
                }else {
                return  item.status == status && item.data_pagto >= data_inicial && item.data_pagto <= data_final
                }
            });
        }

        function converteData(value, params) {
            return params == 'add' ? moment().add(value, 'day').format('YYYY-MM-DD') : moment().subtract(value, 'day').format('YYYY-MM-DD')
        }

        function converteAdd_Sub(status) {
            return  resultado =   status == 'A Pagar' || status == 'A Receber' ?  'add' :  'subtract';
        }

        function selecionaPeriodoPagar(value) {
            
            saldo_contas_pagas.textContent = 'R$ '+filtroDataStatus('Pago', value);
            saldo_contas_recebidas.textContent = 'R$ '+filtroDataStatus('Recebida', value);

            saldo_pago_text.textContent = 'R$ '+ (filtroDataStatus('Recebido', value) - filtroDataStatus('Pago', value)).toFixed(2);
            saldo_pago = (filtroDataStatus('Recebido', value) - filtroDataStatus('Pago', value)).toFixed(2);
            

           if ( Math.sign(saldo_pago)  == -1 ) {
            saldo_pago_text.classList.remove('text-primary');
            saldo_pago_text.classList.add('text-danger');
           }
            
        }

        function selecionaPeriodoAPagar(value) {
            
            saldo_contas_a_pagar.textContent = 'R$ '+filtroDataStatus('A Pagar', value);
            saldo_contas_a_receber.textContent = 'R$ '+filtroDataStatus('A Receber', value);

            saldo_a_pagar_text.textContent = 'R$ '+ (filtroDataStatus('A Receber', value) - filtroDataStatus('A Pagar', value)).toFixed(2);
            saldo_a_pagar = (filtroDataStatus('A Receber', value) - filtroDataStatus('A Pagar', value)).toFixed(2);
            

           if (  Math.sign(saldo_a_pagar)  == -1) {
            saldo_pago_text.classList.remove('text-primary');
            saldo_pago_text.classList.add('text-danger');
           }
            
        }

        function selecionaTipo(status) {
            
        }

        function filtroDataStatus(status, value) {
            let add_sub = '';

            add_sub    = converteAdd_Sub(status)
            data_final = converteData(value, add_sub)
            
            const resultado = filtroAvancado(date, data_final,  status );
            const valor_total =  resultado.map(li => parseFloat(li.valor_parcela)).reduce((sum, val) => sum + val, 0)

                if (valor_total > 0) {
                return valor_total.toFixed(2)
                } else {
                return valor_total
                }
                
        }


    </script>
  <?php $__env->stopSection(); ?>
 



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/dashboard_ecommerce.blade.php ENDPATH**/ ?>