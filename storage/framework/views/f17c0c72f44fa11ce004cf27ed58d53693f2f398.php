<?php $__env->startSection('content'); ?>
<!-- Start Content-->
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Consultorias</a></li>
                        <li class="breadcrumb-item active">Montar Consultoria</li>
                    </ol>
                </div>
                <h4 class="page-title">Montar Consultoria  - 1ª Etapa</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?php echo e(route('consultorias.montar')); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <?php echo method_field('POST'); ?>
            
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Selecione o dados para consultoria</h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group id_cliente">
                                            <label for="projectname">Cliente</label>
                                            <select class="form-control select2" data-toggle="select2" name="id_cliente"
                                                id="id_cliente" required>
                                                <option value="0">Selecione o Cliente ...</option>
                                                <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>" style="text-transform: capitalize;">
                                                        <?php echo e($item->nome); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group id_cliente">
                                            <label for="projectname">Concorrentes</label>
                                            <select multiple class="form-control select2" data-toggle="select2" name="concorrentes[]" 
                                                id="concorrentes" required>
                                                <option value="0">Selecione o Concorrente ...</option>
                                                <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>" style="text-transform: capitalize;">
                                                        <?php echo e($item->nome); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group id_cliente">
                                            <label for="projectname">Referência</label>
                                            <select class="form-control select2" data-toggle="select2" name="id_referencia"
                                                id="id_cliente" required>
                                                <option value="0">Selecione a Referência ...</option>
                                                <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>" style="text-transform: capitalize;">
                                                        <?php echo e($item->nome); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-xl-2">
                                                        <div class="form-group">
                                                            <a class="btn btn-danger" href="<?php echo e(URL::previous()); ?>">
                                                                    Cancelar<span class="badge badge-primary"></span>
                                                            </a>
                                                        </div>
                                                    </div> <!-- end col-->
                                                    <div class="col-xl-2 pl-2">
                                                            <div class="form-group">
                                                                <button class="btn btn-primary" type="submit">
                                                                        Montar<span class="badge badge-primary"></span>
                                                                </button>
                                                            </div>
                                                        </div> <!-- end col-->
                                                </div>
                                            </div> <!-- end card-body -->
                                        </div>
                                    </div>
                                </div> <!-- end card-->
                            </div> <!-- end col-->
                        </div>
                    </form>
                  
                </div> <!-- end card-body-->
            </div> <!-- end card-->
          

        </div> <!-- end col -->
    </div>
    <!-- end row-->
</div> <!-- container -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/agencia_WEB/resources/views/content/consultorias/consultoria.blade.php ENDPATH**/ ?>