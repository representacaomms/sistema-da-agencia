<?php $__env->startSection('content'); ?>
    <div class="container-fluid">

         <!-- start page title -->
         <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Vendas</a></li>
                            <li class="breadcrumb-item active">Proposta</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Proposta</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="container">
                        <div class="card-body">

                            <!-- Invoice Logo-->
                            <div class="clearfix">
                                <div class="float-left mb-3">
                                    <img src="https://agenciamms.com.br/wp-content/uploads/2021/01/Logo-transparente.png"
                                        alt="" height="100">
                                </div>
                                <div class="float-right">
                                    <h4 class="m-0 d-print-none">APRESENTACAO
                                    </h4>
                                </div>
                            </div>

                        

                            <div class="row text-left" style="page-break-inside:avoid">
                                <div class="col-12">
                                    <h4>PRODUTOS E SERVIÇOS:</h4>
                                    <h5>De acordo com as expectativas alinhadas no briefing os produtos e serviços aqui tem
                                        como
                                        principais objetivos ajudar a<br />
                                        <p> <b>AUMENTAR AS VENDAS, GERAR LEADS
                                                QUALIFICADOS .</b></p>
                                    </h5>
                                    <h4 class="text-center pt-3">Para isso determinamos os seguintes produtos e serviços:
                                    </h4>

                                </div> <!-- end col -->
                            </div>
                            <!-- end row -->
                            <div class="row pt-3" style="page-break-inside:avoid">
                                <div class="col-md-12">
                                    <dl class="row mb-0 text-left">
                                        <dt class="col-sm-3">KJASDKAJSK</dt>
                                        <dd class="col-sm-9">AHSDJJKSDKASDHKASHK}</dd>
                                    </dl>
                                    <hr>
                                </div>
                            </div>

                            <div class="row pt-3" style="page-break-inside:avoid">
                                <div class="col-md-12">
                                    <h4>Etapas do Projeto</h4>
                                    <h5 class="text-left">As seguintes etapas do projeto definem seu cronograma assim
                                        como prazos de entrega,
                                        é
                                        importante ressaltar que o prazo é estipulado em dias úteis para uma aprovação.
                                        Sendo
                                        adicionado o mesmo prazo para cada alteração caso seja solicitada com o limite de 3
                                        alterações no total.
                                    </h5>
                                    <h5 class="text-left">
                                        Ex: Se o prazo é estipulado em 5 dias e é solicitada 1 alteração, deve-se estender
                                        mais
                                        5 dias de prazo para a entrega da etapa em questão.</h5>

                                    <h5 class="text-left">As etapas serão passadas por email que ficará como oficial o
                                        relacionamento entre as
                                        partes.</h5>
                                    <h5 class="pt-3 text-left">Por fim, o investimento mensal em campanhas de tráfego no
                                        Facebook Ads para Facebook
                                        e
                                        Instagram e no Google Ads para Google e YouTube.:
                                    </h5>
                                </div>
                            </div>

                            <div class="row pt-3" style="page-break-inside:avoid">
                                <div class="col-md-12 text-left">
                                    <h4>BUDGET DE CAMPANHAS:</h4>
                                    <h5><b>IMPORTANTE:</b>O budget de até R$ 3.000,00 (três mil reais) será
                                        100%
                                        investido em campanhas distribuídos entre as redes de tráfego como isenção. A partir
                                        deste valor 20% do budget é destinado a agência para custos de gestão da campanha.
                                    </h5>
                                    <ul>
                                        <li>01 - Budget Google - R$ 900,00 - mensal</li>
                                        <li>01 - Budget Facebook - R$ 900,00 - mensal</li>
                                    </ul>


                                </div>
                            </div>

                            <div class="row pt-3 " style="page-break-inside:avoid">
                                <div class="col-md-12 text-left">
                                    <h4>LICENÇAS E FERRAMENTAS DE TERCEIROS:</h4>
                                    <h5>Além dos valores estipulados algumas ferramentas são essenciais para a execução do
                                        projeto, a agência fica encarregada apenas da gestão e bom uso das mesmas, mas nunca
                                        dos
                                        valores, pagamentos ou atualizações de planos:
                                    </h5>
                                    <ul>
                                        <li>01 - Elementor Pro - USD 49,00/Anual</li>
                                        <li>01 - ConvertKit - USD 29,00/Mensal</li>
                                    </ul>


                                </div>
                            </div>

                            <div class="row pt-3" style="page-break-inside:avoid">

                                <div class="col-md-12 text-left">
                                    <h4><b>INVESTIMENTO DO PROJETO</b></h4>
                                    <h5>Para a execução do projeto assim como a busca pelos resultados já estipulados nesta
                                        proposta, levando em consideração um contrato de 12 meses para cálculo de descontos,
                                        os
                                        seguintes itens e valores devem ser considerados:</h5>
                                  end table-responsive-->
                                </div>
                            </div>

                            <div class="row pt-3 text-left" style="page-break-inside:avoid">
                                <div class="col-sm-7 forma-pagto">
                                    <div class="clearfix ">
                                        <h6 class="text-muted">FORMAS DE PAGAMENTO:</h6>
                                        <li id="pri_parcelado"></li>
                                        <li id="seg_parcelado" ></li>
                                        <li id="ter_parcelado" ></li>
                                    </div> 
                                </div> <!-- end col -->
                                <div class="col-sm-5">
                                   
                                    <div class=" mt-3 mt-sm-0">
                                        <hr>
                                        <p><b>Projeto: &nbsp;</b> <span class="float-right" id="projeto" ></span>
                                        </p>
                                        <p><b>Desconto: &nbsp;</b> <span class="float-right" id="desconto"></span>
                                        </p>
                                        <p><b>Sub-Total: &nbsp;</b> <span class="float-right" id="subTotal-projeto"></span>
                                    </p>
                                    </div>
                                    <div class=" pt-3 mt-sm-0">
                                        <hr>
                                        <p><b>Gestão: &nbsp;</b> <span  class="float-right" id="plano"></span>
                                        </p>
                                        <p><b>Desconto: &nbsp;</b> <span class="float-right" id="desconto_plano"></span>
                                        </p>
                                        <p><b>Sub-Total: &nbsp;</b> <span class="float-right" id="subTotal-plano"> </span>
                                        </p>
                                        <h4>&nbsp;&nbsp;&nbsp;</h4>
                                    </div>
                                    <div class="clearfix"></div>
                                </div> <!-- end col -->
                            </div>
                            <!-- end row-->
                            <div class="row" style="page-break-inside:avoid">
                                <div class="col-md-12">
                                    <h4>ANOTAÇÕES:</h4>
                                    <h5> As anotações abaixo feitas servem para ajustes a presente proposta e devem ser
                                        consideradas para a confecção do contrato e execução do projeto.</h5>

                                </div>
                            </div>
                            <div class="row pt-4">
                                <div class="col-md-12">
                                    <div id="snow-editor" style="height: 300px"></div>
                                  
                                </div>
                            </div>

                            <div class="row mt-3" style="page-break-inside:avoid">
                                <div class="col-md-12">
                                    <h4>CONDIÇÕES GERAIS::</h4>
                                    <h5 class="text-left"> Na expectativa de oferecer a melhor solução em busca de
                                        AUMENTAR AS VENDAS, OBTER
                                        MAIS LEADS QUALIFICADOS e certos da melhor oferta para os serviços necessários ao
                                        resultado, aproveitamos a oportunidade para apresentar protestos de consideração e
                                        apreço.</h5>
                                    <h5 class="text-left">
                                        <ul>
                                            <li>Validade da proposta: 10 (dez dias) corridos.

                                            </li>
                                        </ul>
                                    </h5>
                                    <h5 class="pt-2 text-left">
                                        Estando de acordo, assinam as partes autorizando confecção do contrato para
                                        pagamento da primeira parcela e início do projeto;
                                    </h5>
                                </div>
                            </div>
                            <div class="row pt-5"></div>

                           
                            <div class="d-print-none mt-4" style="page-break-inside:avoid">
                                <div class="text-right">
                                    <a href="javascript:window.print()" class="btn btn-primary"><i
                                            class="mdi mdi-printer"></i> Imprimir</a>
                                    <a href="<?php echo e(route('propostas.index')); ?>" class="btn btn-info">Fechar</a>
                                   
                                </div>
                            </div>
                            <!-- end buttons -->

                        </div> <!-- end card-body-->
                    </div>
                </div> <!-- end card -->
            </div> <!-- end col-->
        </div>
        <!-- end row-->
    </div> <!-- container -->
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/apresentacao/view.blade.php ENDPATH**/ ?>