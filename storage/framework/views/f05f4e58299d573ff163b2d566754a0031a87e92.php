<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('contas-bancarias.index')); ?>">Compra</a></li>
                            <li class="breadcrumb-item active">Adicionar Conta Bancária</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Conta Bancária</h4>                   
                </div>
            </div>
        </div>
        
        <form name="formOrcamento" action="<?php echo e(route('contas-bancarias.store')); ?>" method="POST">
            
            <?php echo method_field('POST'); ?>
            <?php echo csrf_field(); ?>
            <div class="row">               
                <div class="col-xl-12">
                    <!-- tasks panel -->
                    <div class="collapse show" id="todayTasks">
                        <div class="card mb-0">
                            <div class="card-body">
                                <a class="text-dark" data-toggle="collapse" href="#todayTasks" aria-expanded="false" aria-controls="todayTasks">
                                    <h4 class="m-0 pb-2">
                                        <i class="uil uil-angle-down font-18"></i>Detalhes Conta Bancária <span class="text-muted"></span>
                                    </h4>
                                </a>
                                <hr>
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="form-group cliente">
                                            <label >Banco</label>
                                            <select class="form-control select2" data-toggle="select2" name="banco" >
                                                <option value="" style="text-transform: capitalize;" >Selececione o banco ...</option>
                                                <?php $__currentLoopData = $bancos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($item['id']); ?> " style="text-transform: capitalize;" ><?php echo e($item['nome']); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                            
                                            </select> 
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-4">
                                        <div class="form-group">
                                            <label for="projectname">Conta com Digito</label>
                                            <input type="text" name="conta" id="conta" class="form-control"  >
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-2"></div> <!-- end col-->
                                    <div class="col-xl-2">
                                        <div class="form-group">
                                            <label for="projectname">Status</label>
                                            <select class="form-control"  name="status" >                                               
                                                <option value="A" style="text-transform: capitalize;" >Ativa</option>
                                                <option value="I" style="text-transform: capitalize;" >Inativa</option>
                                                                                      
                                            </select> 
                                        </div>
                                    </div> <!-- end col-->
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">                       
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="projectname">Saldo Inicial  &nbsp;</label>                                           
                                                    <input type="text" name="saldo" placeholder="0,00"  data-toggle="input-mask" data-mask-format="###0,00" data-reverse="true"  class="form-control">
                                                </div>
                                            </div> <!-- end col-->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="projectname">&nbsp;</label>
                                                    <button type="submit" class="btn btn-outline-primary form-control" id="addProd"> Criar </button> 
                                                </div>                                                                                        
                                            </div> <!-- end col-->                                           
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- end row -->        
                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end .collapse-->
                    <br>
                </div>
            </div>
        </form>
    </div> <!-- container --> 
    
    <?php $__env->startSection('js'); ?>
    <?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/financeiros/conta_bancaria/create.blade.php ENDPATH**/ ?>