<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu ">
    <div class="slimscroll-menu" id="left-side-menu-container">

        <!-- LOGO -->
        <a href="<?php echo e(route('home')); ?>" class="logo text-center">
            <span class="logo-lg">
                <img src="https://agenciamms.com/wp-content/uploads/2021/05/Logo-branco2-1.png" alt="" height="72">
            </span>
            <span class="logo-sm">
                <img src="https://agenciamms.com/wp-content/uploads/2021/05/Logo-branco2-1.png" alt="" height="32">
            </span>
        </a>

        <!--- Sidemenu -->
        <ul class="metismenu side-nav">
            <li class="side-nav-title side-nav-item">Navegação</li>

            <li class="side-nav-item">
                <a href="javascript: void(0);" class="side-nav-link">
                    <i class="dripicons-meter"></i>
                    <span class="badge badge-success float-right"></span>
                    <span> Paineis</span>
                </a>
                <ul class="side-nav-second-level" aria-expanded="false">
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Painel CRM')): ?>
                    <li>
                        <a href="<?php echo e(route('home')); ?>">CRM</a>
                    </li>
                    <?php endif; ?>
                    
                </ul>
            </li>
            <li class="side-nav-title side-nav-item">Configuração</li>
            
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Gestão Financeira')): ?>
                <li class="side-nav-item">
                    <a href="javascript: void(0);" class="side-nav-link">
                        <i class="dripicons-battery-full"></i>
                        <span> Financeiro </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="side-nav-second-level" aria-expanded="false">
                        <li><a href="<?php echo e(route('cadastros.diversos')); ?>">Diversos</a></li>
                    </ul>
                    <ul class="side-nav-second-level" aria-expanded="false">
                        <li><a href="<?php echo e(route('custos.index')); ?>">Custos</a></li>
                    </ul>
                    <ul class="side-nav-second-level" aria-expanded="false">
                        <li><a href="<?php echo e(route('custos-salarios.index')); ?>">Salários</a></li>
                    </ul>
    
                    <ul class="side-nav-second-level" aria-expanded="false">
                        <li><a href="<?php echo e(route('receitas.index')); ?>">Receitas</a></li>
                    </ul>
                    <ul class="side-nav-second-level" aria-expanded="false">
                        <li><a href="<?php echo e(route('despesas.index')); ?>">Despesas</a></li>
                    </ul>
                </li>
                <?php endif; ?>
            
            <li class="side-nav-title side-nav-item">Gestão</li>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Gestão Planos')): ?>
            <li class="side-nav-item">
                <a href="javascript: void(0);" class="side-nav-link">
                    <i class="dripicons-article"></i>
                    <span>Planos</span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="side-nav-second-level" aria-expanded="false">
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Listar Planos')): ?>
                    <li class="side-nav-item">
                        <a href="<?php echo e(route('planos.index')); ?>">Todos os Planos</a>
                    </li>
                    <?php endif; ?>
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Listar Serviços')): ?>
                    <li class="side-nav-item">
                        <a href="<?php echo e(route('servicos-agenciado.index')); ?>">Serviços</a>
                    </li>
                    <?php endif; ?>

                </ul>
            </li>
            <?php endif; ?>
            
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Gestão Pessoas')): ?>
                <li class="side-nav-item">
                    <a href="javascript: void(0);" class="side-nav-link">
                        <i class="dripicons-user-group"></i>
                        <span> Pessoas</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="side-nav-second-level" aria-expanded="false">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Listar Prospectos')): ?>
                        <li class="side-nav-item">
                            <a href="<?php echo e(route('leads.index')); ?>">Prospectos</a>
                        </li>
                        <?php endif; ?>

                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Listar Clientes')): ?>
                            <li class="side-nav-item">
                                <a href="<?php echo e(route('clientes.index')); ?>">Clientes</a>
                            </li>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Listar Fornecedores')): ?>
                        <li class="side-nav-item">
                            <a href="<?php echo e(route('fornecedores.index')); ?>">Fornecedores</a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </li>
                    
                <?php endif; ?>
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Gestão Acessos')): ?>
                <li class="side-nav-item">
                    <a href="javascript: void(0);" class="side-nav-link">
                        <i class="dripicons-clockwise"></i>
                        <span> Acessos</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="side-nav-second-level" aria-expanded="false">

                        <li class="side-nav-item">
                            <a href="<?php echo e(route('usuarios.index')); ?>">Usuários</a>
                        </li>
                        <li class="side-nav-item">
                            <a href="<?php echo e(route('funcoes.index')); ?>">Funções</a>
                        </li>
                        <li class="side-nav-item">
                            <a href="<?php echo e(route('permissoes.index')); ?>">Permissões</a>
                        </li>

                    </ul>
                </li>
                <?php endif; ?>
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Gestão Vendas')): ?>
                <li class="side-nav-item">
                    <a href="javascript: void(0);" class="side-nav-link">
                        <i class="dripicons-cart"></i>
                        <span> Vendas </span>
                        <span class="menu-arrow"></span>
                    </a>
                    
                     <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Listar Propostas')): ?>
                     <ul class="side-nav-second-level" aria-expanded="false">
                         <li class="side-nav-item">
                             <a href="<?php echo e(route('propostas.index')); ?>">Propostas</a>
                         </li>
                     </ul>
                     <?php endif; ?>

                    

                </li>
                <?php endif; ?>
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Gestão Marketing')): ?>
                <li class="side-nav-item">
                    <a href="javascript: void(0);" class="side-nav-link">
                        <i class="dripicons-broadcast"></i>
                        <span> Marketing </span>
                        <span class="menu-arrow"></span>
                    </a>
                        
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Gestão Trafego Pago')): ?>
                    <ul class="side-nav-second-level" aria-expanded="false">
                        <li class="side-nav-item">
                            <a href="<?php echo e(route('plataformas.index')); ?>">Trafego Pago</a>
                        </li>
                    </ul>
                    <?php endif; ?>
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Gestão Linha Editorial')): ?>
                    <ul class="side-nav-second-level" aria-expanded="false">
                        <li class="side-nav-item">
                            <a href="<?php echo e(route('linhas-editoriais.index')); ?>">Linha Editorial</a>
                        </li>
                    </ul>
                    <?php endif; ?>

                    

                </li>
                <?php endif; ?>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Gestão Financeira')): ?>
            <li class="side-nav-item">
                <a href="javascript: void(0);" class="side-nav-link">
                    <i class="dripicons-graph-line"></i>
                    <span> Financeiro </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="side-nav-second-level" aria-expanded="false">
                    <li><a href="<?php echo e(route('receitas.index')); ?>">Receitas</a></li>
                    <li><a href="<?php echo e(route('despesas.index')); ?>">Despesas</a></li>
                    <li class="mt-2"><a href="<?php echo e(route('custos.index')); ?>">Custos</a></li>
                </ul>
            </li>
            <?php endif; ?>

            <li class="side-nav-title side-nav-item mt-1">Relatórios</li>

            <li class="side-nav-item">
                <a href="javascript: void(0);" class="side-nav-link">
                    <i class="dripicons-copy"></i>
                    <span> Terceiros </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="side-nav-second-level" aria-expanded="false">
                    <li>
                        <a href="#">### Page</a>
                    </li>

                </ul>
            </li>

            
        </ul>

        <!-- Help Box -->
        <div class="help-box help-box-light text-center">
            <a href="javascript: void(0);" class="float-right close-btn text-body">
                <i class="mdi mdi-close"></i>
            </a>
            <img src="<?php echo e(asset('images/help-icon.svg')); ?>" height="90" alt="Helper Icon Image" />
            <h5 class="mt-3">Acesso ilimitado</h5>
            <p class="mb-3">Faça upgrade para planejar o acesso a relatórios ilimitados</p>
            <a href="javascript: void(0);" class="btn btn-outline-primary btn-sm">Upgrade</a>
        </div>
        <!-- end Help Box -->
        <!-- End Sidebar -->

        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
<?php /**PATH /Users/mac-marcelo/projetos/Sistema Agencia/Web/resources/views/layouts/sidebar.blade.php ENDPATH**/ ?>