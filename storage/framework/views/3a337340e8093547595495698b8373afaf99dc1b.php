<?php $__env->startSection('content'); ?>
<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Prospectos</a></li>
                        <li class="breadcrumb-item active">Listar Prospectos</li>
                    </ol>
                </div>
                <h4 class="page-title">Listar Prospectos</h4>
            </div>
        </div>
    </div>
    <form name="frmTasks">
        <input type="hidden" id="records" value="<?php echo e($records); ?>"></input>
    <!-- end page title -->
                 <div  class="board" >
                    <div class="tasks" style="width: 48%" id="divSemInteresse">
                        <h5 class='mt-0 task-header'>Prospectos - Sem Interesse / Perdido</h5>
                        <input type="text" class="btn btn-outline-primary btn-lg center" id="qte_sem_interesse"></input>
                        <input type="hidden" id="rotulo" value="sem_interesse"></input>
                    </div>
                    
                    <div class="tasks" style="width: 48%" id="divAdquirido">
                        <h5 class='mt-0 task-header'>Prospectos - Adquirido</h5>
                        <input type="text" class="btn btn-outline-primary btn-lg center" id="qte_adquirido"></input>
                        <input type="hidden" id="rotulo" value="adquirido"></input>
                    </div>
                </div>

        <div class="row">
            <div class="col-12">
                <div class="board">
                    <div class="tasks">
                    <h5 class="mt-0 task-header">Prospecto</h5>
                     
                     <input type="hidden" id="rotulo" value="inbound"></input>
                        <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($record->rotulo == 'prospectos' && $record->status == 'inbound' ): ?>
                            <div>
                                
                                <!-- Task Item -->
                                <div class="card mb-0" draggable="true">
                                    <div class="card-body p-3">
                                        
                                        <small class="float-end text-muted"><?php echo e($record->updated_at->format('d/m/Y')); ?></small>
                                        
                                        <input type="hidden" id="id" value="<?php echo e($record->id); ?>"></input>
                                       
        
                                        
                                         <h4 class="mt-0">
                                            <a href="apps-projects-details.html" class="text-title"><?php echo e($record->nome); ?></a>
                                        </h4>
                                        <h4 class="mt-0">
                                            <a href="apps-projects-details.html" class="text-title"><b>EMPRESA: </b><?php echo e($record->fantasia); ?></a>
                                        </h4>
                                        <h6 class="mt-0">
                                            <a href="<?php echo e('https://wa.me/55'.$record->celular); ?>" target="_blank" class="text-title"><b>CELULAR: </b><?php echo e($record->celular); ?></a>
                                        </h6>
                                        
                                        <div class="btn float-right">
                                           <a href="<?php echo e(route('leads.edit', $record->id)); ?>"><div class="badge bg-info mb-3 text-white"><i  class="mdi mdi-pencil me-1">editar</i></div></a>
                                        </div>

                                        <p class="text-muted font-13 mb-3">With supporting text below as a natural lead-in to additional contenposuere erat a ante...<a href="javascript:void(0);" class="fw-bold text-muted">view more</a>
                                        </p>
        
                                        <p class="mb-0">
                                           
                                            <span class="text-nowrap mb-2 d-inline-block">
                                                <i class="mdi mdi-comment-multiple-outline text-muted"></i>
                                                <b>74</b> Conversas
                                            </span>
                                        </p>
        
                                        
                                    </div> <!-- end card-body -->
                                </div>
                                <!-- Task Item End -->
                            </div> <!-- end company-list-1-->
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                    <div class="tasks">
                        <h5 class="mt-0 task-header"> 1ºContato</h5>
                         <input type="hidden" id="rotulo" value="pri_contato"></input>
                        <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($record->rotulo == 'prospectos' && $record->status == 'pri_contato' ): ?>
                                <div>
                                    
                                    <!-- Task Item -->
                                    <div class="card mb-0" draggable="true">
                                        <div class="card-body p-3">
                                            
                                            <small class="float-end text-muted"><?php echo e($record->updated_at->format('d/m/Y')); ?></small>
                                            
                                            <input type="hidden" id="id" value="<?php echo e($record->id); ?>"></input>
                                           
                                            <h4 class="mt-0">
                                                <a href="apps-projects-details.html" class="text-title"><b>EMPRESA: </b><?php echo e($record->fantasia); ?></a>
                                            </h4>
                                            <h4 class="mt-0">
                                                <a href="apps-projects-details.html" class="text-title"><b>Contato: </b><?php echo e($record->nome); ?></a>
                                            </h4>
                                            <h6 class="mt-0">
                                                <a href="apps-projects-details.html" class="text-title"><b>CELULAR: </b><?php echo e($record->celular); ?></a>
                                            </h6>
                                            
                                            <div class="btn float-right">
                                               <a href="<?php echo e(route('leads.edit', $record->id)); ?>"><div class="badge bg-info mb-3 text-white"><i  class="mdi mdi-pencil me-1">editar</i></div></a>
                                            </div>

                                            <p class="text-muted font-13 mb-3"> supporting text below as a natural lead-in to additional contenposuere erat a ante...<a href="javascript:void(0);" class="fw-bold text-muted">view more</a>
                                            </p>
            
                                            <p class="mb-0">
                                               
                                                <span class="text-nowrap mb-2 d-inline-block">
                                                    <i class="mdi mdi-comment-multiple-outline text-muted"></i>
                                                    <b>74</b> Conversas
                                                </span>
                                            </p>
            
                                            
                                        </div> <!-- end card-body -->
                                    </div>
                                    <!-- Task Item End -->
                                </div> <!-- end company-list-1-->
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
                    <div class="tasks">
                    <h5 class="mt-0 task-header">Ficha Cadastral</h5>
                    <input type="hidden" id="rotulo" value="ficha_cadastral"></input>
                    <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($record->rotulo == 'prospectos' && $record->status == 'ficha_cadastral'): ?>
                          <div>
                                <!-- Task Item -->
                                <div class="card mb-0" draggable="true">
                                    <div class="card-body p-3">
                                        
                                        <small class="float-end text-muted"><?php echo e($record->updated_at->format('d/m/Y')); ?></small>
                                        
                                        <input type="hidden" id="id" value="<?php echo e($record->id); ?>"></input>
                                       
        
                                        <h4 class="mt-0">
                                            <a href="apps-projects-details.html" class="text-title"><b>EMPRESA: </b><?php echo e($record->fantasia); ?></a>
                                        </h4>
                                        <h5 class="mt-0">
                                            <a href="apps-projects-details.html" class="text-title"><b>CELULAR: </b><?php echo e($record->celular); ?></a>
                                        </h5>
                                        
                                        <div class="btn float-right">
                                           <a href="<?php echo e(route('leads.edit', $record->id)); ?>"><div class="badge bg-info mb-3 text-white"><i  class="mdi mdi-pencil me-1">editar</i></div></a>
                                        </div>

                                        <p class="text-muted font-13 mb-3">With supporting text below as a natural lead-in to additional contenposuere erat a ante...<a href="javascript:void(0);" class="fw-bold text-muted">view more</a>
                                        </p>
        
                                        <p class="mb-0">
                                           
                                            <span class="text-nowrap mb-2 d-inline-block">
                                                <i class="mdi mdi-comment-multiple-outline text-muted"></i>
                                                <b>74</b> Conversas
                                            </span>
                                        </p>
        
                                        
                                    </div> <!-- end card-body -->
                                </div>
                                <!-- Task Item End -->
                            </div> <!-- end company-list-1-->
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
                    <div class="tasks">
                    <h5 class="mt-0 task-header">Pré Briefing</h5>
                    <input type="hidden" id="rotulo" value="briefing"></input>
                    <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($record->rotulo == 'prospectos' && $record->status == 'briefing'): ?>
                          <div>
                                <!-- Task Item -->
                                <div class="card mb-0" draggable="true">
                                    <div class="card-body p-3">
                                        
                                        <small class="float-end text-muted"><?php echo e($record->updated_at->format('d/m/Y')); ?></small>
                                        
                                        <input type="hidden" id="id" value="<?php echo e($record->id); ?>"></input>
                                       
        
                                        <h4 class="mt-0">
                                            <a href="apps-projects-details.html" class="text-title"><b>EMPRESA: </b><?php echo e($record->fantasia); ?></a>
                                        </h4>
                                        <h5 class="mt-0">
                                            <a href="apps-projects-details.html" class="text-title"><b>CELULAR: </b><?php echo e($record->celular); ?></a>
                                        </h5>
                                        
                                        <div class="btn float-right">
                                           <a href="<?php echo e(route('leads.edit', $record->id)); ?>"><div class="badge bg-info mb-3 text-white"><i  class="mdi mdi-pencil me-1">editar</i></div></a>
                                        </div>

                                        <p class="text-muted font-13 mb-3">With supporting text below as a natural lead-in to additional contenposuere erat a ante...<a href="javascript:void(0);" class="fw-bold text-muted">view more</a>
                                        </p>
        
                                        <p class="mb-0">
                                           
                                            <span class="text-nowrap mb-2 d-inline-block">
                                                <i class="mdi mdi-comment-multiple-outline text-muted"></i>
                                                <b>74</b> Conversas
                                            </span>
                                        </p>
        
                                        
                                    </div> <!-- end card-body -->
                                </div>
                                <!-- Task Item End -->
                            </div> <!-- end company-list-1-->
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
                    <div class="tasks">
                    <h5 class="mt-0 task-header">Consultoria</h5>
                    <input type="hidden" id="rotulo" value="consultoria"></input>
                    <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <?php if($record->rotulo == 'prospectos' && $record->status == 'consultoria'): ?>
                           <div>
                                <!-- Task Item -->
                                <div class="card mb-0" draggable="true">
                                    <div class="card-body p-3">
                                        
                                        <small class="float-end text-muted"><?php echo e($record->updated_at->format('d/m/Y')); ?></small>
                                        
                                        <input type="hidden" id="id" value="<?php echo e($record->id); ?>"></input>
                                       
        
                                        <h4 class="mt-0">
                                            <a href="apps-projects-details.html" class="text-title"><b>EMPRESA: </b><?php echo e($record->fantasia); ?></a>
                                        </h4>
                                        <h5 class="mt-0">
                                            <a href="apps-projects-details.html" class="text-title"><b>CELULAR: </b><?php echo e($record->celular); ?></a>
                                        </h5>
                                        
                                        <div class="btn float-right">
                                           <a href="<?php echo e(route('leads.edit', $record->id)); ?>"><div class="badge bg-info mb-3 text-white"><i  class="mdi mdi-pencil me-1">editar</i></div></a>
                                        </div>

                                        <p class="text-muted font-13 mb-3">With supporting text below as a natural lead-in to additional contenposuere erat a ante...<a href="javascript:void(0);" class="fw-bold text-muted">view more</a>
                                        </p>
        
                                        <p class="mb-0">
                                           
                                            <span class="text-nowrap mb-2 d-inline-block">
                                                <i class="mdi mdi-comment-multiple-outline text-muted"></i>
                                                <b>74</b> Conversas
                                            </span>
                                        </p>
        
                                        
                                    </div> <!-- end card-body -->
                                </div>
                                <!-- Task Item End -->
                            </div> <!-- end company-list-1-->
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
                    <div class="tasks">
                    <h5 class="mt-0 task-header">proposta</h5>
                    <input type="hidden" id="rotulo" value="proposta"></input>
                    <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <?php if($record->rotulo == 'leads' && $record->status == 'proposta'): ?>
                            <div>
                                <!-- Task Item -->
                                <div class="card mb-0" draggable="true">
                                    <div class="card-body p-3">
                                        
                                        <small class="float-end text-muted"><?php echo e($record->updated_at->format('d/m/Y')); ?></small>
                                        
                                        <input type="hidden" id="id" value="<?php echo e($record->id); ?>"></input>
                                       
        
                                        <h4 class="mt-0">
                                            <a href="apps-projects-details.html" class="text-title"><b>EMPRESA: </b><?php echo e($record->fantasia); ?></a>
                                        </h4>
                                        <h5 class="mt-0">
                                            <a href="apps-projects-details.html" class="text-title"><b>CELULAR: </b><?php echo e($record->celular); ?></a>
                                        </h5>
                                        
                                        <div class="btn float-right">
                                           <a href="<?php echo e(route('leads.edit', $record->id)); ?>"><div class="badge bg-info mb-3 text-white"><i  class="mdi mdi-pencil me-1">editar</i></div></a>
                                        </div>

                                        <p class="text-muted font-13 mb-3">With supporting text below as a natural lead-in to additional contenposuere erat a ante...<a href="javascript:void(0);" class="fw-bold text-muted">view more</a>
                                        </p>
        
                                        <p class="mb-0">
                                           
                                            <span class="text-nowrap mb-2 d-inline-block">
                                                <i class="mdi mdi-comment-multiple-outline text-muted"></i>
                                                <b>74</b> Conversas
                                            </span>
                                        </p>
        
                                        
                                    </div> <!-- end card-body -->
                                </div>
                                <!-- Task Item End -->
                            </div> <!-- end company-list-1-->
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
                    <div class="tasks">
                    <h5 class="mt-0 task-header">Lead</h5>
                    <input type="hidden" id="rotulo" value="quente"></input>
                    <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <?php if($record->rotulo == 'leads' && $record->status == 'quente'): ?>
                             <div>
                                <!-- Task Item -->
                                <div class="card mb-0" draggable="true">
                                    <div class="card-body p-3">
                                        
                                        <small class="float-end text-muted"><?php echo e($record->updated_at->format('d/m/Y')); ?></small>
                                        
                                        <input type="hidden" id="id" value="<?php echo e($record->id); ?>"></input>
                                       
        
                                        <h4 class="mt-0">
                                            <a href="apps-projects-details.html" class="text-title"><b>EMPRESA: </b><?php echo e($record->fantasia); ?></a>
                                        </h4>
                                        <h5 class="mt-0">
                                            <a href="apps-projects-details.html" class="text-title"><b>CELULAR: </b><?php echo e($record->celular); ?></a>
                                        </h5>
                                        
                                        <div class="btn float-right">
                                           <a href="<?php echo e(route('leads.edit', $record->id)); ?>"><div class="badge bg-info mb-3 text-white"><i  class="mdi mdi-pencil me-1">editar</i></div></a>
                                        </div>

                                        <p class="text-muted font-13 mb-3">With supporting text below as a natural lead-in to additional contenposuere erat a ante...<a href="javascript:void(0);" class="fw-bold text-muted">view more</a>
                                        </p>
        
                                        <p class="mb-0">
                                           
                                            <span class="text-nowrap mb-2 d-inline-block">
                                                <i class="mdi mdi-comment-multiple-outline text-muted"></i>
                                                <b>74</b> Conversas
                                            </span>
                                        </p>
        
                                        
                                    </div> <!-- end card-body -->
                                </div>
                                <!-- Task Item End -->
                            </div> <!-- end company-list-1-->
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

                </div> <!-- end .board-->
            </div> <!-- end col -->
        </div>
    </form>
    <!-- end row-->
    
    <!--  Task details modal -->
        <div class="modal fade task-modal-content" id="task-detail-modal" tabindex="-1" role="dialog" aria-labelledby="TaskDetailModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="TaskDetailModalLabel">iOS App home page <span class="badge bg-danger ms-2">High</span></h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    
                        <div class="p-2">
                            <h5 class="mt-0">Description:</h5>
    
                            <p class="text-muted mb-4">
                                Voluptates, illo, iste itaque voluptas corrupti ratione reprehenderit magni similique? Tempore, quos delectus asperiores
                                libero voluptas quod perferendis! Voluptate, quod illo rerum? Lorem ipsum dolor sit amet. With supporting text below
                                as a natural lead-in to additional contenposuere erat a ante.
                            </p>
    
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-4">
                                        <h5>Create Date</h5>
                                        <p>17 March 2018 <small class="text-muted">1:00 PM</small></p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4">
                                        <h5>Due Date</h5>
                                        <p>22 December 2018 <small class="text-muted">1:00 PM</small></p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-4" id="tooltip-container">
                                        <h5>Asignee:</h5>
                                        <a href="javascript:void(0);" data-bs-container="#tooltip-container" data-bs-toggle="tooltip" data-bs-placement="top" title="Mat Helme" class="d-inline-block">
                                            <img src="assets/images/users/avatar-6.jpg" class="rounded-circle avatar-xs" alt="friend">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-->

                            <ul class="nav nav-tabs nav-bordered mb-3">
                                <li class="nav-item">
                                    <a href="#home-b1" data-bs-toggle="tab" aria-expanded="false" class="nav-link active">
                                        Comments
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#profile-b1" data-bs-toggle="tab" aria-expanded="true" class="nav-link">
                                        Files
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane show active" id="home-b1">
                                    <textarea class="form-control form-control-light mb-2" placeholder="Write message" id="example-textarea" rows="3"></textarea>
                                    <div class="text-end">
                                        <div class="btn-group mb-2 d-none d-sm-inline-block">
                                            <button type="button" class="btn btn-link btn-sm text-muted font-18"><i class="dripicons-paperclip"></i></button>
                                        </div>
                                        <div class="btn-group mb-2 ms-2 d-none d-sm-inline-block">
                                            <button type="button" class="btn btn-primary btn-sm">Submit</button>
                                        </div>
                                    </div>

                                    <div class="d-flex mt-2">
                                        <img class="me-3 avatar-sm rounded-circle" src="assets/images/users/avatar-3.jpg" alt="Generic placeholder image">
                                        <div class="w-100">
                                            <h5 class="mt-0">Jeremy Tomlinson</h5>
                                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
                                            vulputate at, tempus viverra turpis.
                                    
                                            <div class="d-flex mt-3">
                                                <a class="pe-3" href="#">
                                                    <img src="assets/images/users/avatar-4.jpg" class="avatar-sm rounded-circle" alt="Generic placeholder image">
                                                </a>
                                                <div class="w-100">
                                                    <h5 class="mt-0">Kathleen Thomas</h5>
                                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
                                                    vulputate at, tempus viverra turpis.
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-center mt-2">
                                        <a href="javascript:void(0);" class="text-danger">Load more </a>
                                    </div>
                                </div>
                                <div class="tab-pane" id="profile-b1">
                                    <div class="card mb-1 shadow-none border">
                                        <div class="p-2">
                                            <div class="row align-items-center">
                                                <div class="col-auto">
                                                    <div class="avatar-sm">
                                                        <span class="avatar-title rounded">
                                                            .ZIP
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col ps-0">
                                                    <a href="javascript:void(0);" class="text-muted fw-bold">Hyper-admin-design.zip</a>
                                                    <p class="mb-0">2.3 MB</p>
                                                </div>
                                                <div class="col-auto">
                                                    <!-- Button -->
                                                    <a href="javascript:void(0);" class="btn btn-link btn-lg text-muted">
                                                        <i class="dripicons-download"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card mb-1 shadow-none border">
                                        <div class="p-2">
                                            <div class="row align-items-center">
                                                <div class="col-auto">
                                                    <img src="assets/images/projects/project-1.jpg" class="avatar-sm rounded" alt="file-image" />
                                                </div>
                                                <div class="col ps-0">
                                                    <a href="javascript:void(0);" class="text-muted fw-bold">Dashboard-design.jpg</a>
                                                    <p class="mb-0">3.25 MB</p>
                                                </div>
                                                <div class="col-auto">
                                                    <!-- Button -->
                                                    <a href="javascript:void(0);" class="btn btn-link btn-lg text-muted">
                                                        <i class="dripicons-download"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card mb-0 shadow-none border">
                                        <div class="p-2">
                                            <div class="row align-items-center">
                                                <div class="col-auto">
                                                    <div class="avatar-sm">
                                                        <span class="avatar-title bg-secondary rounded">
                                                            .MP4
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col ps-0">
                                                    <a href="javascript:void(0);" class="text-muted fw-bold">Admin-bug-report.mp4</a>
                                                    <p class="mb-0">7.05 MB</p>
                                                </div>
                                                <div class="col-auto">
                                                    <!-- Button -->
                                                    <a href="javascript:void(0);" class="btn btn-link btn-lg text-muted">
                                                        <i class="dripicons-download"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div> <!-- .p-2 -->
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    <!-- end page title -->

    
    <!-- end row-->
</div> <!-- container -->
<?php $__env->stopSection(); ?>



<?php $__env->startSection('js'); ?>
<!-- DataTables -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>

<script>
    $(document).ready(function () {

        $('#leads-datatable').DataTable({
            language: {
                processing: "Processamento...",
                search: "Pesquisar&nbsp;:",
                lengthMenu: "Mostrar _MENU_ entradas",
                info: "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                infoEmpty: "Exibindo da entrada 0 até 0 de 0 entradas",
                infoFiltered: "(filtro total de _MAX_ entradas)",
                infoPostFix: "",
                loadingRecords: "Carregando ...",
                zeroRecords: "Nenhuma entrada encontrada",
                emptyTable: "Nenhuma entrada disponível na tabela",
                paginate: {
                    first: "Primeiro",
                    previous: "Anterior",
                    next: "Ultimo",
                    last: "Depos"
                },
                aria: {
                    sortAscending: ": ativar para classificar a coluna em ordem crescente",
                    sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                }
            }
        });
       

    });

        const cards = document.querySelectorAll('.card')
        const dropzones = document.querySelectorAll('.tasks')
        
        const records = JSON.parse($('form[name="frmTasks"]').find('input#records').val());
        
        atualizarDados();
        
        /** our cards */
        cards.forEach(card => {
            card.addEventListener('dragstart', dragstart)
            card.addEventListener('drag', drag)
            card.addEventListener('dragend', dragend)
        })

        function dragstart() {
            // log('CARD: Start dragging ')
            dropzones.forEach( dropzone => dropzone.classList.add('highlight'))
        
            // this = card
            this.classList.add('is-dragging')
        }

        function drag() {
         // log('CARD: Is dragging ')
        }

        function dragend() {
            // log('CARD: Stop drag! ')
            dropzones.forEach( dropzone => dropzone.classList.remove('highlight'))
            
            const task = $(this.parentNode).find('input#rotulo').val();
            const id = $(this).find('input#id').val();
            // this = card
            this.classList.remove('is-dragging')
            
             atualizarLista(task, id)
             
             
        }

        /** place where we will drop cards */
        dropzones.forEach( dropzone => {
            dropzone.addEventListener('dragenter', dragenter)
            dropzone.addEventListener('dragover', dragover)
            dropzone.addEventListener('dragleave', dragleave)
            dropzone.addEventListener('drop', drop)
        })

        function dragenter() {
            // log('DROPZONE: Enter in zone ')
              this.classList.remove('over')
             
        }

        function dragover() {
            // this = dropzone
            this.classList.add('over')
        
            // get dragging card
            const cardBeingDragged = document.querySelector('.is-dragging')
        
           
            this.appendChild(cardBeingDragged)
            
            
        }

        function dragleave() {
            // log('DROPZONE: Leave ')
            // this = dropzone
            this.classList.remove('over')
            
             
        
        }

        function drop() {
            // log('DROPZONE: dropped ')
            this.classList.remove('over')
           
           
        }
        
        function atualizarLista(status, id) {
            $.ajaxSetup({
               headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
            });
            records.forEach((record) => {
                if(record.id === parseInt(id)){
                    switch (status) {
                        case 'adquirido':
                            console.log(' Status ADQUIRIDO');
                            record.rotulo = 'prospectos';
                            record.status = status;
                            
                            $.ajax({
                                url:"<?php echo e(url( 'gestao-pessoas/leads')); ?>/" + id,
                                type: "PUT",
                                data:record,
                                dataType: 'json', 
                                success: function(response){}
                            }) 
                            break;
                            
                        case 'sem_interesse':
                            console.log(' Status SEM INTERESSE');
                            record.rotulo = 'prospectos';
                            record.status = status;
                            
                            $.ajax({
                                url:"<?php echo e(url( 'gestao-pessoas/leads')); ?>/" + id,
                                type: "PUT",
                                data:record,
                                dataType: 'json', 
                                success: function(response){}
                            }) 
                            break;
                        
                        case 'inbound':
                            console.log(' Status INBOUND');
                            record.rotulo = 'prospectos';
                            record.status = status;
                            
                            $.ajax({
                                url:"<?php echo e(url( 'gestao-pessoas/leads')); ?>/" + id,
                                type: "PUT",
                                data:record,
                                dataType: 'json', 
                                success: function(response){
                                    console.log(response)
                                     // this = dropzone
                                
                                }
                            }) 
                            break;
                            
                        case 'pri_contato':
                            console.log(' Status INBOUND');
                            record.rotulo = 'prospectos';
                            record.status = status;
                            
                            $.ajax({
                                url:"<?php echo e(url( 'gestao-pessoas/leads')); ?>/" + id,
                                type: "PUT",
                                data:record,
                                dataType: 'json', 
                                success: function(response){
                                    console.log(response)
                                     // this = dropzone
                                
                                }
                            }) 
                            break;
                            
                        case 'ficha_cadastral':
                            console.log(' Status FICHA CADASTRAL');
                            record.rotulo = 'prospectos';
                            record.status = status;
                            
                            $.ajax({
                                url:"<?php echo e(url( 'gestao-pessoas/leads')); ?>/" + id,
                                type: "PUT",
                                data:record,
                                dataType: 'json', 
                                success: function(response){
                                    console.log(response)
                                     // this = dropzone
                                
                                }
                            }) 
                            break;
                            
                        case 'briefing':
                            console.log(' Status BRIEFING');
                            record.rotulo = 'prospectos';
                            record.status = status;
                            
                            $.ajax({
                                url:"<?php echo e(url( 'gestao-pessoas/leads')); ?>/" + id,
                                type: "PUT",
                                data:record,
                                dataType: 'json', 
                                success: function(response){
                                    console.log(response)
                                     // this = dropzone
                                
                                }
                            }) 
                            break;
                            
                        case 'consultoria':
                            console.log(' Status CONSULTORIA');
                            record.rotulo = 'prospectos';
                            record.status = status;
                            
                            $.ajax({
                                url:"<?php echo e(url( 'gestao-pessoas/leads')); ?>/" + id,
                                type: "PUT",
                                data:record,
                                dataType: 'json', 
                                success: function(response){
                                    console.log(response)
                                     // this = dropzone
                                
                                }
                            }) 
                            break;
                            
                        case 'proposta':
                            console.log(' Status PROPOSTA');
                            record.rotulo = 'leads';
                            record.status = status;
                            
                            $.ajax({
                                url:"<?php echo e(url( 'gestao-pessoas/leads')); ?>/" + id,
                                type: "PUT",
                                data:record,
                                dataType: 'json', 
                                success: function(response){
                                    console.log(response)
                                     // this = dropzone
                                
                                }
                            }) 
                            break;
                            
                        case 'quente':
                            console.log(' Status LEADS');
                            record.rotulo = 'leads';
                            record.status = status;
                            
                            $.ajax({
                                url:"<?php echo e(url( 'gestao-pessoas/leads')); ?>/" + id,
                                type: "PUT",
                                data:record,
                                dataType: 'json', 
                                success: function(response){
                                    console.log(response)
                                     // this = dropzone
                                
                                }
                            }) 
                            break;
                       
                        default:
                            console.log(`Status  ${record.status}.`);
                    }
                }
            })
            
            atualizarDados();
        }
        
        function atualizarDados(){
            
            
            total_sem_interesse = records.filter(record => record.status === 'sem_interesse');
            total_adquirido = records.filter(record => record.status === 'adquirido');
            
            $('#qte_sem_interesse').val(total_sem_interesse.length)
            $('#qte_adquirido').val(total_adquirido.length)
        }
    

</script>
<script>
    if (message == 'store') {
        Swal.fire(
            'Parabéns!',
            'Registro cadastrado com sucesso!',
            'success'
        )
    }
    if (message == 'update') {
        Swal.fire(
            'Parabéns!',
            'Registro editado com sucesso!',
            'success'
        )
    }

    if (message == 'destroy') {
        Swal.fire(
            'Alerta!',
            'Registro inativado com sucesso!',
            'info'
        )
    }

    if (message == 'active') {
        Swal.fire(
            'Alerta!',
            'Registro ativado com sucesso!',
            'info'
        )
    }

    
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/livewire/leads.blade.php ENDPATH**/ ?>