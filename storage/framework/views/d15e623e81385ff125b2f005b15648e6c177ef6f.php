<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('despesas.index')); ?>">Despesa</a></li>
                            <li class="breadcrumb-item active">Adicionar Despesa</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Despesa</h4>
                </div>
            </div>
        </div>
       <div class="card">

            <div class="card-body">
                <form class="p-2" method="POST" action="<?php echo e(route('lancamentos.store')); ?>">
                <?php echo csrf_field(); ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nome </label>
                            <select class="form-control select2" data-toggle="select2" name="id_cli" required >
                                <option value="">Selecione ....</option>
                                <?php $__currentLoopData = $pessoas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($item->id); ?> " style="text-transform: uppercase;" ><?php echo e($item->tipo ==  'pj'?$item->fantasia : $item->nome); ?> -- <?php echo e($item->cpf); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tipo</label>
                            <input class="form-control form-control-light" name="tipo" value="Despesa" readonly>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Plano de Conta</label>
                            <select class="form-control select2" data-toggle="select2" name="id_plano_contas" required>
                                <option value="">Selecione ....</option>
                                <?php $__currentLoopData = $contas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($item->id); ?>" style="text-transform: capitalize;" <?php echo e($item->nome == "Empresa" ? 'selected' : ''); ?> ><?php echo e($item->nome); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Centro de Custo</label>
                            <select class="form-control select2" data-toggle="select2" name="id_centro_custo" required >
                                <option value="">Selecione ....</option>
                                <?php $__currentLoopData = $centro_custos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($item->id); ?> " style="text-transform: capitalize;"  <?php echo e($item->nome == "Operacional" ? 'selected' : ''); ?>><?php echo e($item->nome); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Categoria</label>
                            <select class="form-control select2" data-toggle="select2" id="categoria_create" name="id_categoria" required >
                                <option value="">Selecione ....</option>
                                <?php $__currentLoopData = $categorias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($item->id); ?> " style="text-transform: capitalize;" <?php echo e($item->nome == "Estrutura" ? 'selected' : ''); ?> ><?php echo e($item->nome); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Sub-Categoria</label>
                            <select class="form-control select2" data-toggle="select2" name="id_subcategoria" required>
                                <option value="">Selecione ....</option>
                                <?php $__currentLoopData = $subcategorias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($item->id); ?> " style="text-transform: capitalize;" <?php echo e($item->nome == "Pagamento" ? 'selected' : ''); ?> ><?php echo e($item->nome); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="task-priority2">Intervalos de vencimento</label>
                            <select type="text" class="form-control"  name="intervalo" required>
                                <option value="7">Semanal</option>
                                <option value="15">Quinzenal</option>
                                <option value="30" selected >Mensal</option>
                            </select>
                        </div>
                    </div>




                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="task-title"> Data Venda</label>
                            <input type="date" class="form-control form-control-light"  name="vencimento" required>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="task-title"> Data 1º Vencimento</label>
                            <input type="date" class="form-control form-control-light" name="pri_vencimento" required>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Valor Parcela</label>
                            <input type="text" class="form-control form-control-light" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" id="valor_parcela" name="valor_parcela" required>

                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="task-priority2">Parcela</label>
                            <input type="text" class="form-control form-control-light" name="parcela" id="parcela" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="task-priority2">Valor do Lançamento</label>
                            <input type="text" class="form-control form-control-light" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true" name="valor_lancamento" id="valor_lancamento" required>
                            
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="task-priority2">Descrição</label>
                            <input type="text" class="form-control form-control-light"  name="descricao" required>
                        </div>
                    </div>

                </div>

                <div class="text-right mt-5">
                    <a href="<?php echo e(URL::previous()); ?>" class="btn btn-light" data-dismiss="modal">Sair</a>
                    <button type="submit" class="btn btn-primary">Adicionar</button>
                </div>
            </form>
            </div>
       </div>
    </div> <!-- container -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>

    $(document).ready(function() {


        $('#valor_lancamento').focus( function (event){
            event.preventDefault();
            var parcela     =  $('#parcela').val();
            var valor_lanc  =  $('#valor_lancamento').val();
            var valor_parcela  =  $('#valor_parcela').val();

            if(!parcela || parcela == 0){
                $('#valor_lancamento').val(parseFloat(valor_parcela).toFixed(2));
                $('#parcela').val('A Vista');
            } else {
                var valor_lanc = parseFloat(valor_parcela) * parseInt(parcela);
                $('#valor_lancamento').val(valor_lanc.toFixed(2));
            }
        });




    } );
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/financeiros/despesas/create.blade.php ENDPATH**/ ?>