<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('despesas.index')); ?>">Despesa</a></li>
                            <li class="breadcrumb-item active">Efetuar Pagamento</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Efetuar Pagamento</h4>
                </div>
            </div>
        </div>
       <div class="card">
           
           <div class="card-body">
               <form class="p-2" method="POST" action="<?php echo e(route('lancamentos.update',$despesa->id)); ?>"> 
                <?php echo csrf_field(); ?>
                <?php echo method_field('PUT'); ?>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Conta Bancária </label>
                            <select class="form-control select2" data-toggle="select2" name="conta_bancaria" required>
                                <option value="" style="text-transform: capitalize;" >Selecione uma conta bancária</option>
                               <?php $__currentLoopData = $contas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                               <?php $__currentLoopData = $bancos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banco): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($banco['id']  == $item->banco): ?>
                                    <option value="<?php echo e($item->id); ?>" style="text-transform: capitalize;" ><?php echo e($banco['nome']); ?> ( <?php echo e($item->conta); ?> ) </option>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    
                               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Nome </label>
                            <select class="form-control select2" data-toggle="select2" name="id_cli" disabled>
                                <option value="<?php echo e($lancamento->cliente->id); ?>" style="text-transform: capitalize;" ><?php echo e($lancamento->cliente->nome); ?> --- <?php echo e($lancamento->cliente->cpf); ?></option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Tipo</label>
                            <input type="text" class="form-control form-control-light"  name="tipo"  value="<?php echo e($lancamento->tipo); ?>" readonly>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="task-priority2">Descrição</label>
                            <input type="text" class="form-control form-control-light"  name="descricao"  value="<?php echo e($lancamento->descricao); ?>" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Parcela</label>
                            <input type="text" class="form-control form-control-light"  name="parcela"  value="<?php echo e($despesa->parcela); ?>" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Valor Parcela</label>
                            <input type="text" class="form-control form-control-light"  name="valor_parcela"  value="<?php echo e($despesa->valor_parcela); ?>" readonly>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="task-priority2">Valor a Pagar</label>
                            <input type="text" class="form-control form-control-light" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true" name="valor_a_pagar" >
                        </div>
                    </div>
                </div>
    
                <div class="text-right mt-5">
                    <a href="<?php echo e(URL::previous()); ?>" class="btn btn-light" >Sair</a>
                    <button type="submit" class="btn btn-primary">Baixar</button>
                </div>
            </form>
           </div>
       </div>
    </div> <!-- container -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
   
    $(document).ready(function() {
       
       
        $('#valor_parcela_create').focus( function (event){
            event.preventDefault();            
            var parcela     =  $('#parcela_create').val();
            var valor_lanc  =  $('#valor_lancamento_create').val();

            if(!parcela || parcela == 0){
                $('#valor_parcela_create').val(parseInt(valor_lanc).toFixed(2));
                $('#parcela_create').val('A Vista');
            } else {
                var valor_parcela = parseInt(valor_lanc) / parseInt(parcela);
                $('#valor_parcela_create').val(valor_parcela.toFixed(2));
            }
        });  

        
        
          
    } );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/financeiros/despesas/playOf.blade.php ENDPATH**/ ?>