<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Gestão Anuncios</a></li>
                            <li class="breadcrumb-item active">Listar Gestão Anuncios</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Listar Gestão Anuncios</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                                <a href="<?php echo e(route('plataformas.create')); ?>" class="btn btn-danger mb-2"><i
                                        class="mdi mdi-plus-circle mr-2"></i> Adicionar Gestão </a>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-success mb-2 mr-1"><i
                                            class="mdi mdi-settings"></i></button>
                                    <button type="button" class="btn btn-light mb-2 mr-1">Importar</button>
                                    <button type="button" class="btn btn-light mb-2">Exportar</button>
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            <?php if($message = Session::get('success')): ?>
                                <?php if($message == 'store'): ?>
                                    <script>
                                        var message = 'store';
                                    </script>
                                <?php endif; ?>
                                <?php if($message == 'update'): ?>
                                    <script>
                                        var message = 'update';
                                    </script>
                                <?php endif; ?>

                                <?php if($message == 'destroy'): ?>
                                    <script>
                                        var message = 'destroy';
                                    </script>
                                <?php endif; ?>

                                <?php if($message == 'active'): ?>
                                    <script>
                                        var message = 'active';
                                    </script>
                                <?php endif; ?>
                                
                            <?php endif; ?>
                            <table class="table table-centered w-100 dt-responsive " id="plataformas-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Cliente</th>
                                        <th style="width: 100px;">Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                           
                                            <td> <?php echo e($item['cliente']); ?> </td>
                                         
                                            <td>
                                              <a href="<?php echo e(route('plataformas.edit', $item[0]->id_cliente)); ?>"
                                                        class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                                         <a href="<?php echo e(route('plataformas.show', $item[0]->id_cliente)); ?>"
                                                        class="action-icon"> <i
                                                            class="mdi mdi-eye"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div> <!-- container -->


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function() {

            $('#propostas-datatable').DataTable({
                language: {
                    processing: "Processamento...",
                    search: "Pesquisar&nbsp;:",
                    lengthMenu: "Mostrar _MENU_ entradas",
                    info: "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty: "Exibindo da entrada 0 até 0 de 0 entradas",
                    infoFiltered: "(filtro total de _MAX_ entradas)",
                    infoPostFix: "",
                    loadingRecords: "Carregando ...",
                    zeroRecords: "Nenhuma entrada encontrada",
                    emptyTable: "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first: "Primeiro",
                        previous: "Anterior",
                        next: "Ultimo",
                        last: "Depos"
                    },
                    aria: {
                        sortAscending: ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        });

    </script>
    <script type="text/javascript">
        if (message == 'store') {
            Swal.fire(
                'Parabéns!',
                'Registro cadastrado com sucesso!',
                'success'
            )
        }
        if (message == 'update') {
            Swal.fire(
                'Parabéns!',
                'Registro editado com sucesso!',
                'success'
            )
        }

        if (message == 'destroy') {
            Swal.fire(
                'Alerta!',
                'Registro inativado com sucesso!',
                'info'
            )
        }

        if (message == 'active') {
            Swal.fire(
                'Alerta!',
                'Registro ativado com sucesso!',
                'info'
            )
        }

    </script>
    <script type="text/javascript">
        if (message == 'invoice') {
            Swal.fire(
                'Parabéns!',
                'Pedido FATURADO com sucesso!',
                'success'
            )
        }

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/dashboard/painel/resources/views/content/gestao-anuncios/index.blade.php ENDPATH**/ ?>