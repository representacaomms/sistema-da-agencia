<?php $__env->startSection('content'); ?>
 <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('receitas.index')); ?>">Receitas</a></li>
                            <li class="breadcrumb-item active">Recibo</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Recibo de Pagamento</h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <!-- Invoice Logo-->
                        <div class="clearfix">
                            <div class="float-left mb-3">
                                <img src="<?php echo e(url('/images/logo.png')); ?>" alt="" height="70">
                            </div>
                            <div class="float-right">
                                <h4 class="m-0 d-print-none">RECIBO DE PAGAMENTO</h4>
                            </div>
                        </div>
                        <h2 class="text-muted text-center font-24">Recibo de Pagamento</h2>
                        <!-- Invoice Detail-->
                        <div class="row">
                            
                            <div class="col-sm-6">
                                <div class="float-left mt-3">
                                    <p class="text-muted font-13">
                                        Por meio deste documento, declaro, para todos os fins e a quem possa interessar, que recebi da pessoa <?php echo e($lancamento->cliente->tipo == 'pf' ? 'Fisica' : 'Juridica'); ?>,<?php echo e($lancamento->cliente->nome); ?>, portador do documento <?php echo e($lancamento->cliente->tipo == 'pf' ? 'CPF: ' : 'CNPJ: '); ?> nº: <?php echo e($lancamento->cliente->cpf); ?> com sede em <?php echo e($lancamento->cliente->endereco->localidade); ?>, a quantia de R$ <?php echo e($receita->valor_amoretizado); ?> <b>( <?php echo e($receita->valor_amoretizado_extenso); ?> )</b>, referente à seguinte situação:
                                        <p class="text-muted font-13">
                                           Pagamento da parcela <?php echo e($receita->parcela); ?> de <?php echo e($lancamento->parcela); ?>

                                        </p>
                                        <p class="text-muted font-13"> Por ser verdade, assino a presente.</p>
                                    </p>                                    
                                </div>

                            </div><!-- end col -->
                            <div class="col-sm-4 offset-sm-2">
                                <div class="mt-3 float-sm-right">
                                    <p class="font-13"><strong>Data Recibo: </strong> &nbsp;&nbsp;&nbsp; <?php echo e($receita->created_at->format('d/m/Y')); ?></p>
                                    <p class="font-13"><strong>ID Recibo: </strong> <span class="float-right">#<?php echo e($lancamento->id); ?></span></p>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->

                        <div class="row mt-4">
                            <div class="col-sm-4">
                                <h6>Endereço</h6>
                                <address>
                                   <?php echo e($lancamento->cliente->endereco->logradouro); ?>,<?php echo e($lancamento->cliente->endereco->numero); ?> <?php echo e($lancamento->cliente->endereco->complemento); ?><br>                                    
                                     <?php echo e($lancamento->cliente->endereco->bairro); ?><br>                                    
                                   <?php echo e($lancamento->cliente->endereco->localidade); ?>,<?php echo e($lancamento->cliente->endereco->uf); ?> &nbsp; <?php echo e($lancamento->cliente->endereco->cep); ?>  <br>                                   
                                    <abbr title="Phone">Celular:</abbr>                                    
                                    <?php echo e($lancamento->cliente->celular); ?>

                                    
                                </address>
                            </div> <!-- end col-->
                        </div>    
                        <!-- end row -->        

                        <div class="row">
                            <div class="col-sm-7 mt-5">
                                ____________________________________________________________________________________<br>
                                <h3><?php echo e($empresa->nome); ?> -- <?php echo e($empresa->tipo == 'pf' ? 'CPF: ' : 'CNPJ: '); ?> nº: <?php echo e($empresa->cpf); ?> </h3> 
                               
                            </div> <!-- end col -->
                            
                        </div>
                        <!-- end row-->

                        <div class="d-print-none mt-4">
                            <div class="text-right">
                                <a href="javascript:window.print()" class="btn btn-primary imprimir"><i class="mdi mdi-printer"></i> Imprimir</a>
                                <a href="<?php echo e(route('receitas.index')); ?>" class="btn btn-info">Fechar</a>
                            </div>
                        </div>   
                        <!-- end buttons -->

                    </div> <!-- end card-body-->
                </div> <!-- end card -->
            </div> <!-- end col-->
        </div>
        <!-- end row -->
        <hr>
        <div class="row mt-5 recibo d-none">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <!-- Invoice Logo-->
                        <div class="clearfix">
                            <div class="float-left mb-3">
                                <img src="<?php echo e(url('/images/logo.png')); ?>" alt="" height="70">
                            </div>
                            <div class="float-right">
                                <h4 class="m-0 d-print-none">RECIBO DE PAGAMENTO</h4>
                            </div>
                        </div>
                        <h2 class="text-muted text-center font-24">Recibo de Pagamento</h2>
                        <!-- Invoice Detail-->
                        <div class="row">
                            
                            <div class="col-sm-6">
                                <div class="float-left mt-3">
                                    <p class="text-muted font-13">
                                        Por meio deste documento, declaro, para todos os fins e a quem possa interessar, que recebi da pessoa <?php echo e($lancamento->cliente->tipo == 'pf' ? 'Fisica' : 'Juridica'); ?>,<?php echo e($lancamento->cliente->nome); ?>, portador do documento <?php echo e($lancamento->cliente->tipo == 'pf' ? 'CPF: ' : 'CNPJ: '); ?> nº: <?php echo e($lancamento->cliente->cpf); ?> com sede em <?php echo e($lancamento->cliente->endereco->localidade); ?>, a quantia de R$ <?php echo e($receita->valor_amoretizado); ?> <b>( <?php echo e($receita->valor_amoretizado_extenso); ?> )</b>, referente à seguinte situação:
                                        <p class="text-muted font-13">
                                           Pagamento da parcela <?php echo e($receita->parcela); ?> de <?php echo e($lancamento->parcela); ?>

                                        </p>
                                        <p class="text-muted font-13"> Por ser verdade, assino a presente.</p>
                                    </p>                                    
                                </div>

                            </div><!-- end col -->
                            <div class="col-sm-4 offset-sm-2">
                                <div class="mt-3 float-sm-right">
                                    <p class="font-13"><strong>Data Recibo: </strong> &nbsp;&nbsp;&nbsp; <?php echo e($receita->created_at->format('d/m/Y')); ?></p>
                                    <p class="font-13"><strong>ID Recibo: </strong> <span class="float-right">#<?php echo e($lancamento->id); ?></span></p>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->

                        <div class="row mt-4">
                            <div class="col-sm-4">
                                <h6>Endereço</h6>
                                <address>
                                   <?php echo e($lancamento->cliente->endereco->logradouro); ?>,<?php echo e($lancamento->cliente->endereco->numero); ?> <?php echo e($lancamento->cliente->endereco->complemento); ?><br>                                    
                                     <?php echo e($lancamento->cliente->endereco->bairro); ?><br>                                    
                                   <?php echo e($lancamento->cliente->endereco->localidade); ?>,<?php echo e($lancamento->cliente->endereco->uf); ?> &nbsp; <?php echo e($lancamento->cliente->endereco->cep); ?>  <br>                                   
                                    <abbr title="Phone">Celular:</abbr>                                    
                                    <?php echo e($lancamento->cliente->celular); ?>

                                    
                                </address>
                            </div> <!-- end col-->
                        </div>    
                        <!-- end row -->        

                        <div class="row">
                            <div class="col-sm-7 mt-5">
                                ____________________________________________________________________________________<br>
                                <h3><?php echo e($empresa->nome); ?> -- <?php echo e($empresa->tipo == 'pf' ? 'CPF: ' : 'CNPJ: '); ?> nº: <?php echo e($empresa->cpf); ?> </h3> 
                               
                            </div> <!-- end col -->
                            
                        </div>
                        <!-- end row-->

                    </div> <!-- end card-body-->
                </div> <!-- end card -->
            </div> <!-- end col-->
        </div>
        <!-- end row -->
        
    </div> <!-- container -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

<script>
    $('.imprimir').click( function() {
        $('.recibo').removeClass('d-none');
        setInterval(function(){
            $('.recibo').addClass('d-none');
            }, 3000);
    });
</script>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/dashboard/painel/resources/views/content/pdfs/recibo.blade.php ENDPATH**/ ?>