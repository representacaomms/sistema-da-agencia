<?php $__env->startSection('content'); ?>
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('produtos.index')); ?>">Produtos</a></li>
                            <li class="breadcrumb-item active">Adicionar Produtos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar produto</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="<?php echo e(route('produtos.store')); ?>" method="POST">
            <?php echo method_field('POST'); ?>
            <?php echo csrf_field(); ?>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Nome</label>
                        <div class="col-9">
                            <input type="text" class="form-control" style="text-transform: capitalize;" id="nome"
                                name="nome" placeholder="Nome" required>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Caracteristicas</label>
                        <div class="col-9">
                            <textarea class="form-control" id="caracteristicas" name="caracteristicas" placeholder="Caracteristicas" required></textarea>
                        </div>
                    </div>

                    <input type="hidden" id="listaItens" name="listaItens" value="">

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Status</label>
                        <div class="col-9">

                            <select name="status" id="status" class="form-control">
                                <option value="Ativo">Ativo</option>
                                <option value="Inativo">Inativo</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-3">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="<?php echo e(URL::previous()); ?>">
                                                    Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                       
                                        <div class="col-xl-3">
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit" id="add">
                                                    Adicionar<span class="badge badge-primary"></span>
                                                </button>

                                            </div>
                                        </div> 
                                        <div class="col-xl-3">
                                            <div class="form-group">
                                                <button class="btn btn-warning" type="button" data-toggle="modal"
                                                    data-target="#right-modal" id="addItem">
                                                    ADD Itens<span class="badge badge-success"></span>
                                                </button>

                                            </div>
                                        </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>



        </form>
    </div> <!-- container -->
    <div id="right-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title" id="standard-modalLabel">Base dos Custos</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form class="pl-3 pr-3" action="<?php echo e(route('base-custos.store')); ?>" method="POST">
                        <?php echo csrf_field(); ?>
                        <div class="table-responsive">

                            <table class="table table-centered w-100 dt-responsive " id="custos-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th style="width: 10px;">
                                            <label class="form-check-label" for="customCheck1">&nbsp;</label>
                                        </th>
                                        <th>Descrição</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $servicos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td style="width: 10px;">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input checkItem"
                                                        id="<?php echo e($item->id); ?>" name=checkItem>
                                                    <label class="form-check-label" for="customCheck13">&nbsp;</label>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16"><a
                                                        class="text-body"><?php echo e($item->descricao); ?></a></p>
                                            </td>

                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>


                    </form>
                    <div class="form-group text-center mt-5">
                        <button class="btn btn-primary" id="atualizar" class="close" data-dismiss="modal">Incluir Itens</button>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function() {
            var listaItens = [];
            $(".checkItem").click(function(event) {
                var item = listaItens.indexOf(event.target.id);
                if (item < 0) {
                    listaItens.push(event.target.id)
                } else {
                    listaItens.splice(item, 1);
                }
            });

            $("#atualizar").click(function(event) {
               /*  var descricao = $('#descricao').val()
                var caracteristica = $('#caracteristica').val()
                var situacao = $('#situacao').val() */
                
               $('#listaItens').val(listaItens)

            });

            $('#custos-datatable').DataTable({
                language: {
                    processing: "Processamento...",
                    search: "Pesquisar&nbsp;:",
                    lengthMenu: "Mostrar _MENU_ entradas",
                    info: "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty: "Exibindo da entrada 0 até 0 de 0 entradas",
                    infoFiltered: "(filtro total de _MAX_ entradas)",
                    infoPostFix: "",
                    loadingRecords: "Carregando ...",
                    zeroRecords: "Nenhuma entrada encontrada",
                    emptyTable: "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first: "Primeiro",
                        previous: "Anterior",
                        next: "Ultimo",
                        last: "Depos"
                    },
                    aria: {
                        sortAscending: ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        });
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/produtos/create.blade.php ENDPATH**/ ?>