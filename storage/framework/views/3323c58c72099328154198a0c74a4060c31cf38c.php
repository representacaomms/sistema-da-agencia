<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('despesas.index')); ?>">Despesas</a></li>
                            <li class="breadcrumb-item active">Detalhes Despesas</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Detalhes  Despesas</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                               <a href="<?php echo e(route('despesas.index')); ?>" class="btn btn-danger mb-2"><i
                                        class="mdi mdi-arrow-left-bold mr-2"></i> Voltar</a>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-success mb-2 mr-1"><i class="mdi mdi-settings"></i></button>
                                    <button type="button" class="btn btn-light mb-2 mr-1">Importar</button>
                                    <button type="button" class="btn btn-light mb-2">Exportar</button>
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            <?php if($message = Session::get('success')): ?>
                            <?php if($message == 'store'): ?>
                                <script>
                                    var message = 'store';
                                </script>
                            <?php endif; ?>
                            <?php if($message == 'update'): ?>
                                <script>
                                    var message = 'update';
                                </script>
                            <?php endif; ?>

                            <?php if($message == 'destroy'): ?>
                                <script>
                                    var message = 'destroy';
                                </script>
                            <?php endif; ?>

                            <?php if($message == 'active'): ?>
                                <script>
                                    var message = 'active';
                                </script>
                            <?php endif; ?>
                            
                        <?php endif; ?>
                            <table class="table table-centered w-100 dt-responsive " id="despesas-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Descrição</th>                           
                                        <th>Parcela</th>                                                                                
                                        <th>Valor</th>                                                                                
                                        <th >Data</th>       
                                        <th >Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($despesas)): ?>
                                        <?php $__currentLoopData = $despesas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            
                                                <tr>                                  
                                                    <td><p class="m-0 d-inline-block align-middle font-16"><a  class="text-body"><?php echo e($descricao); ?></a></p></td> 
                                                    <td>
                                                        <p class="m-0 d-inline-block align-middle font-16"><a  class="text-body">Parcela: <?php echo e($item->parcela); ?> de <?php echo e($parcelas); ?></a></br>
                                                            <?php if($item->status =='A Pagar'): ?>
                                                            <span class="badge badge-info">A Pagar</span>
                                                            <?php elseif($item->status =='Pendente'): ?>
                                                            <span class="badge badge-warning">Pendente</span>
                                                            <?php else: ?>
                                                            <span class="badge badge-success">Quitado</span>
                                                            <?php endif; ?>
                                                        </p>
                                                    </td>                                          
                                                    <td><p class="m-0 d-inline-block align-middle font-16"><a  class="text-body">R$ <?php echo e($item->valor_parcela); ?></a></p></td>                                          
                                                    <td><?php echo e(date('d/m/Y', strtotime($item->data_pagto))); ?> </td>
                                                    <td> 
                                                    <?php if($item->status != "Quitado"): ?>
                                                    <?php if($item->data_pagto < $hoje ): ?>
                                                    <a href="<?php echo e(route('despesas.show', $item->id)); ?>"  class="action-icon"> <i class="mdi mdi-coin"></i></a> 
                                                    <a target="_blank" href="https://api.whatsapp.com/send?phone=<?php echo e($telefone); ?>&text=Ol%C3%A1%2C%20<?php echo e($cliente); ?> consta%20uma%20parcela%20em%20aberto%20vencida%20no%20dia <?php echo e(date('d/m/Y', strtotime($item->data_pagto))); ?> %20segue%20o%20pix:%20agenciamms20%40gmail.com"  class="action-icon"> <span class="badge badge-danger">Vencida</span> </a>
                                                    <?php else: ?>
                                                    <a href="<?php echo e(route('despesas.show', $item->id)); ?>"  class="action-icon"> <i class="mdi mdi-coin"></i></a>                                                   
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                   
                                                    </td>
                                                </tr> 
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>

                                </tbody>
                            </table>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
            <!-- end row -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function() {
          
            $('#despesas-datatable').DataTable({
                language: {
                    processing:     "Processamento...",
                    search:         "Pesquisar&nbsp;:",
                    lengthMenu:    "Mostrar _MENU_ entradas",
                    info:           "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty:      "Exibindo da entrada 0 até 0 de 0 entradas",
                    infoFiltered:   "(filtro total de _MAX_ entradas)",
                    infoPostFix:    "",
                    loadingRecords: "Carregando ...",
                    zeroRecords:    "Nenhuma entrada encontrada",
                    emptyTable:     "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first:      "Primeiro",
                        previous:   "Anterior",
                        next:       "Ultimo",
                        last:       "Depos"
                    },
                    aria: {
                        sortAscending:  ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        } );
    </script>
    <script type="text/javascript">
    
        if(message == 'store'){
            Swal.fire(
                'Parabéns!',
                'Despesa cadastrada com sucesso!',
                'success'
            )
        }
        if(message == 'update'){
            Swal.fire(
                'Parabéns!',
                'Despesa editada com sucesso!',
                'success'
            )
        }   
        
        if(message == 'destroy'){
            Swal.fire(
                'Alerta!',
                'Despesa inativada com sucesso!',
                'info'
            )
        } 

        if(message == 'active'){
            Swal.fire(
                'Alerta!',
                'Despesa ativada com sucesso!',
                'info'
            )
        }   
        
    </script> 

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/Sistema Agencia/Web/resources/views/content/financeiros/despesas/detal.blade.php ENDPATH**/ ?>