<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('plataformas.index')); ?>">Plataformas</a></li>
                            <li class="breadcrumb-item active">Adicionar Plataforma</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Plataforma</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="<?php echo e(route('plataformas.store')); ?>" method="POST" novalidate>
            <?php echo method_field('POST'); ?>
            <?php echo csrf_field(); ?>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="projectname">Plataforma</label>
                                <input type="text"   name="plataforma" class="form-control " required>
                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="form-group">
                                <button class="btn btn-primary mt-3" type="button" id="adicionar_campo">
                                        Adicionar Campos<span class="badge badge-primary"></span>
                                </button>
                                
                            
                                <button class="btn btn-warning mt-3 text-white" type="button" id="excluir_campo">
                                        Excluir Campos<span class="badge badge-warning"></span>
                                </button>
                                
                            </div>
                        </div>
                    </div>    

                        
                    <div class="row" id="add_campo">
                        <div class="col-md-6" id="1">
                            <div class="form-group">
                                <label id="label_rg">Questão 1</label>
                                <input type="text"  name="campo_1" class="form-control" required >
                            </div>
                        </div>
                        
                    </div>
                   
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="<?php echo e(URL::previous()); ?>">
                                                        Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2">
                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit">
                                                            Adicionar<span class="badge badge-primary"></span>
                                                    </button>
                                                </div>
                                            </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>

        </form>
    </div> <!-- container -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        

       
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/rede-sociais/create.blade.php ENDPATH**/ ?>