<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('plataformas.index')); ?>">Plataformas</a></li>
                            <li class="breadcrumb-item active">Adicionar Plataforma</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Plataforma</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="<?php echo e(route('plataforma-itens.store')); ?>" method="POST" novalidate>
            <?php echo method_field('POST'); ?>
            <?php echo csrf_field(); ?>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    <hr>
                        
                    <div class="row mt-3 "'>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="projectname">Plataforma</label>
                                <input type="text"   name="plataforma" class="form-control " value="<?php echo e($plataforma->nome); ?>" readonly>
                                <input type="hidden"   name="id_plataforma" class="form-control " value="<?php echo e($plataforma->id); ?>">
                            </div>
                        </div>

                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="projectname">Adiconar Termos de consultoria </label>
                                <input type="text"   name="termo_consultoria" class="form-control "  required>
                            </div>
                        </div>
                      
                    </div>
                   
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="<?php echo e(URL::previous()); ?>">
                                                        Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2">
                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit">
                                                            Adicionar<span class="badge badge-primary"></span>
                                                    </button>
                                                </div>
                                            </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>

        </form>
    </div> <!-- container -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
    .d-none{
        display: none;
    }
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        var oldValue = $('select[name="plataforma"]').val();
        $('select[name="plataforma"]').change( function() {
           
            switch ($('select[name="plataforma"]').val()) {

                case 'instagram':
                    addClass('instagram');
                    oldValue = 'instagram';              
                    break;
                case 'facebook':
                    addClass('facebook') ;
                    oldValue = 'facebook';         
                    break;
                case 'youtube':
                    addClass('youtube')  
                    oldValue = 'youtube';                
                    break;
                case 'site':
                    addClass('site')     
                    oldValue = 'site';              
                    break;
                case 'gmn':
                    addClass('gmn')   
                    oldValue = 'gmn';                 
                    break;
            }
            
        })

        function addClass(params) {
            $('#' + params).removeClass('d-none');
            $('#' + oldValue).addClass('d-none') 
        }
       
    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/plataformas/add_itens.blade.php ENDPATH**/ ?>