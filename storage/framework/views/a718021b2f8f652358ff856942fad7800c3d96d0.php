<?php $__env->startSection('content'); ?>

    <div class="container-fluid">                        
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Usuarios</a></li>
                            <li class="breadcrumb-item active">Perfil</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Perfil Usuário</h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 
        <?php if($errors->all()): ?>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>            
                <script>
                    var message = 'error';
                    var content = "<?= $error ?>";
                </script>            
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
        <?php if($message = Session::get('success')): ?>
            <?php if($message == 'store'): ?>
                <script>
                    var message = 'store';
                </script>
            <?php endif; ?>
            <?php if($message == 'update'): ?>
                <script>
                    var message = 'update';
                </script>
            <?php endif; ?>

            <?php if($message == 'destroy'): ?>
                <script>
                    var message = 'destroy';
                </script>
            <?php endif; ?>
            
        <?php endif; ?>

        <div class="row">
            <div class="col-sm-12">
                <!-- Profile -->
                <div class="card bg-primary">
                    <div class="card-body profile-user-box">

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="media">
                                    <span class="float-left m-2 mr-4"><img src="<?php echo e(asset('images/users/avatar-2.jpg')); ?>" style="height: 100px;" alt="" class="rounded-circle img-thumbnail"></span>
                                    <div class="media-body">

                                        <h4 class="mt-1 mb-1 text-white"><?php echo e(Auth::user()->name); ?></h4>
                                        <p class="font-13 text-white-50"> <?php echo e(Auth::user()->permissao); ?></p>

                                        <ul class="mb-0 list-inline text-light">
                                            <li class="list-inline-item mr-3">
                                                <h5 class="mb-1">$ 25,184</h5>
                                                <p class="mb-0 font-13 text-white-50">Rendimento total</p>
                                            </li>
                                            <li class="list-inline-item">
                                                <h5 class="mb-1">5482</h5>
                                                <p class="mb-0 font-13 text-white-50">Número de pedidos</p>
                                            </li>
                                        </ul>
                                    </div> <!-- end media-body-->
                                </div>
                            </div> <!-- end col-->

                            <div class="col-sm-4">
                                <div class="text-center mt-sm-0 mt-3 text-sm-right">
                                    <a href="#" class="btn btn-light" data-toggle="modal" data-target="#editar-usuario">
                                        <i class="mdi mdi-account-edit mr-1"></i> Editar Perfil 
                                    </a>
                                </div>
                            </div> <!-- end col-->
                        </div> <!-- end row -->

                    </div> <!-- end card-body/ profile-user-box-->
                </div><!--end profile/ card -->
            </div> <!-- end col-->
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-md-4">
                <!-- Personal-Information -->
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mt-0 mb-3">Informações do vendedor</h4>
                        <hr>

                        <div class="text-left">
                            <p class="text-muted"><strong>Nome Completo:</strong> <span class="ml-2"><?php echo e(Auth::user()->name); ?></span></p>

                            <p class="text-muted"><strong>Celular:</strong><span class="ml-2"><?php echo e(Auth::user()->telefone); ?></span></p>

                            <p class="text-muted"><strong>Email:</strong> <span class="ml-2"><?php echo e(Auth::user()->email); ?></span></p>

                            <p class="text-muted"><strong>Função:</strong> <span class="ml-2"><?php echo e(Auth::user()->funcao); ?></span></p>

                            <p class="text-muted"><strong>Localização:</strong> <span class="ml-2">USA</span></p>
                        </div>
                    </div>
                </div>
                <!-- Personal-Information -->

                <!-- Toll free number box-->
                <div class="card text-white bg-info overflow-hidden">
                    <div class="card-body">
                        <div class="toll-free-box text-center">
                            <h4> <i class="mdi mdi-deskphone"></i> Celular: <?php echo e(Auth::user()->telefone); ?></h4>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
                <!-- End Toll free number box-->
                <!-- Toll free number box-->
                <div class="card text-white bg-info overflow-hidden">
                    <div class="card-body">
                        <div class="toll-free-box text-center">
                            <h4> <i class="mdi mdi-deskphone"></i> Função: <?php echo e(Auth::user()->funcao); ?></h4>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
                <!-- End Toll free number box-->

            </div> <!-- end col-->

            <div class="col-md-8">

                <!-- Chart-->
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-3">Proposta &amp; Pedidos</h4>
                        <div style="height: 260px;" class="chartjs-chart"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                            <canvas id="high-performing-product" width="625" height="260" class="chartjs-render-monitor" style="display: block; width: 625px; height: 260px;"></canvas>
                        </div>        
                    </div>
                </div>
                <!-- End Chart-->

                <div class="row">
                    <div class="col-sm-4">
                        <div class="card tilebox-one">
                            <div class="card-body">
                                <i class="dripicons-basket float-right text-muted"></i>
                                <h6 class="text-muted text-uppercase mt-0">Propostas</h6>
                                <h2 class="m-b-20">1,587</h2>
                                <span class="badge badge-primary"> +11% </span> <span class="text-muted">Do período anterior</span>
                            </div> <!-- end card-body-->
                        </div> <!--end card-->
                    </div><!-- end col -->

                    <div class="col-sm-4">
                        <div class="card tilebox-one">
                            <div class="card-body">
                                <i class="dripicons-box float-right text-muted"></i>
                                <h6 class="text-muted text-uppercase mt-0">Pedidos</h6>
                                <h2 class="m-b-20">$<span>46,782</span></h2>
                                <span class="badge badge-danger"> -29% </span> <span class="text-muted">Do período anterior</span>
                            </div> <!-- end card-body-->
                        </div> <!--end card-->
                    </div><!-- end col -->

                    <div class="col-sm-4">
                        <div class="card tilebox-one">
                            <div class="card-body">
                                <i class="dripicons-jewel float-right text-muted"></i>
                                <h6 class="text-muted text-uppercase mt-0">Produto vendido</h6>
                                <h2 class="m-b-20">1,890</h2>
                                <span class="badge badge-primary"> +89% </span> <span class="text-muted">Ano passado</span>
                            </div> <!-- end card-body-->
                        </div> <!--end card-->
                    </div><!-- end col -->

                </div>
                <!-- end row -->

            </div>
            <!-- end col -->

        </div>
        <!-- end row -->
        
    </div>

    <!----   MODAL ---    ------>
    <!-- Success Header Modal -->
    <div id="editar-usuario" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="success-header-modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" action="#" method="POST">
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('PUT'); ?>
                    <div class="modal-header ">
                        <h4 class="modal-title" id="success-header-modalLabel">Editar Perfil</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">
                            <h4 class="mb-3 header-title"><?php echo e(Auth::user()->name); ?></h4>
                            
                                <div class="form-group row mb-3">
                                    <label for="inputEmail3" class="col-3 col-form-label">Email</label>
                                    <div class="col-9">
                                        <input type="email" class="form-control" id="inputEmail3" name="email" value="<?php echo e(Auth::user()->email); ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label for="inputPassword3" class="col-3 col-form-label">Password</label>
                                    <div class="col-9">
                                        <input type="password"  class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="password"  autocomplete="new-password">
                                        <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($message); ?></strong>
                                            </span>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label for="inputPassword5" class="col-3 col-form-label">Re Password</label>
                                    <div class="col-9">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password">
                                    </div>
                                </div>                               

                                <div class="form-group row mb-3">
                                    <label for="inputPassword5" class="col-3 col-form-label">Celular</label>
                                    <div class="col-9">
                                        <input type="text" class="form-control" data-toggle="input-mask" data-mask-format="(00) 00000-0000" id="inputPassword5" name="celular" value="<?php echo e(Auth::user()->celular); ?>">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label for="inputPassword5" class="col-3 col-form-label">Função</label>
                                    <div class="col-9">
                                        <input type="text" class="form-control" id="inputPassword5" name="funcao" value="<?php echo e(Auth::user()->funcao); ?>">
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-success">Atualizar</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?> 
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@sweetalert2/theme-bootstrap-4/bootstrap-4.css">
<?php $__env->stopSection(); ?>


<?php $__env->startSection('js'); ?>

    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>
   
    <script type="text/javascript">
        if(message == 'error'){
            Swal.fire(
                'Error!',
                 content,
                'error'
            )
        }
        if(message == 'store'){
            Swal.fire(
                'Parabéns!',
                'Registro cadastrado com sucesso!',
                'success'
            )
        }
        if(message == 'update'){
            Swal.fire(
                'Parabéns!',
                'Registro editado com sucesso!',
                'success'
            )
        }   
        
        if(message == 'destroy'){
            Swal.fire(
                'Parabéns!',
                'Registro excluido com sucesso!',
                'success'
            )
        }  
        
    </script> 

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/Sistema Agencia/Web/resources/views/content/usuarios/perfil.blade.php ENDPATH**/ ?>