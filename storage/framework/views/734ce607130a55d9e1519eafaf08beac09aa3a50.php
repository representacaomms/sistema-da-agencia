<?php $__env->startSection('content'); ?>
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Vendas</a></li>
                            <li class="breadcrumb-item active">Proposta</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Proposta</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="container">
                        <div class="card-body">

                            <!-- Invoice Logo-->
                            <div class="clearfix">
                                <div class="float-left mb-3">
                                    <img src="https://agenciamms.com.br/wp-content/uploads/2021/01/Logo-transparente.png"
                                        alt="" height="100">
                                </div>
                                <div class="float-right">
                                    <h4 class="m-0 d-print-none">PROPOSTA DE PRESTAÇÃO DE SERVIÇOS
                                    </h4>
                                </div>
                            </div>

                            <!-- Invoice Detail-->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="float-left mt-3">

                                        <p class="text-muted font-13">

                                            Este documento visa formalizar a proposta de prestação dos serviços para o
                                            projeto
                                            de desenvolvimento e marketing digital para empresa <?php echo e($cliente->fantasia); ?> com
                                            base no
                                            documento
                                            de briefing Nº
                                            <?php echo e($proposta->created_at->format('Y')); ?>.<?php echo e(str_pad($proposta->nro_proposta, 3, 0, STR_PAD_LEFT)); ?>.
                                            Sendo assim, descrevemos aqui as informações acerca da
                                            metodologia, cronograma de atividades, recursos necessários e da execução do
                                            projeto, assim como, os prazos previamente definidos.</p>
                                    </div>

                                </div><!-- end col -->
                                <div class="col-sm-4 offset-sm-2">
                                    <div class="mt-3 float-sm-right">
                                        <p class="font-13"><strong>Data Proposta: </strong> &nbsp;&nbsp;&nbsp;
                                            <?php echo e($proposta->created_at->format('d/m/Y')); ?> </p>
                                        <p class="font-13"><strong>Status Proposta: </strong>
                                            <?php if($proposta->status == 'Aguardando Aprovacao'): ?>
                                                &nbsp;&nbsp;&nbsp; <span class="badge badge-warning float-right">Aguardando
                                                    Aprovação</span>
                                            <?php elseif($proposta->status == 'Aprovado'): ?>
                                                &nbsp;&nbsp;&nbsp; <span
                                                    class="badge badge-primary float-right">Aprovada</span>
                                            <?php elseif($proposta->status == 'Faturado'): ?>
                                                &nbsp;&nbsp;&nbsp; <span
                                                    class="badge badge-success float-right">Faturado</span>
                                            <?php else: ?>
                                                &nbsp;&nbsp;&nbsp; <span
                                                    class="badge badge-danger float-right">Orçamento</span>
                                            <?php endif; ?>

                                        </p>
                                        <p class="font-13"><strong>ID Proposta: </strong> <span class="float-right">#
                                                <?php echo e($proposta->created_at->format('Y')); ?>.<?php echo e(str_pad($proposta->nro_proposta, 3, 0, STR_PAD_LEFT)); ?></span>
                                        </p>
                                    </div>
                                </div><!-- end col -->
                            </div>
                            <!-- end row -->

                            <div class="row mt-4">
                                <div class="col-sm-5">
                                    <h6>Plano Selecionado</h6>
                                    <h4><?php echo e($plano->nome); ?></h4>
                                    <address>
                                        
                                    </address>
                                </div> <!-- end col-->

                                

                                <div class="col-sm-4">
                                    <div class="text-sm-right">
                                        
                                    </div>
                                </div> <!-- end col-->
                            </div>
                            <!-- end row -->

                            <div class="row" style="page-break-inside:avoid">
                                <div class="col-12">
                                    <h4>PRODUTOS E SERVIÇOS:</h4>
                                    <h5>De acordo com as expectativas alinhadas no briefing os produtos e serviços aqui tem
                                        como
                                        principais objetivos ajudar a<br />
                                        <p> <b>AUMENTAR AS VENDAS, GERAR LEADS
                                                QUALIFICADOS .</b></p>
                                    </h5>
                                    <h4 class="text-center pt-3">Para isso determinamos os seguintes produtos e serviços:
                                    </h4>

                                </div> <!-- end col -->
                            </div>
                            <!-- end row -->
                            <div class="row pt-3" style="page-break-inside:avoid">
                                <div class="col-md-12">
                                    <?php $__currentLoopData = $servicos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <dl class="row mb-0">
                                            <dt class="col-sm-3"><?php echo e($item->servico->descricao); ?></dt>
                                            <dd class="col-sm-9"><?php echo e($item->servico->escopo); ?></dd>
                                        </dl>
                                        <hr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                            
                            <div class="row pt-3" style="page-break-inside:avoid">
                                <div class="col-md-12">
                                    <h4>Etapas do Projeto</h4>
                                    <h5>As seguintes etapas do projeto definem seu cronograma assim como prazos de entrega,
                                        é
                                        importante ressaltar que o prazo é estipulado em dias úteis para uma aprovação.
                                        Sendo
                                        adicionado o mesmo prazo para cada alteração caso seja solicitada com o limite de 3
                                        alterações no total.
                                    </h5>
                                    <h5>
                                        Ex: Se o prazo é estipulado em 5 dias e é solicitada 1 alteração, deve-se estender
                                        mais
                                        5 dias de prazo para a entrega da etapa em questão.</h5>
                                        
                                        <h5>As etapas serão passadas por email que ficará como oficial o relacionamento entre as
                                            partes.</h5>
                                            <h5 class="pt-3">Por fim, o investimento mensal em campanhas de tráfego no Facebook Ads para Facebook
                                                e
                                                Instagram e no Google Ads para Google e YouTube.:
                                            </h5>
                                        </div>
                                    </div>
                                    
                                    <div class="row pt-3" style="page-break-inside:avoid">
                                        <div class="col-md-12">
                                            <h4>BUDGET DE CAMPANHAS:</h4>
                                            <h5><b>IMPORTANTE:</b>O budget de até R$ 3.000,00 (três mil reais) será
                                                100%
                                                investido em campanhas distribuídos entre as redes de tráfego como isenção. A partir
                                                deste valor 20% do budget é destinado a agência para custos de gestão da campanha.
                                            </h5>
                                            <ul>
                                                <li>01 - Budget Google - R$ 900,00 - mensal</li>
                                                <li>01 - Budget Facebook - R$ 900,00 - mensal</li>
                                            </ul>
                                            
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="row pt-3" style="page-break-inside:avoid">
                                        <div class="col-md-12">
                                            <h4>LICENÇAS E FERRAMENTAS DE TERCEIROS:</h4>
                                    <h5>Além dos valores estipulados algumas ferramentas são essenciais para a execução do
                                        projeto, a agência fica encarregada apenas da gestão e bom uso das mesmas, mas nunca
                                        dos
                                        valores, pagamentos ou atualizações de planos:
                                    </h5>
                                    <ul>
                                        <li>01 - Elementor Pro - USD 49,00/Anual</li>
                                        <li>01 - ConvertKit - USD 29,00/Mensal</li>
                                    </ul>


                                </div>
                            </div>

                            <div class="row pt-3" style="page-break-inside:avoid">

                                <div class="col-md-12">
                                    <h4><b>INVESTIMENTO DO PROJETO</b></h4>
                                    <h5>Para a execução do projeto assim como a busca pelos resultados já estipulados nesta
                                        proposta, levando em consideração um contrato de 12 meses para cálculo de descontos,
                                        os
                                        seguintes itens e valores devem ser considerados:</h5>
                                    <div class="table-responsive">
                                        <table class="table mt-4">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Item</th>
                                                    <th>Quantidade</th>
                                                    <th>Valor Unit</th>
                                                    <th class="text-right">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $__currentLoopData = $servicos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><?php echo e($key + 1); ?></td>
                                                        <td>
                                                            <b><?php echo e($item->servico->descricao); ?></b>

                                                        </td>
                                                        <td> <?php echo e($item->qtde); ?></td>
                                                        <td> R$ <?php echo e(number_format($item->valor, 2, ',', '.')); ?></td>
                                                        <td class="text-right"> R$
                                                            <?php echo e(number_format($item->subtotal, 2, ',', '.')); ?></td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </tbody>
                                        </table>
                                    </div> <!-- end table-responsive-->
                                </div>
                            </div>

                            <div class="row pt-3" style="page-break-inside:avoid">
                                <div class="col-sm-8">
                                    <div class="clearfix pt-3">
                                        <h6 class="text-muted">FORMAS DE PAGAMENTO:</h6>
                                        <li>(&nbsp;&nbsp;&nbsp;) À vista com desconto adicional de 20% no projeto de
                                            <b><?php echo e(' R$ ' . number_format($plano->valor_plano, 2, ',', '.')); ?> </b>por<b>
                                                <?php echo e(' R$ ' . number_format($plano->valor_plano - $plano->valor_plano * 0.2, 2, ',', '.')); ?></b>
                                        </li>
                                        <li>(&nbsp;&nbsp;&nbsp; ) Projeto em 1 + 3 de
                                            <b><?php echo e(' R$ ' . number_format($plano->valor_plano / 3, 2, ',', '.')); ?></b>
                                        </li>
                                        <li>(&nbsp;&nbsp;&nbsp; ) Valor mensal em contrato de 12 meses.
                                            <b> <?php echo e(' R$ ' . number_format($plano->valor_plano / 12, 2, ',', '.')); ?></b>
                                        </li>
                                    </div>
                                </div> <!-- end col -->
                                <div class="col-sm-4">
                                    <div class="float-right mt-3 mt-sm-0">

                                        <p><b>Projeto: &nbsp;</b> <span
                                                class="float-right"><?php echo e(' R$ ' . number_format($plano->valor_plano, 2, ',', '.')); ?></span>
                                        </p>
                                        <p><b>Desconto: &nbsp;</b> <span
                                                class="float-right"><?php echo e(' R$ ' . number_format($plano->valor_plano * 0.2, 2, ',', '.')); ?></span>
                                        </p>
                                        <p><b>Projeto - Desconto: &nbsp;</b> <span
                                                class="float-right"><?php echo e(' R$ ' . number_format($plano->valor_plano - $plano->valor_plano * 0.2, 2, ',', '.')); ?></span>
                                        </p>
                                        <h4>&nbsp;&nbsp;&nbsp;</h4>
                                    </div>
                                    <div class="clearfix"></div>
                                </div> <!-- end col -->
                            </div>
                            <!-- end row-->
                            <div class="row" style="page-break-inside:avoid">
                                <div class="col-md-12">
                                    <h4>ANOTAÇÕES:</h4>
                                    <h5> As anotações abaixo feitas servem para ajustes a presente proposta e devem ser
                                        consideradas para a confecção do contrato e execução do projeto.</h5>

                                </div>
                            </div>
                            <div class="row pt-4">
                                <div class="col-md-12">
                                    <b>
                                        <hr class="pt-3">
                                        <hr class="pt-3">
                                        <hr class="pt-3">
                                        <hr class="pt-3">
                                        <hr class="pt-3">
                                    </b>
                                </div>
                            </div>

                            <div class="row" style="page-break-inside:avoid">
                                <div class="col-md-12">
                                    <h4>CONDIÇÕES GERAIS::</h4>
                                    <h5> Na expectativa de oferecer a melhor solução em busca de AUMENTAR AS VENDAS, OBTER
                                        MAIS LEADS QUALIFICADOS e certos da melhor oferta para os serviços necessários ao
                                        resultado, aproveitamos a oportunidade para apresentar protestos de consideração e
                                        apreço.</h5>
                                    <h5>
                                        <ul>
                                            <li>Validade da proposta: 10 (dez dias) corridos.

                                            </li>
                                        </ul>
                                    </h5>
                                    <h5 class="pt-2">
                                        Estando de acordo, assinam as partes autorizando confecção do contrato para
                                        pagamento da primeira parcela e início do projeto;
                                    </h5>
                                </div>
                            </div>
                            <div class="row pt-5"></div>

                            <div class="row pt-5" style="page-break-inside:avoid">
                                <div class="col-md-6">
                                    <p>____________________________________________
                                    <p>
                                    <p><b>AgenciaMMS</b> - Marcelo Andrade da Silva</p>
                                </div>

                                <div class="col-md-6">
                                    <p>____________________________________________</p>
                                    <p style="text-transform: capitalize;"><b><?php echo e($cliente->fantasia); ?></b></p>
                                </div>
                            </div>
                            <div class="d-print-none mt-4" style="page-break-inside:avoid">
                                <div class="text-right">
                                    <a href="javascript:window.print()" class="btn btn-primary"><i
                                            class="mdi mdi-printer"></i> Imprimir</a>
                                    <a href="<?php echo e(route('propostas.index')); ?>" class="btn btn-info">Fechar</a>
                                    <?php if($proposta->status === 'Aprovado'): ?>
                                        <a href="<?php echo e(route('propostas.invoice', $proposta->id)); ?>"
                                            class="btn btn-success"> <i class="mdi mdi-coin"></i>Faturar</a>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <!-- end buttons -->

                        </div> <!-- end card-body-->
                    </div>
                </div> <!-- end card -->
            </div> <!-- end col-->
        </div>
        <!-- end row -->

    </div> <!-- container -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/dashboard/painel/resources/views/content/pdfs/resumo.blade.php ENDPATH**/ ?>