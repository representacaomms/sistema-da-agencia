<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('plataformas.index')); ?>">Gestão Anuncios</a></li>
                            <li class="breadcrumb-item active">Adicionar Gestão Anuncios</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Gestão Anuncios</h4>

                </div>
            </div>
        </div>

        <form action="<?php echo e(route('gestao-anuncios.store')); ?>" method="post">
            <?php echo csrf_field(); ?>
            <?php echo method_field('POST'); ?>

            <div class="row">
                <div class="col-xl-12">
                    <!-- tasks panel -->
                    <div class="collapse show" id="todayTasks">
                        <div class="card mb-0">
                            <div class="card-body">
                                <a class="text-dark" data-toggle="collapse" href="#todayTasks" aria-expanded="false"
                                    aria-controls="todayTasks">
                                    <label class="m-0 pb-2">
                                        <i class="uil uil-angle-down font-18"></i> Gestão Anuncios <span
                                            class="text-muted"></span>
                                    </label>
                                </a>
                                <hr>
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="form-group id_cliente">
                                            <label for="projectname">Cliente</label>
                                            <select class="form-control select2" data-toggle="select2" name="id_cliente"
                                                id="id_cliente" required>
                                                <option value="0">Selecione o Cliente ...</option>
                                                <?php $__currentLoopData = $clientes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>" style="text-transform: capitalize;">
                                                        <?php echo e($item->fantasia ? $item->fantasia : $item->nome); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-4">
                                        <div class="form-group">
                                            <label for="projectname">Gestor</label>
                                            <input type="text" name="vendedor" id="vendedor" class="form-control"
                                                value="<?php echo e(Auth::user()->name); ?>" readonly>
                                            <input type="hidden" name="id_vendedor" id="id_vendedor" class="form-control"
                                                value="<?php echo e(Auth::user()->id); ?>" required>
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-4">
                                        <div class="form-group">
                                            <label for="projectname">Data Lançamento</label>
                                            <input type="date" name="data_lancamento" id="data_lancamento"
                                                class="form-control" value="<?php echo e(Auth::user()->name); ?>" required>

                                        </div>
                                    </div> <!-- end col-->
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="projectname">Plataforma</label>
                                                <select class="form-control select2" data-toggle="select2" name="plataforma"
                                                    id="id_cliente" required>
                                                    <option value="0">Selecione a Plataforma ...</option>
                                                    <option value="google_ads" style="text-transform: capitalize;">Google
                                                        ADS</option>
                                                    <option value="facebook_ads" style="text-transform: capitalize;">
                                                        Facebook ADS</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group ">
                                            <label for="projectname">Tipo de Campanha</label>
                                            <select class="form-control select2" data-toggle="select2" name="tipo_campanha"
                                                id="id_cliente" required>
                                                <option value="0">Selecione o Tipo ...</option>
                                                <option value="conversao" style="text-transform: capitalize;">Conversão
                                                </option>
                                                <option value="trafego" style="text-transform: capitalize;">Trafego</option>
                                                <option value="Distribuicao" style="text-transform: capitalize;">
                                                    Distribuição</option>
                                            </select>
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-5">
                                        <div class="form-group">
                                            <label for="projectname">Nome da Campanha</label>
                                            <input type="text" name="nome_campanha" class="form-control"
                                                style="text-transform: capitalize;" required>
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="fantasia">Custo Total</label>
                                                <input type="text" id="custo_total" name="custo_total" class="form-control"
                                                    data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true"
                                                    placeholder="Custo Total" required>
                                            </div>
                                        </div>
                                    </div> <!-- end col-->
                                </div>
                                <!-- end row -->
                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end .collapse-->
                    <br>
                </div>
            </div>

            <div class="card">

                <div class="card-body">

                <h4 class="header-title mb-3"> Adicionar dados</h4>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="projectname">Impressões</label>
                                <input type="text" name="impressoes" id="impressoes" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="projectname">Cliques</label>
                                <input type="text" name="cliques" id="cliques" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="projectname">Conversão</label>
                                <input type="text" name="conversoes" id="conversoes" class="form-control">
                            </div>
                        </div>
                    </div>
                    
                </div>

                    <!-- Bool Switch-->
                <div class="text-right mt-5">
                    <a href="<?php echo e(URL::previous()); ?>" class="btn btn-light" data-dismiss="modal">Sair</a>
                    <button type="submit" class="btn btn-primary">Adicionar</button>
                </div>
                </div><!-- end col-->
              


            </div>
    </div>

    </form>
    </div>



<?php $__env->startSection('js'); ?>
    <script>
        $(document).ready(function() {

            $('#conversoes').change(function() {
                var impressoes = $('#impressoes').val();
                var cliques = $('#cliques').val();
                var conversoes = $('#conversoes').val();
                var custo_total = $('#custo_total').val();
                var pagina_destino = $('#view_pagina_destino').val();

                var taxa_cliques = ((parseInt(cliques) / parseInt(impressoes)).toFixed(2)) * 100 + ' %';
                var taxa_conversao = ((parseInt(conversoes) / parseInt(cliques)).toFixed(2)) * 100 + ' %';
                var taxa_page_view = ((parseInt(pagina_destino) / parseInt(cliques)).toFixed(2)) * 100 + ' %';
                var custo_conversao = 'R$ ' + (parseFloat(custo_total) / (conversoes)).toFixed(2);
                var cpc_medio = 'R$ ' + (parseFloat(custo_total) / (cliques)).toFixed(2);
                var cpm = 'R$ ' + ((parseFloat(custo_total) / parseInt(impressoes)).toFixed(2)) / 1000;


                $('#impressoes_funil').val(cpm)
                $('#ctr').val(taxa_cliques);
                $('#taxa_page_view').val(taxa_page_view);
                $('#custo_conversao').val(custo_conversao);
                $('#cpc_medio').val(cpc_medio);
            });



        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/gestao-anuncios/create.blade.php ENDPATH**/ ?>