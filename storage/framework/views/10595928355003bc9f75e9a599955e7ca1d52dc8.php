<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('planos.index')); ?>">Planos</a></li>
                            <li class="breadcrumb-item active">Adicionar Plano</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Plano</h4>
                    <input type="hidden" id="operador" value="<?php echo e($operador); ?>">
                    <input type="hidden" id="base_custos" value="<?php echo e($base_custos); ?>">
                    <input type="hidden" id="gestor" value="<?php echo e($gestor); ?>">
                    <input type="hidden" id="servicos_agenciado" value="<?php echo e($servicos_agenciado); ?>">
                    <input type="hidden" id="tempo" value="12">

                </div>
            </div>
        </div>

        <form name="formOrcamento" action="<?php echo e(route('planos.store')); ?>" method="POST">
            <input type="hidden" name="relProdutos" id="relProdutos">
            <?php echo method_field('POST'); ?>
            <?php echo csrf_field(); ?>
            <div class="row">
                <div class="col-xl-12">
                    <!-- tasks panel -->
                    <div class="collapse show" id="todayTasks">
                        <div class="card mb-0">
                            <div class="card-body">
                                <a class="text-dark" data-toggle="collapse" href="#todayTasks" aria-expanded="false"
                                    aria-controls="todayTasks">
                                    <h4 class="m-0 pb-2">
                                        <i class="uil uil-angle-down font-18"></i>Detalhes Planos <span
                                            class="text-muted"></span>
                                    </h4>
                                </a>
                                <hr>
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="form-group cliente">
                                            <label for="projectname">Nome</label>
                                            <input type="text" name="nome" class="form-control">
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-2">
                                        <div class="form-group cliente">

                                            <label for="projectname">Hora Gerenciar</label>
                                            <input type="number" name="hora_gerenciar" id="hora_gerenciar"
                                                class="form-control" min="1" max="12">
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-4">

                                    </div> <!-- end col-->
                                    <div class="col-xl-2">

                                        <label for="projectname">Status</label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="ativo">Ativo</option>
                                            <option value="inativo">Inativo</option>
                                        </select>
                                    </div> <!-- end col-->
                                </div>
                                <!-- end row -->
                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end .collapse-->
                    <br>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="projectname">Serviços</label>
                                        <select class="form-control select2" data-toggle="select2" id="produto">
                                            <option value="0">Selecione um produto</option>
                                            <?php $__currentLoopData = $servicos_agenciado; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($item->id); ?>"><?php echo e($item->descricao); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div> <!-- end col-->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="projectname">&nbsp;</label>
                                        <input type="number" name="qtde" id="qtde" class="form-control" min="1">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="projectname">&nbsp;</label>
                                        <button type="button" class="btn btn-outline-primary form-control" id="addProd">
                                            Adicionar </button>
                                    </div>
                                </div> <!-- end col-->
                            </div>
                            <div class="table-responsive">
                                <table class="table table-borderless table-centered mb-0 no-wrap" id="Table"
                                    style=" max-height: 150px;">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>#</th>
                                            <th>Descricao</th>
                                            <th>Qtde</th>
                                            <th>Valor</th>
                                            <th>SubTotal</th>
                                            <th style="width: 50px"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="Tbody" style="height-max:400px"></tbody>
                                </table>
                            </div> <!-- end table-responsive-->

                            <!-- action buttons-->
                            <div class="row mt-4">
                                <div class="col-sm-6">
                                    <a href="apps-ecommerce-products.html"
                                        class="btn text-muted d-none d-sm-inline-block btn-link font-weight-semibold">
                                        <i class="mdi mdi-arrow-left"></i> Voltar </a>
                                </div> <!-- end col -->
                                <div class="col-sm-6">
                                    <div class="text-sm-right">
                                        <button type="submit" class="btn btn-danger">
                                            <i class="mdi mdi-cart-plus mr-1"></i> Concluir </button>
                                    </div>
                                </div> <!-- end col -->
                            </div> <!-- end row-->
                        </div>
                        <!-- end col -->

                        <div class="col-lg-5">
                            <div class="border p-3 mt-4 mt-lg-0 rounded">
                                <h4 class="header-title mb-3">Resumo da Proposta</h4>

                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <tbody>
                                            <tr>
                                                <td>Plano Base</td>
                                                <td>
                                                    <input type="text" name="plano_base" data-toggle="input-mask"
                                                        data-mask-format="#.##0.00" data-reverse="true" id="plano_base"
                                                        class="form-control">

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Gerenciamento: </td>
                                                <td>
                                                    <input type="text" name="gerenciamento" data-toggle="input-mask"
                                                        data-mask-format="# ##0.00" data-reverse="true" id="gerenciamento"
                                                        class="form-control">

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Margem Lucro</td>
                                                <td>
                                                    <input type="text" name="margem_lucro" id="margem_lucro"
                                                        class="form-control">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Impostos</td>
                                                <td>
                                                    <input type="text" name="impostos" data-toggle="input-mask"
                                                        data-mask-format="# ##0.00" data-reverse="true" id="impostos"
                                                        class="form-control">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Valor do Plano</td>
                                                <td>
                                                    <input type="text" name="valor_plano" data-toggle="input-mask"
                                                        data-mask-format="# ##0.00" data-reverse="true" id="valor_plano"
                                                        class="form-control">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Plano Individual</td>
                                                <td>
                                                    <input type="text" name="valor_plano_individual"
                                                        data-toggle="input-mask" data-mask-format="# ##0.00"
                                                        data-reverse="true" id="valor_plano_individual"
                                                        class="form-control">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- end table-responsive -->
                            </div>

                        </div> <!-- end col -->

                    </div> <!-- end row -->
                </div>
            </div>
        </form>
    </div> <!-- container -->

<?php $__env->startSection('js'); ?>
    <script>
        var arrayProdutos = [];
        var tempo = $('#tempo').val();

        $('#qtde').val(1);
        var qtde = $('#qtde').val();



        $(document).ready(function() {
            var operador = $('#operador').val();
            var base_custos = $('#base_custos').val();
            var gestor = $('#gestor').val();


            base_custos = JSON.parse(base_custos);

            $('#plano_base').val();
            $('#gerenciamento').val();
            $('#impostos').val();

            $("#hora_gerenciar").change(function() {
                var hora = $('#hora_gerenciar').val();
                var plano_base = ((parseFloat(operador) * parseInt(hora)) * parseInt(tempo)).toFixed(2);
                var margem_lucro = (((parseFloat(plano_base) + parseFloat(gestor)) * base_custos
                    .margem_lucro) / 100).toFixed(2);
                var porc_margim = 1 - (base_custos.impostos / 100);
                var impostos = ((parseFloat(plano_base) + parseFloat(gestor) + parseFloat(
                        margem_lucro)) /
                    porc_margim).toFixed(2);

                var valor_plano = (Math.ceil(impostos / 100) * 100).toFixed(2);
                var valor_plano_individual = (parseFloat(valor_plano) / parseInt(tempo)).toFixed(2)


                $('#plano_base').val(plano_base);
                $('#gerenciamento').val(parseFloat(gestor)
                    .toFixed(2));
                $('#margem_lucro').val(margem_lucro);
                $('#impostos').val(
                    impostos);
                $('#valor_plano').val(valor_plano);
                $('#valor_plano_individual').val(
                    valor_plano_individual);

            });

            $("#qtde").change(function() {
                qtde = $('#qtde').val();
            });

            $("#addProd").click(function() {
                var id = $('#produto').val();
                if (id > 0) {
                    var servicos_agenciado = $('#servicos_agenciado').val();
                    var produto = '';
                    var valor = '';

                    servicos_agenciado = JSON.parse(servicos_agenciado);
                    servicos_agenciado.forEach(element => {
                        if (element.id == id) {
                            produto = element.descricao
                            valor = element.esforco_valor
                            qtde = qtde
                            sub_total = parseFloat(valor) * parseInt(qtde)
                        }
                    });
                    arrayProdutos.push({
                        produto: produto,
                        valor: valor,
                        id: id,
                        qtde: qtde,
                        sub_total: sub_total
                    });



                    var id = arrayProdutos.length;
                    var markup = "<tr><td idlinha=" + id + ">" + id + "</td><td data-prod=" + produto +
                        ">" +
                        produto + "</td><td data-qtde=" + qtde + ">" +
                        qtde + "</td><td data-valor=" + valor + ">" +
                        valor +
                        "</td><td data-sub_total=" + sub_total + ">" +
                        sub_total +
                        "</td><td><button type='button' class='btn btn-warning  remover'>Excluir</button></td></tr>";
                    $("table #Tbody").append(markup);
                    $('#relProdutos').val(JSON.stringify(arrayProdutos));

                    var valor_plano = $('#valor_plano').val();

                    var total_plano = (parseFloat(valor_plano) + parseFloat(sub_total)).toFixed(2);

                    $('#valor_plano').val(total_plano);
                    $('#valor_plano_individual').val((total_plano / parseInt(tempo)).toFixed(2));
                }
            });

            $('#Table').on('click', '.remover', function(e) {
                $('#desconto').val(0.00);
                var produto = $(this).closest('tr').find('td[data-prod]').data('prod');
                var sub_total = $(this).closest('tr').find('td[data-sub_total]').data('sub_total');
                var indice = $("td").attr("idlinha");

                arrayProdutos.splice(indice);
                $(this).closest('tr').remove();

                var valor_plano = $('#valor_plano').val();

                var total_plano = (parseFloat(valor_plano) - parseFloat(sub_total)).toFixed(2);

                $('#valor_plano').val(total_plano);
                $('#valor_plano_individual').val((total_plano / parseInt(tempo)).toFixed(2));

            });


        });

    </script>
<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/agencia_WEB/resources/views/content/financeiros/planos/create.blade.php ENDPATH**/ ?>