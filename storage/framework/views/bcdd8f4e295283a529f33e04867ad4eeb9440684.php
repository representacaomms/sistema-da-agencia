<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8" />
    <title>Painel | Agencia MMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Soluções me Tecnologia para empresas." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <!-- App favicon -->
    <link rel="shortcut icon" href="https://agenciamms.com/wp-content/uploads/2021/11/cropped-Favicon-192x192.jpeg"
        sizes="192x192">

    <!-- third party css -->
    <link href="<?php echo e(asset('/css/vendor/jquery-jvectormap-1.2.2.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- third party css end -->


    <!-- App css -->
    <link href="<?php echo e(asset('/css/icons.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/app.min.css')); ?>" rel="stylesheet" type="text/css" />

    <!-- third party css -->
    <link href="<?php echo e(asset('css/vendor/responsive.bootstrap4.css')); ?>" rel="stylesheet" type="text/css" />


</head>

<body>
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="card mt-2">
                    <div class="card-body">
                        <h3 float-center>Apresentação da Consultoria Gratuita</h3>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->

        </div>

        <div class="row">
            <div class="col-12">
                <div class="card mt-2">
                    <div class="card-body">

                        <form>
                            <div id="progressbarwizard">

                                <ul class="nav nav-pills nav-justified form-wizard-header mb-3">
                                    <li class="nav-item d-none">
                                        <a href="#window-1" data-bs-toggle="tab" data-toggle="tab"
                                            class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-account-circle me-1"></i>
                                            <span class="d-none d-sm-inline">Tela 1</span>
                                        </a>
                                    </li>
                                    <li class="nav-item d-none">
                                        <a href="#window-2" data-bs-toggle="tab" data-toggle="tab"
                                            class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-face-profile me-1"></i>
                                            <span class="d-none d-sm-inline">Tela 2</span>
                                        </a>
                                    </li>
                                    <li class="nav-item d-none">
                                        <a href="#window-3" data-bs-toggle="tab" data-toggle="tab"
                                            class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-checkbox-marked-circle-outline me-1"></i>
                                            <span class="d-none d-sm-inline">Tela 3</span>
                                        </a>
                                    </li>
                                    <li class="nav-item d-none">
                                        <a href="#window-4" data-bs-toggle="tab" data-toggle="tab"
                                            class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-checkbox-marked-circle-outline me-1"></i>
                                            <span class="d-none d-sm-inline">Tela 4</span>
                                        </a>
                                    </li>
                                    <li class="nav-item d-none">
                                        <a href="#window-5" data-bs-toggle="tab" data-toggle="tab"
                                            class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-checkbox-marked-circle-outline me-1"></i>
                                            <span class="d-none d-sm-inline">Tela 5</span>
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content b-0 mb-0">

                                    <div id="bar" class="progress mb-3" style="height: 7px;">
                                        <div
                                            class="bar progress-bar progress-bar-striped progress-bar-animated bg-success">
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="window-1" style="height: 500px;">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="row mb-3">
                                                    <div class="col-5">
                                                        <h1 class="display-1"><b><?php echo e($record->dados->nome); ?></b>
                                                        </h1>
                                                        <h3 class="display-6 pt-5"><b>Proposta comercial de consultoria,
                                                                desenvolvimento e marketing</b></h3>
                                                    </div>
                                                    <div class="col-1"></div>
                                                    <div class="col-6">
                                                        <h1 class="h2 mt-5"><b>OBJETIVO:</b></h1>
                                                        <li class="h3 pt-3">Aumentar as vendas</li>
                                                        <li class="h3 pt-3">Alcançar mais clientes (leads)
                                                            qualificados;</li>
                                                        <li class="h3 pt-3">Fidelizar os clientes atuais e vender
                                                            mais para eles;</li>
                                                    </div>
                                                </div>

                                            </div> <!-- end col -->
                                        </div> <!-- end row -->
                                    </div>

                                    <div class="tab-pane" id="window-2" style="height: 500px;">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="row mb-3">
                                                    <h2 class="col-md-12 display-3" for="name1"> Agência MMS</h2>
                                                    <h2 class="col-md-12 display-6 pt-3" for="name1"><b>Sobre a agência:</b><p class="h3"> Somos uma empresa de MARKETING focada em resultados. Os nossos objetivos vão desde o planejamento estratégicos das ações como: a criação, a comunicação e a entrega de valor de seus projetos para os clientes.</p></h2>
                                                    <h2 class="col-md-12 display-6 pt-3" for="name1"><b>Proposta de valor:</b><p class="h3"> Ajudar as pequenas e médias empresas a terem o maior faturamento possivel, competindo com as grandes empresas usando a internet para trazer otimos resultados.</p></h2>
                                                </div>
                                               
                                            </div> <!-- end col -->
                                        </div> <!-- end row -->
                                    </div>
                                    <div class="tab-pane" id="window-3">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="row mt-5"></div>
                                                <div class="row pt-3"></div>
                                                <div class="row mb-3">
                                                    <h2 class="col-md-12 display-6" for="name1">Vamos conseguir esse<br> resultado para você pois:</h2>
                                                </div>

                                                <div class="row mb-3">
                                                    <h2 class="col-md-12 h4" for="name1">Diferencial da agência</h2>
                                                </div>
                                                <div class="row mb-3">
                                                    <li class="col-md-12 h4" for="name1">Somos uma empresa 100% focada no resultado de nossos clientes.</li>
                                                    <li class="col-md-12 h4" for="name1">Garantia de satisfação do cliente, mediante resultados mostrados.</li>
                                                </div>
                                                
                                                <div class="row mt-5"></div>
                                                <div class="row mt-5"></div>
                                            </div> <!-- end col -->
                                        </div> <!-- end row -->
                                    </div>
                                    <div class="tab-pane" id="window-4">
                                        <div class="row">
                                            <div class="col-12">
                                                
                                                <div class="row pt-3"></div>
                                                <div class="row mb-3">
                                                    <h2 class="col-md-12 display-1" for="name1">O PROJETO</h2>
                                                </div>

                                                <div class="row mb-3">
                                                </div>
                                                <div class="row mb-3">
                                                    <li class="col-md-12 h4" for="name1">Aumentar as vendas</li>
                                                    <li class="col-md-12 h4" for="name1">Alcançar mais clientes (leads)<br>qualificados.</li>
                                                    <li class="col-md-12 h4" for="name1">Fidelizar os clientes atuais e<br>  vender mais para eles;</li>
                                                    
                                                </div>
                                                <div class="row mt-5"></div>
                                                <div class="row mt-5"></div>
                                            </div> <!-- end col -->
                                        </div> <!-- end row -->
                                    </div>
                                    <div class="tab-pane" id="window-5">
                                        <div class="row">
                                            <div class="col-12">
                                                
                                                <div class="row mb-3">
                                                    <h2 class="col-md-12 display-1" for="name1">Requisitos do Projeto</h2>
                                                </div>

                                                <div class="row mb-3">
                                                    <div class="col-6">
                                                        <h3>Consultoria e INFRA</h3>
                                                        <?php $__currentLoopData = $record->item_plano->itens; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php $__currentLoopData = $item->servico; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <li class="col-md-12 h4" for="name1"><?php echo e($value->descricao); ?> (<?php echo e($item->qtde); ?>)</li>
                                                          
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                       
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    
                                                </div>
                                                <div class="row mt-5"></div>
                                            </div> <!-- end col -->
                                        </div> <!-- end row -->
                                    </div>
                                   
                                    <hr>    
                                    <ul class="list-inline mb-0 wizard">
                                        <li class="previous list-inline-item">
                                            <a href="#" class="btn btn-info">Voltar</a>
                                        </li>
                                        <li class="next list-inline-item float-end">
                                            <a href="#" class="btn btn-info">Próximo</a>
                                        </li>
                                        <li class="list-inline-item float-right">
                                            <a href="<?php echo e(route('apresentacao.index')); ?>" class="btn btn-danger">Sair</a>
                                        </li>
                                    </ul>

                                </div> <!-- tab-content -->
                            </div> <!-- end #progressbarwizard-->
                        </form>

                    </div> <!-- end card-body-->
                </div>
            </div>
        </div>
        <!-- end row-->
    </div> <!-- container -->
</body>

<!-- bundle -->
<script src="<?php echo e(asset('js/vendor.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/app.min.js')); ?>"></script>


<!-- Scripst -->
<script src="<?php echo e(asset('js/scripts.js')); ?>"></script>
<script>
    $('.dropdown-toggle').dropdown();
    $(function() {
        "use strict";
        $("#basicwizard").bootstrapWizard(),
            $("#progressbarwizard").bootstrapWizard({
                onTabShow: function(t, r, a) {
                    var o = (a + 1) / r.find("li").length * 100;
                    $("#progressbarwizard").find(".bar").css({
                        width: o + "%"
                    })
                }
            }),
            $("#btnwizard").bootstrapWizard({
                nextSelector: ".button-next",
                previousSelector: ".button-previous",
                firstSelector: ".button-first",
                lastSelector: ".button-last"
            }), $("#rootwizard").bootstrapWizard({
                onNext: function(t, r, a) {
                    var o = $($(t).data("targetForm"));
                    if (o && (o.addClass("was-validated"), !1 === o[0].checkValidity())) return event
                        .preventDefault(), event.stopPropagation(), !1
                }
            })
    });
</script>
<?php /**PATH /Users/mac-marcelo/projetos/agencia_WEB/resources/views/content/apresentacao/view.blade.php ENDPATH**/ ?>