<?php $__env->startSection('content'); ?>
<!-- Start Content-->
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">COntratos</a></li>
                        <li class="breadcrumb-item active">Listar COntratos</li>
                    </ol>
                </div>
                <h4 class="page-title">Listar COntratos</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-sm-4">
                            <a href="<?php echo e(route('contratos.create')); ?>" class="btn btn-danger mb-2"><i
                                    class="mdi mdi-plus-circle mr-2"></i> Adicionar Consultoria</a>
                        </div>
                        <div class="col-sm-8">
                            <div class="text-sm-right">
                                        <a href="<?php echo e(route('contratos.show',1)); ?>" class="btn btn-light mb-2"><i
                                            class="mdi mdi-book-outline mr-2"></i> Montar Consultoria</a>
                            </div>
                        </div><!-- end col-->
                    </div>

                    <div class="table-responsive">
                        <?php if($message = Session::get('success')): ?>
                            <?php if($message == 'store'): ?>
                                <script>
                                    var message = 'store';
                                </script>
                            <?php endif; ?>
                            <?php if($message == 'update'): ?>
                                <script>
                                    var message = 'update';
                                </script>
                            <?php endif; ?>

                            <?php if($message == 'destroy'): ?>
                                <script>
                                    var message = 'destroy';
                                </script>
                            <?php endif; ?>

                            <?php if($message == 'active'): ?>
                                <script>
                                    var message = 'active';
                                </script>
                            <?php endif; ?>
                            
                        <?php endif; ?>
                    </div>

                    <table class="table table-centered w-100 dt-responsive " id="contratos-datatable">
                        <thead class="thead-light">
                            <tr>
                                <th>Tipo</th>
                                <th class="all">Nome</th>                                                               
                                <th>Site</th>
                                <th>Instagram</th>
                                <th>Facebook</th>
                                <th>Youtube</th> 
                                <th>GMN</th> 

                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td>
                                        <p class="m-0 d-inline-block align-middle font-12">
                                            <a href="<?php echo e(route('contratos.edit', $item->id)); ?>" class="text-body"style="text-transform: uppercase;"><?php echo e($item->tipo); ?></a>
                                        </p><br>
                                        <?php if($item->status == 'ativo'): ?>
                                        <span class="badge badge-success">Ativo</span>
                                        <?php else: ?>
                                        <span class="badge badge-danger">Inativo</span>
                                        <?php endif; ?>  
                                    </td>                                    
                                    <td>
                                        <p class="m-0 d-inline-block align-middle font-12">
                                            <a href="<?php echo e(route('contratos.edit', $item->id)); ?>" class="text-body"style="text-transform: uppercase;"><?php echo e($item->nome); ?></a>
                                        </p>
                                    </td>
                                    
                                    <td>
                                        <p class="m-0 d-inline-block align-middle font-12">
                                            <a href="<?php echo e(route('contratos.edit', $item->id)); ?>" class="text-body"style="text-transform: uppercase;"><?php echo e($item->site); ?></a>
                                        </p>
                                    </td>
                                    <td>
                                        <p class="m-0 d-inline-block align-middle font-12">
                                            <a href="<?php echo e(route('contratos.edit', $item->id)); ?>" class="text-body"style="text-transform: uppercase;"><?php echo e($item->instagram); ?></a>
                                        </p>
                                    </td>
                                    <td>
                                        <p class="m-0 d-inline-block align-middle font-12">
                                            <a href="<?php echo e(route('contratos.edit', $item->id)); ?>" class="text-body"style="text-transform: uppercase;"><?php echo e($item->facebook); ?></a>
                                        </p>
                                    </td>
                                    <td>
                                        <p class="m-0 d-inline-block align-middle font-12">
                                            <a href="<?php echo e(route('contratos.edit', $item->id)); ?>" class="text-body"style="text-transform: uppercase;"><?php echo e($item->youtube); ?></a>
                                        </p>
                                    </td>
                                    <td>
                                        <p class="m-0 d-inline-block align-middle font-12">
                                            <a href="<?php echo e(route('contratos.edit', $item->id)); ?>" class="text-body"style="text-transform: uppercase;"><?php echo e($item->gmn); ?></a>
                                        </p>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>
                    </table>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
          

        </div> <!-- end col -->
    </div>
    <!-- end row-->
</div> <!-- container -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<!-- DataTables -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script>
    $(document).ready(function () {
       
        $('#contratos-datatable').DataTable({
            language: {
                processing: "Processamento...",
                search: "Pesquisar&nbsp;:",
                lengthMenu: "Mostrar _MENU_ entradas",
                info: "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                infoEmpty: "Exibindo da entrada 0 até 0 de 0 entradas",
                infoFiltered: "(filtro total de _MAX_ entradas)",
                infoPostFix: "",
                loadingRecords: "Carregando ...",
                zeroRecords: "Nenhuma entrada encontrada",
                emptyTable: "Nenhuma entrada disponível na tabela",
                paginate: {
                    first: "Primeiro",
                    previous: "Anterior",
                    next: "Ultimo",
                    last: "Depos"
                },
                aria: {
                    sortAscending: ": ativar para classificar a coluna em ordem crescente",
                    sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                }
            }
        });
    
    });
    switch (message) {
        case 'store':
            Swal.fire(
                'Parabéns!',
                'Registro cadastrado com sucesso!',
                'success'
            )
            break;
            case 'update':
            Swal.fire(
                'Parabéns!',
                'Registro editado com sucesso!',
                'success'
            )
            break;
            case 'destroy':
            Swal.fire(
                'Alerta!',
                'Registro inativado com sucesso!',
                'info'
            )
            break;
            case 'active':
            Swal.fire(
                'Alerta!',
                'Registro ativado com sucesso!',
                'info'
            )
            break;
    
        default:
            break;
    }

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/contratos/index.blade.php ENDPATH**/ ?>