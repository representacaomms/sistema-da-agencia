<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('custos.index')); ?>">Custos</a></li>
                            <li class="breadcrumb-item active">Listar Custos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Listar Custos</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                                <a href="<?php echo e(route('custos.create')); ?>" class="btn btn-danger mb-2"><i class="mdi mdi-plus-circle mr-2"></i> Adicionar Custos</a>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-success mb-2 mr-1" data-toggle="modal" data-target="#right-modal"><i class="mdi mdi-settings"></i></button>
                                    <button type="button" class="btn btn-light mb-2 mr-1">Importar</button>
                                    <button type="button" class="btn btn-light mb-2">Exportar</button>
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            <?php if($message = Session::get('success')): ?>
                            <?php if($message == 'store'): ?>
                                <script>
                                    var message = 'store';
                                </script>
                            <?php endif; ?>
                            <?php if($message == 'update'): ?>
                                <script>
                                    var message = 'update';
                                </script>
                            <?php endif; ?>

                            <?php if($message == 'destroy'): ?>
                                <script>
                                    var message = 'destroy';
                                </script>
                            <?php endif; ?>

                            <?php if($message == 'active'): ?>
                                <script>
                                    var message = 'active';
                                </script>
                            <?php endif; ?>

                            <?php endif; ?>
                            <table class="table table-centered w-100 dt-responsive " id="custos-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Tipo</th>
                                        <th>Descrição</th>
                                        <th>Valor</th>
                                        <th >Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $custos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16">
                                                    <?php if($item->tipo =='insumos'): ?>
                                                        <span class=" font-16 badge badge-info">Insumos</span>
                                                    <?php elseif($item->tipo =='operacional'): ?>
                                                        <span class=" font-16 badge badge-warning">Operacional</span>
                                                    <?php elseif($item->tipo =='licencas'): ?>
                                                        <span class=" font-16 badge badge-secondary">Licenças</span>
                                                    <?php elseif($item->tipo =='budget'): ?>
                                                        <span class=" font-16 badge badge-primary">Budget</span>
                                                    <?php else: ?>
                                                        <span class="font-16 badge badge-success">Fixos</span>
                                                    <?php endif; ?>

                                                </p>
                                            </td>
                                            <td><p class="m-0 d-inline-block align-middle font-16"><a  class="text-body"><?php echo e($item->nome); ?></a></p></td>
                                            <td><p class="m-0 d-inline-block align-middle font-16"><a  class="text-body">R$ <?php echo e(number_format($item->valor,2)); ?></a></p></td>

                                            <td>
                                                <a href="<?php echo e(route('custos.edit', $item->id)); ?>" class="btn btn-warning mb-2">Editar</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="right-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-right">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="standard-modalLabel">Base dos Custos</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <form class="pl-3 pr-3" action="<?php echo e(route('base-custos.store')); ?>" method="POST">
                                            <?php echo csrf_field(); ?>
                                            <div class="form-group">
                                                <label for="username">Clientes</label>
                                                <input class="form-control" type="text" name="clientes" value="<?php echo e($clientes <= 10 ? 10 : $clientes); ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="emailaddress">Despesas</label>
                                                <input class="form-control" type="text" name="despesas"  value="<?php echo e(number_format($despesas_fixas, 2)); ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="password">Margem Negócio</label>
                                                <input class="form-control" type="text" required id="margem_negocio" name="margem_negocio" value="<?php echo e($base_custos ? $base_custos->margem_negocio : ''); ?>">
                                            </div>
                                           <div class="form-group">
                                                <label for="password">Margem Lucro</label>
                                                <input class="form-control" type="text" required id="margem_lucro" name="margem_lucro" value="<?php echo e($base_custos ? $base_custos->margem_lucro : ''); ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="password">Impostos</label>
                                                <input class="form-control" type="text" required id="impostos" name="impostos" value="<?php echo e($base_custos ? $base_custos->impostos : ''); ?>">
                                            </div>



                                            <div class="form-group text-center">
                                                <button class="btn btn-primary" type="submit">Atualizar</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal -->
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
            <!-- end row -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function() {

            $('#custos-datatable').DataTable({
                language: {
                    processing:     "Processamento...",
                    search:         "Pesquisar&nbsp;:",
                    lengthMenu:    "Mostrar _MENU_ entradas",
                    info:           "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty:      "Exibindo da entrada 0 até 0 de 0 entradas",
                    infoFiltered:   "(filtro total de _MAX_ entradas)",
                    infoPostFix:    "",
                    loadingRecords: "Carregando ...",
                    zeroRecords:    "Nenhuma entrada encontrada",
                    emptyTable:     "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first:      "Primeiro",
                        previous:   "Anterior",
                        next:       "Ultimo",
                        last:       "Depos"
                    },
                    aria: {
                        sortAscending:  ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        } );
    </script>
    <script type="text/javascript">

        if(message == 'store'){
            Swal.fire(
                'Parabéns!',
                'Despesa cadastrada com sucesso!',
                'success'
            )
        }
        if(message == 'update'){
            Swal.fire(
                'Parabéns!',
                'Despesa editada com sucesso!',
                'success'
            )
        }

        if(message == 'destroy'){
            Swal.fire(
                'Alerta!',
                'Despesa inativada com sucesso!',
                'info'
            )
        }

        if(message == 'active'){
            Swal.fire(
                'Alerta!',
                'Despesa ativada com sucesso!',
                'info'
            )
        }

    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/Sistema Agencia/Web/resources/views/content/financeiros/custos/index.blade.php ENDPATH**/ ?>