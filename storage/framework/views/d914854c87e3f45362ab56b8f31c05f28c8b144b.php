<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('planos.index')); ?>">Planos</a></li>
                            <li class="breadcrumb-item active">Listar Planos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Listar Planos</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                                <a href="<?php echo e(route('planos.create')); ?>" class="btn btn-danger mb-2"><i
                                        class="mdi mdi-plus-circle mr-2"></i> Adicionar Planos</a>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-success mb-2 mr-1" data-toggle="modal"
                                        data-target="#right-modal"><i class="mdi mdi-settings"></i></button>
                                    <button type="button" class="btn btn-light mb-2 mr-1">Importar</button>
                                    <button type="button" class="btn btn-light mb-2">Exportar</button>
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            <?php if($message = Session::get('success')): ?>
                                <?php if($message == 'store'): ?>
                                    <script>
                                        var message = 'store';

                                    </script>
                                <?php endif; ?>
                                <?php if($message == 'update'): ?>
                                    <script>
                                        var message = 'update';

                                    </script>
                                <?php endif; ?>

                                <?php if($message == 'destroy'): ?>
                                    <script>
                                        var message = 'destroy';

                                    </script>
                                <?php endif; ?>

                                <?php if($message == 'active'): ?>
                                    <script>
                                        var message = 'active';

                                    </script>
                                <?php endif; ?>

                            <?php endif; ?>
                            <table class="table table-centered w-100 dt-responsive " id="planos-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Descrição</th>
                                        <th>Valor Plano</th>
                                        <th>Valor Mensal</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $planos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16"><a
                                                        class="text-body"><?php echo e($item->nome); ?></a></p>
                                            </td>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16"><a class="text-body">R$
                                                        <?php echo e($item->valor_plano); ?></a>
                                                </p>
                                            </td>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16"><a class="text-body">R$
                                                        <?php echo e($item->plano_individual); ?></a></p>
                                            </td>

                                            <td>

                                               
                                                    <form action="<?php echo e(route('planos.destroy', $item->id)); ?>" method="post">
                                                        <?php echo csrf_field(); ?>
                                                        <?php echo method_field('DELETE'); ?>
                                                        <a href="<?php echo e(route('planos.edit', $item->id)); ?>"
                                                            class="btn btn-primary mb-2">Editar</a>
                                                    <button type="submit" class="btn btn-danger mb-2">Excluir</button>
                                                    </form>
                                                   
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>

                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
        <!-- end row -->

    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('js'); ?>
        <!-- DataTables -->
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script>
            $(document).ready(function() {

                $('#planos-datatable').DataTable({
                    language: {
                        processing: "Processamento...",
                        search: "Pesquisar&nbsp;:",
                        lengthMenu: "Mostrar _MENU_ entradas",
                        info: "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                        infoEmpty: "Exibindo da entrada 0 até 0 de 0 entradas",
                        infoFiltered: "(filtro total de _MAX_ entradas)",
                        infoPostFix: "",
                        loadingRecords: "Carregando ...",
                        zeroRecords: "Nenhuma entrada encontrada",
                        emptyTable: "Nenhuma entrada disponível na tabela",
                        paginate: {
                            first: "Primeiro",
                            previous: "Anterior",
                            next: "Ultimo",
                            last: "Depos"
                        },
                        aria: {
                            sortAscending: ": ativar para classificar a coluna em ordem crescente",
                            sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                        }
                    }
                });
            });

        </script>
        <script type="text/javascript">
            if (message == 'store') {
                Swal.fire(
                    'Parabéns!',
                    'Despesa cadastrada com sucesso!',
                    'success'
                )
            }
            if (message == 'update') {
                Swal.fire(
                    'Parabéns!',
                    'Despesa editada com sucesso!',
                    'success'
                )
            }

            if (message == 'destroy') {
                Swal.fire(
                    'Alerta!',
                    'Plano excluido com sucesso!',
                    'info'
                )
            }

            if (message == 'active') {
                Swal.fire(
                    'Alerta!',
                    'Despesa ativada com sucesso!',
                    'info'
                )
            }

        </script>

    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/Sistema Agencia/Web/resources/views/content/financeiros/planos/index.blade.php ENDPATH**/ ?>