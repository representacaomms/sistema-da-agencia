<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('propostas.index')); ?>">Propostas</a></li>
                            <li class="breadcrumb-item active">Editar Propostas</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Propostas</h4>                   
                </div>
            </div>
        </div>
        
        <form name="formOrcamento" action="<?php echo e(route('propostas.update', $proposta->id)); ?>" method="POST">
            
            <?php echo method_field('PUT'); ?>
            <?php echo csrf_field(); ?>
            <div class="row">
                <div class="col-xl-12">
                    <!-- tasks panel -->
                    <div class="collapse show" id="todayTasks">
                        <div class="card mb-0">
                            <div class="card-body">
                                <a class="text-dark" data-toggle="collapse" href="#todayTasks" aria-expanded="false"
                                    aria-controls="todayTasks">
                                    <label class="m-0 pb-2">
                                        <i class="uil uil-angle-down font-18"></i>Dados Propostas <span
                                            class="text-muted"></span>
                                    </label>
                                </a>
                                <hr>
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="form-group id_cliente">
                                            <label for="projectname">Cliente</label>
                                             <input type="text" name="cliente" id="cliente" class="form-control"
                                                value="<?php echo e($proposta->dados->nome); ?>" readonly>
                                        </div>
                                    </div> <!-- end col-->

                                    <div class="col-xl-4">
                                        <div class="form-group">
                                            <label for="projectname">Vendedor</label>
                                            <input type="text" name="vendedor" id="vendedor" class="form-control"
                                                value="<?php echo e(Auth::user()->name); ?>" readonly>
                                            <input type="hidden" name="id_vendedor" id="id_vendedor" class="form-control"
                                                value="<?php echo e(Auth::user()->id); ?>">
                                        </div>
                                    </div> <!-- end col-->
                                </div>
                                <!-- end row -->
                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end .collapse-->
                    <br>
                </div>
            </div>

            <div class="card">

                <div class="card-body">

                    <h4 class="header-title mb-3"> Detalhes da Proposta </h4>


                    <div class="row">
                        <?php $__currentLoopData = $planos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <div class="col-md-4">
                                <!-- Portlet card -->
                                <div class="card mb-md-0 mb-3">
                                    <div class="<?php echo e($item->id == $proposta->plano ? 'card-body border-danger border' : 'card-body border-primary border'); ?>">
                                        <form action="<?php echo e(route('propostas.update', $proposta->id)); ?>" method="post">
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('PUT'); ?>
                                            <h5 class="card-title mb-0 text-center"><?php echo e($item->nome); ?>

                                            </h5>
                                            <hr>
                                            <div class="card-body">
                                                <?php $__currentLoopData = $item->itens; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $iten): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <h5 class="card-title"><?php echo e($iten->servico->descricao); ?></h5>
                                                    <p class="card-text"><?php echo e($iten->servico->escopo); ?></p>
                                                    <hr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <input type="hidden" name="plano" value="<?php echo e($item->id); ?>">
                                                <input type="hidden" name="cliente" value="<?php echo e($cliente->nome); ?>">
                                                <input type="hidden" name="situacao" value="orcamento">
                                                <input type="hidden" name="vendedor" value="<?php echo e(Auth::user()->name); ?>">
                                                <?php if($item->id != $proposta->plano): ?>
                                                    <input type="submit" value="Selecionar" class="btn btn-primary btn-sm " >
                                                <?php endif; ?>
                                                  
                                            </div>
                                        </form>

                                    </div>
                                </div> <!-- end card-->

                            </div><!-- end col -->
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                    <!-- Bool Switch-->

                </div>
            </div>
        </form>
    </div> <!-- container --> 
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script>
        $(document).ready(function() {

            $('#id_cliente').change(function() {
                var id_cliente = ($(this).val());
                $('.cliente').val(id_cliente);
            });



        });

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/dashboard/painel/resources/views/content/propostas/edit.blade.php ENDPATH**/ ?>