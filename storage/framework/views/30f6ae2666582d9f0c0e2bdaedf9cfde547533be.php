<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('cadastros.diversos')); ?>">Cadastros Diversos</a></li>
                            <li class="breadcrumb-item active">Listar Cadastros Diversos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Listar Cadastros Diversos</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-12">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-success mb-2 mr-1"><i class="mdi mdi-settings"></i></button>
                                    <button type="button" class="btn btn-light mb-2 mr-1">Importar</button>
                                    <button type="button" class="btn btn-light mb-2">Exportar</button>
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <!-- Todo-->
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title mb-3">Plano de Contas</h4>
                                        <div class="todoapp">
                                            <ul class="list-group list-group-flush slimscroll todo-list" style="max-height: 330px" id="todo-list">
                                                <div class="table-responsive">
                                                    <table class="table table-centered w-100 dt-responsive " >
                                                        <thead class="thead-light">
                                                            <tr>
                                                                <th>Descrição</th>
                                                                <th >Ação</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="max-height: 25px;">
                                                        <?php $__currentLoopData = $planoContas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$plano): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <tr>
                                                                <td>
                                                                    <p class="m-0 d-inline-block align-middle font-16">
                                                                        <a href="apps-ecommerce-products-details.html" class="text-body"><?php echo e($plano->nome); ?></a>
                                                                    </p>
                                                                </td>

                                                                <td style="width:20px">
                                                                    <form action="<?php echo e(route('plano-contas.destroy',['plano_conta' =>$plano->id])); ?>" method="post">
                                                                        <?php echo csrf_field(); ?>
                                                                        <?php echo method_field('Delete'); ?>
                                                                        <button type="submit" class="btn-icon btn-light"> <i class="mdi mdi-delete"></i></button>
                                                                    </form>
                                                                </td>

                                                            </tr>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </ul>
                                            <form action="<?php echo e(route('plano-contas.store')); ?>" method="POST" class="needs-validation" novalidate>
                                                <?php echo csrf_field(); ?>
                                                <div class="row">
                                                    <div class="col">
                                                        <input type="text" id="todo-input-text" name="nome" class="form-control"
                                                            required>
                                                        <div class="invalid-feedback">
                                                            Digite o nome do plano de contas
                                                        </div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="btn-primary btn-md btn-block btn waves-effect waves-light" type="submit" id="todo-btn-submit">+</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div> <!-- end .todoapp-->
                                    </div> <!-- end card-body -->
                                </div> <!-- end card-->
                            </div>
                            <div class="col-md-6">
                                <!-- Todo-->
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title mb-3">Centro de Custos</h4>

                                        <div class="todoapp">

                                            <ul class="list-group list-group-flush slimscroll todo-list" style="max-height: 330px" id="todo-list">
                                                <div class="table-responsive">
                                                    <table class="table table-centered w-100 dt-responsive " >
                                                        <thead class="thead-light">
                                                            <tr>
                                                                <th>Descrição</th>
                                                                <th >Ação</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="max-height: 25px;">
                                                            <?php $__currentLoopData = $centroCustos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$centro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <tr>
                                                                    <td><?php echo e($centro->nome); ?></td>

                                                                    <td style="width:20px">
                                                                        <form action="<?php echo e(route('centro-custos.destroy', $centro->id)); ?>" method="post">
                                                                            <?php echo csrf_field(); ?>
                                                                            <?php echo method_field('Delete'); ?>
                                                                            <button type="submit" class="btn-icon btn-light"> <i class="mdi mdi-delete"></i></button>
                                                                        </form>
                                                                    </td>

                                                                </tr>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </ul>

                                            <form action="<?php echo e(route('centro-custos.store')); ?>" method="POST" class="needs-validation" novalidate>
                                                <?php echo csrf_field(); ?>
                                                <div class="row">
                                                    <div class="col">
                                                        <input type="text" id="todo-input-text" name="nome" class="form-control"
                                                           required>
                                                        <div class="invalid-feedback">
                                                            Digite o nome do Centro de Custo
                                                        </div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="btn-primary btn-md btn-block btn waves-effect waves-light" type="submit" id="todo-btn-submit">+</button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div> <!-- end .todoapp-->

                                    </div> <!-- end card-body -->
                                </div> <!-- end card-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <!-- Todo-->
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title mb-3">Categorias</h4>
                                        <div class="todoapp">
                                            <ul class="list-group list-group-flush slimscroll todo-list" style="max-height: 330px" id="todo-list">
                                                <div class="table-responsive">
                                                    <table class="table table-centered w-100 dt-responsive ">
                                                        <thead class="thead-light">
                                                            <tr>
                                                                <th>Descrição</th>
                                                                <th >Ação</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="max-height: 25px;">
                                                            <?php $__currentLoopData = $categorias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <tr>
                                                                    <td><?php echo e($cat->nome); ?></td>

                                                                    <td style="width:20px">
                                                                        <form action="<?php echo e(route('categorias.destroy',$cat->id)); ?>" method="post">
                                                                            <?php echo csrf_field(); ?>
                                                                            <?php echo method_field('Delete'); ?>
                                                                            <button type="submit" class="btn-icon btn-light"> <i class="mdi mdi-delete"></i></button>
                                                                        </form>
                                                                    </td>

                                                                </tr>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </ul>
                                            <form action="<?php echo e(route('categorias.store')); ?>" method="POST" class="needs-validation" novalidate>
                                                <?php echo csrf_field(); ?>
                                                <div class="row">
                                                    <div class="col">
                                                        <input type="text" id="todo-input-text" name="nome" class="form-control"
                                                             required>
                                                        <div class="invalid-feedback">
                                                            Digite o nome da categoria
                                                        </div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="btn-primary btn-md btn-block btn waves-effect waves-light" type="submit" id="todo-btn-submit">+</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div> <!-- end .todoapp-->
                                    </div> <!-- end card-body -->
                                </div> <!-- end card-->
                            </div>

                            <div class="col-md-6">
                                <!-- Todo-->
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title mb-3">Sub-Categorias</h4>
                                        <div class="todoapp">
                                            <ul class="list-group list-group-flush slimscroll todo-list" style="max-height: 330px" id="todo-list">
                                                <div class="table-responsive">
                                                    <table class="table table-centered w-100 dt-responsive " >
                                                        <thead class="thead-light">
                                                            <tr>

                                                                <th>Descrição</th>
                                                                <th >Ação</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="max-height: 25px;">
                                                        <?php $__currentLoopData = $subcategorias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <tr>
                                                                <td><?php echo e($sub->nome); ?></td>

                                                                <td style="width:20px">
                                                                    <form action="<?php echo e(route('subcategorias.destroy', $sub->id)); ?>" method="post">
                                                                        <?php echo csrf_field(); ?>
                                                                        <?php echo method_field('Delete'); ?>
                                                                        <button type="submit" class="btn-icon btn-light"> <i class="mdi mdi-delete"></i></button>
                                                                    </form>
                                                                </td>

                                                            </tr>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </ul>

                                            <form action="<?php echo e(route('subcategorias.store')); ?>" method="POST" class="needs-validation" novalidate>
                                                <?php echo csrf_field(); ?>

                                                <div class="row">
                                                    <div class="col">
                                                        <input type="text" id="todo-input-text" name="nome" class="form-control"
                                                             required>
                                                        <div class="invalid-feedback">
                                                            Digite o nome da subcategoria
                                                        </div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="btn-primary btn-md btn-block btn waves-effect waves-light" type="submit" id="todo-btn-submit">+</button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div> <!-- end .todoapp-->

                                    </div> <!-- end card-body -->
                                </div> <!-- end card-->
                            </div>
                        </div>
                    </div> <!-- end col-->
                </div>
                <!-- end row-->

            </div> <!-- end col -->
        </div>
            <!-- end row -->
    </div> <!-- container -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/dashboard/painel/resources/views/content/financeiros/cadastros/index.blade.php ENDPATH**/ ?>