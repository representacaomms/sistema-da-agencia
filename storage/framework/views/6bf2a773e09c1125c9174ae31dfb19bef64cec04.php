    
    <?php $__env->startSection('content'); ?>
    <!-- Start Content-->
    <div class="container-fluid">
        <input type="hidden" id="contas_a_receber_mes" value="<?php echo e($contas_a_receber_mes); ?>"/>
        <input type="hidden" id="contas_a_pagar_mes" value="<?php echo e($contas_a_pagar_mes); ?>"/>
        <input type="hidden" id="contas_a_receber_lista" value="<?php echo e($contas_a_receber_lista); ?>"/>
         <input type="hidden" id="contas_a_pagar_lista" value="<?php echo e($contas_a_pagar_lista); ?>"/>
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <form class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control form-control-light" id="dash-daterange">
                                    <div class="input-group-append">
                                        <span class="input-group-text bg-primary border-primary text-white">
                                            <i class="mdi mdi-calendar-range font-13"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript: void(0);" class="btn btn-primary ml-2">
                                <i class="mdi mdi-autorenew"></i>
                            </a>
                            <a href="javascript: void(0);" class="btn btn-primary ml-1">
                                <i class="mdi mdi-filter-variant"></i>
                            </a>
                        </form>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">

                <div class="row">
                    <div class="col-lg-3">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-right">
                                    <i class="mdi mdi-account-multiple widget-icon"></i>
                                </div>
                                <h5 class="text-muted font-weight-normal mt-0" title="Numero de Clientes">Prospectos<br>Todos</h5>
                                <h3 class="mt-3 mb-3"><?php echo e($dados['prospectos']); ?></h3>
                                <p class="mb-0 text-muted">
                                    <?php if($dados['prospectos']  > 0 ): ?>
                                        <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> <?php echo e($dados['prospectos_percentual']); ?>%</span> 
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php else: ?>
                                        <span class="text-danger mr-2"><i class="mdi mdi-arrow-down-bold"></i><?php echo e($dados['prospectos_percentual']); ?>%</span>
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php endif; ?>

                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->

                    <div class="col-lg-3">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-right">
                                    <i class="mdi mdi-account-multiple widget-icon"></i>
                                </div>
                                <h5 class="text-muted font-weight-normal mt-0" title="Numero de Clientes">Prospectos<br> Sem Interesse / Perdido</h5>
                                <h3 class="mt-3 mb-3"><?php echo e($dados['prospectos_sem_interesse']); ?></h3>
                                <p class="mb-0 text-muted">
                                    <?php if($dados['prospectos_sem_interesse']  > 0 ): ?>
                                        <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> <?php echo e($dados['prospectos_sem_interesse_percentual']); ?>%</span> 
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php else: ?>
                                        <span class="text-danger mr-2"><i class="mdi mdi-arrow-down-bold"></i><?php echo e($dados['prospectos_sem_interesse_percentual']); ?>%</span>
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php endif; ?>

                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->
              
                    <div class="col-lg-3">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-right">
                                    <i class="mdi mdi-account-multiple widget-icon"></i>
                                </div>
                                <h5 class="text-muted font-weight-normal mt-0" title="Numero de Clientes">Prospectos<br> Concretizado</h5>
                                <h3 class="mt-3 mb-3"><?php echo e($dados['prospectos_adquirido']); ?></h3>
                                <p class="mb-0 text-muted">
                                    <?php if($dados['prospectos_adquirido']  > 0 ): ?>
                                        <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> <?php echo e($dados['prospectos_adquirido_percentual']); ?>%</span> 
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php else: ?>
                                        <span class="text-danger mr-2"><i class="mdi mdi-arrow-down-bold"></i><?php echo e($dados['prospectos_adquirido_percentual']); ?>%</span>
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php endif; ?>

                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->

                    <div class="col-lg-3">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-right">
                                    <i class="mdi mdi-account-multiple widget-icon"></i>
                                </div>
                                <h5 class="text-muted font-weight-normal mt-0" title="Numero de Clientes">Prospectos<br> Em Andamento</h5>
                                <h3 class="mt-3 mb-3"><?php echo e($dados['prospectos_andamento']); ?></h3>
                                <p class="mb-0 text-muted">
                                    <?php if($dados['prospectos_andamento']  > 0 ): ?>
                                        <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> <?php echo e($dados['prospectos_andamento_percentual']); ?>%</span> 
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php else: ?>
                                        <span class="text-danger mr-2"><i class="mdi mdi-arrow-down-bold"></i><?php echo e($dados['prospectos_andamento_percentual']); ?>%</span>
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php endif; ?>

                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->
                </div> <!-- end row -->
                
                  <div class="row">
                    <div class="col-lg-3">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-right">
                                    <i class="mdi mdi-account-multiple widget-icon"></i>
                                </div>
                                <h5 class="text-muted font-weight-normal mt-0" title="Numero de Clientes">Propostas<br>Todos</h5>
                                <h3 class="mt-3 mb-3"><?php echo e($dados['propostas']); ?></h3>
                                <p class="mb-0 text-muted">
                                    <?php if($dados['propostas']  > 0 ): ?>
                                        <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> <?php echo e($dados['propostas_percentual']); ?>%</span> 
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php else: ?>
                                        <span class="text-danger mr-2"><i class="mdi mdi-arrow-down-bold"></i><?php echo e($dados['propostas_percentual']); ?>%</span>
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php endif; ?>

                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->

                    <div class="col-lg-3">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-right">
                                    <i class="mdi mdi-account-multiple widget-icon"></i>
                                </div>
                                <h5 class="text-muted font-weight-normal mt-0" title="Numero de Clientes">Propostas<br> Perdidas</h5>
                                <h3 class="mt-3 mb-3"><?php echo e($dados['prospectos_sem_interesse']); ?></h3>
                                <p class="mb-0 text-muted">
                                    <?php if($dados['prospectos_sem_interesse']  > 0 ): ?>
                                        <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> <?php echo e($dados['prospectos_sem_interesse_percentual']); ?>%</span> 
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php else: ?>
                                        <span class="text-danger mr-2"><i class="mdi mdi-arrow-down-bold"></i><?php echo e($dados['prospectos_sem_interesse_percentual']); ?>%</span>
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php endif; ?>

                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->
              
                    <div class="col-lg-3">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-right">
                                    <i class="mdi mdi-account-multiple widget-icon"></i>
                                </div>
                                <h5 class="text-muted font-weight-normal mt-0" title="Numero de Clientes">Propostas<br> Aceitas</h5>
                                <h3 class="mt-3 mb-3"><?php echo e($dados['prospectos_adquirido']); ?></h3>
                                <p class="mb-0 text-muted">
                                    <?php if($dados['prospectos_adquirido']  > 0 ): ?>
                                        <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> <?php echo e($dados['prospectos_adquirido_percentual']); ?>%</span> 
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php else: ?>
                                        <span class="text-danger mr-2"><i class="mdi mdi-arrow-down-bold"></i><?php echo e($dados['prospectos_adquirido_percentual']); ?>%</span>
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php endif; ?>

                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->

                    <div class="col-lg-3">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-right">
                                    <i class="mdi mdi-account-multiple widget-icon"></i>
                                </div>
                                <h5 class="text-muted font-weight-normal mt-0" title="Numero de Clientes">Propostas<br> Em Andamento</h5>
                                <h3 class="mt-3 mb-3"><?php echo e($dados['prospectos_andamento']); ?></h3>
                                <p class="mb-0 text-muted">
                                    <?php if($dados['prospectos_andamento']  > 0 ): ?>
                                        <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> <?php echo e($dados['prospectos_andamento_percentual']); ?>%</span> 
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php else: ?>
                                        <span class="text-danger mr-2"><i class="mdi mdi-arrow-down-bold"></i><?php echo e($dados['prospectos_andamento_percentual']); ?>%</span>
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php endif; ?>

                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->
                </div> <!-- end row -->

            </div> <!-- end col -->

           <!-- <div class="col-xl-7">
                <div class="card">
                    <div class="card-body">
                        <div class="dropdown float-right">
                            <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                                <i class="mdi mdi-dots-vertical"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- item-->
                               <!-- <a href="javascript:void(0);" class="dropdown-item">Relatório de vendas</a>
                                <!-- item-->
                               <!-- <a href="javascript:void(0);" class="dropdown-item">Exportar Relatório</a>
                                <!-- item-->
                               <!-- <a href="javascript:void(0);" class="dropdown-item">Lucro</a>
                            </div>
                        </div>
                        <h4 class="header-title mb-3">PROJEÇÕES VS ATUAIS</h4>

                        <div style="height: 263px;" class="chartjs-chart">
                            <canvas id="high-performing-product"></canvas>
                        </div>
                    </div> <!-- end card-body-->
               <!-- </div> <!-- end card-->

           <!-- </div> <!-- end col -->
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dropdown float-right">
                            <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                                <i class="mdi mdi-dots-vertical"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item" onclick="return listarPeriodos(event);">Período Anual</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item" onclick="return listarPeriodos(event);">Período Mensal</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item" onclick="return listarPeriodos(event);">Lucro</a>
                            </div>
                        </div>
                        <div class="anual d-none">
                            <h4 class="header-title mb-3">Financeiro Anual</h4>
                            <h5>Saldo: R$ <?php echo e(number_format($contas_a_receber - $contas_a_pagar, 2, ",", ".")); ?></h5>
    
                            <div class="chart-content-bg">
                                <div class="row text-center">
                                    
                                    <div class="col-md-6">
                                      
                                            <p class="text-muted mb-0 mt-3">Contas a Pagar</p>
                                            <h2 class="font-weight-normal mb-3">
                                            <small class="mdi mdi-checkbox-blank-circle text-danger align-middle mr-1"></small>
                                            <span>R$ <?php echo e(number_format($contas_a_pagar, 2, ",", ".")); ?></span>
                                            <span> 
                                                <a href="<?php echo e(route('despesas.index')); ?>" class="btn btn-outline-danger float-right">Ver Lançamentos
                                                    <i class="mdi mdi-arrow-right ml-2"></i>
                                                </a>
                                            </span>
                                        </h2>
                                      
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <p class="text-muted mb-0 mt-3">Contas a Receber</p>
                                        <h2 class="font-weight-normal mb-3">
                                            <small class="mdi mdi-checkbox-blank-circle text-success align-middle mr-1"></small>
                                            <span>R$ <?php echo e(number_format($contas_a_receber, 2, ",", ".")); ?></span>
                                            <span> 
                                                <a href="<?php echo e(route('receitas.index')); ?>" class="btn btn-outline-success float-right">Ver Lançamentos
                                                    <i class="mdi mdi-arrow-right ml-2"></i>
                                                </a>
                                            </span>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="mensal">
                            <h4 class="header-title mb-3">Financeiro - Mensal</h4>
                            <h5>Saldo: R$ <?php echo e(number_format($valor_contas_mes['receber'] - $valor_contas_mes['pagar'], 2, ',', '.')); ?></h5>
    
                            <div class="chart-content-bg">
                                <div class="row text-center">
                                    <div class="col-md-6">
                                        <p class="text-muted mb-0 mt-3">Contas a Pagar</p>
                                        <h2 class="font-weight-normal mb-3">
                                            <small class="mdi mdi-checkbox-blank-circle text-danger align-middle mr-1"></small>
                                            <span>R$ <?php echo e(number_format($valor_contas_mes['pagar'], 2, ',', '.')); ?></span>
                                            <span> 
                                                <a href="<?php echo e(route('despesas.index')); ?>" class="btn btn-outline-danger float-right">Ver Lançamentos
                                                    <i class="mdi mdi-arrow-right ml-2"></i>
                                                </a>
                                            </span>
                                        </h2>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="text-muted mb-0 mt-3">Contas a Receber</p>
                                        <h2 class="font-weight-normal mb-3">
                                            <small class="mdi mdi-checkbox-blank-circle text-success align-middle mr-1"></small>
                                            <span>R$ <?php echo e(number_format($valor_contas_mes['receber'], 2, ',', '.')); ?></span>
                                            <span> 
                                                <a href="<?php echo e(route('receitas.index')); ?>" class="btn btn-outline-success float-right">Ver Lançamentos
                                                    <i class="mdi mdi-arrow-right ml-2"></i>
                                                </a>
                                            </span>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="dash-item-overlay d-none d-md-block">
                            <!--<h5>Saldo: R$ <?php echo e(number_format($contas_a_receber - $contas_a_pagar, 2, ",", ".")); ?></h5> -->
                            <h5 id="saldo_mes"></h5>
                            
                            <p class="text-muted font-13 mb-3 mt-2">Saldo referente ao periodo total</p>
                            <a href="#" class="btn btn-outline-primary">Ver Lançamentos
                                <i class="mdi mdi-arrow-right ml-2"></i>
                            </a>
                        </div>

                       <!-- <div class="mt-3 chartjs-chart" style="height: 400px;">
                            <canvas id="revenue-chart"></canvas>
                        </div> -->
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->

            
        </div>
        <!-- end row -->


        
        <!-- end row -->

    </div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <script type="text/javascript">
    const meses = ['Jan', 'Fev', 'Mar','Abr','Mai','Jun', 'Jul','Ago', 'Set', 'Out', 'Nov', 'Dez'];
    let labels = []

    let contas_a_receber_lista = JSON.parse(document.getElementById('contas_a_receber_lista').value)
    let contas_a_receber_mes = JSON.parse(document.getElementById('contas_a_receber_mes').value)
    let valor_contas_a_receber_mes = []
    
    var valor_despesa_receita = []
    
    var mes = String(new Date().getMonth() + 1).padStart(2, '0');
    var contas_a_receber_mes_atual = 0;
    contas_a_receber_lista.forEach(dados => {
        if(new Date(dados.data_pagto).getMonth() + 1 == mes){
            contas_a_receber_lista += parseFloat(dados.valor_parcela);
        }
    })

    for (const key in contas_a_receber_mes) {
        if(contas_a_receber_mes[key][0]['mes'] != undefined){
            labels.push(contas_a_receber_mes[key][0]['mes']);
        }
        valor_contas_a_receber_mes.push(contas_a_receber_mes[key]['valor_total_mes']); 
        valor_despesa_receita.push(contas_a_receber_mes[key]['valor_total_mes']); 
    }
    

    let contas_a_pagar_lista = JSON.parse(document.getElementById('contas_a_pagar_lista').value)
    let contas_a_pagar_mes = JSON.parse(document.getElementById('contas_a_pagar_mes').value)
    let valor_contas_a_pagar_mes = []
    
    var contas_a_pagar_mes_atual = 0;
    
    contas_a_pagar_lista.forEach(dados => {
        if(new Date(dados.data_pagto).getMonth() + 1 == mes){
            contas_a_pagar_mes_atual += parseFloat(dados.valor_parcela);
        }
    })
    
    for (const key in contas_a_pagar_mes) {
        valor_contas_a_pagar_mes.push(contas_a_pagar_mes[key]['valor_total_mes']); 
        valor_despesa_receita.push(contas_a_pagar_mes[key]['valor_total_mes']); 
    }
    
    const data = {
    labels: labels,
    datasets: [
        {
            label: 'Contas a Pagar',
            data: valor_contas_a_pagar_mes,
            borderColor: '#F9C3D0',
            backgroundColor:'#F9C3D000 ',
        },
        {
            label: 'Contas a Receber',
            data: valor_contas_a_receber_mes,
            borderColor: '#0ACF97',
            backgroundColor: '#0ACF9700 ',
        }
    ]
    };

    function listarPeriodos(e){
        const anual = document.querySelectorAll('.anual');
        const mensal = document.querySelectorAll('.mensal');

       switch (e.srcElement.innerText) {
            case 'Período Anual':
                anual.forEach( a => a.classList.toggle('d-none'));
                mensal.forEach( a => a.classList.toggle('d-none'));
                break;
            case 'Período Mensal':
                anual.forEach( a => a.classList.toggle('d-none'));
                mensal.forEach( a => a.classList.toggle('d-none'));
        }
        
    }
    window.onload = function() {
        var ctx = document.getElementById("revenue-chart").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'line',
            data: data,
                options: {
                    responsive: true,
                    scales: {
                        y: {
                            suggestedMin: Math.min(...valor_despesa_receita),
                            suggestedMax: Math.max(...valor_despesa_receita),
                        }
                    }
                },
                
        });
    };
</script>

    <?php $__env->stopSection(); ?>
 



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/dashboard/painel/resources/views/content/dashboard_ecommerce.blade.php ENDPATH**/ ?>