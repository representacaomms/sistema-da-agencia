<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('consultorias.index')); ?>">Consultorias</a></li>
                            <li class="breadcrumb-item active">Editar Consultoria</li>
                        </ol>
                    </div>
                   

                </div>
            </div>
        </div>

        <form action="<?php echo e(route('consultorias.update', $record->id)); ?>" method="post">
            <?php echo csrf_field(); ?>
            <?php echo method_field('PUT'); ?>

            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
            
                            <h4 class="header-title mb-3"> Editar Consultoria</h4>
            
                                <div id="basicwizard">
            
                                    <ul class="nav nav-pills nav-justified form-wizard-header mb-4">
                                        <li class="nav-item">
                                            <a href="#basictab1" data-bs-toggle="tab" data-toggle="tab"  class="nav-link rounded-0 pt-2 pb-2"> 
                                                <i class="mdi mdi-account-circle me-1"></i>
                                                <span class="d-none d-sm-inline">Dados Gerais</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#basictab2" data-bs-toggle="tab" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                                <i class="mdi mdi-face-profile me-1"></i>
                                                <span class="d-none d-sm-inline">Dados Instagram</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#basictab3" data-bs-toggle="tab" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                                <i class="mdi mdi-checkbox-marked-circle-outline me-1"></i>
                                                <span class="d-none d-sm-inline">Dados Facebook</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#basictab4" data-bs-toggle="tab" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                                <i class="mdi mdi-checkbox-marked-circle-outline me-1"></i>
                                                <span class="d-none d-sm-inline">Dados Youtube</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#basictab5" data-bs-toggle="tab" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                                <i class="mdi mdi-checkbox-marked-circle-outline me-1"></i>
                                                <span class="d-none d-sm-inline">Dados Site</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#basictab6" data-bs-toggle="tab" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                                <i class="mdi mdi-checkbox-marked-circle-outline me-1"></i>
                                                <span class="d-none d-sm-inline">Dados GMN</span>
                                            </a>
                                        </li>
                                    </ul>
            
                                    <div class="tab-content b-0 mb-0">
                                        <div class="tab-pane" id="basictab1">
                                            <div class="row mt-1">
                                                <div class="col-md-4">
                                                    <div class="form-group" >
                                                        <label id="label_cpf">CNPJ</label>
                                                        <input type="text" data-toggle="input-mask" data-mask-format="00.000.000/0000-00" data-reverse="true"  name="cnpj" class="form-control " value="<?php echo e($record->cnpj); ?>" required>
                                                    </div>
                                                </div> <!-- end col-->
                                                <div class="col-md-8">
                                                    <div class="form-group" >
                                                        <label id="label_cpf">NOME</label>
                                                        <input type="text" name="nome" class="form-control " value="<?php echo e($record->nome); ?>" required>
                                                    </div>
                                                </div> <!-- end col-->
                                                <div class="col-md-4">
                                                    <div class="form-group" >
                                                        <label id="label_cpf">E-mail</label>
                                                        <input type="text" name="email" class="form-control " value="<?php echo e($record->email); ?>" required>
                                                    </div>
                                                </div> <!-- end col-->
                                                <div class="col-md-4">
                                                    <div class="form-group" >
                                                        <label id="label_cpf">Telefone</label>
                                                        <input type="text"  name="telefone" class="form-control " data-toggle="input-mask" data-mask-format="(00)0.0000-0000" value="<?php echo e($record->telefone); ?>" required>
                                                    </div>
                                                </div> <!-- end col-->
                                                <div class="col-md-4">
                                                    <div class="form-group" >
                                                        <label id="label_cpf">Status</label>
                                                        <select name="status"  class="form-control">
                                                            <option value="A">Ativo</option>
                                                            <option value="I">Inativo</option>
                                                        </select>
                                                    </div>
                                                </div> <!-- end col-->
                                            </div>
                                        </div>
            
                                        <div class="tab-pane" id="basictab2">
                                            <div class="row mt-1">

                                                 
                                                    <?php $__currentLoopData = $dados['instagram']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <div class="col-md-6">
                                                                <div class="form-group" >
                                                                    <label id="label_cpf"><?php echo e($item->descricao); ?></label>
                                                                        <input type="text" name="<?php echo e($item->input_name); ?>"  class="form-control " value="<?php echo e($item->acao); ?>">
                                                                
                                                                </div>
                                                            </div> <!-- end col-->
                                                        
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    
                                               
                        
                                            </div>
                                        </div>
            
                                        <div class="tab-pane" id="basictab3">
                                            <div class="row mt-1">
                                
                                                <?php $__currentLoopData = $dados['facebook']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="col-md-6">
                                                    <div class="form-group" >
                                                        <label id="label_cpf"><?php echo e($item->descricao); ?></label>
                                                            <input type="text" name="<?php echo e($item->input_name); ?>"  class="form-control " value="<?php echo e($item->acao); ?>">
                                                    
                                                    </div>
                                                </div> <!-- end col-->
                                            
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div> <!-- end row -->
                                        </div>

                                        <div class="tab-pane" id="basictab4">
                                            <div class="row mt-1">
                                
                                                <?php $__currentLoopData = $dados['youtube']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="col-md-6">
                                                    <div class="form-group" >
                                                        <label id="label_cpf"><?php echo e($item->descricao); ?></label>
                                                            <input type="text" name="<?php echo e($item->input_name); ?>"  class="form-control " value="<?php echo e($item->acao); ?>">
                                                    
                                                    </div>
                                                </div> <!-- end col-->
                                            
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div> <!-- end row -->
                                        </div>

                                        <div class="tab-pane" id="basictab5">
                                            <div class="row mt-1">
                                
                                                <?php $__currentLoopData = $dados['site']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="col-md-6">
                                                    <div class="form-group" >
                                                        <label id="label_cpf"><?php echo e($item->descricao); ?></label>
                                                            <input type="text" name="<?php echo e($item->input_name); ?>"  class="form-control " value="<?php echo e($item->acao); ?>">
                                                    
                                                    </div>
                                                </div> <!-- end col-->
                                            
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div> <!-- end row -->
                                        </div>

                                        <div class="tab-pane" id="basictab6">
                                            <div class="row mt-1">
                                
                                                <?php $__currentLoopData = $dados['gmn']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="col-md-6">
                                                    <div class="form-group" >
                                                        <label id="label_cpf"><?php echo e($item->descricao); ?></label>
                                                            <input type="text" name="<?php echo e($item->input_name); ?>"  class="form-control " value="<?php echo e($item->acao); ?>">
                                                    
                                                    </div>
                                                </div> <!-- end col-->
                                            
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div> <!-- end row -->
                                        </div>
            
                                       
                                    </div> <!-- tab-content -->
                                </div> <!-- end #basicwizard-->
            
                        </div> <!-- end card-body -->
                    </div> <!-- end card-->
                </div> <!-- end col -->
            </div>
           
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="<?php echo e(URL::previous()); ?>">
                                                        Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2">
                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit">
                                                            Editar<span class="badge badge-primary"></span>
                                                    </button>
                                                </div>
                                            </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>
        </form>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
    .d-none{
        display: none;
    }
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        var oldValue = $('select[name="plataforma"]').val();
        $('select[name="plataforma"]').change( function() {
           
            switch ($('select[name="plataforma"]').val()) {

                case 'instagram':
                    addClass('instagram');
                    oldValue = 'instagram';              
                    break;
                case 'facebook':
                    addClass('facebook') ;
                    oldValue = 'facebook';         
                    break;
                case 'youtube':
                    addClass('youtube')  
                    oldValue = 'youtube';                
                    break;
                case 'site':
                    addClass('site')     
                    oldValue = 'site';              
                    break;
                case 'gmn':
                    addClass('gmn')   
                    oldValue = 'gmn';                 
                    break;
            }
            
        })

        function addClass(params) {
            $('#' + params).removeClass('d-none');
            $('#' + oldValue).addClass('d-none') 
        }
       
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/consultorias/edit.blade.php ENDPATH**/ ?>