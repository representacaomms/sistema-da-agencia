<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('custos.index')); ?>">Custo</a></li>
                            <li class="breadcrumb-item active">Adicionar Custo</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Custo</h4>
                </div>
            </div>
        </div>
       <div class="card">

           <div class="card-body">
               <form class="p-2" method="POST" action="<?php echo e(route('custos.store')); ?>">
                <?php echo csrf_field(); ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Descrição </label>
                            <input type="text" name="nome" id="nome" class="form-control">
                        </div>
                    </div>
                     <div class="col-md-2">
                        <div class="form-group">
                            <label>Tipo</label>
                            <select class="form-control form-control-light" name="tipo" readonly>
                                <option value="insumos">Insumos</option>
                                <option value="operacional">Operácional</option>
                                <option value="licencas">Licenças</option>
                                <option value="budget">Budget</option>
                                <option value="equipe">Equipe</option>
                                <option value="fixos" selected>Fixos</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Valor</label>
                            <input type="text" class="form-control form-control-light" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true"  name="valor" required>
                        </div>
                    </div>
                </div>

                <div class="text-right mt-5">
                    <a href="<?php echo e(URL::previous()); ?>" class="btn btn-light" data-dismiss="modal">Sair</a>
                    <button type="submit" class="btn btn-primary">Adicionar</button>
                </div>
            </form>
           </div>
       </div>
    </div> <!-- container -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/dashboard/painel/resources/views/content/financeiros/custos/create.blade.php ENDPATH**/ ?>