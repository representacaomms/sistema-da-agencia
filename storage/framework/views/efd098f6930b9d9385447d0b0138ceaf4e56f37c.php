<?php $__env->startSection('content'); ?>
<!-- Start Content-->
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Consultorias</a></li>
                        <li class="breadcrumb-item active">Listar Consultorias</li>
                    </ol>
                </div>
                <h4 class="page-title">Listar Consultorias</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="<?php echo e(route('consultorias.create')); ?>" class="btn btn-danger mb-2"><i
                                    class="mdi mdi-plus-circle mr-2"></i> Adicionar Consultoria</a>
                        </div>
                        <div class="col-sm-8">
                            <div class="text-sm-right">
                                        <a href="<?php echo e(route('consultorias.consultoria')); ?>" class="btn btn-light mb-2"><i
                                            class="mdi mdi-book-outline mr-2"></i> Montar Consultoria</a>
                            </div>
                        </div><!-- end col-->
                    </div>

                  

                    <div class="table-responsive">
                        <?php if($message = Session::get('success')): ?>
                            <?php if($message == 'store'): ?>
                                <script>
                                    var message = 'store';
                                </script>
                            <?php endif; ?>
                            <?php if($message == 'update'): ?>
                                <script>
                                    var message = 'update';
                                </script>
                            <?php endif; ?>

                            <?php if($message == 'destroy'): ?>
                                <script>
                                    var message = 'destroy';
                                </script>
                            <?php endif; ?>

                            <?php if($message == 'active'): ?>
                                <script>
                                    var message = 'active';
                                </script>
                            <?php endif; ?>
                            
                        <?php endif; ?>
                    </div>

                   
                </div> <!-- end card-body-->
            </div> <!-- end card-->
          

        </div> <!-- end col -->
    </div>

    <div class="row">
        <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-md-6 col-xxl-3">
            <div class="card">
                <div class="card-body">
                    <div class="dropdown float-right">
                        <a href="#" class="dropdown-toggle arrow-none card-drop" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="mdi mdi-dots-horizontal"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end">
                            <!-- item-->
                            <a href="<?php echo e(route('consultorias.edit', $item->id)); ?>" class="dropdown-item">Editar Cliente</a>
                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item">Project Info</a>
                        </div>
                    </div>

                    <div class="text-center">
                        <img src="https://agenciamms.com/wp-content/uploads/2021/11/cropped-Favicon-192x192.jpeg" class="rounded-circle avatar-md img-thumbnail" alt="friend">
                        <h4 class="mt-3 my-1"><?php echo e($item->nome); ?> </i></h4>
                        <p class="mb-0 text-muted"><i class="mdi mdi-email-outline me-1"></i><?php echo e($item->email); ?></p>
                        <hr class="bg-dark-lighten my-3">
                        <h5 class="mt-3 fw-semibold text-muted"><b>CNPJ:</b> <?php echo e($item->cnpj); ?></h5>
                    
                        <div class="row mt-3">
                            <div class="col-4">
                                <a href="javascript:void(0);" class="btn w-100 btn-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Instagram">Instagram</a>
                            </div>
                            <div class="col-4">
                                <a href="javascript:void(0);" class="btn w-100 btn-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Facebook">Facebook</a>
                            </div>
                            <div class="col-4">
                                <a href="javascript:void(0);" class="btn w-100 btn-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Youtube">Youtube</a>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-4">
                                <a href="javascript:void(0);" class="btn w-100 btn-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Instagram">Site</a>
                            </div>
                            <div class="col-4">
                                <a href="javascript:void(0);" class="btn w-100 btn-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Facebook">GMN</a>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End col -->
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <!-- end row-->
</div> <!-- container -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<!-- DataTables -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script>
    $(document).ready(function () {
       
        $('#consultorias-datatable').DataTable({
            language: {
                processing: "Processamento...",
                search: "Pesquisar&nbsp;:",
                lengthMenu: "Mostrar _MENU_ entradas",
                info: "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                infoEmpty: "Exibindo da entrada 0 até 0 de 0 entradas",
                infoFiltered: "(filtro total de _MAX_ entradas)",
                infoPostFix: "",
                loadingRecords: "Carregando ...",
                zeroRecords: "Nenhuma entrada encontrada",
                emptyTable: "Nenhuma entrada disponível na tabela",
                paginate: {
                    first: "Primeiro",
                    previous: "Anterior",
                    next: "Ultimo",
                    last: "Depos"
                },
                aria: {
                    sortAscending: ": ativar para classificar a coluna em ordem crescente",
                    sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                }
            }
        });
    
    });
    switch (message) {
        case 'store':
            Swal.fire(
                'Parabéns!',
                'Registro cadastrado com sucesso!',
                'success'
            )
            break;
            case 'update':
            Swal.fire(
                'Parabéns!',
                'Registro editado com sucesso!',
                'success'
            )
            break;
            case 'destroy':
            Swal.fire(
                'Alerta!',
                'Registro inativado com sucesso!',
                'info'
            )
            break;
            case 'active':
            Swal.fire(
                'Alerta!',
                'Registro ativado com sucesso!',
                'info'
            )
            break;
    
        default:
            break;
    }

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/consultorias/index.blade.php ENDPATH**/ ?>