<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('clientes.index')); ?>">Usuários</a></li>
                            <li class="breadcrumb-item active">Editar Usuário</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Usuário</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="<?php echo e(route('usuarios.update', $usuario->id)); ?>" method="POST" novalidate>
            <?php echo method_field('PUT'); ?>
            <?php echo csrf_field(); ?>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectname">Nome</label>
                                <input type="text"  name="name" class="form-control"  style="text-transform: capitalize;" placeholder="Nome Completo" value="<?php echo e(old('name') ?? $usuario->name); ?>" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label id="label_cpf">Email</label>
                                <input type="email"  name="email" class="form-control" placeholder="Insira o email válido" value="<?php echo e(old('email') ?? $usuario->email); ?>" required >
                            </div>
                        </div>
                        <div class="col-md-3" >
                            <div class="form-group" >
                                <div class="form-group">
                                    <label id="label_cpf">Senha</label>
                                    <input type="password" name="password" class="form-control" placeholder="Insira a senha" value="<?php echo e(old('senha')); ?>"  >
                                </div>
                            </div>
                        </div> <!-- end col-->
                        

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="<?php echo e(URL::previous()); ?>">
                                                        Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2 ml-3">
                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit">
                                                            Editar<span class="badge badge-primary"></span>
                                                    </button>
                                                </div>
                                            </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>

        </form>
    </div> <!-- container -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        $('select[name="tipo"]').change( function() {
            if($('select[name="tipo"]').val() === 'pf'){
                $('.juridica').addClass('d-none');
                $('.fisica').removeClass('d-none');
            } else{
                $('.fisica').addClass('d-none');
                $('.juridica').removeClass('d-none');
            }
        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/usuarios/edit.blade.php ENDPATH**/ ?>