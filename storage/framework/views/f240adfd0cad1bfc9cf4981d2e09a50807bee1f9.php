<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('funcoes.index')); ?>">Funções</a></li>
                            <li class="breadcrumb-item active">Adicionar Permissão</li>
                        </ol>
                    </div>
                    <h3 class="page-title">Adicionar Permissão</h3>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="<?php echo e(route('usuarios.funcoesSync', $usuario->id)); ?>" method="POST" novalidate>
            <?php echo method_field('PUT'); ?>
            <?php echo csrf_field(); ?>
             <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Funcão de: <?php echo e($usuario->name); ?></h4>
                            <hr class="mb-4">
                            <?php $__currentLoopData = $funcoes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $funcao): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="all mt-1" style="font-size:16px;">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="<?php echo e($funcao->id); ?>" id="<?php echo e($funcao->id); ?>" <?php echo e(($funcao->can == 1 ? 'checked' : '')); ?>>
                                    <label class="custom-control-label" for="<?php echo e($funcao->id); ?>">#<?php echo e($funcao->id); ?> - <?php echo e($funcao->name); ?></label>
                                </div>
                            </div>
                            
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <hr>
                            <div class="row mt-5">
                                <div class="col-xl-1">
                                    <div class="form-group">
                                        <a class="btn btn-danger" href="<?php echo e(URL::previous()); ?>">
                                                Cancelar<span class="badge badge-primary"></span>
                                        </a>
                                    </div>
                                </div> <!-- end col-->
                                <div class="col-xl-1">
                                        <div class="form-group">
                                            <button class="btn btn-primary" type="submit">
                                                    Sincronizar<span class="badge badge-primary"></span>
                                            </button>
                                        </div>
                                    </div> <!-- end col-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div> <!-- container -->

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/Sistema Agencia/Web/resources/views/content/usuarios/funcoes.blade.php ENDPATH**/ ?>