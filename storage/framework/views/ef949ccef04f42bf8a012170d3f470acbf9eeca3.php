
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8" />
    <title>Painel | Agenciamms - Sistema de Gestão</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Soluções me Tecnologia para empresas." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo e(asset('/images/favicon.ico')); ?>">

    <!-- third party css -->
    <link href="<?php echo e(asset('/css/vendor/jquery-jvectormap-1.2.2.css')); ?>" rel="stylesheet" type="text/css" />    
    <link href="<?php echo e(asset('/css/vendor/fullcalendar.min.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- third party css end -->


    <!-- App css -->
    <link href="<?php echo e(asset('/css/icons.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/app.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/gestao.css')); ?>" rel="stylesheet" type="text/css" />

    <!-- third party css -->
    <link href="<?php echo e(asset('css/vendor/dataTables.bootstrap4.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('css/vendor/responsive.bootstrap4.css')); ?>" rel="stylesheet" type="text/css" />

    <!-- SELECT 2 -->
    <link href="<?php echo e(asset('css/vendor/select.bootstrap4.css')); ?>" rel="stylesheet" type="text/css" />
    <?php echo $__env->yieldContent('css'); ?>
</head>

 <body>

    <!-- Begin page -->
     <div class="wrapper">

        <?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">
                <?php echo $__env->make('layouts.topbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <!-- Start Content-->
               <?php echo $__env->yieldContent('content'); ?>
                <!-- container -->
            </div>
            <!-- content -->
            <!-- Footer Start -->
           <?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <!-- end Footer -->

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->

   

    <?php echo $__env->make('layouts.right_bar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="rightbar-overlay"></div>
    <!-- /Right-bar -->


    <!-- bundle -->
    <script src="<?php echo e(asset('js/vendor.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/app.min.js')); ?>"></script>
   
    <!-- third party js -->
    <script src="<?php echo e(asset('js/vendor/Chart.bundle.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/vendor/jquery-jvectormap-1.2.2.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/vendor/jquery-jvectormap-world-mill-en.js')); ?>"></script>
    <!-- third party js ends -->
    <!-- third party js -->
    <script src="<?php echo e(asset('js/vendor/jquery.dataTables.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/vendor/dataTables.bootstrap4.js')); ?>"></script>
    <script src="<?php echo e(asset('js/vendor/dataTables.responsive.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/vendor/responsive.bootstrap4.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/vendor/dataTables.checkboxes.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/vendor/fullcalendar.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/vendor/jquery-ui.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/vendor/dragula.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/ui/component.dragula.js')); ?>"></script>
    <!-- third party js ends -->

    <!-- Datatables js -->
    <script src="<?php echo e(asset('js/vendor/jquery.dataTables.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/vendor/dataTables.bootstrap4.js')); ?>"></script>
    <script src="<?php echo e(asset('js/vendor/dataTables.responsive.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/vendor/responsive.bootstrap4.min.js')); ?>"></script>  
    
    
    <!-- Typehead -->
    <script src="<?php echo e(asset('js/vendor/handlebars.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/vendor/typeahead.bundle.min.js')); ?>"></script>

    <!-- Scripst -->
    <script src="<?php echo e(asset('js/scripts.js')); ?>"></script>
    <script>
        $('.dropdown-toggle').dropdown();
   </script>
    <?php echo $__env->yieldContent('js'); ?>
</body>
</html>
<?php /**PATH /Users/mac-marcelo/projetos/Sistema Agencia/Web/resources/views/layouts/app.blade.php ENDPATH**/ ?>