    
    <?php $__env->startSection('content'); ?>
    <!-- Start Content-->
    <div class="container-fluid">
        <input type="hidden" id="contas_a_receber_mes" value="<?php echo e($contas_a_receber_mes); ?>"/>
        <input type="hidden" id="contas_a_pagar_mes" value="<?php echo e($contas_a_pagar_mes); ?>"/>
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <form class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control form-control-light" id="dash-daterange">
                                    <div class="input-group-append">
                                        <span class="input-group-text bg-primary border-primary text-white">
                                            <i class="mdi mdi-calendar-range font-13"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript: void(0);" class="btn btn-primary ml-2">
                                <i class="mdi mdi-autorenew"></i>
                            </a>
                            <a href="javascript: void(0);" class="btn btn-primary ml-1">
                                <i class="mdi mdi-filter-variant"></i>
                            </a>
                        </form>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-5">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-right">
                                    <i class="mdi mdi-account-multiple widget-icon"></i>
                                </div>
                                <h5 class="text-muted font-weight-normal mt-0" title="Numero de Clientes">Clientes</h5>
                                <h3 class="mt-3 mb-3"><?php echo e($dados['clientes']); ?></h3>
                                <p class="mb-0 text-muted">
                                    <?php if($dados['clientes_percentual']  > 0 ): ?>
                                        <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> <?php echo e($dados['clientes_percentual']); ?>%</span>
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php else: ?>
                                        <span class="text-danger mr-2"><i class="mdi mdi-arrow-down-bold"></i><?php echo e($dados['clientes_percentual']); ?>%</span>
                                        <span class="text-nowrap">Desde o último mês</span>
                                    <?php endif; ?>

                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->

                    <div class="col-lg-6">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-right">
                                    <i class="mdi mdi-cart-plus widget-icon"></i>
                                </div>
                                <h5 class="text-muted font-weight-normal mt-0" title="Number of Orders">Propostas</h5>
                                
                            </div> <!-- end card-body-->

                        </div> <!-- end card-->
                    </div> <!-- end col-->
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-right">
                                    <i class="mdi mdi-currency-usd widget-icon"></i>
                                </div>
                                <h5 class="text-muted font-weight-normal mt-0" title="Number of Orders">Vendas</h5>
                                
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->

                    <div class="col-lg-6">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-right">
                                    <i class="mdi mdi-pulse widget-icon"></i>
                                </div>
                                <h5 class="text-muted font-weight-normal mt-0" title="Number of Orders">Proposta Reprovada</h5>
                                
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->
                </div> <!-- end row -->

            </div> <!-- end col -->

            <div class="col-xl-7">
                <div class="card">
                    <div class="card-body">
                        <div class="dropdown float-right">
                            <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                                <i class="mdi mdi-dots-vertical"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Relatório de vendas</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Exportar Relatório</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Lucro</a>
                            </div>
                        </div>
                        <h4 class="header-title mb-3">PROJEÇÕES VS ATUAIS</h4>

                        <div style="height: 263px;" class="chartjs-chart">
                            <canvas id="high-performing-product"></canvas>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->

            </div> <!-- end col -->
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dropdown float-right">
                            <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                                <i class="mdi mdi-dots-vertical"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Relatório de Vendas</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Exportar Relatório</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Lucro</a>
                            </div>
                        </div>
                        <h4 class="header-title mb-3">Financeiro</h4>

                        <div class="chart-content-bg">
                            <div class="row text-center">
                                <div class="col-md-6">
                                    <p class="text-muted mb-0 mt-3">Contas a Pagar</p>
                                    <h2 class="font-weight-normal mb-3">
                                        <small class="mdi mdi-checkbox-blank-circle text-danger align-middle mr-1"></small>
                                        <span>R$ <?php echo e(number_format($contas_a_pagar, 2, ",", ".")); ?></span>
                                    </h2>
                                </div>
                                <div class="col-md-6">
                                    <p class="text-muted mb-0 mt-3">Contas a Receber</p>
                                    <h2 class="font-weight-normal mb-3">
                                        <small class="mdi mdi-checkbox-blank-circle text-success align-middle mr-1"></small>
                                        <span>R$ <?php echo e(number_format($contas_a_receber, 2, ",", ".")); ?></span>
                                    </h2>
                                </div>
                            </div>
                        </div>

                        <div class="dash-item-overlay d-none d-md-block">
                            <h5>Saldo: R$ <?php echo e(number_format($contas_a_receber - $contas_a_pagar, 2, ",", ".")); ?></h5>
                            <p class="text-muted font-13 mb-3 mt-2">Saldo referente ao periodo total</p>
                            <a href="#" class="btn btn-outline-primary">Ver Lançamentos
                                <i class="mdi mdi-arrow-right ml-2"></i>
                            </a>
                        </div>

                        <div class="mt-3 chartjs-chart" style="height: 500px;">
                            <canvas id="revenue-chart"></canvas>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->

            
        </div>
        <!-- end row -->


        
        <!-- end row -->

    </div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
<script type="text/javascript">
    const meses = ['Jan', 'Fev', 'Mar','Abr','Mai','Jun', 'Jul','Ago', 'Set', 'Out', 'Nov', 'Dez'];
    let labels = []

    let contas_a_receber_mes = JSON.parse(document.getElementById('contas_a_receber_mes').value)
    let valor_contas_a_receber_mes = []
    for (const key in contas_a_receber_mes) {
        if(contas_a_receber_mes[key][0]['mes'] != undefined){
            labels.push(contas_a_receber_mes[key][0]['mes']);
        }
        valor_contas_a_receber_mes.push(contas_a_receber_mes[key]['valor_total_mes']); 
    }

    let contas_a_pagar_mes = JSON.parse(document.getElementById('contas_a_pagar_mes').value)
    let valor_contas_a_pagar_mes = []
    
    for (const key in contas_a_pagar_mes) {
        valor_contas_a_pagar_mes.push(contas_a_pagar_mes[key]['valor_total_mes']); 
    }
   
    const data = {
    labels: labels,
    datasets: [
        {
            label: 'Contas a Pagar',
            data: valor_contas_a_pagar_mes,
            borderColor: '#F9C3D0',
            backgroundColor:'#F9C3D000 ',
        },
        {
            label: 'Contas a Receber',
            data: valor_contas_a_receber_mes,
            borderColor: '#0ACF97',
            backgroundColor: '#0ACF9700 ',
        }
    ]
    };

    window.onload = function() {
        var ctx = document.getElementById("revenue-chart").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'line',
            data: data,
                 options: {
                    responsive: true,
                  },
        });
    };
</script>

    <?php $__env->stopSection(); ?>
 



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/Sistema Agencia/Web/resources/views/content/dashboard_ecommerce.blade.php ENDPATH**/ ?>