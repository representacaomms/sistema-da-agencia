<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('fornecedores.index')); ?>">Fornecedores</a></li>
                            <li class="breadcrumb-item active">Editar Fornecedor</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Fornecedor</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="<?php echo e(route('fornecedores.update', $fornecedore->id)); ?>" method="POST">           
            <?php echo method_field('PUT'); ?>
            <?php echo csrf_field(); ?>
            <input type="hidden" name="id" value="<?php echo e($fornecedore->id); ?>">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="projectname">Tipo</label>
                                <select name="tipo" id="tipo" class="form-control">  
                                    <option value="pj" <?php echo e((old('tipo') == 'pj'  ? 'selected' : ($fornecedore->tipo == 'pj' ? 'selected' : '') )); ?>>Pessoa Juridica</option>                              
                                </select>
                                
                            </div>
                        </div>

                        <div class="col-md-4 fisica ">
                            <div class="form-group" >
                                <label id="label_cpf">CNPJ</label>
                                <input type="text" data-toggle="input-mask" data-mask-format="00.000.000/000-00" data-reverse="true"  name="cpf" class="form-control "  placeholder="Documento CPF" value="<?php echo e(old('cpf') ?? $fornecedore->cpf); ?>" >
                            </div>
                        </div> <!-- end col--> 
                        <div class="col-md-4 fisica ">
                            <div class="form-group" >
                                <label id="label_rg">Insc Est</label>                            
                                <input type="text" data-toggle="input-mask" data-mask-format="00.000.000-a" data-reverse="true"  name="rg" class="form-control" placeholder="Documento RG" value="<?php echo e(old('rg') ?? $fornecedore->rg); ?>" >
                                            
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-2">
                            <div class="form-group" >
                                <label id="label_cpf">Status</label>
                                <select name="status"  class="form-control">                                
                                    <option value="A" <?php echo e((old('tipo') == 'A'  ? 'selected' : ($fornecedore->status == 'A' ? 'selected' : '') )); ?>>Ativo</option> 
                                    <option value="I" <?php echo e((old('tipo') == 'I'  ? 'selected' : ($fornecedore->status == 'I' ? 'selected' : '') )); ?>>Inativo</option>                               
                               
                                </select>              
                            </div>
                        </div> <!-- end col-->
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectname">Nome</label>
                                <input type="text"  name="nome" class="form-control"  style="text-transform: capitalize;" placeholder="Nome Completo" value="<?php echo e(old('nome') ?? $fornecedore->nome); ?>">
                            </div>                            
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectname">Fantasia</label>
                                <input type="text"  name="fantasia" class="form-control"  style="text-transform: capitalize;" placeholder="Nome Completo" value="<?php echo e(old('fantasia') ?? $fornecedore->fantasia); ?>">
                            </div>                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="label_cpf">Email</label>
                                <input type="email"  name="email" class="form-control" value="<?php echo e(old('email') ?? $fornecedore->email); ?>" >  
                            </div>                            
                        </div>
                        <div class="col-md-4" >
                            <div class="form-group" >
                                <div class="form-group">
                                    <label id="label_cpf">Celular</label>
                                    <input type="text" data-toggle="input-mask" data-mask-format="(00) 00000-0000" id="celular" name="celular" class="form-control"  value="<?php echo e(old('celular') ?? $fornecedore->celular); ?>">
                                </div>                                            
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-4" >
                            <div class="form-group" >
                                <div class="form-group">
                                    <label id="label_cpf">Telefone</label>
                                    <input type="text" data-toggle="input-mask" data-mask-format="(00) 0000-0000" id="telefone" name="telefone" class="form-control"  value="<?php echo e(old('telefone') ?? $fornecedore->telefone); ?>">
                                </div>                                            
                            </div>
                        </div> <!-- end col-->
                        
                    </div>
                </div>
            </div> 
           
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Endereço</h5>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-xl-3">
                                        <div class="form-group">
                                            <label for="projectname">CEP</label>
                                            <input type="text" data-toggle="input-mask" data-mask-format="00.000-000" name="cep" class="form-control zip_code_search" placeholder="Digite seu CEP" value="<?php echo e(old('cep') ?? $fornecedore->endereco->cep); ?>"> 
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-5">
                                        <div class="form-group">
                                            <label for="projectname">Logradouro</label>
                                            <input type="text" name="logradouro" class="form-control logradouro" value="<?php echo e(old('logradouro') ?? $fornecedore->endereco->logradouro); ?>"  >
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-4">
                                        <div class="form-group">
                                            <label for="projectname">Complemento</label>
                                            <input type="text" name="complemento" class="form-control complemento" style="text-transform: capitalize;" value="<?php echo e(old('complemento') ?? $fornecedore->endereco->complemento); ?>">
                                        </div>
                                        
                                    </div> <!-- end col-->
                                </div>
                
                                <div class="row">
                                    <div class="col-xl-2">
                                        <div class="form-group">
                                            <label for="projectname">Numero</label>
                                            <input type="text"  name="numero" class="form-control numero" value="<?php echo e(old('numero') ?? $fornecedore->endereco->numero); ?>">
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-5">
                                        <div class="form-group">
                                            <label for="projectname">Bairro</label>
                                            <input type="text"  name="bairro" class="form-control bairro" value="<?php echo e(old('bairro') ?? $fornecedore->endereco->bairro); ?>" >
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-3">
                                        <div class="form-group">
                                            <label for="projectname">Cidade</label>
                                            <input type="text" name="localidade" class="form-control localidade" value="<?php echo e(old('localidade') ?? $fornecedore->endereco->localidade); ?>"  >
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-2">
                                        <div class="form-group">
                                            <label for="projectname">UF</label>
                                            <input type="text"  name="uf" class="form-control uf"  value="<?php echo e(old('uf') ?? $fornecedore->endereco->uf); ?>"  >
                                        </div>
                                    </div> <!-- end col-->
                                </div>
                            </div> <!-- end col-->
                        </div> <!-- end card-body -->
                    </div> <!-- end card-->
                </div> <!-- end col-->
               
            </div> 
            
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="<?php echo e(URL::previous()); ?>">
                                                        Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2">
                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit">
                                                        Atualizar<span class="badge badge-primary"></span>
                                                    </button>
                                                </div>
                                            </div> <!-- end col-->                                       
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>                            
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>
          
        </form>
    </div> <!-- container -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        $('select[name="tipo"]').change( function() {
            if($('select[name="tipo"]').val() === 'pf'){
                $('.juridica').addClass('d-none');     
                $('.fisica').removeClass('d-none');     
            } else{
                $('.fisica').addClass('d-none');     
                $('.juridica').removeClass('d-none'); 
            }
        })
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/Sistema Agencia/Web/resources/views/content/fornecedores/edit_pj.blade.php ENDPATH**/ ?>