<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Produtos</a></li>
                            <li class="breadcrumb-item active">Listar Produtos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Listar Produtos</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                                <a href="<?php echo e(route('produtos.create')); ?>" class="btn btn-danger mb-2"><i
                                        class="mdi mdi-plus-circle mr-2"></i> Adicionar Produtos</a>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-success mb-2 mr-1"><i
                                            class="mdi mdi-settings"></i></button>
                                    
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            <?php if($message = Session::get('success')): ?>
                                <?php if($message == 'store'): ?>
                                    <script>
                                        var message = 'store';

                                    </script>
                                <?php endif; ?>
                                <?php if($message == 'update'): ?>
                                    <script>
                                        var message = 'update';

                                    </script>
                                <?php endif; ?>

                                <?php if($message == 'destroy'): ?>
                                    <script>
                                        var message = 'destroy';

                                    </script>
                                <?php endif; ?>

                            <?php endif; ?>
                            <table class="table table-centered w-100 dt-responsive " id="products-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th class="all">ID</th>
                                        <th>Nome</th>
                                        <th>Caracteristicas</th>
                                        <th>Preço Venda</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $produtos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16">
                                                    <a href="<?php echo e(route('produtos.edit', $item->id)); ?>"
                                                        class="text-body"><?php echo e($item->id); ?> </a>
                                                </p>
                                            </td>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-12">
                                                    <a href="<?php echo e(route('produtos.edit', $item->id)); ?>"
                                                        class="text-body"><?php echo e($item->nome); ?></a>
                                                </p>

                                            </td>

                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-12">
                                                    <a href="<?php echo e(route('produtos.edit', $item->id)); ?>"
                                                        class="text-body"><?php echo e($item->caracteristicas); ?></a>
                                                </p>

                                            </td>
                                            
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16">
                                                    <a href="<?php echo e(route('produtos.edit', $item->id)); ?>"
                                                        class="text-body">R$: <?php echo e(number_format($item->valor_total, 2)); ?>

                                                    </a>
                                                </p>
                                            </td>
                                            <td>
                                                <?php if($item->status == 'Ativo'): ?>
                                                    <span class="badge badge-success">Ativo</span>
                                                <?php else: ?>
                                                    <span class="badge badge-danger">Inativo</span>
                                                <?php endif; ?>
                                            </td>

                                        </tr>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div> <!-- container -->


<?php $__env->stopSection(); ?>


<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
<?php $__env->stopSection(); ?>


<?php $__env->startSection('js'); ?>

    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>




    <script>
        $(document).ready(function() {
            var a = $('#products-datatable').DataTable({

                dom: 'Bfrtip',
                buttons: [{
                        text: 'Imprimir',
                        extend: 'print',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        text: 'Selecionar',
                        extend: 'colvis',
                        columns: ':gt(0)'
                    },
                ],


                language: {
                    processing: "Processamento...",
                    search: "Pesquisar&nbsp;:",
                    lengthMenu: "Mostrar _MENU_ entradas",
                    info: "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty: "Exibinod da entrada 0 até 0 de 0 entradas",
                    infoFiltered: "(filtro total de _MAX_ entradas)",
                    infoPostFix: "",
                    loadingRecords: "Carregando ...",
                    zeroRecords: "Nenhuma entrada encontrada",
                    emptyTable: "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first: "Primeiro",
                        previous: "Anterior",
                        next: "Ultimo",
                        last: "Depos"
                    },
                    aria: {
                        sortAscending: ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        });

    </script>
    <script type="text/javascript">
        if (message == 'store') {
            Swal.fire(
                'Parabéns!',
                'Registro cadastrado com sucesso!',
                'success'
            )
        }
        if (message == 'update') {
            Swal.fire(
                'Parabéns!',
                'Registro editado com sucesso!',
                'success'
            )
        }

        if (message == 'destroy') {
            Swal.fire(
                'Parabéns!',
                'Registro excluido com sucesso!',
                'success'
            )
        }

    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/produtos/index.blade.php ENDPATH**/ ?>