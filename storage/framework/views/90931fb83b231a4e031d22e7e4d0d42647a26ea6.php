<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('servicos-agenciado.index')); ?>">Serviços
                                    Agenciado</a></li>
                            <li class="breadcrumb-item active">Editar Serviços Agenciado</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Serviços Agenciado </h4>
                </div>
            </div>
        </div>
        <div class="card">

            <div class="card-body">
                <form class="p-2" method="POST"
                    action="<?php echo e(route('servicos-agenciado.update', $servicos_agenciado->id)); ?>">
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('PUT'); ?>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nome</label>

                                <input type="text" name="descricao" id="descricao"
                                    value="<?php echo e($servicos_agenciado->descricao); ?>" class="form-control" required>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="task-priority2">Função</label>
                                <select name="base" id="base" class="form-control" required>
                                    <?php $__currentLoopData = $salarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->salario + $item->total_provisoes); ?>"
                                            <?php echo e($servicos_agenciado->base === $item->salario + $item->total_provisoes ? 'selected' : ''); ?>>
                                            <?php echo e($item->nome); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="task-priority2">Esforço Hrs</label>
                                <input type="text" name="esforco_hrs" id="esforco_hrs" class="form-control"
                                    value=<?php echo e($servicos_agenciado->esforco_hrs); ?> required>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="task-priority2">Esforço R$</label>
                                <input type="text" name="esforco_valor" id="esforco_valor" class="form-control"
                                    value=<?php echo e($servicos_agenciado->esforco_valor); ?> required>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="task-priority2">Individual</label>
                                <input type="text" name="valor_individual" id="valor_individual" class="form-control"
                                    value=<?php echo e($servicos_agenciado->valor_individual); ?> required>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="task-priority2">Status</label>
                                <select name="situacao" id="situacao" class="form-control" required>
                                    <option value="ativo">Ativo</option>
                                    <option value="inativo">Inativo</option>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Escopo</label>
                                <textarea name="escopo" id="escopo" class="form-control"
                                    required><?php echo e($servicos_agenciado->escopo); ?></textarea>
                            </div>
                        </div>
                    </div>



                    <div class="text-right mt-5">
                        <a href="<?php echo e(URL::previous()); ?>" class="btn btn-light" data-dismiss="modal">Sair</a>
                        <button type="submit" class="btn btn-primary">Atualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </div> <!-- container -->

<?php $__env->startSection('js'); ?>
    <script>
        var arrayProdutos = [];
        $(document).ready(function() {
            $('#esforco_hrs').blur(function(event) {
                event.preventDefault();

                var funcao = $('#base').val();
                var esforco_hrs = $('#esforco_hrs').val();
                var valor_hora = (parseFloat(funcao) / 20) / 6.4;
                var esforco_valor = esforco_hrs * valor_hora;
                var valor_individual = (esforco_hrs * valor_hora) * 2;
                $('#esforco_valor').val(esforco_valor.toFixed(2));
                $('#valor_individual').val(valor_individual.toFixed(2));
                /* var margem_lucro = $('#margem_lucro').val();
                var valor_hora = (parseFloat(funcao) / 20) / 6.4;
                var valor_total = valor_hora * (1 + (margem_lucro / 100));
                var texto = $('#texto').val();
                $('#adicionais').val('R$ ' + valor_total.toFixed(2) + ' ' + texto); */
            });
        });

    </script>
<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/dashboard/painel/resources/views/content/financeiros/servicos_agenciado/edit.blade.php ENDPATH**/ ?>