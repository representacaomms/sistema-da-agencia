<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('custos-salarios.index')); ?>">Salários</a></li>
                            <li class="breadcrumb-item active">Listar Salários</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Listar Salários</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                                <a href="<?php echo e(route('custos-salarios.create')); ?>" class="btn btn-danger mb-2"><i class="mdi mdi-plus-circle mr-2"></i> Adicionar Salário</a>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-success mb-2 mr-1" data-toggle="modal" data-target="#right-modal"><i class="mdi mdi-settings"></i></button>
                                    <button type="button" class="btn btn-light mb-2 mr-1">Importar</button>
                                    <button type="button" class="btn btn-light mb-2">Exportar</button>
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            <?php if($message = Session::get('success')): ?>
                            <?php if($message == 'store'): ?>
                                <script>
                                    var message = 'store';
                                </script>
                            <?php endif; ?>
                            <?php if($message == 'update'): ?>
                                <script>
                                    var message = 'update';
                                </script>
                            <?php endif; ?>

                            <?php if($message == 'destroy'): ?>
                                <script>
                                    var message = 'destroy';
                                </script>
                            <?php endif; ?>

                            <?php if($message == 'active'): ?>
                                <script>
                                    var message = 'active';
                                </script>
                            <?php endif; ?>

                            <?php endif; ?>
                            <table class="table table-centered w-100 dt-responsive " id="custos-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th>#ID</th>
                                        <th>Descrição</th>
                                        <th>Salário</th>
                                        <th>Custo Salário</th>
                                        <th>Base Salário</th>
                                        <th >Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $salarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16">
                                                   <?php echo e($key + 1); ?>

                                                </p>
                                            </td>
                                            <td><p class="m-0 d-inline-block align-middle font-16"><a  class="text-body"><?php echo e($item->nome); ?></a></p></td>
                                            <td><p class="m-0 d-inline-block align-middle font-16"><a  class="text-body">R$ <?php echo e(number_format($item->salario,2)); ?></a></p></td>
                                            <td><p class="m-0 d-inline-block align-middle font-16"><a  class="text-body">R$ <?php echo e(number_format($item->total_provisoes,2)); ?></a></p></td>
                                            <td><p class="m-0 d-inline-block align-middle font-16"><a  class="text-body">R$ <?php echo e(number_format($item->total_provisoes + $item->salario,2)); ?></a></p></td>

                                            <td>
                                                <a href="<?php echo e(route('custos-salarios.edit', $item->id)); ?>" class="btn btn-warning mb-2">Editar</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>

                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
            <!-- end row -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function() {

            $('#custos-datatable').DataTable({
                language: {
                    processing:     "Processamento...",
                    search:         "Pesquisar&nbsp;:",
                    lengthMenu:    "Mostrar _MENU_ entradas",
                    info:           "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty:      "Exibindo da entrada 0 até 0 de 0 entradas",
                    infoFiltered:   "(filtro total de _MAX_ entradas)",
                    infoPostFix:    "",
                    loadingRecords: "Carregando ...",
                    zeroRecords:    "Nenhuma entrada encontrada",
                    emptyTable:     "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first:      "Primeiro",
                        previous:   "Anterior",
                        next:       "Ultimo",
                        last:       "Depos"
                    },
                    aria: {
                        sortAscending:  ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        } );
    </script>
    <script type="text/javascript">

        if(message == 'store'){
            Swal.fire(
                'Parabéns!',
                'Salário cadastrado com sucesso!',
                'success'
            )
        }
        if(message == 'update'){
            Swal.fire(
                'Parabéns!',
                'Salário editado com sucesso!',
                'success'
            )
        }

        if(message == 'destroy'){
            Swal.fire(
                'Alerta!',
                'Salário inativado com sucesso!',
                'info'
            )
        }

        if(message == 'active'){
            Swal.fire(
                'Alerta!',
                'Salário ativado com sucesso!',
                'info'
            )
        }

    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/dashboard/painel/resources/views/content/financeiros/custo_salario/index.blade.php ENDPATH**/ ?>