<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('custos-salarios.index')); ?>">Salários</a></li>
                            <li class="breadcrumb-item active">Editar Salário</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Salário</h4>
                </div>
            </div>
        </div>
       <div class="card">
           <div class="card-body">
               <form class="p-2" method="POST" action="<?php echo e(route('custos-salarios.update',$custos_salario->id)); ?>">
                <?php echo method_field('PUT'); ?>
                <?php echo csrf_field(); ?>
                <div class="row">
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nome </label>
                                    <input type="text" name="nome" id="nome" class="form-control" value="<?php echo e($custos_salario->nome); ?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="task-priority2">Valor Salário</label>
                                    <input type="text" class="form-control" value="<?php echo e($custos_salario->salario); ?>" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true"  name="salario"  id="salario" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="task-priority2">Valor Total Provisões </label>
                                    <input type="text" class="form-control" value="<?php echo e($custos_salario->total_provisoes); ?>" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true"  name="total_provisoes"  id="total_provisoes" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="task-priority2">FGTS Salário  </label>
                                <input type="text" class="form-control" value="<?php echo e($custos_salario->fgts_salario); ?>" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true"  name="fgts_salario" id="fgts_salario" required >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="task-priority2">Férias 1/12 </label>
                                <input type="text" class="form-control" value="<?php echo e($custos_salario->ferias); ?>" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true"  name="ferias" id="ferias" required >
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="task-priority2">Adicional Férias 1/3 </label>
                                <input type="text" class="form-control" value="<?php echo e($custos_salario->adicional_ferias); ?>" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true"  name="adicional_ferias" id="adicional_ferias" required >
                                </div>
                            </div>


                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="task-priority2">FGTS Férias e Adicional 1/3 </label>
                                <input type="text" class="form-control" value="<?php echo e($custos_salario->fgts_ferias_adicional); ?>" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true"  name="fgts_ferias_adicional" id="fgts_ferias_adicional" required >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="task-priority2">13º Salário </label>
                                <input type="text" class="form-control" value="<?php echo e($custos_salario->salario_13); ?>" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true"  name="salario_13" id="salario_13" required >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="task-priority2">FGTS 13º Salário </label>
                                <input type="text" class="form-control" value="<?php echo e($custos_salario->fgts_salario_13); ?>" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true"  name="fgts_salario_13" id="fgts_salario_13" required >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="task-priority2">Aviso Prévio</label>
                                <input type="text" class="form-control" value="<?php echo e($custos_salario->aviso_previo); ?>" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true"  name="aviso_previo" id="aviso_previo" required >
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="task-priority2">FGTS Aviso Prévio </label>
                                <input type="text" class="form-control" value="<?php echo e($custos_salario->fgts_aviso_previo); ?>" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true"  name="fgts_aviso_previo" id="fgts_aviso_previo" required >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="task-priority2">Multa FGTS </label>
                                <input type="text" class="form-control" value="<?php echo e($custos_salario->multa_fgts); ?>" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true"  name="multa_fgts" id="multa_fgts" required >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right mt-5">
                    <a href="<?php echo e(URL::previous()); ?>" class="btn btn-light" data-dismiss="modal">Sair</a>
                    <button type="submit" class="btn btn-primary">Atualizar</button>
                </div>
            </form>
           </div>
       </div>
    </div> <!-- container -->

    <?php $__env->startSection('js'); ?>
    <script>
        $(document).ready(function() {

            $('#salario').change( function (event){
                event.preventDefault();
                var salario               =  $('#salario').val();

                $('#fgts_salario').val((parseFloat(salario) * 0.08 ).toFixed(2));
                $('#ferias').val((parseFloat(salario) / 12 ).toFixed(2));
                $('#adicional_ferias').val(((parseFloat(salario) / 12) /3 ).toFixed(2));
                $('#fgts_ferias_adicional').val(((((parseFloat(salario) / 12) / 3) + (parseFloat(salario) / 12 )) * 0.08).toFixed(2));
                $('#salario_13').val((parseFloat(salario) / 12 ).toFixed(2));
                $('#fgts_salario_13').val(((parseFloat(salario) / 12) * 0.08 ).toFixed(2));
                $('#aviso_previo').val((parseFloat(salario) / 12 ).toFixed(2));
                $('#fgts_aviso_previo').val(((parseFloat(salario) / 12) * 0.08).toFixed(2));
                $('#multa_fgts').val((((parseFloat(salario) * 0.08 ) + ((((parseFloat(salario) / 12) / 3) + (parseFloat(salario) / 12 )) * 0.08) +  ((parseFloat(salario) / 12) * 0.08 ) + ((parseFloat(salario) / 12) * 0.08)) * 0.5).toFixed(2)   );


                var ferias                =  $('#ferias').val();
                var adicional_ferias      =  $('#adicional_ferias').val();
                var fgts_ferias_adicional =  $('#fgts_ferias_adicional').val();
                var salario_13            =  $('#salario_13').val();
                var fgts_salario_13       =  $('#fgts_salario_13').val();
                var aviso_previo          =  $('#aviso_previo').val();
                var fgts_aviso_previo     =  $('#fgts_aviso_previo').val();
                var multa_fgts            =  $('#multa_fgts').val();

                $('#total_provisoes').val((parseFloat(ferias) + parseFloat(adicional_ferias) + parseFloat(fgts_ferias_adicional) + parseFloat(salario_13) + parseFloat(fgts_salario_13) + parseFloat(aviso_previo) + parseFloat(fgts_aviso_previo) + parseFloat(multa_fgts)).toFixed(2));


            });

        } );
    </script>
    <?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/financeiros/custo_salario/edit.blade.php ENDPATH**/ ?>