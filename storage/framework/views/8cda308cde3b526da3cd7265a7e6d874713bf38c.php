<?php $__env->startSection('content'); ?>

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Contas Bancaria</a></li>
                            <li class="breadcrumb-item active">Listar Contas Bancária</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Listar Contas Bancária</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                                <a href="<?php echo e(route('contas-bancarias.create')); ?>" class="btn btn-danger mb-2"><i class="mdi mdi-plus-circle mr-2"></i> Adicionar Conta Bancária</a>
                            </div>

                            
                        </div>

                        <div class="table-responsive">
                            <?php if($message = Session::get('success')): ?>
                                <?php if($message == 'store'): ?>
                                    <script>
                                        var message = 'store';
                                    </script>
                                <?php endif; ?>
                                <?php if($message == 'update'): ?>
                                    <script>
                                        var message = 'update';
                                    </script>
                                <?php endif; ?>

                                <?php if($message == 'destroy'): ?>
                                    <script>
                                        var message = 'destroy';
                                    </script>
                                <?php endif; ?>

                            <?php endif; ?>

                            <table class="table table-centered w-100 dt-responsive " id="estoque-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th class="all">ID</th>
                                        <th>Cod Banco</th>
                                        <th>Nome Banco</th>
                                        <th>Conta</th>
                                        <th>Status</th>
                                        <th style="width: 75px;">Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $contas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16"><?php echo e($item->id); ?></p>
                                            </td>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16">
                                                    <a class="text-body"><?php echo e($item->banco); ?></a>
                                                    <br/>
                                                </p>
                                            </td>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16">
                                                    <a class="text-body">
                                                        <?php $__currentLoopData = $bancos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banco): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($banco['id']  == $item->banco): ?>
                                                                <?php echo e($banco['nome']); ?>

                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </a>
                                                    <br/>
                                                </p>
                                            </td>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16">
                                                    <a class="text-body"><?php echo e($item->conta); ?></a>
                                                    <br/>
                                                </p>
                                            </td>
                                           
                                            <td>
                                                    <?php if($item->status == 'A'): ?>
                                                    <span class="badge badge-success"><p class="m-0 d-inline-block align-middle font-16">Ativa</p></span>
                                                    <?php else: ?>
                                                    <span class="badge badge-danger"><p class="m-0 d-inline-block align-middle font-16">Inativa</p></span>
                                                    <?php endif; ?>

                                            </td>
                                            <td>
                                                <form action="<?php echo e(route('contas-bancarias.destroy', $item->id)); ?>" method="post">
                                                    <?php echo csrf_field(); ?>
                                                    <?php echo method_field('Delete'); ?>
                                                    <a href="<?php echo e(route('contas-bancarias.edit', $item->id)); ?>"  class="action-icon"> <i class="mdi mdi-pencil"></i></a>
                                                    <button type="submit"  class="btn action-icon"> <i class="mdi mdi-delete"></i></button>
                                                </form>
                                            </td>
                                        </tr>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </tbody>
                            </table>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
            <!-- end row -->
    </div> <!-- container -->


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function() {

            $('#estoque-datatable').DataTable({
                language: {
                    processing:     "Processamento...",
                    search:         "Pesquisar&nbsp;:",
                    lengthMenu:    "Mostrar _MENU_ entradas",
                    info:           "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty:      "Exibindo da entrada 0 até 0 de 0 entradas",
                    infoFiltered:   "(filtro total de _MAX_ entradas)",
                    infoPostFix:    "",
                    loadingRecords: "Carregando ...",
                    zeroRecords:    "Nenhuma entrada encontrada",
                    emptyTable:     "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first:      "Primeiro",
                        previous:   "Anterior",
                        next:       "Ultimo",
                        last:       "Depos"
                    },
                    aria: {
                        sortAscending:  ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        } );
    </script>
    <script type="text/javascript">

        if(message == 'store'){
            Swal.fire(
                'Parabéns!',
                'Registro cadastrado com sucesso!',
                'success'
            )
        }
        if(message == 'update'){
            Swal.fire(
                'Parabéns!',
                'Registro editado com sucesso!',
                'success'
            )
        }

        if(message == 'destroy'){
            Swal.fire(
                'Alerta!',
                'Registro excluido com sucesso!',
                'info'
            )
        }

        if(message == 'active'){
            Swal.fire(
                'Alerta!',
                'Registro ativado com sucesso!',
                'info'
            )
        }

    </script>
    <script type="text/javascript">

        if(message == 'invoice'){
            Swal.fire(
                'Parabéns!',
                'Pedido FATURADO com sucesso!',
                'success'
            )
        }

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/financeiros/conta_bancaria/index.blade.php ENDPATH**/ ?>