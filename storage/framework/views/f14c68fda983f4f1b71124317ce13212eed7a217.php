<?php $__env->startSection('content'); ?>
 <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('gestao-anuncios.index')); ?>">Receitas</a></li>
                            <li class="breadcrumb-item active">Recibo</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Recibo de Pagamento</h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <!-- Invoice Logo-->
                        <div class="clearfix">
                            <div class="float-right mb-3">
                                <img src="<?php echo e(url('https://agenciamms.com/wp-content/uploads/2021/11/cropped-logo.png')); ?>" alt="" height="70">
                            </div>
                            <div class="float-left">
                                <h1 class="m-5" style="width: 300px"><?php echo e($record->cliente->nome); ?></h1>
                            </div>
                        </div>
                        <h2 class="text-muted text-center font-24">Recibo de Pagamento</h2>
                       
                            

                        <div class="d-print-none mt-4">
                            <div class="text-right">
                                <a href="javascript:window.print()" class="btn btn-primary imprimir"><i class="mdi mdi-printer"></i> Imprimir</a>
                                <a href="<?php echo e(route('gestao-anuncios.index')); ?>" class="btn btn-info">Fechar</a>
                            </div>
                        </div>   
                        <!-- end buttons -->

                    </div> <!-- end card-body-->
                </div> <!-- end card -->
            </div> <!-- end col-->
        </div>
        <!-- end row -->
        
    </div> <!-- container -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

<script>
    $('.imprimir').click( function() {
        $('.recibo').removeClass('d-none');
        setInterval(function(){
            $('.recibo').addClass('d-none');
            }, 3000);
    });
</script>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mac-marcelo/projetos/GitHub/agencia_WEB/resources/views/content/pdfs/relatorio_mkt.blade.php ENDPATH**/ ?>