<?php

use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

if (Auth::check()) {
    $user = Auth::user();
    $wcFbUserSection['user_name']        = Str::before(Str::lower($user['name']), ' ');
    $wcFbUserSection['user_lastname']    = Str::afterLast(Str::lower($user['name']), ' ');

    $wcFbUserSection['user_phone']   = (!empty($user['celular']) ? $user['celular'] : ($user['telefone'] ? $user['telefone'] : null) );
    if(!empty($wcFbUserSection['user_phone'])){
        $wcFbUserSection['user_phone'] = "55" .str_replace( ['(', ')', '-', '.', ' '], '',$wcFbUserSection['user_phone']);
    }

    $wcFbUserSection['user_cep']       = !empty($user->endereco->cep) ? str_replace(['.', '-'],'',$user->endereco->cep) : null;
    $wcFbUserSection['user_cidade']    = !empty($user->endereco->localidade) ? Str::lower($user->endereco->localidade) : null;
    $wcFbUserSection['user_uf']        = !empty($user->endereco->uf) ? Str::lower($user->endereco->uf) : null;
    $wcFbUserSection['link']           =  Str::contains(url()->current(), 'blog'); 
 
    
}else {
   
}

?>
<!-- Facebook Pixel Code -->
<script>
    $(function(){
        FB_PIXEL = '211629366611554';
        WC_USER  = <?= (!empty($wcFbUserSection) ? json_encode($wcFbUserSection) : '"null"'); ?> ;

        

        // FACEBOOK PIXEL
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');

        //WC TRACK
        if(WC_USER === 'null'){
            fbq('init', FB_PIXEL);
        }else{
            fbq('init', FB_PIXEL, {
                em: WC_USER.email, //Email
                fn: WC_USER.user_name, // Nome
                ln: WC_USER.user_lastname, // Sobrenome
                ph: WC_USER.user_phone, // Telefone
                /* ge: 'm', */ //Genero
                /* db: '19812002', */ // Data de nascimento YYYYMMDD
                ct: WC_USER.user_cidade, // Cidade
                st: WC_USER.user_uf, // Estado
                zp: WC_USER.user_cep //Cep
            });
        }
        // WC EVENTS
        fbq('track', 'PageView');
        if(WC.link){
            fbq('track', 'ViewContent',{
               content_name: '<?= (!empty($categoria) ? $categoria : null)?>',
               referrer: document.referrer,
               userAgent: navigator.userAgent,
               language: navigator.language
            })
        }
    });
</script>
<noscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=211629366611554&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->