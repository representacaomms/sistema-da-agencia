import Vue from 'vue';

new Vue({
    el: '.tasks',
    data:{
        message: 'My Task Project',
        tasks: [
            {
                name: 'Marcelo Andrade'
            },
            {
                name: 'Miriam Andrade'
            },
            {
                name: 'Casados Andrade'
            }
        ]
    },
    methods: {
        
    }
})