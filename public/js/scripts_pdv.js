    var arrayProdutos = [];
    var subtotal;
    
    $(function() {
        $(".subtotal").text('0.00');
        $('input[name="term_search"]').on("keyup", function() {
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                }
            });
            var form = $(this).parent("form");
            var input = $('input[name="term_search"]').val();

            $.ajax({
                url: "http://localhost/solutiomms/public/gestao/produtos/search",
                data: { action: input },
                type: "POST",
                dataType: "json",
                success: function(data) {
                    if (data.product) {
                        if (!$(form).find(".j_term_content").length) {
                            form.append("<div class='j_term_content'></div>");
                        }

                        $(".j_term_content").empty();

                        $.each(data.product, function(key, value) {
                            $(".j_term_content").append(
                                "<div class='j_term_content_item' onclick='selecionado(" +value["id"] +")'>" +
                                    "<img src='http://localhost/solutiomms/public/images/products/product-0.jpg' width='100' height='100'>" +
                                    "<div class='j_term_content_item_description'>" +
                                    "<p><a href='#" +value["id"] + "'>" + value["descricao"] +"</a></p>" +
                                    "<p>R$ " +value["preco_venda"] +"</p>" +"<p>NCM " +value["ncm"] +" - EAN " +value["ncm"] +"</p>" +
                                    "</div>" +
                                "</div>"
                            );
                        });
                    }

                    if (data.product_clear) {
                        if ($(".j_term_content").length) {
                            $(".j_term_content").remove();
                        }
                    }
                }
            });
        });

        $("body").on("click", function(e) {
            if (!$(".j_term_content").has(e.target).length) {
                $(".j_term_content").remove();
            }
        });

        $("#Table").on("click", ".remover", function(e) {

            const tr = $(this).closest("tr");
            const indice = tr[0].cells[0].attributes[0].value;

            if(arrayProdutos.length > 1){
                arrayProdutos.splice(indice,1);
                var total=0;
                var quantidade=0;

                arrayProdutos.forEach(element => {
                    const valor_elemento = parseFloat(element.valor_total);
                    total = total + valor_elemento;
                    const valor_qtde = parseInt(element.quantidade);
                    quantidade = quantidade + valor_qtde
                });
                $(".subtotal").text(total.toFixed(2));
            }else{
                arrayProdutos = [];
                $(".subtotal").text('0.00');
            }
       
            $(this).closest("tr").remove();
        });

        $("#Table").on("blur", "#qtd", function(e) {
            $(this).closest("tr").remove();
            var total = 0
            const qtde = e.target.valueAsNumber;
            const indice = e.target.attributes[6].value;
            const produto = arrayProdutos[indice];
            produto.quantidade = qtde;
            produto.valor_total = (produto.venda * qtde).toFixed(2);
            var id = arrayProdutos.length - 1;
            
            var quantidade=0;
            var markup;
            arrayProdutos.forEach(element => {
                console.log(element)
                const valor_qtde = parseInt(produto.quantidade);
                quantidade = quantidade + valor_qtde;
                const valor_elemento = parseFloat(element.valor_total);
                    total = total + valor_elemento;
                
               markup =
                   "<tr><td idlinha=" +
                   id +
                   ">" +
                   element.id +
                   "</td><td>" +
                   "<img src='http://localhost/solutiomms/public/images/products/product-0.jpg'  alt='contact-img'" +
                   "title='contact-img' class='rounded mr-3' height='64' />" +
                   "<p class='m-0 d-inline-block align-middle font-16'>" +
                   "<a href='apps-ecommerce-products-details.html' class='text-body'>" +
                   element.produto +
                   "</a><br>" +
                   "<small class='mr-2'><b>NCM: </b>" +
                   element.ncm +
                   "</small>" +
                   "<small><b>EAN: </b>" +
                   element.ean +
                   "</small>" +
                   "</p>" +
                   "</td>" +
                   "<td>R$ " +
                   element.venda +
                   "</td>" +
                   "<td><input type='number' min='1' value=" +
                   qtde +
                   " class='form-control' id='qtd' style='width: 90px;'  data-id=" +
                   id +
                   "></td>" +
                   "<td class='editavel' name='valor_total'>" +
                   element.valor_total +
                   "</td>" +
                   "<td><a href='javascript:void(0);' class='action-icon remover'> <i class='mdi mdi-delete'></i></a></td>" +
                   "</tr>";
                   $(".subtotal").text((valor_elemento).toFixed(2));
           });

           $("table #Tbody").append(markup);
        });
    });
    function selecionado(e) {
        var total = 0;
        $(".j_term_content").empty();

        $.ajax({
            url: "http://localhost/solutiomms/public/gestao/produtos/" + e,
            type: "get",
            dataType: "json",
            success: function(response) {
                arrayProdutos.push({
                    id: response.id,
                    produto: response.descricao,
                    quantidade: 1,
                    venda: response.preco_venda,
                    ncm: response.ncm,
                    ean: response.ean,
                    valor_total: response.preco_venda
                });
                subtotal = response.preco_venda * 1;
                var id = arrayProdutos.length -1;
                $(".total_produtos").text(arrayProdutos.length);

              
                var markup;
                arrayProdutos.forEach(element => {

                    
                    const valor_elemento = parseFloat(element.valor_total)
                    total = total + valor_elemento;
                    markup =
                        "<tr><td idlinha=" +
                        id +
                        ">" +
                        element.id +
                        "</td><td>" +
                        "<img src='http://localhost/solutiomms/public/images/products/product-0.jpg'  alt='contact-img'" +
                        "title='contact-img' class='rounded mr-3' height='64' />" +
                        "<p class='m-0 d-inline-block align-middle font-16'>" +
                        "<a href='apps-ecommerce-products-details.html' class='text-body'>" +
                        element.produto +
                        "</a><br>" +
                        "<small class='mr-2'><b>NCM: </b>" +
                        element.ncm +
                        "</small>" +
                        "<small><b>EAN: </b>" +
                        element.ean +
                        "</small>" +
                        "</p>" +
                        "</td>" +
                        "<td>R$ " +
                        element.venda +
                        "</td>" +
                        "<td><input type='number' min='1' value='1' class='form-control' id='qtd' style='width: 90px;' data-id=" +
                        id +
                        "></td>" +
                        "<td class='editavel' name='valor_total'>" +
                        element.valor_total +
                        "</td>" +
                        "<td><a href='javascript:void(0);' class='action-icon remover'> <i class='mdi mdi-delete'></i></a></td>" +
                        "</tr>";

                        $(".subtotal").text(total.toFixed(2));
                });
               

                $("table #Tbody").append(markup);
                $('input[name="term_search"]').val("");
                $('input[name="term_search"]').focus();

            }
        });

    
    }

   


