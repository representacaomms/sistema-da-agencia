$(function () {

    // SELECT2
    $('.select2').select2({
        language: "pt-BR"
    });

    // SEARCH ZIPCODE
    $('.zip_code_search').blur(function () {

        function emptyForm() {
            $(".logradouro").val("");
            $(".bairro").val("");
            $(".cidade").val("");
            $(".ibge").val("");
            $(".uf").val("");
        }

        var zip_code = $(this).val().replace(/\D/g, '');
        var validate_zip_code = /^[0-9]{8}$/;

        if (zip_code != "" && validate_zip_code.test(zip_code)) {

            $(".logradouro").val("");
            $(".bairro").val("");
            $(".localidade").val("");
            $(".ibge").val("");
            $(".uf").val("");

            $.getJSON("https://viacep.com.br/ws/" + zip_code + "/json/?callback=?", function (data) {

                if (!("erro" in data)) {
                    $(".logradouro").val(data.logradouro);
                    $(".bairro").val(data.bairro);
                    $(".localidade").val(data.localidade);
                    $(".ibge").val(data.ibge);
                    $(".uf").val(data.uf);
                } else {
                    emptyForm();
                    alert("CEP não encontrado.");
                }
            });
        } else {
            emptyForm();
            alert("Formato de CEP inválido.");
        }
    });



});

