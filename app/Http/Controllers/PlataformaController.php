<?php

namespace App\Http\Controllers;

use App\Models\Plataforma;
use App\Models\PlataformaItens;

use Illuminate\Http\Request;

class PlataformaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = Plataforma::all();

        return view('content.plataformas.index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.plataformas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $campos = collect([]);
        for ($i=1; $i < count($request->all()) -2 ; $i++) { 
            $campos->push('campo_'.$i);
        }
       
        $plataforma = new Plataforma();
        $plataforma->nome = $request->plataforma;
        $plataforma->save();

        $itens = new PlataformaItens();

        foreach ($request->all() as $key => $value) {
            
           foreach ($campos as $campo) {
            if($key == $campo){
                $itens->id_plataforma = $plataforma->id;
                $itens->descricao = $value;
                $itens->acao = 'null';
                $itens->save();
            }
               # code...
           }
            # code...
        }

        return redirect()->route('plataformas.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Plataforma  $plataforma
     * @return \Illuminate\Http\Response
     */
    public function show(Plataforma $plataforma)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Plataforma  $plataforma
     * @return \Illuminate\Http\Response
     */
    public function edit(Plataforma $plataforma)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Plataforma  $plataforma
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Plataforma $plataforma)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Plataforma  $plataforma
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plataforma $plataforma)
    {
        //
    }
}
