<?php

namespace App\Http\Controllers;

use App\Models\Clausula;
use Illuminate\Http\Request;

class ClausulaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Clausula  $clausula
     * @return \Illuminate\Http\Response
     */
    public function show(Clausula $clausula)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Clausula  $clausula
     * @return \Illuminate\Http\Response
     */
    public function edit(Clausula $clausula)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Clausula  $clausula
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Clausula $clausula)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Clausula  $clausula
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clausula $clausula)
    {
        //
    }
}
