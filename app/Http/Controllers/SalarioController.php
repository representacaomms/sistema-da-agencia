<?php

namespace App\Http\Controllers;

use App\Models\Salario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SalarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salarios = Salario::where('id_empresa',Auth::user()->id_empresa)->get();
        return view('content.financeiros.custo_salario.index', compact('salarios'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('content.financeiros.custo_salario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $salario = new Salario();
        $salario->id_empresa            = Auth::user()->id_empresa;
        $salario->nome                  = $request->nome;
        $salario->total_provisoes       = $request->total_provisoes;
        $salario->salario               = $request->salario;
        $salario->fgts_salario          = $request->fgts_salario;
        $salario->ferias                = $request->ferias;
        $salario->adicional_ferias      = $request->adicional_ferias;
        $salario->fgts_ferias_adicional = $request->fgts_ferias_adicional;
        $salario->salario_13            = $request->salario_13;
        $salario->fgts_salario_13       = $request->fgts_salario_13;
        $salario->aviso_previo          = $request->aviso_previo;
        $salario->fgts_aviso_previo     = $request->fgts_aviso_previo;
        $salario->multa_fgts            = $request->multa_fgts;
        $salario->save();

        return redirect()->route('custos-salarios.index')->with('success','store');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Salario  $salario
     * @return \Illuminate\Http\Response
     */
    public function show(Salario $salario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Salario  $salario
     * @return \Illuminate\Http\Response
     */
    public function edit(Salario $custos_salario)
    {
        return view('content.financeiros.custo_salario.edit', compact('custos_salario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Salario  $salario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Salario $custos_salario)
    {
        $custos_salario->id_empresa            = Auth::user()->id_empresa;
        $custos_salario->nome                  = $request->nome;
        $custos_salario->total_provisoes       = $request->total_provisoes;
        $custos_salario->salario               = $request->salario;
        $custos_salario->fgts_salario          = $request->fgts_salario;
        $custos_salario->ferias                = $request->ferias;
        $custos_salario->adicional_ferias      = $request->adicional_ferias;
        $custos_salario->fgts_ferias_adicional = $request->fgts_ferias_adicional;
        $custos_salario->salario_13            = $request->salario_13;
        $custos_salario->fgts_salario_13       = $request->fgts_salario_13;
        $custos_salario->aviso_previo          = $request->aviso_previo;
        $custos_salario->fgts_aviso_previo     = $request->fgts_aviso_previo;
        $custos_salario->multa_fgts            = $request->multa_fgts;
        $custos_salario->save();

        return redirect()->route('custos-salarios.index')->with('success','update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Salario  $salario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Salario::where('id',$id)->delete();
      return redirect()->route('custos-salarios.index')->with('success','destroy');
    }
}
