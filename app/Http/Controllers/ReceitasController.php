<?php

namespace App\Http\Controllers;

use App\Models\CategoriaLanc;
use App\Models\CentroCusto;
use App\Models\Conta;
use App\Models\Lancamento;
use App\Models\Lancamento_itens;
use App\Models\Pessoa;
use App\Models\SubCategoriaLanc;
use App\Models\ContaBancaria;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ReceitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hoje = date('Y-m-d H:m:s');
        $receitas = Lancamento::where('id_empresa',Auth::user()->id_empresa)->where('tipo', 'Receita')->get();
        foreach ($receitas as $key => $receita) {

            $itensReceita = Lancamento_itens::where('id_lancamento',$receita->id)->get();

            foreach ($itensReceita as $key => $value) {
                if($value->data_pagto < $hoje && $value->status != "Recebido"){
                    $receita['status_parc'] = 'Vencida';
                }
            }

            $status = Pessoa::where('id',$receita->id_cli)->first()->status;
            $receita->status = $status;

            
        }


       
        return view('content.financeiros.receitas.index', compact('receitas'));
    }
    
     public function detal($id)
    {
        $parcelas = Lancamento::where('id',$id)->first()->parcela;
        $descricao = Lancamento::where('id',$id)->first()->descricao;

        $pessoas = Pessoa::where('id',Lancamento::where('id',$id)->first()->id_cli)->first();
        $telefone = "55".Str::replace(array("(" , ")", "-", " "), '', $pessoas->celular);
        $cliente = $pessoas->fantasia;
        $receitas = Lancamento_itens::where('id_lancamento',$id)->get();
        $hoje = date('Y-m-d H:m:s');

        return view('content.financeiros.receitas.detal', compact('receitas', 'parcelas', 'descricao', 'telefone', 'cliente', 'hoje'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $pessoas = Pessoa::where('id_empresa',Auth::user()->id_empresa)
        ->where(function ($query) {
            $query->where('rotulo', 'clientes')
                  ->orWhere('rotulo', 'fornecedores');
        })
        ->get();
        $lancamentos = Lancamento::where('id_empresa',Auth::user()->id_empresa)->where('tipo','Receita')->get();
        $centro_custos = CentroCusto::where('id_empresa',Auth::user()->id_empresa)->get();
        $contas = Conta::where('id_empresa',Auth::user()->id_empresa)->get();
        $categorias = CategoriaLanc::where('id_empresa',Auth::user()->id_empresa)->get();
        $subcategorias = SubCategoriaLanc::where('id_empresa',Auth::user()->id_empresa)->get();

        return view('content.financeiros.receitas.create', compact('lancamentos', 'centro_custos','contas', 'categorias','subcategorias', 'pessoas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bancos = collect([
            ['id' => '000', 'nome' => 'Caixa da Empresa'],
            ['id' => '001', 'nome' => 'Banco do Brasil'],
            ['id' => '003', 'nome' => 'Banco da Amazônia'],
            ['id' => '004', 'nome' => 'Banco do Nordeste'],
            ['id' => '021', 'nome' => 'Banestes'],
            ['id' => '025', 'nome' => 'Banco Alfa'],
            ['id' => '027', 'nome' => 'Besc'],
            ['id' => '029', 'nome' => 'Banerj'],
            ['id' => '031', 'nome' => 'Banco Beg'],
            ['id' => '033', 'nome' => 'Banco Santander Banespa'],
            ['id' => '036', 'nome' => 'Banco Bem'],
            ['id' => '037', 'nome' => 'Banpará'],
            ['id' => '038', 'nome' => 'Banestado'],
            ['id' => '039', 'nome' => 'BEP'],
            ['id' => '040', 'nome' => 'Banco Cargill'],
            ['id' => '041', 'nome' => 'Banrisul'],
            ['id' => '044', 'nome' => 'BVA'],
            ['id' => '045', 'nome' => 'Banco Opportunity'],
            ['id' => '047', 'nome' => 'Banese'],
            ['id' => '062', 'nome' => 'Hipercard'],
            ['id' => '063', 'nome' => 'Ibibank'],
            ['id' => '065', 'nome' => 'Lemon Bank'],
            ['id' => '066', 'nome' => 'Banco Morgan Stanley Dean Witter'],
            ['id' => '069', 'nome' => 'BPN Brasil'],
            ['id' => '070', 'nome' => 'Banco de Brasília – BRB'],
            ['id' => '072', 'nome' => 'Banco Rural'],
            ['id' => '073', 'nome' => 'Banco Popular'],
            ['id' => '074', 'nome' => 'Banco J. Safra'],
            ['id' => '075', 'nome' => 'Banco CR2'],
            ['id' => '076', 'nome' => 'Banco KDB'],
            ['id' => '077', 'nome' => 'Banco Inter'],
            ['id' => '096', 'nome' => 'Banco BMF'],
            ['id' => '104', 'nome' => 'Caixa Econômica Federal'],
            ['id' => '107', 'nome' => 'Banco BBM'],
            ['id' => '116', 'nome' => 'Banco Único'],
            ['id' => '151', 'nome' => 'Nossa Caixa'],
            ['id' => '175', 'nome' => 'Banco Finasa'],
            ['id' => '184', 'nome' => 'Banco Itaú BBA'],
            ['id' => '204', 'nome' => 'American Express Bank'],
            ['id' => '208', 'nome' => 'Banco Pactual'],
            ['id' => '212', 'nome' => 'Banco Matone'],
            ['id' => '213', 'nome' => 'Banco Arbi'],
            ['id' => '214', 'nome' => 'Banco Dibens'],
            ['id' => '217', 'nome' => 'Banco Joh Deere'],
            ['id' => '218', 'nome' => 'Banco Bonsucesso'],
            ['id' => '222', 'nome' => 'Banco Calyon Brasil'],
            ['id' => '224', 'nome' => 'Banco Fibra'],
            ['id' => '225', 'nome' => 'Banco Brascan'],
            ['id' => '229', 'nome' => 'Banco Cruzeiro'],
            ['id' => '230', 'nome' => 'Unicard'],
            ['id' => '233', 'nome' => 'Banco GE Capital'],
            ['id' => '237', 'nome' => 'Bradesco'],
            ['id' => '241', 'nome' => 'Banco Clássico'],
            ['id' => '243', 'nome' => 'Banco Stock Máxima'],
            ['id' => '246', 'nome' => 'Banco ABC Brasil'],
            ['id' => '248', 'nome' => 'Banco Boavista Interatlântico'],
            ['id' => '249', 'nome' => 'Investcred Unibanco'],
            ['id' => '250', 'nome' => 'Banco Schahin'],
            ['id' => '252', 'nome' => 'Fininvest'],
            ['id' => '254', 'nome' => 'Paraná Banco'],
            ['id' => '263', 'nome' => 'Banco Cacique'],
            ['id' => '265', 'nome' => 'Banco Fator'],
            ['id' => '266', 'nome' => 'Banco Cédula'],
            ['id' => '300', 'nome' => 'Banco de la Nación Argentina'],
            ['id' => '318', 'nome' => 'Banco BMG'],
            ['id' => '320', 'nome' => 'Banco Industrial e Comercial'],
            ['id' => '356', 'nome' => 'ABN Amro Real'],
            ['id' => '341', 'nome' => 'Itau'],
            ['id' => '347', 'nome' => 'Sudameris'],
            ['id' => '351', 'nome' => 'Banco Santander'],
            ['id' => '353', 'nome' => 'Banco Santander Brasil'],
            ['id' => '366', 'nome' => 'Banco Societe Generale Brasil'],
            ['id' => '370', 'nome' => 'Banco WestLB'],
            ['id' => '376', 'nome' => 'JP Morgan'],
            ['id' => '389', 'nome' => 'Banco Mercantil do Brasil'],
            ['id' => '394', 'nome' => 'Banco Mercantil de Crédito'],
            ['id' => '399', 'nome' => 'HSBC'],
            ['id' => '403', 'nome' => 'Banco Cora'],
            ['id' => '409', 'nome' => 'Unibanco'],
            ['id' => '412', 'nome' => 'Banco Capital'],
            ['id' => '422', 'nome' => 'Banco Safra'],
            ['id' => '453', 'nome' => 'Banco Rural'],
            ['id' => '456', 'nome' => 'Banco Tokyo Mitsubishi UFJ'],
            ['id' => '464', 'nome' => 'Banco Sumitomo Mitsui Brasileiro'],
            ['id' => '477', 'nome' => 'Citibank'],
            ['id' => '479', 'nome' => 'Itaubank (antigo Bank Boston)'],
            ['id' => '487', 'nome' => 'Deutsche Bank'],
            ['id' => '488', 'nome' => 'Banco Morgan Guaranty'],
            ['id' => '492', 'nome' => 'Banco NMB Postbank'],
            ['id' => '494', 'nome' => 'Banco la República Oriental del Uruguay'],
            ['id' => '495', 'nome' => 'Banco La Provincia de Buenos Aires'],
            ['id' => '505', 'nome' => 'Banco Credit Suisse'],
            ['id' => '600', 'nome' => 'Banco Luso Brasileiro'],
            ['id' => '604', 'nome' => 'Banco Industrial'],
            ['id' => '610', 'nome' => 'Banco VR'],
            ['id' => '611', 'nome' => 'Banco Paulista'],
            ['id' => '612', 'nome' => 'Banco Guanabara'],
            ['id' => '613', 'nome' => 'Banco Pecunia'],
            ['id' => '623', 'nome' => 'Banco Panamericano'],
            ['id' => '626', 'nome' => 'Banco Ficsa'],
            ['id' => '630', 'nome' => 'Banco Intercap'],
            ['id' => '633', 'nome' => 'Banco Rendimento'],
            ['id' => '634', 'nome' => 'Banco Triângulo'],
            ['id' => '637', 'nome' => 'Banco Sofisa'],
            ['id' => '638', 'nome' => 'Banco Prosper'],
            ['id' => '643', 'nome' => 'Banco Pine'],
            ['id' => '652', 'nome' => 'Itaú Holding Financeira'],
            ['id' => '653', 'nome' => 'Banco Indusval'],
            ['id' => '654', 'nome' => 'Banco A.J. Renner'],
            ['id' => '655', 'nome' => 'Banco Votorantim'],
            ['id' => '707', 'nome' => 'Banco Daycoval'],
            ['id' => '719', 'nome' => 'Banif'],
            ['id' => '721', 'nome' => 'Banco Credibel'],
            ['id' => '734', 'nome' => 'Banco Gerdau'],
            ['id' => '735', 'nome' => 'Banco Pottencial'],
            ['id' => '738', 'nome' => 'Banco Morada'],
            ['id' => '739', 'nome' => 'Banco Galvão de Negócios'],
            ['id' => '740', 'nome' => 'Banco Barclays'],
            ['id' => '741', 'nome' => 'BRP'],
            ['id' => '743', 'nome' => 'Banco Semear'],
            ['id' => '745', 'nome' => 'Banco Citibank'],
            ['id' => '746', 'nome' => 'Banco Modal'],
            ['id' => '747', 'nome' => 'Banco Rabobank International'],
            ['id' => '748', 'nome' => 'Banco Cooperativo Sicredi'],
            ['id' => '749', 'nome' => 'Banco Simples'],
            ['id' => '751', 'nome' => 'Dresdner Bank'],
            ['id' => '752', 'nome' => 'BNP Paribas'],
            ['id' => '753', 'nome' => 'Banco Comercial Uruguai'],
            ['id' => '755', 'nome' => 'Banco Merrill Lynch'],
            ['id' => '756', 'nome' => 'Banco Cooperativo do Brasil'],
            ['id' => '757', 'nome' => 'KEB'],
        ]);

        $receita = Lancamento_itens::where('id', $id)->first();
        $lancamento = Lancamento::where('id', $receita->id_lancamento)->first();
        $contas = ContaBancaria::all();

        return view('content.financeiros.receitas.playOf', compact( 'receita', 'lancamento', 'contas', 'bancos'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Lancamento $receita)
    {

        $pessoas = Pessoa::where('id_empresa',Auth::user()->id_empresa)->where('rotulo', 'clientes')->where('rotulo', 'fornecedores')->get();;
        $centro_custos = CentroCusto::all();
        $contas = Conta::all();
        $categorias = CategoriaLanc::all();
        $subcategorias = SubCategoriaLanc::all();
        return view('content.financeiros.receitas.edit', compact('centro_custos','contas', 'categorias','subcategorias', 'pessoas', 'receita'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
