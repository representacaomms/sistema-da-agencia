<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Models\Endereco;
use App\Models\Pessoa;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SheetDB\SheetDB;

class LeadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$records = Pessoa::where('id_empresa',Auth::user()->id_empresa)->where('rotulo', 'leads')->orWhere('rotulo', 'empresa')->get();
        $leadsDB = new SheetDB('8toh8z5kghryz');
        $leads = $leadsDB->get();
       
            foreach($leads as $item){
                $lead = new Pessoa();
                $lead->id_empresa   = Auth::user()->id_empresa;
                $lead->nome         = $item->nome;
                $lead->email        = $item->email;
                $lead->fantasia     = $item->empresa;
                $lead->celular      = $item->celular;
                $lead->status       = $item->rotulo;
                $lead->rotulo       = 'prospectos';
                $lead->tipo         = 'pj';
                $lead->save();
                $leadsDB->delete('nome',$item->nome);
            }
        
        
        $records = Pessoa::where('id_empresa', Auth::user()->id_empresa)->get();

        return view('content.leads.index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.leads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $lead = new Pessoa();
        $lead->id_empresa   = Auth::user()->id_empresa;
        $lead->nome         = $request->nome;
        $lead->tipo         = $request->tipo;
        if ($lead->tipo == 'pf') {
            $lead->rg       = $request->rg;
            $lead->cpf      = $request->cpf;
        } else {
            $lead->rg       = $request->insc_est;
            $lead->cpf      = $request->cnpj;
        }
        $lead->fantasia     = $request->fantasia;
        $lead->email        = $request->email;
        $lead->celular      = $request->celular;
        $lead->telefone     = $request->telefone;
        $lead->status       = $request->status;
        $lead->rotulo       = 'leads';
        $lead->save();

        $endereco = new Endereco();
        $endereco->cep         = ($request->cep == '00.000-000') ? '-' : $equest->cep ;
        $endereco->logradouro  = $request->logradouro;
        $endereco->complemento = $request->complemento;
        $endereco->numero      = $request->numero;
        $endereco->bairro      = $request->bairro;
        $endereco->localidade  = $request->localidade;
        $endereco->ibge        = $request->ibge;
        $endereco->uf          = $request->uf;
        $endereco->id_empresa  = $lead->id;
        $endereco->save(); 

        return redirect()->route('leads.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 'show';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Pessoa $lead)
    {
        
        
        if ($lead->tipo === 'pf') {
            return view('content.leads.edit', compact('lead'));
        } else {
            return view('content.leads.edit_pj', compact('lead'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pessoa $lead)
    {

        $lead->nome     = $request->nome;
        $lead->tipo     = $request->tipo;
        $lead->rg       = $request->rg;
        $lead->cpf      = $request->cpf;
        $lead->fantasia = $request->fantasia;
        $lead->email    = $request->email;
        $lead->celular  = $request->celular;
        $lead->telefone = $request->telefone;
        $lead->status   = $request->status;
        $lead->rotulo   = $request->rotulo;
        $lead->update();
        
       

        $endereco = Endereco::where('id_empresa', $lead->id)->first();
        if (!empty($endereco)) {
            $endereco->cep         = $request->cep;
            $endereco->logradouro  = $request->logradouro;
            $endereco->complemento = $request->complemento;
            $endereco->numero      = $request->numero;
            $endereco->bairro      = $request->bairro;
            $endereco->localidade  = $request->localidade;
            $endereco->ibge        = $request->ibge;
            $endereco->uf          = $request->uf;
            $endereco->save();
        } else {
            $endereco = new Endereco();
            $endereco->cep         = $request->cep ?? '-';
            $endereco->logradouro  = $request->logradouro ?? '-';
            $endereco->complemento = $request->complemento ?? '-';
            $endereco->numero      = $request->numero ?? '-';
            $endereco->bairro      = $request->bairro ?? '-';
            $endereco->localidade  = $request->localidade ?? '-';
            $endereco->ibge        = $request->ibge ?? '-';
            $endereco->uf          = $request->uf ?? '-';
            $endereco->id_empresa  = $lead->id;
            $endereco->save();
        }


        $retorno = true;
        echo json_encode($retorno);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pessoa $lead)
    {
        if ($lead->status == 'A') {
            $lead->status = 'I';
            $lead->update();
            return redirect()->route('leads.index')->with('success', 'destroy');
        } else {
            $lead->status = 'A';
            $lead->update();
            return redirect()->route('leads.index')->with('success', 'active');
        }
    }
}
