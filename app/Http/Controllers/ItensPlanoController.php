<?php

namespace App\Http\Controllers;

use App\ItensPlano;
use Illuminate\Http\Request;

class ItensPlanoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItensPlano  $itensPlano
     * @return \Illuminate\Http\Response
     */
    public function show(ItensPlano $itensPlano)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItensPlano  $itensPlano
     * @return \Illuminate\Http\Response
     */
    public function edit(ItensPlano $itensPlano)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItensPlano  $itensPlano
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItensPlano $itensPlano)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItensPlano  $itensPlano
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItensPlano $itensPlano)
    {
        //
    }
}
