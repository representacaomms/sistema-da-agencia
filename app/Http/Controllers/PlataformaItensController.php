<?php

namespace App\Http\Controllers;

use App\Models\PlataformaItens;
use App\Models\Plataforma;
use Illuminate\Http\Request;

class PlataformaItensController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $value)
    {
        $records = PlataformaItens::where('id_plataforma', $value->id)->get();
        return view('content.plataformas.itens.index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $plataforma = Plataforma::where('id', $request->id)->first();

        return view('content.plataformas.itens.create', compact('plataforma'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $plataformaItens = new PlataformaItens();
        $plataformaItens->descricao = $request->termo_consultoria;
        $plataformaItens->id_plataforma = $request->id_plataforma;
        $plataformaItens->tipo = $request->tipo;
        $plataformaItens->label = $request->label;
        $plataformaItens->input = $request->input;
        $plataformaItens->acao = 'null';
        $plataformaItens->save();

        return back()->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PlataformaItens  $plataformaItens
     * @return \Illuminate\Http\Response
     */
    public function show(PlataformaItens $plataformaItens)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        

        $plataformaItens = PlataformaItens::where('id', $id)->first();


        $plataforma = Plataforma::where('id', $plataformaItens->id_plataforma)->first();
        return view('content.plataformas.itens.edit', compact('plataformaItens', 'plataforma')); 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PlataformaItens  $plataformaItens
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $plataformaItens = PlataformaItens::where('id_plataforma', $request->id_plataforma)->first();
        $plataformaItens->descricao =  $request->termo_consultoria ;
        $plataformaItens->tipo = $request->tipo;
        $plataformaItens->label = $request->label;
        $plataformaItens->input = $request->input;
        $plataformaItens->save();

        return back()->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PlataformaItens  $plataformaItens
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlataformaItens $plataformaItens)
    {
        //
    }
}
