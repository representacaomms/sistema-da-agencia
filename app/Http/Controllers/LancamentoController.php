<?php

namespace App\Http\Controllers;

use App\Models\CategoriaLanc;
use App\Models\CentroCusto;
use App\Models\Conta;
use App\Models\Lancamento;
use App\Models\Lancamento_itens;
use App\Models\Pessoa;
use App\Models\SubCategoriaLanc;
use App\Models\ContaBancaria;
use App\Models\ContaBancariaItens;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LancamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pessoas = Pessoa::where('id_empresa', Auth::user()->id_empresa)->where('rotulo', 'clientes')->where('rotulo', 'fornecedores')->get();;

        $lancamentos = Lancamento::where('id_empresa', Auth::user()->id_empresa)->get();
        $centro_custos = CentroCusto::where('id_empresa', Auth::user()->id_empresa)->get();
        $contas = Conta::where('id_empresa', Auth::user()->id_empresa)->get();
        $categorias = CategoriaLanc::where('id_empresa', Auth::user()->id_empresa)->get();
        $subcategorias = SubCategoriaLanc::where('id_empresa', Auth::user()->id_empresa)->get();
        return view('content.financeiros.lancamentos.index', compact('lancamentos', 'centro_custos', 'contas', 'categorias', 'subcategorias', 'pessoas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = Pessoa::where('id_rotulo', '2')->get();

        return view('content.financeiros.lancamentos.create', compact('clientes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $date = Carbon::create($request->pri_vencimento, 'America/Sao_Paulo');
        $lancamento                   =  new Lancamento;
        $lancamento->id_cli           = $request->id_cli;
        $lancamento->id_empresa       = Auth::user()->id_empresa;
        $lancamento->descricao        = $request->descricao;
        $lancamento->id_plano_contas  = $request->id_plano_contas;
        $lancamento->id_centro_custo  = $request->id_centro_custo;
        $lancamento->id_categoria     = $request->id_categoria;
        $lancamento->id_subcategoria  = $request->id_subcategoria;
        $lancamento->tipo             = $request->tipo;
        $lancamento->valor_lancamento = $request->valor_lancamento;
        $lancamento->parcela          = $request->parcela;
        $lancamento->status           = 'Ativo';
        $lancamento->save();


        /** INSERIR ITENS NO LANÇAMENTO */

        if ($request->parcela === 'A Vista') {
            $lancamento_itens                     =  new Lancamento_itens;
            $lancamento_itens->id_lancamento      = $lancamento->id;
            $lancamento_itens->parcela            = $request->parcela;
            $lancamento_itens->valor_amoretizado  = '0.00';
            $lancamento_itens->valor_parcela      = $request->valor_parcela;
            $lancamento_itens->data_pagto         = $date;


            if ($request->tipo == 'Despesa') {
                $lancamento_itens->status           = 'Pago';
            } else {
                $lancamento_itens->status           = 'Recebido';
            }
            $lancamento_itens->save();

            if ($request->tipo === 'Receita') {
                return redirect()->route('receitas.index')->with('success', 'store');
            } else {
                return redirect()->route('despesas.index')->with('success', 'store');
            }
        } else {

            for ($i = 0; $i < $request->parcela; $i++) {

                $lancamento_itens                     =  new Lancamento_itens;
                $lancamento_itens->id_lancamento      = $lancamento->id;
                $lancamento_itens->parcela            = $i + 1;
                $lancamento_itens->valor_amoretizado  = '0.00';
                $lancamento_itens->valor_parcela      = $request->valor_parcela;
                
                if($lancamento_itens->parcela == 1){
                    $lancamento_itens->data_pagto = $date;
                }else{
                    if ($request->intervalo == '30') {
                        $lancamento_itens->data_pagto = $date->addMonth();
                    } elseif ($request->intervalo == '15') {
                        $lancamento_itens->data_pagto = $date->add(15, 'day');
                    } else {
                        $lancamento_itens->data_pagto = $date->add(7, 'day');
                    }
                }
                
                if ($request->tipo == 'Despesa') {
                    $lancamento_itens->status           = 'A Pagar';
                } else {
                    $lancamento_itens->status           = 'A Receber';
                }
                $lancamento_itens->save();
            }

            if ($request->tipo === 'Receita') {
                return redirect()->route('receitas.index')->with('success', 'store');
            } else {
                return redirect()->route('despesas.index')->with('success', 'store');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lancamento  $lancamento
     * @return \Illuminate\Http\Response
     */
    public function show(Lancamento $lancamento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lancamento  $lancamento
     * @return \Illuminate\Http\Response
     */
    public function edit(Lancamento $lancamento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lancamento  $lancamento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $lancamento_itens = Lancamento_itens::where('id', $id)->first();
        $lancamento = Lancamento::where('id', $lancamento_itens->id_lancamento)->first();


        $conta_bancaria = ContaBancaria::find($request->conta_bancaria);
        $conta_bancaria_itens = new ContaBancariaItens();

        $conta_bancaria_itens->id_conta_bancaria = $conta_bancaria->id;
        $conta_bancaria_itens->data_pagto        = Carbon::now();
        $conta_bancaria_itens->id_fatura         = $id;
        $conta_bancaria_itens->valor             = $request->valor_parcela;
        $conta_bancaria_itens->tipo              = $lancamento->tipo;

        $conta_bancaria_itens->tipo  == 'Receita' ? $conta_bancaria->saldo = floatval($conta_bancaria_itens->valor) + floatval($conta_bancaria->saldo) : $conta_bancaria->saldo = floatval($conta_bancaria->saldo) - floatval($conta_bancaria_itens->valor);
        
        $conta_bancaria_itens->save();
        $conta_bancaria->save();

        if ($lancamento->tipo == 'Receita') {
            if ($request->valor_a_pagar === $request->valor_parcela) {
                $lancamento_itens->valor_amoretizado   = $request->valor_a_pagar;
                $lancamento_itens->status = 'Recebido';
            } else {
                $lancamento_itens->valor_amoretizado   = $request->valor_a_pagar;
                $lancamento_itens->valor_parcela       = number_format($lancamento_itens->valor_parcela - $request->valor_a_pagar, 2, '.', '');
                $lancamento_itens->data_pagto          = Carbon::now();
                $lancamento_itens->status              = 'Pendente';
            }
            $lancamento_itens->save();
            return redirect()->route('pdf.recibo', $lancamento_itens->id);
        } else {
            if ($request->valor_a_pagar === $request->valor_parcela) {
                $lancamento_itens->valor_amoretizado   = $request->valor_a_pagar;
                $lancamento_itens->status = 'Pago';
            } else {
                $lancamento_itens->valor_amoretizado   = $request->valor_a_pagar;
                $lancamento_itens->valor_parcela       = number_format($lancamento_itens->valor_parcela - $request->valor_a_pagar, 2, '.', '');
                $lancamento_itens->data_pagto          = Carbon::now();
                $lancamento_itens->status              = 'Pendente';
            }
            
            $lancamento_itens->save();
            return redirect()->route('despesas.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lancamento  $lancamento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lancamento $lancamento)
    {
        //
    }

}
