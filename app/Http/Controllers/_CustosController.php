<?php

namespace App\Http\Controllers;

use App\Models\Custos;
use App\Models\Pessoa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustosController extends Controller
{

    public function index()
    {
        $custos = Custos::where('id_empresa',Auth::user()->id_empresa)->get();
        $custofv = Custos::where('id_empresa',Auth::user()->id_empresa)->where(function ($query) {
            $query->where('tipo', 'variavel')->orWhere('tipo', 'fixo');
        });
        $pagamento = Custos::where('id_empresa',Auth::user()->id_empresa)->where(function ($query) {
            $query->where('tipo', 'operacional');
        })->get();
        $clientes = Pessoa::where('id_empresa',Auth::user()->id_empresa)
        ->where(function ($query) {
            $query->where('rotulo', 'clientes')->orWhere('rotulo', 'empresa');
        })->get();
        $base_custos = 0;
        $base_operacional = $pagamento->sum('valor') / 25;
        if(count($clientes) < 10 ){
            $base_custos =  $custofv->sum('valor') / 10;
        }else{
            $base_custos =  $custofv->sum('valor') / count($clientes);
        }

        return view('content.financeiros.custos.index', compact('custos', 'base_custos', 'base_operacional'));
    }


    public function create()
    {
       return view('content.financeiros.custos.create');
    }


    public function store(Request $request)
    {
        $custo = new Custos;
        $custo->descricao = $request->descricao;
        $custo->tipo = $request->tipo;
        $custo->valor = $request->valor;
        $custo->id_empresa = Auth::user()->id_empresa;
        $custo->save();

        return redirect()->route('custos.index')->with('success','store');
    }


    public function show(Custos $custos)
    {
        //
    }


    public function edit(Custos $custo)
    {
        return view('content.financeiros.custos.edit', compact('custo'));
    }


    public function update(Request $request, Custos $custo)
    {
        $custo->descricao = $request->descricao;
        $custo->tipo = $request->tipo;
        $custo->valor = $request->valor;
        $custo->id_empresa = Auth::user()->id_empresa;
        $custo->save();

        return redirect()->route('custos.index')->with('success','update');
    }


    public function destroy(Custos $custos)
    {

    }
}
