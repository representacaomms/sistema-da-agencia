<?php

namespace App\Http\Controllers;

use App\Models\GestaoAnuncio;
use App\Models\Pessoa;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GestaoAnuncioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->hasPermissionTo('Listar Trafego Pago')) {

        return redirect()->route('plataformas.show', Auth::user()->id_empresa);
        }   
        $records = collect(GestaoAnuncio::all())->groupBy('id_cliente');

      
        foreach ($records as $key => $value) {
            $value['cliente'] =  $value[0]->cliente->fantasia;
          
        }

        return view('content.gestao-anuncios.index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = Pessoa::where('id_empresa', Auth::user()->id_empresa)
        ->where(function ($query) {
            $query->where('rotulo', 'clientes');
        })->where(function ($query) {
            $query->where('status', 'A');
        })
        ->get();

        return view('content.gestao-anuncios.create', compact('clientes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $gestao = new GestaoAnuncio();
        $gestao->data_lancamento = $request->data_lancamento;
        $gestao->id_cliente = $request->id_cliente;
        $gestao->gestor = $request->vendedor;
        $gestao->plataforma = $request->plataforma;
        $gestao->nome_campanha = $request->nome_campanha;
        $gestao->tipo_campanha = $request->tipo_campanha;
        $gestao->impressoes = $request->impressoes;
        $gestao->custo_total = $request->custo_total;
        $gestao->cliques = $request->cliques;
        $gestao->conversoes = $request->conversoes;
        $gestao->cpc_medio =  number_format(intval($gestao->custo_total) / intval($gestao->cliques), 2);
        $gestao->ctr =   number_format(intval($gestao->impressoes) / intval($gestao->cliques),2);
        $gestao->taxa_conversao =   number_format(intval($gestao->cliques) / intval($gestao->conversoes),2);
        $gestao->custo_conversao =   number_format(intval($gestao->custo_total) / intval($gestao->conversoes),2);

        $gestao->save();

        return redirect()->route('gestao-anuncios.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GestaoAnuncio  $gestaoAnuncio
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $record = GestaoAnuncio::where('id_cliente', $id)->get();
        return view('content.gestao-anuncios.grafico', compact('record'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GestaoAnuncio  $gestaoAnuncio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $records = GestaoAnuncio::where('id', $id)->get();
        return $records;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GestaoAnuncio  $gestaoAnuncio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GestaoAnuncio $gestaoAnuncio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GestaoAnuncio  $gestaoAnuncio
     * @return \Illuminate\Http\Response
     */
    public function destroy(GestaoAnuncio $gestaoAnuncio)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GestaoAnuncio  $gestaoAnuncio
     * @return \Illuminate\Http\Response
     */
    public function relatorio($id)
    {
        $record = GestaoAnuncio::where('id_cliente', $id)->first();
        return view('content.pdfs.relatorio_mkt', compact('record'));
    }

    
}
