<?php

namespace App\Http\Controllers;

use App\Models\BaseCusto;
use App\Models\Salario;
use App\Models\ServicoAgenciado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServicoAgenciadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicos_agenciado = ServicoAgenciado::where('id_empresa', Auth::user()->id_empresa)->where('categoria', '<>', 'gestao')->get();
        return view('content.financeiros.servicos_agenciado.index', compact('servicos_agenciado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $base_custos = BaseCusto::where('id_empresa', Auth::user()->id_empresa)->first();
        $salarios = Salario::where('id_empresa', Auth::user()->id_empresa)->get();
        foreach ($salarios as $key => $value) {
            $salarios[$key]->base = number_format( (($value->salario + $value->total_provisoes) /20) /6.4, 2, '.', '');
        }

        return view('content.financeiros.servicos_agenciado.create', compact('salarios', 'base_custos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $servicos_agenciado = new ServicoAgenciado();
        $servicos_agenciado->id_empresa  = Auth::user()->id_empresa;
        $servicos_agenciado->descricao = $request->descricao;
        $servicos_agenciado->categoria = $request->categoria;
        $servicos_agenciado->base = $request->base;
        $servicos_agenciado->esforco_hrs = $request->esforco_hrs;
        $servicos_agenciado->esforco_valor = $request->esforco_valor;
        $servicos_agenciado->valor_individual = $request->valor_individual;
        $servicos_agenciado->situacao = $request->situacao;
        $servicos_agenciado->escopo = $request->escopo;
        $servicos_agenciado->save();

        return redirect()->route('servicos-agenciado.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Servico  $servico
     * @return \Illuminate\Http\Response
     */
    public function show(Servico $servico)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Servico  $servico
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $valor_total_base = 0;
        $servicos_agenciado = ServicoAgenciado::where('id', $id)->first();
        $salarios = Salario::where('id_empresa', Auth::user()->id_empresa)->get();
        foreach ($salarios as $key => $value) {
            $salarios[$key]->base = number_format( (($value->salario + $value->total_provisoes) /20) /6.4, 2, '.', '');

            $valor_total_base = $valor_total_base + $salarios[$key]->base;
        }
        $servicos_agenciado->salario = $servicos_agenciado->base;
        $servicos_agenciado->base = number_format($valor_total_base / count($salarios),2);


        $servicos_agenciado->esforco_valor = $servicos_agenciado->base *  $servicos_agenciado->esforco_hrs;
        $base_custos = BaseCusto::where('id_empresa', Auth::user()->id_empresa)->first();


        return view('content.financeiros.servicos_agenciado.edit', compact('servicos_agenciado', 'salarios', 'base_custos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Servico  $servico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $servicos_agenciado = ServicoAgenciado::where('id', $id)->first();
        $servicos_agenciado->id_empresa  = Auth::user()->id_empresa;
        $servicos_agenciado->descricao = $request->descricao;
        $servicos_agenciado->categoria = $request->categoria;
        $servicos_agenciado->base = $request->base;
        $servicos_agenciado->esforco_hrs = $request->esforco_hrs;
        $servicos_agenciado->esforco_valor = $request->esforco_valor;
        $servicos_agenciado->valor_individual = $request->valor_individual;
        $servicos_agenciado->situacao = $request->situacao;
        $servicos_agenciado->escopo = $request->escopo;
        $servicos_agenciado->save();

        return redirect()->route('servicos-agenciado.index')->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Servico  $servico
     * @return \Illuminate\Http\Response
     */
    public function destroy(Servico $servico)
    {
        //
    }
}
