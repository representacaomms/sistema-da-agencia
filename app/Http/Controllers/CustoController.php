<?php

namespace App\Http\Controllers;

use App\Models\BaseCusto;
use App\Models\Custo;
use App\Models\Pessoa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $custos = Custo::where('id_empresa',Auth::user()->id_empresa)->get();
        $despesas_fixas = Custo::where('id_empresa',Auth::user()->id_empresa)->sum('valor');
        $clientes = Pessoa::where('id_empresa',Auth::user()->id_empresa)->where('rotulo', 'clientes')->where('status', 'ativo')->count();
        $base_custos = BaseCusto::where('id_empresa',Auth::user()->id_empresa)->first();

        return view('content.financeiros.custos.index', compact('custos', 'despesas_fixas', 'clientes', 'base_custos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.financeiros.custos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $custo = new Custo();
        $custo->id_empresa = Auth::user()->id_empresa;
        $custo->nome       = $request->nome;
        $custo->tipo       = $request->tipo;
        $custo->valor      = $request->valor;
        $custo->save();

        return redirect()->route('custos.index')->with('success','store');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Custo  $custo
     * @return \Illuminate\Http\Response
     */
    public function show(Custo $custo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Custo  $custo
     * @return \Illuminate\Http\Response
     */
    public function edit(Custo $custo)
    {
        return view('content.financeiros.custos.edit', compact('custo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Custo  $custo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Custo $custo)
    {
        $custo->id_empresa = Auth::user()->id_empresa;
        $custo->nome       = $request->nome;
        $custo->tipo       = $request->tipo;
        $custo->valor      = $request->valor;
        $custo->save();

        return redirect()->route('custos.index')->with('success','update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Custo  $custo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Custo $custo)
    {
        //
    }
}
