<?php

namespace App\Http\Controllers;

use App\Models\ItensCompra;
use App\Models\Compra;
use App\Models\Estoque;
use App\Models\Pessoa;
use App\Models\Produto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $compras =  Compra::where('id_empresa',Auth::user()->id_empresa)->get();
        return view('content.compras.index', compact('compras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fornecedor = Pessoa::where('id_empresa',Auth::user()->id_empresa)
        ->where('rotulo', 'fornecedores')
        ->get();
        $produtos = Produto::where('id_empresa',Auth::user()->id_empresa)
        ->where('situacao', 'Ativo')
        ->get();


        return view('content.compras.create', compact('fornecedor', 'produtos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $compra['id_empresa']      = Auth::user()->id_empresa;
        $compra['id_fornecedor']   = $request->fornecedor;
        $compra['total_compra']    = $request->total_compra;
        $compra['status']          = 'Pendente';

        $compras = Compra::create($compra);
        $relProdutos = $request->relPodutos;

        $dados = json_encode($relProdutos);

        foreach ($dados as $key => $value) {
            $item['id_compra']   = $compras->id;
            $item['id_produto']  = $value->produto;
            $item['qtde']        = $value->quantidade;
            $item['valor_unit']  = $value->valor;
            $item['valor_total'] = $value->valor_total;
            $item['status']      = 'Pendente';

            ItensCompra::create($item);
        }

        return redirect()->route('compras.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return $request->all();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function edit(Compra $compra)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Compra $compra)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function destroy(Compra $compra)
    {
        //
    }

    public function upload(Request $request)
    {

        $arquivo = $request->arquivo;

        if(isset($arquivo)){

            $data = file_get_contents($arquivo);
            $xml = simplexml_load_string($data);

            $produtos = [];
            $relprod = $xml->NFe->infNFe->det;
            $id_nota = $xml->NFe->infNFe->ide->cNF;
            $total_nota = $xml->NFe->infNFe->total->ICMSTot->vProd;

            $compra = Compra::where('id_cNF', $id_nota)->get();

            if(count($compra) == 0){
                $compra = new Compra();
                $compra->id_cNF       = $id_nota;
                $compra->id_empresa   = Auth::user()->id_empresa;
                $compra->total_compra = $total_nota;
                $compra->status       = 'Comcluida';
                $compra->save();

            } else {
                return redirect()->route('compras.index')->with('success', 'destroy');
            }


            foreach ($relprod as $key => $value) {
                array_push($produtos,$value->prod);
            }

            $produtosBD = Produto::where('id_empresa', Auth::user()->id_empresa)->get();

            if(count($produtosBD) == 0){
                foreach ($produtos as $key => $request) {
                    $produto = new Produto;
                    $produto->id_empresa     = Auth::user()->id_empresa;
                    $produto->descricao      = $request->xProd;
                    $produto->preco_custo    = $request->vProd;
                    $produto->preco_venda    = $request->preco_venda;
                    $produto->ncm            = $request->NCM;
                    $produto->ean            = $request->cEAN;
                    $produto->unidade        = $request->uCom;
                    $produto->regra_padrao   = 'Padrão';
                    $produto->situacao       = 'Ativo';
                    $produto->save();
                    $request->id = $produto->id;

                    $estoque = new Estoque();
                    $estoque->id_empresa     = Auth::user()->id_empresa;
                    $estoque->id_produto     = $produto->id;
                    $estoque->estoque_minimo = '10';
                    $estoque->estoque        = $request->qCom;
                    $estoque->save();

                }

            }else{
                foreach ($produtosBD as $key => $prod) {
                    foreach ($produtos as $key => $request) {
                        if($prod->descricao == $request->xProd){

                            $prod->descricao      = $request->xProd;
                            $prod->preco_custo    = $request->vProd;
                            $prod->preco_venda    = $request->preco_venda;
                            $prod->ncm            = $request->NCM;
                            $prod->ean            = $request->cEAN;
                            $prod->unidade        = $request->uCom;
                            $prod->regra_padrao   = $prod->regra_padrao;
                            $prod->situacao       = $prod->situacao;
                            $prod->save();
                            $request->id = $prod->id;

                            $estoque = Estoque::where('id_produto', $prod->id)->first();
                            $estoque->estoque = $estoque->estoque + $request->qCom;
                            $estoque->save();
                        }
                        else{

                            $produto = new Produto;
                            $produto->id_empresa     = Auth::user()->id_empresa;
                            $produto->descricao      = $request->xProd;
                            $produto->preco_custo    = $request->vProd;
                            $produto->preco_venda    = $request->preco_venda;
                            $produto->ncm            = $request->NCM;
                            $produto->ean            = $request->cEAN;
                            $produto->unidade        = $request->uCom;
                            $produto->regra_padrao   = 'Padrão';
                            $produto->situacao       = 'Ativo';
                            $produto->save();
                            $request->id = $prod->id;

                            $estoque = new Estoque();
                            $estoque->id_empresa     = Auth::user()->id_empresa;
                            $estoque->id_produto     = $produto->id;
                            $estoque->estoque_minimo = '10';
                            $estoque->estoque        = $request->qCom;
                            $estoque->save();

                        }
                    }
                }

            }


            return view('content.compras.xml', compact( 'produtos'));

        }else{
            $fornecedor = Pessoa::where('id_empresa',Auth::user()->id_empresa)->where('rotulo', 'clientes')->where('rotulo', 'fornecedores')->get();;
            $produtos = Produto::where('id_empresa', Auth::user()->id_empresa)->get();
            return view('content.compras.create', compact('fornecedor', 'produtos'));
        }

    }

    public function addProduto($id)
    {

        return $id;


    }
}
