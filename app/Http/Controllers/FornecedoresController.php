<?php

namespace App\Http\Controllers;

use App\Models\Contatos;
use App\Models\Endereco;
use App\Models\Pessoa;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FornecedoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = Pessoa::where('id_empresa',Auth::user()->id_empresa)->where('rotulo', 'fornecedores')->get();
        return view('content.fornecedores.index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.fornecedores/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $fornecedor = new Pessoa();
        $fornecedor->id_empresa   = Auth::user()->id_empresa;
        $fornecedor->nome     = $request->nome;
        $fornecedor->tipo     = $request->tipo;
        if($fornecedor->tipo == 'pf'){
            $fornecedor->rg       = $request->rg;
            $fornecedor->cpf      = $request->cpf;
        }else{
            $fornecedor->rg       = $request->insc_est;
            $fornecedor->cpf      = $request->cnpj;
        }
        $fornecedor->fantasia = $request->fantasia;
        $fornecedor->email    = $request->email;
        $fornecedor->celular  = $request->celular;
        $fornecedor->telefone = $request->telefone;
        $fornecedor->status   = $request->status;
        $fornecedor->rotulo   = 'fornecedores';
        $fornecedor->save();

        $endereco = new Endereco();
        $endereco->cep         = $request->cep;
        $endereco->logradouro  = $request->logradouro;
        $endereco->complemento = $request->complemento;
        $endereco->numero      = $request->numero;
        $endereco->bairro      = $request->bairro;
        $endereco->localidade  = $request->localidade;
        $endereco->uf          = $request->uf;
        $endereco->id_empresa     = $fornecedor->id;
        $endereco->save();

        return redirect()->route('fornecedores.index')->with('success', 'store');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Pessoa $fornecedore)
    {
        if($fornecedore->tipo === 'pf'){
            return view('content.fornecedores.edit', compact('fornecedore'));
        } else {
            return view('content.fornecedores.edit_pj', compact('fornecedore'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pessoa $fornecedor)
    {

        $fornecedor->id       = $request->id;
        $fornecedor->nome     = $request->nome;
        $fornecedor->tipo     = $request->tipo;
        $fornecedor->rg       = $request->rg;
        $fornecedor->cpf      = $request->cpf;
        $fornecedor->fantasia = $request->fantasia;
        $fornecedor->email    = $request->email;
        $fornecedor->celular  = $request->celular;
        $fornecedor->telefone = $request->telefone;
        $fornecedor->status   = $request->status;
        $fornecedor->rotulo   = 'fornecedores';
        $fornecedor->update();

        $endereco = Endereco::where('id_empresa',$fornecedor->id)->first();

        $endereco->cep         = $request->cep;
        $endereco->logradouro  = $request->logradouro;
        $endereco->complemento = $request->complemento;
        $endereco->numero      = $request->numero;
        $endereco->bairro      = $request->bairro;
        $endereco->localidade  = $request->localidade;
        $endereco->uf          = $request->uf;
        $endereco->save();

        return redirect()->route('fornecedores.index')->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pessoa $fornecedor)
    {
        if($fornecedor->status == 'A'){
            $fornecedor->status = 'I';
            $fornecedor->update();
            return redirect()->route('fornecedores.index')->with('success', 'destroy');
        }else{
            $fornecedor->status = 'A';
            $fornecedor->update();
            return redirect()->route('fornecedores.index')->with('success', 'active');
        }




    }
}
