<?php

namespace App\Http\Controllers;

use App\Models\Prospeccao;
use App\Models\Conversa;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SheetDB\SheetDB;
use Illuminate\Support\Collection;

class ProspeccaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $SheetDB = new SheetDB('8toh8z5kghryz');
        $prospectos = $SheetDB->get();
        if($prospectos){
            foreach($prospectos as $item){
                $prospecto = new Prospeccao();
                $prospecto->id_empresa   = Auth::user()->id_empresa;
                $prospecto->nome         = $item->NOME;
                $prospecto->email        = $item->EMAIL;
                $prospecto->empresa      = $item->EMPRESA;
                $prospecto->telefone     = $item->CELULAR;
                $prospecto->site         = $item->SITE;
                $prospecto->status       = 'inbound';
                $prospecto->tipo         = 'p';
                $prospecto->save();
                $SheetDB->delete('NOME',$item->NOME);
            }
        }
        
       
        $dados = collect(Prospeccao::all());
        $prospeccoes = $dados->sortBy([
            ['updated_at', 'desc']
        ]);
        
        $prospeccoes->values()->all();
        $conversas = Conversa::all();
        
        foreach($prospeccoes as $prospeccao){
             $prospeccao->telefone = preg_replace('/[^0-9]/', '', $prospeccao->telefone);
         }
        
        return view('content.prospeccoes.index', compact('prospeccoes', 'conversas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('content.prospeccoes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $prospeccao = new Prospeccao;
        $prospeccao->id_empresa = Auth::user()->id_empresa;
        $prospeccao->tipo = $request->tipo;
        $prospeccao->nome = $request->nome;
        $prospeccao->funcionarios = $request->funcionario;
        $prospeccao->empresa = $request->empresa;
        $prospeccao->site = $request->site;
        $prospeccao->status = $request->status;
        $prospeccao->email = $request->email;
        $prospeccao->telefone = $request->celular;
        $prospeccao->save();

        return redirect()->route('prospeccoes.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CentroCusto  $centroCusto
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CentroCusto  $centroCusto
     * @return \Illuminate\Http\Response
     */
    public function edit(Prospeccao $prospeccao)
    {
        
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CentroCusto  $centroCusto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $prospeccao = Prospeccao::find($id);
       
        $prospeccao->status = $request->status;
       
        $prospeccao->save();
        
        $retorno = $id;
        echo json_encode($retorno);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CentroCusto  $centroCusto
     * @return \Illuminate\Http\Response
     */
    public function destroy(CentroCusto $centroCusto)
    {
        
    }
}
