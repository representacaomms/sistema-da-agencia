<?php

namespace App\Http\Controllers;

use App\Models\ItemProduto;
use App\Models\Produto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;

class ItemProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('content.produtos.item.create', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $itemProduto = new ItemProduto;

        $itemProduto->nome = $request->nome;
        $itemProduto->id_produto = $request->id_produto;
        $itemProduto->descricao = $request->descricao;
        $itemProduto->valor = $request->valor;
        $itemProduto->situacao = $request->situacao;
        $itemProduto->save();

        return redirect()->route('produtos.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemProduto = ItemProduto::where('id_produto', $id)->first();
        return view('content.produtos.item.edit', compact('itemProduto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produto = Produto::where('id', $id)->first();
        $produto->descricao = $request->descricao;
        $produto->beneficios = $request->beneficios;
        $produto->causa = $request->causas;
        $produto->situacao = $request->situacao;
        $produto->valor = 0.00;
        $produto->save();

        return redirect()->route('produtos.index')->with('success', 'update');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
