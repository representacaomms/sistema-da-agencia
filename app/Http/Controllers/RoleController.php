<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $records = Role::all();
       return view('content.funcoes.index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.funcoes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $record = new Role();
        $record->name = $request->name;
        $record->save();
        return redirect()->route('funcoes.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $record = Role::where('id', $id)->first();
        return view('content.funcoes.edit', compact('record'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $record = Role::where('id', $id)->first();
        $record->name = $request->name;
        $record->save();
        return redirect()->route('funcoes.index')->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = Role::where('id', $id)->first();
        $record->delete();
        return redirect()->route('funcoes.index')->with('success', 'destroy');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function permissoes($id)
    {
        $funcao = Role::where('id', $id)->first();
        $permissoes = Permission::all();

        foreach ($permissoes as $permissao) {
            if ($funcao->hasPermissionTo($permissao->name)) {
                $permissao->can = true;
            } else {
                $permissao->can = false;
            }
        }
        return view('content.funcoes.permissoes', compact('funcao', 'permissoes'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function permissoesSync(Request $request, $id)
    {
        $permissoesSelecionadas = $request->except(['_token', '_method']);
        foreach ($permissoesSelecionadas as $key => $value) {
            $funcoes[] = Permission::where('id',$key)->first();
        }
        $funcao = Role::where('id', $id)->first();

        if(!empty($funcoes)){
            $funcao->syncPermissions($funcoes);
        }else{
            $funcao->syncPermissions(null);
        }
        
        return redirect()->route('funcoes.permissoes', $funcao->id);
    }
}
