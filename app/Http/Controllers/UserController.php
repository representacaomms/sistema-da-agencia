<?php

namespace App\Http\Controllers;

use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = User::all();
        
        return view('content.usuarios.index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.usuarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $usuario = new User();
       $usuario->id_empresa = 1;
       $usuario->name = $request->name;
       $usuario->email = $request->email;
       $usuario->password = Hash::make($request->password);;
       $usuario->celular = 'null';
       $usuario->acesso = 'null';
       $usuario->permissao = 'null';
       $usuario->funcao = 'null';
       $usuario->save();
       return redirect()->route('usuarios.index')->with('success', 'store');
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::where('id', $id)->first();
        return view('content.usuarios.edit', compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $usuario)
    {

        $usuario->id_empresa = 1;
        $usuario->name = $request->name;
        $usuario->email = $request->email;

        if (!empty($request->password)) {
            $usuario->password = Hash::make($request->password);
        }
        $usuario->celular = 'null';
        $usuario->acesso = 'null';
        $usuario->permissao = 'null';
        $usuario->funcao = 'null';
        $usuario->save();
        return redirect()->route('usuarios.index')->with('success', 'update');
       /*  if($request->password){
            $validator = Validator::make($request->all(), [
                'password' => ['string', 'min:8', 'confirmed'],
            ]);

            if ($validator->fails()) {
                return  redirect()->route('perfil.index')->with('success', 'error')->withErrors($validator)->withInput();
            }
             $perfil->password = Hash::make($request->password);
        }

        $perfil->celular = $request->celular ?? 'null';
        $perfil->funcao   = $request->funcao ?? 'null';
        $perfil->update();

        return redirect()->route('usuarios.index')->with('success', 'update'); */
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::where('id', $id)->first();
        $usuario->delete();

        return redirect()->route('usuarios.index')->with('success', 'destroy');

    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function funcoes($id)
    {
        $usuario = User::where('id', $id)->first();
        $funcoes = Role::all();

        
        foreach ($funcoes as $funcao) {
            if ($usuario->hasRole($funcao->name)) {
                $funcao->can = true;
            } else {
                $funcao->can = false;
            } 
        }

        return view('content.usuarios.funcoes', compact('usuario', 'funcoes'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function funcoesSync(Request $request, $id)
    {
        $funcoesSelecionadas = $request->except(['_token', '_method']);
        foreach ($funcoesSelecionadas as $key => $value) {
            $funcoes[] = Role::where('id',$key)->first();
        }
        $usuario = User::where('id', $id)->first();

        if(!empty($funcoes)){
            $usuario->syncRoles($funcoes);
        }else{
            $usuario->syncRoles(null);
        }
        
        return redirect()->route('usuarios.funcoes', $usuario->id);
    }
}
