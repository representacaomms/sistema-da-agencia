<?php

namespace App\Http\Controllers;

use App\Mail\solutiomms;
use App\Models\Pessoa;
use App\Models\Propostas;
use App\Models\Lancamento_itens;
use Illuminate\Http\Request;
use App\Models\Rotulo;
use App\Models\Prospeccao;
use App\Models\ContaBancaria;
use App\User;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Client\Response;


use stdClass;
use \Carbon\Carbon;
use \Carbon\CarbonPeriod;

use Spatie\Permission\Exceptions;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (!Auth::user()->hasPermissionTo('Listar Trafego Pago')) {
            return redirect()->route('plataformas.show', Auth::user()->id_empresa);
        }   
        
       
        $date_String = Carbon::now()->toDateTimeString();
        $date = Carbon::parse($date_String);
        $mes_anterior =  date('m', strtotime('-1 months', strtotime(date('Y-m-d'))));
        $mes_atual = date('m', strtotime(date('Y-m-d')));
        $valor_contas_mes = collect([]);

        /** LISTAR CONTAS */
        
        $lista_contas = Lancamento_itens::all();
      

         /** CONTA BANCÁRIAS */

        $contas_bancaria = ContaBancaria::all();
        
        
         /** CLIENTES */
        $clientes = Pessoa::where('rotulo', 'clientes')->where('status','A')->where('id_empresa', Auth::user()->id_empresa)->count();
        $clientes_mes_atual = Pessoa::where('rotulo', 'clientes')->where('id_empresa', Auth::user()->id_empresa)->whereMonth('created_at', today())->count();
        $clientes_mes_anterior = Pessoa::where('rotulo', 'clientes')->where('id_empresa', Auth::user()->id_empresa)->whereMonth('created_at', $mes_anterior)->count();
        if($clientes_mes_anterior == 0 ){
            $clientes_mes_anterior = 1;
        }

        $clientes_percentual = ( $clientes_mes_atual -  $clientes_mes_anterior) / $clientes_mes_anterior * 100;

        $dados['clientes'] = $clientes;
        $dados['clientes_percentual'] = $clientes_percentual;
       

        /** PROPOSTAS */
        /** TODOS  */
        
        $situacao = ['cancelado', 'pedido','orcamento'];
        $propostas = Propostas::where('situacao',$situacao)->count();
        $propostas_mes_atual = Propostas::where('situacao',$situacao)->whereMonth('created_at', today())->count();
        $propostas_mes_anterior = Propostas::where('situacao',$situacao)->whereMonth('created_at', $mes_anterior)->count();
        if($propostas_mes_anterior == 0 ){
            $propostas_mes_anterior = 1;
        }

        $propostas_percentual = ( $propostas_mes_atual -  $propostas_mes_anterior) / $propostas_mes_anterior * 100;

        $dados['propostas'] = $propostas;
        $dados['propostas_percentual'] = $propostas_percentual; 
        
         /** PERDIDAS */
        $propostas_perdidas = Propostas::where('situacao','cancelado')->count();
        $propostas_perdidas_mes_atual = Propostas::where('situacao','cancelado')->whereMonth('created_at', today())->count();
        $propostas_perdidas_mes_anterior = Propostas::where('situacao','cancelado')->whereMonth('created_at', $mes_anterior)->count();
        if($propostas_perdidas_mes_anterior == 0 ){
           $propostas_perdidas_mes_anterior = 1;
        }

        $propostas_perdidas_percentual = ( $propostas_perdidas_mes_atual -  $propostas_perdidas_mes_anterior) / $propostas_perdidas_mes_anterior * 100;

        $dados['propostas_perdidas'] = $propostas_perdidas;
        $dados['propostas_perdidas_percentual'] = $propostas_perdidas_percentual; 
        
        /** CONCLUIDA */
        $propostas_pedido = Propostas::where('situacao','pedido')->count();
        $propostas_pedido_mes_atual = Propostas::where('situacao','pedido')->whereMonth('created_at', today())->count();
        $propostas_pedido_mes_anterior = Propostas::where('situacao','pedido')->whereMonth('created_at', $mes_anterior)->count();
        if($propostas_pedido_mes_anterior == 0 ){
           $propostas_pedido_mes_anterior = 1;
        }

        $propostas_pedido_percentual = ( $propostas_pedido_mes_atual -  $propostas_pedido_mes_anterior) / $propostas_pedido_mes_anterior * 100;

        $dados['propostas_pedido'] = $propostas_pedido;
        $dados['propostas_pedido_percentual'] = $propostas_pedido_percentual; 
        
        /** ANDAMENTO */
        $propostas_andamento = Propostas::where('situacao','andamento')->count();
        $propostas_andamento_mes_atual = Propostas::where('situacao','andamento')->whereMonth('created_at', today())->count();
       $propostas_andamento_mes_anterior = Propostas::where('situacao','andamento')->whereMonth('created_at', $mes_anterior)->count();
        if($propostas_andamento_mes_anterior == 0 ){
           $propostas_andamento_mes_anterior = 1;
        }

        $propostas_andamento_percentual = ( $propostas_andamento_mes_atual -  $propostas_andamento_mes_anterior) / $propostas_andamento_mes_anterior * 100;

        $dados['propostas_andamento'] = $propostas_andamento;
        $dados['propostas_andamento_percentual'] = $propostas_andamento_percentual; 
      
        
        return view('content.dashboard_ecommerce', compact('dados', 'lista_contas', 'contas_bancaria'));
    }

    public function email()
    {
        /* $user = new User();
        $user->name = Auth::user()->name;
        $user->email = Auth::user()->email;

        Mail::send(new solutiomms($user)); */
    }

    private function mesString($value)
    {
        switch ($value) {
            case '1':
               return $mes = 'Jan';
                break;
            case '2':
                return $mes = 'Fev';
                break;
            case '3':
                return $mes = 'Mar';
                break;
            case '4':
                return $mes = 'Abr';
                break;
            case '5':
                return $mes = 'Mai';
                break;
            case '6':
                return $mes = 'Jun';
                break;
            case '7':
                return $mes = 'Jul';
                break;
            case '8':
                return $mes = 'Ago';
                break;
            case '9':
                return $mes = 'Set';
                break;
            case '10':
                return $mes = 'Out';
                break;
            case '11':
                return $mes = 'Nov';
                break;
            case '12':
                return $mes = 'Dez';
                break;    
           
        }
    }
      private function addMensalidade($value, $lista_mes) 
    {
        $mes = HomeController::mesString(Carbon::parse($value->data_pagto)->month); 
        $array = [$mes => $value->valor_parcela];
        return  $array;
    }

     

}
