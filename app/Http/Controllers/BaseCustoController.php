<?php

namespace App\Http\Controllers;

use App\Models\BaseCusto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BaseCustoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $base_custos = BaseCusto::where('id_empresa', Auth::user()->id_empresa)->get();
        if (count($base_custos) < 1) {
            $base_custos = new BaseCusto();
            $base_custos->id_empresa     = Auth::user()->id_empresa;
            $base_custos->clientes       = $request->clientes;
            $base_custos->valor_despesas = $request->despesas;
            $base_custos->margem_negocio = $request->margem_negocio;
            $base_custos->margem_lucro   = $request->margem_lucro;
            $base_custos->impostos       = $request->impostos;
            $base_custos->onboarding       = $request->onboarding;
            $base_custos->save();
            return redirect()->route('custos.index')->with('success', 'update');
        } else {
            $base_custos[0]->id_empresa     = Auth::user()->id_empresa;
            $base_custos[0]->clientes       = $request->clientes;
            $base_custos[0]->valor_despesas = $request->despesas;
            $base_custos[0]->margem_negocio = $request->margem_negocio;
            $base_custos[0]->margem_lucro   = $request->margem_lucro;
            $base_custos[0]->impostos       = $request->impostos;
            $base_custos[0]->onboarding       = $request->onboarding;
            $base_custos[0]->save();
            return redirect()->route('custos.index')->with('success', 'update');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BaseCusto  $baseCusto
     * @return \Illuminate\Http\Response
     */
    public function show(BaseCusto $baseCusto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BaseCusto  $baseCusto
     * @return \Illuminate\Http\Response
     */
    public function edit(BaseCusto $baseCusto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BaseCusto  $baseCusto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BaseCusto $baseCusto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BaseCusto  $baseCusto
     * @return \Illuminate\Http\Response
     */
    public function destroy(BaseCusto $baseCusto)
    {
        //
    }
}
