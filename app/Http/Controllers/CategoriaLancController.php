<?php

namespace App\Http\Controllers;

use App\Models\CategoriaLanc;
use App\Models\SubCategoriaLanc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoriaLancController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categoria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categorias             = new CategoriaLanc();
        $categorias->id_empresa = Auth::user()->id_empresa;
        $categorias->nome       = $request->nome;
        $categorias->save();

        return redirect()->route('cadastros.diversos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CategoriaLanc  $categoriaLanc
     * @return \Illuminate\Http\Response
     */
    public function show(CategoriaLanc $categoriaLanc)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CategoriaLanc  $categoriaLanc
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoriaLanc $categoriaLanc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoriaLanc  $categoriaLanc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoriaLanc $categoriaLanc)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CategoriaLanc  $categoriaLanc
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoriaLanc $categoria)
    {
        $categoria->delete();

        $id_categoria = $categoria->id;
        $subCateg = SubCategoriaLanc::where('id_categoria', $id_categoria)->get();
       
        foreach ($subCateg as $key => $value) {
            $value->delete();
        }
        
        return redirect()->route('cadastros.diversos');
    }
}
