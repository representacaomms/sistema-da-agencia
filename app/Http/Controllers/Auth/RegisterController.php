<?php

namespace App\Http\Controllers\Auth;

use App\Models\Pessoa;
use Illuminate\Support\Str;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'password' => ['required'],            
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],            
            'celular' => ['required', 'string', 'min:9'],
            
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $password            = $data['password'];
        $empresa             =  new Pessoa;
        $empresa->nome       = $data['name'];
        $empresa->email       = $data['email'];
        $empresa->tipo       = 'pj';
        $empresa->cpf        = $data['cpf'];
        $empresa->rotulo     = 'empresa';
        $empresa->id_empresa = '1';
        $empresa->status = 'A';
        $empresa->save();
        
        
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($password),
            'celular' => $data['celular'],
            'acesso' => $password,
            'permissao' => 'Administrador',
            'id_empresa' => $empresa->id,
        ]);

        


    }
}
