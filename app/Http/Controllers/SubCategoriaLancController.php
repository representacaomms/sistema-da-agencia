<?php

namespace App\Http\Controllers;

use App\Models\CategoriaLanc;
use App\Models\SubCategoriaLanc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubCategoriaLancController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = CategoriaLanc::all();
        return view('subcategoria.create', compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subcategoria             = new SubCategoriaLanc();
        $subcategoria->id_empresa = Auth::user()->id_empresa;
        $subcategoria->nome       = $request->nome;
        $subcategoria->save();

        return redirect()->route('cadastros.diversos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubCategoriaLanc  $subCategoriaLanc
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $subcategorias = SubCategoriaLanc::where('id_categoria', $id)->get();
        echo json_encode($subcategorias); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubCategoriaLanc  $subCategoriaLanc
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategoriaLanc $subCategoriaLanc)
    {
        return $subCategoriaLanc;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubCategoriaLanc  $subCategoriaLanc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubCategoriaLanc $subCategoriaLanc)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubCategoriaLanc  $subCategoriaLanc
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategoriaLanc $subCategoriaLanc)
    {
        //
    }
}
