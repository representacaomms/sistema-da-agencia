<?php

namespace App\Http\Controllers;

use App\Models\Consultoria;
use App\Models\ConsultoriaItens;
use App\Models\Pessoa;
use App\Models\Plataforma;
use App\Models\PlataformaItens;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class ConsultoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = Consultoria::where('id_empresa', Auth::user()->id_empresa)->get();
       
        return view('content.consultorias.index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = Pessoa::where('id_empresa', Auth::user()->id_empresa)
        ->where(function ($query) {
            $query->where('rotulo', 'clientes')
                ->orderBy('id', 'desc');
        })->where(function ($query) {
            $query->where('status', 'A');
        })
        ->get();

        $plataformas = Plataforma::all();

        foreach ($plataformas as $key => $value) {
           $value['item'] = $value->plataforma_item;
           foreach ($value['item'] as $k => $val) {
                $val->input_name = str_replace('[', '&', $val->descricao);
                $val->input_name = str_replace(']', '*', $val->input_name);
            }
        }



        return view('content.consultorias.create', compact('clientes', 'plataformas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $plataformaItens =  PlataformaItens::all();
        $resultados= collect();

        foreach ($request->all() as $key => $value) {
            if($key !== '_token' && $key !== '_method' && $key !== 'cnpj' && $key !== 'nome' && $key !== 'email' && $key !== 'telefone' && $key !== 'status'){
                $key = Str::of($key)->replace('_', ' ');

                $resultados->push(['termo'=>$key, 'valor'=>$value]);
            }
        }


       
        $consultoria = new Consultoria();
        $consultoria->id_empresa = Auth::user()->id_empresa;
        $consultoria->status = $request->status;
        $consultoria->cnpj = $request->cnpj;
        $consultoria->nome = $request->nome;
        $consultoria->email = $request->email;
        $consultoria->telefone = $request->telefone;
        $consultoria->save();

        foreach ($resultados as $key => $value) {

            $consultoriaItem                     = new ConsultoriaItens();
            $consultoriaItem->status             = 'Ativo';
            $consultoriaItem->id_consultoria     = $consultoria->id;


            foreach ($plataformaItens as $plataforma) {
                $value['termo'] = str_replace('&', '[', $value['termo']);
                $value['termo'] = str_replace('*', ']', $value['termo']);
                if ($plataforma->descricao == $value['termo']) {
                    

                    $consultoriaItem->id_termo = $plataforma->id;   
                    $consultoriaItem->acao = $value['valor'] ?? 'null';
                } 
            }
            $consultoriaItem->save();
            
        }

        return redirect()->route('consultorias.index')->with('success', 'store');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $dados['consultoria'] = Consultoria::where('status', 'ativo')->where('tipo', 'consultoria')->first();
        $dados['concorrente'] = Consultoria::where('status', 'ativo')->where('tipo', 'concorrente')->get();
        $dados['referencia'] = Consultoria::where('status', 'ativo')->where('tipo', 'referencia')->get();

       
       
        $data = Carbon::now();

        $ano = $data->isoFormat('YY');
        $dia = $data->isoFormat('DD');
        $hora = $data->isoFormat('HH') - 3;
        $mes = $data->isoFormat('MM');
        $minuto = $data->isoFormat('mm');

        $nro_doc = $ano.$mes.'.'.$dia.$hora.$minuto;
        return view('content.consultorias.resumo', compact('dados', 'nro_doc'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $record = Consultoria::where('id', $id)->first();
        $plataformas = Plataforma::all();
        $plataformaItens = PlataformaItens::all();

        $dados = collect([]);
        $dados['instagram'] = collect([]);
        $dados['facebook'] = collect([]);
        $dados['youtube'] = collect([]);
        $dados['site'] = collect([]);
        $dados['gmn'] = collect([]);

        foreach ($record->item as $key => $value) {

            foreach ($plataformaItens as $k => $plataformaItem) {
                $plataforma = Str::of($plataformaItem->descricao)->explode(' ')[0];

                $nPlat = Str::of($plataforma)->replace(['[', ']'], '');
                

                if($plataformaItem->id == $value->id_termo){
                    $value->descricao = $plataformaItem->descricao;
                    $value->input_name = str_replace('[', '&', $plataformaItem->descricao);
                    $value->input_name = str_replace(']', '*',  $value->input_name);
                    $dados[Str::lower($nPlat)]->push($value);
                }
            }
        }

        return view('content.consultorias.edit', compact('record', 'dados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Consultoria $consultoria)
    {

        $plataformaItens =  PlataformaItens::all();
        $resultados= collect();

        foreach ($request->all() as $key => $value) {
            if($key !== '_token' && $key !== '_method' && $key !== 'cnpj' && $key !== 'nome' && $key !== 'email' && $key !== 'telefone' && $key !== 'status'){
                $key = Str::of($key)->replace('_', ' ');

                $resultados->push(['termo'=>$key, 'valor'=>$value]);
            }
        }


       
       
        $consultoria->id_empresa = Auth::user()->id_empresa;
        $consultoria->status = $request->status;
        $consultoria->cnpj = $request->cnpj;
        $consultoria->nome = $request->nome;
        $consultoria->email = $request->email;
        $consultoria->telefone = $request->telefone;
        $consultoria->update();

        foreach ($resultados as $key => $value) {

            $consultoriaItem                     = ConsultoriaItens::where('id_consultoria', $consultoria->id)->first();




            $consultoriaItem->status             = 'Ativo';
            $consultoriaItem->id_consultoria     = $consultoria->id;


            foreach ($plataformaItens as $plataforma) {
                $value['termo'] = str_replace('&', '[', $value['termo']);
                $value['termo'] = str_replace('*', ']', $value['termo']);

                
                if ($plataforma->descricao == $value['termo']) {

                    $consultoriaItem->id_termo = $plataforma->id;   
                    $consultoriaItem->acao = $value['valor'] ?? 'null';
                    
                } 
            }
            
            $consultoriaItem->update();

            
        }

        return redirect()->route('consultorias.index')->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get Consult the specified resource from storage.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function consultoria()
    {
        $records = Consultoria::where('id_empresa', Auth::user()->id_empresa)->get();
        return view('content.consultorias.consultoria', compact('records'));
    }

    public function montar(Request $request)
    {
        # concorrentes  id_referencia
        $consultorias = Consultoria::all();

        $dados['consultoria'] = collect([]);
        $dados['concorrente'] = collect([]);
        $dados['referencia'] = collect([]);

        foreach ($consultorias as $key => $value) {
            if ($value->id == $request->id_cliente ) {
                $dados['consultoria'] = $value;
            }
        }

        foreach ($consultorias as $key => $value) {
            foreach ($request->concorrentes as $key => $concorrente) {
                if ($concorrente == $value->id ) {
                    $dados['concorrente']->push($value);
                }
            }
        }

        foreach ($consultorias as $key => $value) {
            if ($value->id == $request->id_referencia ) {

                $dados['referencia'] = $value;
            }
        }

      

        $data = Carbon::now();

        $ano = $data->isoFormat('YY');
        $dia = $data->isoFormat('DD');
        $hora = $data->isoFormat('HH') - 3;
        $mes = $data->isoFormat('MM');
        $minuto = $data->isoFormat('mm');

        $nro_doc = $ano.$mes.'.'.$dia.$hora.$minuto;
       
        return view('content.consultorias.resumo', compact('dados', 'nro_doc'));
    }
    
}
