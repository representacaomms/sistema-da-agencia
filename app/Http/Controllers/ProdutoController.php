<?php

namespace App\Http\Controllers;

use App\Models\Produto;
use App\Models\ServicoAgenciado;
use App\Models\ItemProduto;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produtos = collect(Produto::all());
        
        foreach ($produtos as $key => $value) {
            $item_produto = ItemProduto::where('id_prod', $value->id)->get();
            $valor_total = 0;

            foreach ($item_produto as $key => $item) {
                $servico = ServicoAgenciado::where('id', $item->id)->first();
                $valor_total +=  intVal ($servico->esforco_valor);
            }
            $value['valor_total'] = number_format($valor_total, 2);  
        }

        return view('content.produtos.index', compact('produtos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $servicos = ServicoAgenciado::where('id_empresa', Auth::user()->id_empresa)->where('categoria', '<>', 'gestao')->get();

        return view('content.produtos.create', compact('servicos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        $produto = new Produto;
        $produto->nome = $request->nome;
        $produto->caracteristicas = $request->caracteristicas;
        $produto->status = $request->status;
        $produto->save();

        $listaItens = explode( ',',$request->listaItens);

        foreach ($listaItens as $key => $value) {
            $item_produto = new ItemProduto;
            $item_produto->id_prod = $produto->id;
            $item_produto->id_servico = $value;
            $item_produto->qtde = 1;
            $item_produto->status = 'Ativo';
            $item_produto->save();
        }

        

        return redirect()->route('produtos.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produto = Produto::where('id', $id)->first();
        return view('content.produtos.edit', compact('produto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produto = Produto::where('id', $id)->first();
        $produto->descricao = $request->descricao;
        $produto->beneficios = $request->beneficios;
        $produto->causa = $request->causas;
        $produto->situacao = $request->situacao;
        $produto->valor = 0.00;
        $produto->save();

        return redirect()->route('produtos.index')->with('success', 'update');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
