<?php

namespace App\Http\Controllers;

use App\Models\BaseCusto;
use App\Models\Salario;
use App\Models\PlanoGestao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlanoGestaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $planos = PlanoGestao::all();
        return view('content.planos-gestao.index', compact('planos'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $base_custos = BaseCusto::where('id_empresa', Auth::user()->id_empresa)->first();
        $salarios = Salario::where('id_empresa', Auth::user()->id_empresa)->get();
        foreach ($salarios as $key => $value) {
            $salarios[$key]->base = number_format( (($value->salario + $value->total_provisoes) /20) /6.4, 2, '.', '');
        }

        return view('content.planos-gestao.create', compact('base_custos', 'salarios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $planoGestao = new PlanoGestao();       
        $planoGestao->nome = $request->descricao;
        $planoGestao->escopo = $request->escopo; 
        $planoGestao->hrs = $request->hrs;
        $planoGestao->ca = $request->ca; 
        $planoGestao->infra = $request->infraestrutura; 
        $planoGestao->ga = $request->ga; 
        $planoGestao->fa = $request->fa; 
        $planoGestao->em = $request->em; 
        $planoGestao->rm = $request->rm; 
        $planoGestao->status = $request->situacao; 
        $planoGestao->adicionais = $request->adicionais ?? 'null'; 
        $planoGestao->vp = $request->vp; 
        $planoGestao->vi = $request->vi; 
        $planoGestao->save();


        return redirect()->route('planos-gestao.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PlanoGestao  $planoGestao
     * @return \Illuminate\Http\Response
     */
    public function show(PlanoGestao $planoGestao)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PlanoGestao  $planoGestao
     * @return \Illuminate\Http\Response
     */
    public function edit(PlanoGestao $planoGestao)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PlanoGestao  $planoGestao
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PlanoGestao $planoGestao)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PlanoGestao  $planoGestao
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlanoGestao $planoGestao)
    {
        //
    }
}
