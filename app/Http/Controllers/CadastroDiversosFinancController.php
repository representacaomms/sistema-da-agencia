<?php

namespace App\Http\Controllers;

use App\Models\CategoriaLanc;
use App\Models\CentroCusto;
use App\Models\Conta;
use App\Models\SubCategoriaLanc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CadastroDiversosFinancController extends Controller
{
    public function index()
    {

        $centroCustos = CentroCusto::where('id_empresa',Auth::user()->id_empresa)->get();
        $planoContas = Conta::where('id_empresa',Auth::user()->id_empresa)->get();
        $categorias = CategoriaLanc::where('id_empresa',Auth::user()->id_empresa)->get();
        $subcategorias = SubCategoriaLanc::where('id_empresa',Auth::user()->id_empresa)->get();

        return view('content.financeiros.cadastros.index', compact('categorias', 'subcategorias', 'planoContas', 'centroCustos'));
    }
}
