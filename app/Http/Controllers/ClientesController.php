<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Models\Endereco;
use App\Models\Pessoa;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
require_once('vendor/autoload.php');

$client = new \GuzzleHttp\Client();

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$records = Pessoa::where('id_empresa',Auth::user()->id_empresa)->where('rotulo', 'clientes')->orWhere('rotulo', 'empresa')->get();

     
        
        $response = $client->request('GET', 'https://https//agenciamms-sandbox.api-us1.com.api-us1.com/api/3/contacts?status=-1&orders[email]=ASC', [
          'headers' => [
            'Api-Token' => 'f09bf9ffbe5832b03834d1d962c47a99f2be096593521cbfc19ed80e8faa84e254adaaca',
            'accept' => 'application/json',
          ],
        ]);
        
        dd($response->getBody());


        $records = Pessoa::where('id_empresa',Auth::user()->id_empresa)
        ->where(function ($query) {
            $query->where('rotulo', 'clientes')
                  ->orWhere('rotulo', 'empresa');
        })->where(function ($query){
            $query->where('status', 'A');
        })
        ->get();
        return view('content.clientes.index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $cliente = new Pessoa();
        $cliente->id_empresa   = Auth::user()->id_empresa;
        $cliente->nome         = $request->nome;
        $cliente->tipo         = $request->tipo;
        if($cliente->tipo == 'pf'){
            $cliente->rg       = $request->rg;
            $cliente->cpf      = $request->cpf;
        }else{
            $cliente->rg       = $request->insc_est;
            $cliente->cpf      = $request->cnpj;
        }
        $cliente->fantasia     = $request->fantasia;
        $cliente->email        = $request->email;
        $cliente->celular      = $request->celular;
        $cliente->telefone     = $request->telefone;
        $cliente->status       = $request->status;
        $cliente->rotulo       = 'clientes';
        $cliente->save();

        $endereco = new Endereco();
        $endereco->cep         = $request->cep;
        $endereco->logradouro  = $request->logradouro;
        $endereco->complemento = $request->complemento;
        $endereco->numero      = $request->numero;
        $endereco->bairro      = $request->bairro;
        $endereco->localidade  = $request->localidade;
        $endereco->ibge        = $request->ibge;
        $endereco->uf          = $request->uf;
        $endereco->id_empresa  = $cliente->id;
        $endereco->save();

        return redirect()->route('clientes.index')->with('success', 'store');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 'show';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Pessoa $cliente)
    {

        return view('content.clientes.edit', compact('cliente'));
        if($cliente->tipo === 'pf'){
            return view('content.clientes.edit', compact('cliente'));
        } else {
            return view('content.clientes.edit_pj', compact('cliente'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pessoa $cliente)
    {
        $cliente->nome     = $request->nome;
        $cliente->tipo     = $request->tipo;
        $cliente->rg       = $request->rg;
        $cliente->cpf      = $request->cpf;
        $cliente->fantasia = $request->fantasia;
        $cliente->email    = $request->email;
        $cliente->celular  = $request->celular;
        $cliente->telefone = $request->telefone;
        $cliente->status   = $request->status;
        $cliente->rotulo   = 'clientes';
        $cliente->update();

        $endereco = Endereco::where('id_empresa',$cliente->id)->first();
        if(!empty($endereco)){
            $endereco->cep         = $request->cep;
            $endereco->logradouro  = $request->logradouro;
            $endereco->complemento = $request->complemento;
            $endereco->numero      = $request->numero;
            $endereco->bairro      = $request->bairro;
            $endereco->localidade  = $request->localidade;
            $endereco->ibge        = $request->ibge;
            $endereco->uf          = $request->uf;
            $endereco->save();
        } else {
            $endereco = new Endereco();
            $endereco->cep         = $request->cep;
            $endereco->logradouro  = $request->logradouro;
            $endereco->complemento = $request->complemento;
            $endereco->numero      = $request->numero;
            $endereco->bairro      = $request->bairro;
            $endereco->localidade  = $request->localidade;
            $endereco->ibge        = $request->ibge;
            $endereco->uf          = $request->uf;
            $endereco->id_empresa  = $cliente->id;
            $endereco->save();
        }


        return redirect()->route('clientes.index')->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pessoa $cliente)
    {
        if($cliente->status == 'A'){
            $cliente->status = 'I';
            $cliente->update();
            return redirect()->route('clientes.index')->with('success', 'destroy');
        }else{
            $cliente->status = 'A';
            $cliente->update();
            return redirect()->route('clientes.index')->with('success', 'active');
        }




    }
}
