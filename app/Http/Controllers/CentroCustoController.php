<?php

namespace App\Http\Controllers;

use App\Models\CentroCusto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CentroCustoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('centro_custo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $centroCusto = new CentroCusto();
        $centroCusto->nome = $request->nome;
        $centroCusto->id_empresa = Auth::user()->id_empresa;
        $centroCusto->save();

        return redirect()->route('cadastros.diversos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CentroCusto  $centroCusto
     * @return \Illuminate\Http\Response
     */
    public function show(CentroCusto $centroCusto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CentroCusto  $centroCusto
     * @return \Illuminate\Http\Response
     */
    public function edit(CentroCusto $centroCusto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CentroCusto  $centroCusto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CentroCusto $centroCusto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CentroCusto  $centroCusto
     * @return \Illuminate\Http\Response
     */
    public function destroy(CentroCusto $centroCusto)
    {
        $centroCusto->delete();
        return redirect()->route('cadastros.diversos');
    }
}
