<?php

namespace App\Http\Controllers;

use App\Models\BaseCusto;
use App\Models\ItensPlano;
use App\Models\Plano;
use App\Models\Salario;
use App\Models\ServicoAgenciado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;

class PlanosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $planos = Plano::where('id_empresa', Auth::user()->id_empresa)->get();

        return view('content.financeiros.planos.index', compact('planos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $servicos_agenciado = ServicoAgenciado::where('id_empresa', Auth::user()->id_empresa)->orderby('descricao')->get();
        $salarios = Salario::where('id_empresa', Auth::user()->id_empresa)->get();
        $base_custos = BaseCusto::where('id_empresa', Auth::user()->id_empresa)->first();

        if (count($salarios) > 0) {
            foreach ($salarios as $key => $value) {
                $gestor = (($salarios[1]->salario + $salarios[1]->total_provisoes) / 20) / 4.6;
                $operador = (($salarios[0]->salario + $salarios[0]->total_provisoes) / 20) / 4.6;
            }
        } else {
            $gestor = '0,00';
            $operador = '0,00';
        }

        return view('content.financeiros.planos.create', compact('servicos_agenciado', 'gestor', 'operador', 'base_custos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $plano = new Plano();
        $plano->id_empresa = Auth::user()->id_empresa;
        $plano->nome              = $request->nome;
        $plano->hora_gerenciar    = $request->hora_gerenciar;
        $plano->status            = $request->status;
        $plano->preco_plano_hora  = $request->preco_plano_hora;
        $plano->custo_operacional = $request->custo_operacional;
        $plano->margem_lucro      = $request->margem_lucro;
        $plano->impostos          = $request->impostos;
        $plano->valor_plano       = $request->valor_plano;
        $plano->plano_individual  = $request->valor_plano_individual;
        $plano->save();

        $relProdutos = $request->relProdutos;
        $dados =  json_decode($relProdutos);


        foreach ($dados as $key => $value) {
            $itens_plano = new ItensPlano();
            $itens_plano->id_empresa = $plano->id_empresa;
            $itens_plano->id_plano = $plano->id;
            $itens_plano->id_servico_agenciado = $value->id;
            $itens_plano->valor = $value->valor;
            $itens_plano->qtde = $value->qtde;
            $itens_plano->subtotal = $value->sub_total;
            $itens_plano->save();
        }

        return redirect()->route('planos.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('content.pdfs.resumo', compact('proposta', 'itensProposta', 'cliente', 'endereco', 'total'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $plano = Plano::where('id', $id)->first();
        $itens = ItensPlano::where('id_plano', $id)->get();
        $servicos_agenciado = ServicoAgenciado::where('id_empresa', Auth::user()->id_empresa)->orderby('descricao')->get();
        $salarios = Salario::where('id_empresa', Auth::user()->id_empresa)->get();
        $base_custos = BaseCusto::where('id_empresa', Auth::user()->id_empresa)->first();

        $produtos = collect([]);


        foreach ($itens as $key => $value) {
            $produto['descricao'] = $value->servico->descricao;
            $produto['id'] = $value->servico->id;
            $produto['esforco_valor'] = $value->servico->esforco_valor;
            $produto['valor'] = $value->valor;
            $produto['qtde'] = $value->qtde;
            $produto['sub_total'] = $value->subtotal;

            $produtos->push($produto);
        }
        //$produtos = $produtos->toJson();

        if (count($salarios) > 0) {
            foreach ($salarios as $key => $value) {

                $gestor = (($salarios[1]->salario + $salarios[1]->total_provisoes) / 20) / 4.6;
                $operador = (($salarios[0]->salario + $salarios[0]->total_provisoes) / 20) / 4.6;
            }
        } else {
            $gestor = '0,00';
            $operador = '0,00';
        }

      

        return view('content.financeiros.planos.edit', compact('plano', 'produtos', 'servicos_agenciado', 'gestor', 'operador', 'base_custos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $plano = Plano::where('id', $id)->first();
        $plano->id_empresa = Auth::user()->id_empresa;
        $plano->nome              = $request->nome;
        $plano->hora_gerenciar    = $request->hora_gerenciar;
        $plano->status            = $request->status;
        $plano->preco_plano_hora  = $request->preco_plano_hora;
        $plano->custo_operacional = $request->custo_operacional;
        $plano->margem_lucro      = $request->margem_lucro;
        $plano->impostos          = $request->impostos;
        $plano->valor_plano       = $request->valor_plano;
        $plano->plano_individual  = $request->valor_plano_individual;
        $plano->save();

        $relProdutos = $request->relProdutos;
        $dados =  json_decode($relProdutos);
        ItensPlano::where('id_plano', $id)->delete();

        if ($dados) {
            foreach ($dados as $key => $value) {
                $itens_plano = new ItensPlano();
                $itens_plano->id_empresa = $plano->id_empresa;
                $itens_plano->id_plano = $plano->id;
                $itens_plano->id_servico_agenciado = $value->id;
                $itens_plano->valor = $value->valor;
                $itens_plano->qtde = $value->qtde;
                $itens_plano->subtotal = $value->sub_total;
                $itens_plano->save();
            }
        }



        return redirect()->route('planos.index')->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plano $plano)
    {
        $plano->delete();
        ItensPlano::where('id_plano', $plano->id)->delete();
        return redirect()->route('planos.index')->with('success', 'destroy');
    }
}
