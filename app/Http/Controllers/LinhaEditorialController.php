<?php

namespace App\Http\Controllers;

use App\Models\LinhaEditorial;
use App\Models\Pessoa;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LinhaEditorialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = LinhaEditorial::all();
        return view('content.linha_editorial.index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pessoas = Pessoa::where('id_empresa',Auth::user()->id_empresa)
        ->where(function ($query) {
            $query->where('rotulo', 'clientes');
        })
        ->get();
        return view('content.linha_editorial.create', compact('pessoas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $record = new LinhaEditorial();
        $record->id_cliente = $request->id_cliente;
        $record->PSM = $request->psm;
        $record->publico_alvo = $request->publico_alvo;
        $record->mundo_ideal  = $request->mundo_ideal ;
        $record->promessa     = $request->promessa    ;
        $record->motivo_racional = $request->motivo_racional;
        $record->vantagem_competitiva = $request->vantagem_competitiva;
        $record->posicao_unica_vendas = $request->posicao_unica_venda;
        $record->persona = $request->persona;
        $record->o_que_ele_busca = $request->o_que_ele_busca;
        $record->o_que_ele_teme = $request->o_que_ele_teme;
        $record->o_que_impede_ele = $request->o_que_impede_ele;
        $record->a_b = $request->a_b;
        $record->linha_principal = $request->linha_principal;
        $record->auxiliar1 = $request->auxiliar1;
        $record->auxiliar2 = $request->auxiliar2;
        $record->auxiliar3 = $request->auxiliar3 == null ? 'null': $request->auxiliar3;
        $record->auxiliar4 = $request->auxiliar4 == null ? 'null': $request->auxiliar4;

        $record->save();

        return redirect()->route('linhas-editoriais.index')->with('success', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LinhaEditorial  $linhaEditorial
     * @return \Illuminate\Http\Response
     */
    public function show(LinhaEditorial $linhaEditorial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LinhaEditorial  $linhaEditorial
     * @return \Illuminate\Http\Response
     */
    public function edit(LinhaEditorial $linhaEditorial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LinhaEditorial  $linhaEditorial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LinhaEditorial $linhaEditorial)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LinhaEditorial  $linhaEditorial
     * @return \Illuminate\Http\Response
     */
    public function destroy(LinhaEditorial $linhaEditorial)
    {
        //
    }
}
