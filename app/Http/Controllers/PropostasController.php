<?php

namespace App\Http\Controllers;

use App\Models\Pessoa;
use App\Models\Plano;
use App\Models\Proposta;
use App\Models\ItensPlano;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class PropostasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $propostas = Proposta::all();
        return view('content.propostas.index', compact('propostas'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = Pessoa::where('id_empresa', Auth::user()->id_empresa)
            ->where(function ($query) {
                $query->where('rotulo', 'clientes')
                    ->orWhere('rotulo', 'leads')
                    ->orderBy('id', 'desc');
            })
            ->get();

        $planos = Plano::where('id_empresa', Auth::user()->id_empresa)->get();

        return view('content.propostas.create', compact('planos', 'clientes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $plano = Plano::where('id', $request->plano)->first();
        $proposta = new Proposta;
        $proposta->cliente = $request->cliente;
        $proposta->vendedor = $request->vendedor;
        $proposta->plano = $request->plano;
        $proposta->valor = $plano->valor_plano;
        $proposta->situacao = $request->situacao;
        $proposta->save();

        return redirect()->route('propostas.show', ['proposta' => $proposta->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CentroCusto  $centroCusto
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proposta = Proposta::where('id', $id)->first();
        $proposta->nro_proposta = str_pad($id , 4 , '0' , STR_PAD_LEFT);;
        $plano = Plano::where('id', $proposta->plano)->first();
        $cliente = Pessoa::where('id', $proposta->cliente)->first();
        $servicos = ItensPlano::where('id_plano',  $proposta->plano)->get();
        
        return view('content.pdfs.resumo', compact('plano', 'proposta', 'servicos', 'cliente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CentroCusto  $centroCusto
     * @return \Illuminate\Http\Response
     */
    public function edit(Proposta $proposta)
    {
        
        $planos = Plano::where('id_empresa', Auth::user()->id_empresa)->get();
        $servicos = ItensPlano::where('id_plano',  $proposta->plano)->get();
        $cliente = Pessoa::where('id', $proposta->cliente)->first();

       
        return view('content.propostas.edit', compact('proposta', 'servicos', 'planos','cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CentroCusto  $centroCusto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        
        $proposta = Proposta::where('id', $id)->first();
        $proposta->plano = $request->plano;
        $proposta->situacao = $request->status;
        $plano = Plano::where('id', $proposta->plano)->first();
        $proposta->valor = $plano->valor_plano;
        $proposta->save();

        return redirect()->route('propostas.index')->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CentroCusto  $centroCusto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       return $id;
    }

    public function invoice($id)
    {
       $proposta = Proposta::where('id', $id)->first();


       $date = Carbon::now('America/Sao_Paulo');
       return $date;

       $lancamento                   =  new Lancamento;
       $lancamento->id_cli           = $proposta->cliente;
       $lancamento->id_empresa       = Auth::user()->id_empresa;
       $lancamento->descricao        = 'CONTRATAÇÃO DE PLANOS E GESTÃO DE SERVIÇO.';
       $lancamento->id_plano_contas  = '1';
       $lancamento->id_centro_custo  = '1';
       $lancamento->id_categoria     = '1';
       $lancamento->id_subcategoria  = '5';
       $lancamento->tipo             = 'Receita';
       $lancamento->valor_lancamento = $proposta->valor;
       $lancamento->parcela          = '12';
       $lancamento->status           = 'Ativo';
       $lancamento->save();

       /** INSERIR ITENS NO LANÇAMENTO */
        for ($i = 0; $i < $request->parcela; $i++) {

            $lancamento_itens                     =  new Lancamento_itens;
            $lancamento_itens->id_lancamento      = $lancamento->id;
            $lancamento_itens->parcela            = $i + 1;
            $lancamento_itens->valor_amoretizado  = '0.00';
            $lancamento_itens->valor_parcela      = $proposta->valor / 12;
            
            $lancamento_itens->data_pagto = $date->addMonth();
            $lancamento_itens->status           = 'A Receber';
            $lancamento_itens->save();
        }

        return redirect()->route('receitas.index')->with('success', 'store');
          
       
    }

    
}
