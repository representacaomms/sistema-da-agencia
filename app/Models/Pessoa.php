<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $table  = 'pessoas';
    protected $fillable = ['id_empresa','nome', 'cpf','rg','fantasia', 'rotulo','tipo','status', 'celular', 'telefone', 'email'];

    public function endereco()
    {
        return $this->hasOne('App\Models\Endereco','id_empresa','id');
    }


}
