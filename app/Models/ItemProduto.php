<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemProduto extends Model
{
    protected $table = 'item_produtos';
    protected $fillable = ['id', 'id_prod', 'id_servico', 'qtde', 'status'];
}
