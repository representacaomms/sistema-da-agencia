<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GestaoAnuncio extends Model
{
    protected $table = 'gestao_anuncios';
    protected $fillable = ['id', 'id_cliente','data_lancamento', 'nome_campanha', 'tipo_campanha', 'plataforma', 'custo_total', 'cliques', 'conversoes', 'cpc_medio', ' ctr', 'taxa_conversao', 'custo_conversao'];

    public function cliente()
    {
        return $this->hasOne('App\Models\Pessoa', 'id', 'id_cliente'); 
    }
}
