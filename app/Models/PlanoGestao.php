<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanoGestao extends Model
{
    protected $table  = 'plano_gestaos';
    protected $fillable = ['id', 'nome', 'hrs', 'ca','infra', 'ga', 'fa', 'em', 'rm', 'vp', 'vi', 'adicionais', 'escopo', 'status'];

    /* public function itens()
    {
        return $this->hasMany(ItensPlano::class, 'id_plano', 'id');
    } */
}




