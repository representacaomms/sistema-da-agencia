<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoriaLanc extends Model
{
    protected $fillable = ['nome', 'id_empresa'];

    public function subcategoria($id)
    {
        return $this->hasMany(SubCategoriaLanc::find($id)->nome); 
    }
}
