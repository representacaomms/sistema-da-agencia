<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategoriaLanc extends Model
{
    protected $fillable = ['nome', 'id_empresa'];

    public function categoria()
    {
        return $this->belongsTo('App\Models\CategoriaLanc','id_categoria', 'id');
    }
}
