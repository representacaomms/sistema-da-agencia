<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proposta extends Model
{
    protected $table  = 'proposta';
    protected $fillable = ['id', 'cliente', 'vendedor', 'plano', 'valor', 'situacao'];

    public function item_plano()
    {
        return $this->hasOne('App\Models\Plano', 'id', 'plano');
    }

    public function dados()
    {
        return $this->hasOne(Pessoa::class, 'id', 'cliente');
    }
}
