<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lancamento_itens extends Model
{
    protected $table = 'lancamentos_itens';
    protected $fillable = [
        'id_cli',
        'id_lancamento',
        'valor_lancamento',
        'parcela',
        'valor_amoretizado',
        'valor_parcela',
        'data_pagto',
        'status'
    ];
    public function lancamento()
    {
        return $this->belongsTo('App\Models\Lancamento_itens', 'id_lancamento', 'id'); 
    }
    
}
