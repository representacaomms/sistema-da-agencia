<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = ['titulo', 'descricao', 'img', 'autor', 'categoria'];
}
