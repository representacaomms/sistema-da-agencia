<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigEmpresa extends Model
{
    protected $fillable = ['id', 'id_empresa', 'caminho_certificado', 'senha_cert', 'serie', 'ultNotaFiscalEmit', 'versao', 'regime_tributario', 'atualizacao', 'schemes', 'token_ibtp', 'ambiente', 'contribuinte'];
}
