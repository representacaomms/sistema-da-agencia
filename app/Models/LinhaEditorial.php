<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LinhaEditorial extends Model
{
    protected $table  = 'linhas_editoriais';
    protected $fillable = ['id','id_cliente','PSM','publico_alvo', 'mundo_ideal','promessa','motivo_racional', 'vantagem_competitiva','posicao_unica_vendas','persona', 'o_que_ele_busca', 'o_que_ele_teme', 'o_que_impede_ele', 'a_b'];
}
