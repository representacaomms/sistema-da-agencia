<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItensCompra extends Model
{
    protected $fillable = ['id_empresa', 'id_compra','id_produto', 'qtde', 'valor_unit', 'valor_total', 'status'];
}
