<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServicoAgenciado extends Model
{
    protected $table  = 'servicos_agenciado';
    protected $fillable = ['id_empresa', 'descricao', 'escopo', 'esforco_hrs', 'base', 'esforco_valor', 'valor_individual', 'situacao'];

    
}
