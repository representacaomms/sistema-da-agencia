<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    protected $fillable = ['id_cNF', 'id_empresa', 'id_fornecedor', 'total_compra', 'status'];


    public function fornecedor()
    {
        return $this->hasOne('App\Models\Pessoa','id','id_fornecedor');
    }
}
