<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Prospeccao extends Model
{
    protected $table  = 'prospeccao';
    protected $fillable = ['id', 'id_empresa', 'tipo','status', 'nome','email', 'empresa','telefone', 'site', 'funcionarios'];

   
    public function conversas()
    {
        return $this->hasMany('App\Models\Conversa','prospecto','id');
    }
   


}