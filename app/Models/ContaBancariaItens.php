<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContaBancariaItens extends Model
{
    protected $fillable = ['id_conta_bancaria','data_pagto', 'id_fatura','valor', 'tipo'];
}
