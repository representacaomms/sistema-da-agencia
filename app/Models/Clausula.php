<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clausula extends Model
{
    protected $table  = 'clausulas';
    protected $fillable = ['id_empresa', 'nome', 'descricao'];

    public function clausulas()
    {
        return $this->hasOne('App\Models\Clausula','id','id_clausula');
    }
}
