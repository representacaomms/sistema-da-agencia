<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $table  = 'enderecos';
    protected $fillable = ['id_empresa', 'logradouro', 'numero','cep', 'complemento', 'bairro', 'localidade', 'ibge', 'uf'];

}
