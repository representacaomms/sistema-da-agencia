<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Custos extends Model
{
    protected $fillable = ['tipo', 'descricao','id_empresa', 'valor', 'vencimento'];
}
