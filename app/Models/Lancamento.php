<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lancamento extends Model
{
    protected $fillable = [
        'id_cli', 
        'id_empresa',
        'descricao',
        'id_plano_contas',
        'id_centro_custo',
        'id_categoria',
        'id_subcategoria',
        'tipo',
        'valor_lancamento',
        'parcela',
        'status'
    ];

    public function cliente()
    {
        return $this->hasOne('App\Models\Pessoa', 'id', 'id_cli'); 
    }

    public function plano_conta()
    {
        return $this->hasOne('App\Models\Conta', 'id', 'id_plano_contas');
    }

    public function centro_custo()
    {
        return $this->hasOne('App\Models\CentroCusto', 'id', 'id_centro_custo'); 
    }

    public function categoria()
    {
        return $this->hasOne('App\Models\CategoriaLanc', 'id', 'id_categoria'); 
    }

    public function subcategoria()
    {
        return $this->hasOne('App\Models\SubCategoriaLanc', 'id', 'id_subcategoria'); 
    }

    public function itens()
    {
        return $this->hasMany('App\Models\Lancamento_itens', 'id_lancamento', 'id'); 
    }

    
}
