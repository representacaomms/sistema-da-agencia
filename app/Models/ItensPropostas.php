<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItensPropostas extends Model
{
    protected $table  = 'iten_propostas';
    protected $fillable = ['id_proposta', 'produto', 'quantidade', 'valor', 'valor_total'];
}