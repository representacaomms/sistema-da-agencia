<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salario extends Model
{
    protected $fillable = ['id_empresa', 'nome', 'salario', 'ferias', 'adicional_ferias',
        'fgts_ferias_adicional' , 'salario_13', 'fgts_salario_13', 'aviso_previo',
        'fgts_aviso_previo', 'multa_fgts', 'total_provisoes', 'fgts_salario'
    ];
}
