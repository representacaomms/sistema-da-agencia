<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegraImposto extends Model
{
    protected $fillable = ['id_empresa','descricao' ,'cfop', 'cst_icms','cst_pis','cst_cofins','cst_ipi','alicota_icms','alicota_pis','alicota_cofins','alicota_ipi'];
}
