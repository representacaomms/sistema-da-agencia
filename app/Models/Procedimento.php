<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Procedimento extends Model
{
    protected $table  = 'procedimentos';
    protected $fillable = ['nome', 'id_empresa', 'descricao','id_cli'];

}