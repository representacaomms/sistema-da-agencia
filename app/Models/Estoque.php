<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estoque extends Model
{
    protected $table  = 'estoques';
    protected $fillable = ['id_produto','estoque_minimo', 'estoque'];

    public function produto()
    {
        return $this->hasOne('App\Models\Produto','id','id_produto');
    }
}
