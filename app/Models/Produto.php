<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Produto extends Model
{
    protected $table  = 'produtos';
    protected $fillable = ['id', 'nome', 'caracteristicas', 'status'];

    /* public function fornecedor()
    {
        return $this->hasOne('App\Models\Pessoa','id','id_fornecedor');
    }
 */
}
