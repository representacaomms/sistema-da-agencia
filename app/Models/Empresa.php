<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = [
        'nome',
        'email',
        'telefone',
        'fantasia',
        'cnpj',
        'ins_est',
        'situacao',
        'natureza_juridica',
        'status',
        'tipo',
        'porte'
    ];

    public function endereco()
    {
        return $this->hasOne('App\Models\Endereco','id_cli','id');
    }
}
