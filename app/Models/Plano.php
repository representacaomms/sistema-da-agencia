<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plano extends Model
{
    protected $table  = 'planos';
    protected $fillable = ['id', 'nome', 'plano_base'];

    public function itens()
    {
        return $this->hasMany(ItensPlano::class, 'id_plano', 'id');
    }
}
