<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plataforma extends Model
{
    protected $fillable = ['id', 'nome'];

    public function plataforma_item()
    {
        return $this->hasMany('App\Models\PlataformaItens','id_plataforma','id');
    }

}
