<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlataformaItens extends Model
{
    protected $fillable = ['id_plataforma', 'descricao','acao'];

}