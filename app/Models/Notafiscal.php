<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notafiscal extends Model
{
    protected $table  = 'notafiscal';
    protected $fillable = ['id_empresa','id_venda', 'id_cliente','total_venda','chave', 'protocolo','xml','xml_cancelamento', 'recibo'];

    public function cliente()
    {
        return $this->hasOne('App\Models\Pessoa', 'id', 'id_cliente');
    }
}
