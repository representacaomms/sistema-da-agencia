<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Consultoria extends Model
{
    protected $table  = 'consultoria';
    protected $fillable = ['id', 'id_empresa', 'status', 'cnpj', 'nome', 'email', 'telefone'];

    public function item()
    {
        return $this->hasMany('App\Models\ConsultoriaItens','id_consultoria','id');
    }

}