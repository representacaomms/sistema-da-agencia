<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ConsultoriaItens extends Model
{
    protected $table  = 'consultoria_itens';
    protected $fillable = ['id', 'id_empresa','id_consultoria', 'status', 'id_termo', 'acao'];

    public function dadosItens()
    {
        return $this->belongsTo('App\Models\PlataformaItens','id_termo','id');
    }
}