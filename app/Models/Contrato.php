<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    protected $table  = 'contratos';
    protected $fillable = ['id_empresa', 'id_clausula', 'nome'];

    public function clausulas()
    {
        return $this->hasMany('App\Models\Clausula','id','id_clausula');
    }
}
