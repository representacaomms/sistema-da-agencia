<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Propostas extends Model
{
    protected $table  = 'proposta';
    protected $fillable = ['id_cli', 'id_empresa', 'tipo','status', 'id_vendedor', 'desconto','fr_pagamento', 'cnd_pagamento'];

    public function cliente()
    {
        return $this->hasOne( Pessoa::class, 'id', 'id_cli');
    }

    public function vendedor()
    {
        return $this->hasOne( User::class, 'id', 'id_vendedor');
    }

    public function itens_proposta()
    {
        return $this->hasMany( Plano::class, 'plano', 'id');
    }


}