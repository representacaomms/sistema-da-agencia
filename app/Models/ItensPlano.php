<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItensPlano extends Model
{
    protected $fillable = ['id', 'id_empresa', 'id_plano', 'id_servico_agenciado'];

    public function servico()
    {
        return $this->hasOne('App\Models\ServicoAgenciado', 'id', 'id_servico_agenciado');
       
    }
}
