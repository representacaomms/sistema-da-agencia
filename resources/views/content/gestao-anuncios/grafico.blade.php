@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Gestão Anuncios</a></li>
                            <li class="breadcrumb-item active">Relatório</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Relatório</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                               
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-success mb-2 mr-1"><i
                                            class="mdi mdi-settings"></i></button>
                                    <button type="button" class="btn btn-light mb-2 mr-1">Importar</button>
                                    <button type="button" class="btn btn-light mb-2">Exportar</button>
                                </div>
                            </div><!-- end col-->
                        </div>
                        <input type="hidden" id="record" value="{{$record}}"/>

                        <div class="table-responsive">
                            <div class="mt-3 chartjs-chart" style="height: 700px;">
                                <div class="col-xl-4">
                                    <div class="form-group">
                                        <label for="projectname">Cliente</label>
                                        <input type="text"  name="cliente" class="form-control"  value="{{$record[0]->cliente->fantasia ?? ''}}" style="text-transform: capitalize;" readonly >
                                        
                                    </div>
                                </div><canvas id="myChart"></canvas> 
                                <button type="button" class="btn btn-success mb-2 mr-1" id="btnImpressoes" onclick="movImpressoes()">Impressoes</button>
                                <button type="button" class="btn btn-light mb-2 mr-1" id="btnCliques" onclick="movCliques()">Cliques</button>
                                <button type="button" class="btn btn-light mb-2 mr-1" id="btnConversoes" onclick="movConversoes()">Conversões</button> 

                                <input type="text" class="btn btn-light mb-2 mr-1 ml-5"  value="CTR: {{$record[0]->ctr}}" /> 
                                <input type="text" class="btn btn-light mb-2 mr-1" value="CPC: {{$record[0]->cpc_medio}}" /> 
                                <input type="text" class="btn btn-light mb-2 mr-1" value="CPA: {{$record[0]->custo_conversao}}" /> 
                            </div>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div> <!-- container -->


@endsection
@section('js')
    <!-- DataTables -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<script type="text/javascript">
   
const record = JSON.parse(document.getElementById('record').value);
const impressoes = [];
const cliques = [];
const conversoes = [];
const array_mes = [];
const array_dias = [];
record.forEach(element => {

    impressoes.push(element.impressoes);
    cliques.push(element.cliques);
    conversoes.push(element.conversoes);
    const dia = new Date(element.data_lancamento).getDate();
    const mes = new Date(element.data_lancamento).getMonth()+1;

    array_dias.push(dia);


    switch (mes) {
        case 1:
            array_mes.push('Jan')
            break;
        case 2:
            array_mes.push('Fev')
            break;
        case 3:
            array_mes.push('Mar')
            break;
        case 4:
            array_mes.push('Abr')
            break;
        case 5:
            array_mes.push('Mai')
            break;
        case 6:
            array_mes.push('Jun')
            break;
        case 7:
            array_mes.push('Jul')
            break;
        case 8:
            array_mes.push('Ago')
            break;
        case 9:
            array_mes.push('Set')
            break;
        case 10:
            array_mes.push('Out')
            break;
        case 11:
            array_mes.push('Nov')
            break;
        case 12:
            array_mes.push('Dez')
            break;
    
        default:
            break;
    }
});
let labels = [];
const labels_dias = [...new Set(array_dias.map(item => item))];
const unique = [...new Set(array_mes.map(item => item))];
if(unique.length == 1){
    labels = labels_dias
}else{
    labels = unique;
}


const data = {
  labels: labels,
  datasets: [
    {
        label: 'Impressoes',
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgb(255, 99, 132)',
        data: impressoes,
    },
   /*  {
        label: 'Cliques',
        backgroundColor: 'rgb(125, 17, 219)',
        borderColor: 'rgb(125, 17, 219)',
        data: cliques,
    },
    {
        label: 'Conversões',
        backgroundColor: 'rgb(37, 196, 111)',
        borderColor: 'rgb(37, 196, 111)',
        data: conversoes,
    } */
   
  ]
};
const config = {
    type: 'bar',
    data: data,
    options: {
        responsive: true,
        plugins: {
        legend: {
            position: 'top',
        },
        title: {
            display: true,
            text:'Relatório de Gestão de Anuncios'
        }
        }
    },
};

    var myChart = new Chart(
        document.getElementById('myChart'),
        config
    );
    let btnImpressoes = true;
    let btnCliques    = false;
    let btnConversoes    = false;


    function movImpressoes() {
        btnImpressoes =  !btnImpressoes;
        if(btnImpressoes == true){
            $("#btnImpressoes").removeClass("btn-light"); 
            $("#btnImpressoes").addClass("btn-success"); 
                 
            const value = {
                label: 'Impressoes',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: impressoes,
            }
            myChart.data.datasets.push(value);
            myChart.update();
        }else{

            $("#btnImpressoes").removeClass("btn-success"); 
            $("#btnImpressoes").addClass("btn-light");         

            for (const key in myChart.data.datasets) {
                if(myChart.data.datasets[key].label === 'Impressoes'){
                    myChart.data.datasets.splice(key, 1);
                }
            }
            myChart.update();
                
        }
        
    }

    function movCliques() {
        btnCliques =  !btnCliques;
        if(btnCliques == true){
            $("#btnCliques").removeClass("btn-light"); 
            $("#btnCliques").addClass("btn-success"); 
                 

            const value = {
                label: 'Cliques',
                backgroundColor: 'rgb(125, 17, 219)',
                borderColor: 'rgb(125, 17, 219)',
                data: cliques,
            }
            myChart.data.datasets.push(value);
            myChart.update();

        }else{
            $("#btnCliques").removeClass("btn-success"); 
            $("#btnCliques").addClass("btn-light"); 
            
            for (const key in myChart.data.datasets) {
                if(myChart.data.datasets[key].label === 'Cliques'){
                    myChart.data.datasets.splice(key, 1);
                }
            }
            
            myChart.update();
                
        }
        
    }

    function movConversoes() {
        btnConversoes =  !btnConversoes;
        if(btnConversoes == true){
            $("#btnConversoes").removeClass("btn-light"); 
            $("#btnConversoes").addClass("btn-success"); 
                 

            const value = {
                label: 'Conversoes',
                backgroundColor: 'rgb(37, 196, 111)',
                borderColor: 'rgb(37, 196, 111)',
                data: conversoes,
            }
            myChart.data.datasets.push(value);
            myChart.update();

        }else{
            $("#btnConversoes").removeClass("btn-success"); 
            $("#btnConversoes").addClass("btn-light"); 
            
            for (const key in myChart.data.datasets) {
                if(myChart.data.datasets[key].label === 'Conversoes'){
                    myChart.data.datasets.splice(key, 1);
                }
            }
            
            myChart.update();
                
        }
        
    }
   
</script>
  
@stop
