@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('propostas.index')}}">Propostas</a></li>
                            <li class="breadcrumb-item active">Editar Propostas</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Propostas</h4>                   
                </div>
            </div>
        </div>
        
        <form name="formOrcamento" action="{{ route('propostas.update', $proposta->id)}}" method="POST">
            
            @method('PUT')
            @csrf
            <div class="row">
                <div class="col-xl-12">
                    <!-- tasks panel -->
                    <div class="collapse show" id="todayTasks">
                        <div class="card mb-0">
                            <div class="card-body">
                                <a class="text-dark" data-toggle="collapse" href="#todayTasks" aria-expanded="false"
                                    aria-controls="todayTasks">
                                    <label class="m-0 pb-2">
                                        <i class="uil uil-angle-down font-18"></i> Gestão Anuncios <span
                                            class="text-muted"></span>
                                    </label>
                                </a>
                                <hr>
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="form-group id_cliente">
                                            <label for="projectname">Cliente</label>
                                            <select class="form-control select2" data-toggle="select2" name="id_cliente"
                                                id="id_cliente">
                                                <option value="0">Selecione o Cliente ...</option>
                                                @foreach ($clientes as $item)
                                                    <option value="{{ $item->id }}" style="text-transform: capitalize;">
                                                        {{ $item->fantasia }}</option>
                                                @endforeach
                                                

                                            </select>
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-4">
                                        <div class="form-group">
                                            <label for="projectname">Gestor</label>
                                            <input type="text" name="vendedor" id="vendedor" class="form-control"
                                                value="{{ Auth::user()->name }}" readonly>
                                            <input type="hidden" name="id_vendedor" id="id_vendedor" class="form-control"
                                                value="{{ Auth::user()->id }}">
                                        </div>
                                    </div> <!-- end col-->
                                </div>
                                <div class="row">
                                    <div class="col-md-4" >
                                        <div class="form-group" >
                                            <div class="form-group">
                                                <label for="projectname">Plataforma</label>
                                                <select class="form-control select2" data-toggle="select2" name="plataforma"
                                                id="id_cliente">
                                                <option value="0">Selecione a Plataforma ...</option>
                                                
                                                <option value="google_ads" style="text-transform: capitalize;">Google ADS</option>
                                                <option value="facebook_ads" style="text-transform: capitalize;">Facebook ADS</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-xl-4">
                                    <div class="form-group ">
                                        <label for="projectname">Tipo de Campanha</label>
                                        <select class="form-control select2" data-toggle="select2" name="tipo_campanha"
                                            id="id_cliente">
                                            <option value="0">Selecione o Tipo ...</option>
                                                <option value="conversao" style="text-transform: capitalize;">Conversão</option>
                                                <option value="trafego" style="text-transform: capitalize;">Trafego</option>
                                                <option value="Distribuicao" style="text-transform: capitalize;">Distribuição</option>
                                        </select>
                                    </div>
                                </div> <!-- end col-->
                                <div class="col-xl-4">
                                    <div class="form-group">
                                        <label for="projectname">Nome da Campanha</label>
                                        <input type="text"  name="nome_campanha" class="form-control"  style="text-transform: capitalize;" >
                                    </div>
                                </div> <!-- end col-->
                                </div>
                                <!-- end row -->
                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end .collapse-->
                    <br>
                </div>
            </div>

            <div class="card">

                <div class="card-body">

                    <h4 class="header-title mb-3"> Adicionar dados</h4>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="projectname">Impressões</label>
                                <input type="text"  id="impressoes" name="impressoes" class="form-control"  style="text-transform: capitalize;" placeholder="Impressões" required>
                            </div>
                        </div>
                        <div class="col-md-3" >
                            <div class="form-group" >
                                <div class="form-group">
                                    <label for="fantasia">Cliques</label>
                                    <input type="text" id="cliques" name="cliques" class="form-control"  style="text-transform: capitalize;" placeholder="Cliques" >
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-3" >
                            <div class="form-group" >
                                <div class="form-group">
                                    <label for="fantasia">Visualização Página Destino</label>
                                    <input type="text" id="view_pagina_destino" name="view_pagina_destino" class="form-control" placeholder="Visualização página destino" >
                                </div>
                            </div>
                        </div> <!-- end col-->

                        <div class="col-md-3" >
                            <div class="form-group" >
                                <div class="form-group">
                                    <label for="fantasia">Conversões</label>
                                    <input type="text" id="conversoes" name="conversoes" class="form-control"  style="text-transform: capitalize;" placeholder="Conversões" >
                                </div>
                            </div>
                        </div> 
                        
                    </div>
                    
                    <div class="row">
                        <div class="col-md-3" >
                            <div class="form-group" >
                                <div class="form-group">
                                    <label for="fantasia">Custo Total</label>
                                    <input type="text" id="custo_total" name="custo_total" class="form-control"  data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" placeholder="Custo Total" >
                                </div>
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="projectname">CPC Médio</label>
                                <input type="text" id="cpc_medio"  name="cpc_medio" class="form-control"  style="text-transform: capitalize;"  readonly>
                            </div>
                        </div>
                        <div class="col-md-2" >
                            <div class="form-group" >
                                <div class="form-group">
                                    <label for="fantasia">CTR (Taxa de Cliques)</label>
                                    <input type="text"  id="ctr" name="ctr" class="form-control"  style="text-transform: capitalize;" readonly >
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-2" >
                            <div class="form-group" >
                                <div class="form-group">
                                    <label for="fantasia">Taxa de Conversões</label>
                                    <input type="text" id="taxa_conversoes" name="taxa_conversoes"  class="form-control"  style="text-transform: capitalize;" readonly >
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-3" >
                            <div class="form-group" >
                                <div class="form-group">
                                    <label for="fantasia">Custo por Conversão</label>
                                    <input type="text" id="custo_conversao" name="custo_conversao" class="form-control"  style="text-transform: capitalize;" readonly >
                                </div>
                            </div>
                        </div> <!-- end col-->

                    </div>

                    <!-- Bool Switch-->
                    <div class="text-right mt-5">
                        <a href="{{ URL::previous()}}" class="btn btn-light" data-dismiss="modal">Sair</a>
                        <button type="submit" class="btn btn-primary">Adicionar</button>
                    </div>
                </div>
            </div>
        </form>
    </div> <!-- container --> 
@endsection
@section('js')
<script>
    $(document).ready(function() {

        $('#custo_total').change(function() {
            var impressoes =  $('#impressoes').val();
            var cliques =  $('#cliques').val();
            var conversoes =  $('#conversoes').val();
            var custo_total =  $('#custo_total').val();

            var taxa_cliques = ((parseInt(cliques) / parseInt(impressoes)).toFixed(2))*100 + ' %';
            var taxa_conversao = ((parseInt(conversoes) / parseInt(cliques)).toFixed(2))*100 + ' %';
            var custo_conversao = 'R$ '+(parseFloat(custo_total) / (conversoes)).toFixed(2);
            var cpc_medio = 'R$ '+(parseFloat(custo_total) / (cliques)).toFixed(2);


            $('#ctr').val(taxa_cliques);
            $('#taxa_conversoes').val(taxa_conversao);
            $('#custo_conversao').val(custo_conversao);
            $('#cpc_medio').val(cpc_medio);
        });



    });

</script>
@endsection