@extends('layouts.app')
@section('content')
<!-- Start Content-->
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Prospectos</a></li>
                        <li class="breadcrumb-item active">Listar Prospectos</li>
                    </ol>
                </div>
                <h4 class="page-title">Listar Prospeccões</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-sm-4">
                            <a href="{{ route('prospeccoes.create') }}" class="btn btn-danger mb-2"><i
                                    class="mdi mdi-plus-circle mr-2"></i> Adicionar Prospecção</a>
                        </div>
                        <div class="col-sm-8">
                            <div class="text-sm-right">
                                <button type="button" class="btn btn-success mb-2 mr-1"><i
                                        class="mdi mdi-settings"></i></button>
                                <button type="button" class="btn btn-light mb-2 mr-1">Importar</button>
                                <button type="button" class="btn btn-light mb-2">Exportar</button>
                            </div>
                        </div><!-- end col-->
                    </div>

                    <div class="table-responsive">
                        @if ($message = Session::get('success'))
                            @if ($message == 'store')
                                <script>
                                    var message = 'store';
                                </script>
                            @endif
                            @if ($message == 'update')
                                <script>
                                    var message = 'update';
                                </script>
                            @endif

                            @if ($message == 'destroy')
                                <script>
                                    var message = 'destroy';
                                </script>
                            @endif

                            @if ($message == 'active')
                                <script>
                                    var message = 'active';
                                </script>
                            @endif
                            
                        @endif
                    </div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
            <form name="frmTasks" >
                <input type="hidden" id="records" value="{{$prospeccoes}}"></input>
                <input type="hidden" id="conversas" value="{{$conversas}}"></input>
                <!-- end page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="board" style="max-height: 600px">
                            <div class="tasks" style="min-height: 1000px;width: 230px">
                                 <h4 class="text-title text-center pt-2">Prospecto</h4>
                                    <input type="hidden" id="rotulo" value="prospect"></input>
                                    @foreach ($prospeccoes as $record)
                                        @if($record->status == 'prospect' )
                                        <div class="card mb-0" draggable="true">
                                            <div class="card-body p-3">
                                                <input type="hidden" id="id" value="{{$record->id}}"></input>
                                                <small class="text-nowrap mb-2 d-inline-block text-muted">{{$record->updated_at->format('d/m/Y')}}
                                                <a href="#" class="card-drop chamar_conversa" id="{{$record->id}},{{$record->nome}}" style="margin-left:90%">
                                                    <i class="mdi mdi-dots-vertical"></i>
                                                </a></small><br>
                                                <small class="mt-0">
                                                    <a href="#" class="text-muted" style="text-transform: capitalize;">{{$record->nome}}</a>
                                                </small><br>
                                                <small class="mt-0">
                                                    <a href="{{strlen($record->telefone) > 10 ?  'https://wa.me/55'.$record->telefone : '#' }}" target="_blank" class="text-muted">{{strlen($record->telefone) > 10 ? '('.substr($record->telefone, 0, 2).') '.substr($record->telefone, 2, 1).'.'.substr($record->telefone, 3, 4). ' - '.substr($record->telefone, 7, 4): '('.substr($record->telefone, 0, 2).') ' .substr($record->telefone, 2, 4).' - '.substr($record->telefone, 5, 4)}}</a>
                                                </small><br>
                                            </div> <!-- end card-body -->
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <div class="tasks" style="min-height: 1000px;width: 230px">
                                 <h4 class="text-title text-center pt-2">Qualificação</h4>
                                 <input type="hidden" id="rotulo" value="qualificacao"></input>
                                @foreach ($prospeccoes as $record)
                                    @if($record->status == 'qualificacao' )
                                        <div class="card mb-0" draggable="true">
                                            <div class="card-body p-3">
                                                <input type="hidden" id="id" value="{{$record->id}}"></input>
                                                <small class="text-nowrap mb-2 d-inline-block text-muted">{{$record->updated_at->format('d/m/Y')}}
                                                <a href="#" class="card-drop" id="chamar_conversa" style="margin-left:90%">
                                                    <i class="mdi mdi-dots-vertical"></i>
                                                </a></small><br>
                                                <small class="mt-0">
                                                    <a href="#" class="text-muted" style="text-transform: capitalize;">{{$record->nome}}</a>
                                                </small><br>
                                                <small class="mt-0">
                                                    <a href="{{strlen($record->telefone) > 10 ?  'https://wa.me/55'.$record->telefone : '#' }}" target="_blank" class="text-muted">{{strlen($record->telefone) > 10 ? '('.substr($record->telefone, 0, 2).') '.substr($record->telefone, 2, 1).'.'.substr($record->telefone, 3, 4). ' - '.substr($record->telefone, 7, 4): '('.substr($record->telefone, 0, 2).') ' .substr($record->telefone, 2, 4).' - '.substr($record->telefone, 5, 4)}}</a>
                                                </small><br>
                                            </div> <!-- end card-body -->
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <div class="tasks" style="min-height: 1000px;width: 230px">
                                 <h4 class="text-title text-center pt-2">Proposta</h4>
                                <input type="hidden" id="rotulo" value="proposta"></input>
                                @foreach ($prospeccoes as $record)
                                    @if($record->status == 'proposta')
                                        <div class="card mb-0" draggable="true">
                                            <div class="card-body p-3">
                                                <input type="hidden" id="id" value="{{$record->id}}"></input>
                                                <small class="text-nowrap mb-2 d-inline-block text-muted">{{$record->updated_at->format('d/m/Y')}}
                                                <a href="#" class="card-drop chamar_conversa"  id=1 style="margin-left:90%">
                                                    <i class="mdi mdi-dots-vertical"></i>
                                                </a></small><br>
                                                <small class="mt-0">
                                                    <a href="#" class="text-muted" style="text-transform: capitalize;">{{$record->nome}}</a>
                                                </small><br>
                                                <small class="mt-0">
                                                    <a href="{{strlen($record->telefone) > 10 ?  'https://wa.me/55'.$record->telefone : '#' }}" target="_blank" class="text-muted">{{strlen($record->telefone) > 10 ? '('.substr($record->telefone, 0, 2).') '.substr($record->telefone, 2, 1).'.'.substr($record->telefone, 3, 4). ' - '.substr($record->telefone, 7, 4): '('.substr($record->telefone, 0, 2).') ' .substr($record->telefone, 2, 4).' - '.substr($record->telefone, 5, 4)}}</a>
                                                </small><br>
                                            </div> <!-- end card-body -->
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <div class="tasks" style="min-height: 1000px;width: 230px">
                                 <h4 class="text-title text-center pt-2">Negociação</h4>
                                <input type="hidden" id="rotulo" value="negociacao"></input>
                                @foreach ($prospeccoes as $record)
                                     @if($record->status == 'negociacao')
                                        <div class="card mb-0" draggable="true">
                                            <div class="card-body p-3">
                                                <input type="hidden" id="id" value="{{$record->id}}"></input>
                                                <small class="text-nowrap mb-2 d-inline-block text-muted">{{$record->updated_at->format('d/m/Y')}}
                                                <a href="#" class="card-drop" id="chamar_conversa" style="margin-left:90%">
                                                    <i class="mdi mdi-dots-vertical"></i>
                                                </a></small><br>
                                                <small class="mt-0">
                                                    <a href="#" class="text-muted" style="text-transform: capitalize;">{{$record->nome}}</a>
                                                </small><br>
                                                <small class="mt-0">
                                                    <a href="{{strlen($record->telefone) > 10 ?  'https://wa.me/55'.$record->telefone : '#' }}" target="_blank" class="text-muted">{{strlen($record->telefone) > 10 ? '('.substr($record->telefone, 0, 2).') '.substr($record->telefone, 2, 1).'.'.substr($record->telefone, 3, 4). ' - '.substr($record->telefone, 7, 4): '('.substr($record->telefone, 0, 2).') ' .substr($record->telefone, 2, 4).' - '.substr($record->telefone, 5, 4)}}</a>
                                                </small><br>
                                                
                                            </div> <!-- end card-body -->
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <div class="tasks" style="min-height: 1000px;width: 230px">
                                 <h4 class="text-title text-center pt-2">Conclusão</h4>
                                <input type="hidden" id="rotulo" value="conclusao"></input>
                                @foreach ($prospeccoes as $record)
                                     @if($record->status == 'conquistado' || $record->status == 'cancelado' || $record->status == 'perdido' || $record->status == 'suspenso' || $record->status == 'sem_interesse')
                                        <div class="card mb-0" draggable="true">
                                            <div class="card-body p-3">
                                                <input type="hidden" id="id" value="{{$record->id}}"></input>
                                                <small class="text-nowrap mb-2 d-inline-block text-muted">{{$record->updated_at->format('d/m/Y')}}
                                                </small><br>
                                                <small class="mt-0">
                                                    <a href="#" class="text-muted" style="text-transform: capitalize;">{{$record->nome}}</a>
                                                </small><br>
                                                <small class="mt-0">
                                                    <a href="{{strlen($record->telefone) > 10 ?  'https://wa.me/55'.$record->telefone : '#' }}" target="_blank" class="text-muted">{{strlen($record->telefone) > 10 ? '('.substr($record->telefone, 0, 2).') '.substr($record->telefone, 2, 1).'.'.substr($record->telefone, 3, 4). ' - '.substr($record->telefone, 7, 4): '('.substr($record->telefone, 0, 2).') ' .substr($record->telefone, 2, 4).' - '.substr($record->telefone, 5, 4)}}</a>
                                                </small><br>
                                                <small class="mb-0">
                                                    @if($record->status == 'perdido')
                                                    <span class="text-nowrap mb-2 d-inline-block badge badge-warning text-light" >
                                                        <b>Perdido</b>
                                                    </span>
                                                    @endif
                                                    
                                                    @if($record->status == 'cancelado')
                                                    <span class="text-nowrap mb-2 d-inline-block badge badge-danger text-light" >
                                                        <b>Cancelado</b>
                                                    </span>
                                                    @endif
                                                    
                                                    @if($record->status == 'conquistado')
                                                    <span class="text-nowrap mb-2 d-inline-block badge badge-success text-light" >
                                                        <b>Conquistado</b>
                                                    </span>
                                                    @endif
                                                    
                                                    @if($record->status == 'suspenso')
                                                    <span class="text-nowrap mb-2 d-inline-block badge badge-secondary text-light" >
                                                        <b>Suspenso</b>
                                                    </span>
                                                    @endif
                                                    
                                                    @if($record->status == 'sem_interesse')
                                                    <span class="text-nowrap mb-2 d-inline-block badge badge-secondary text-light" >
                                                        <b>Sem Interesse</b>
                                                    </span>
                                                    @endif
                                                </small>
                                            </div> <!-- end card-body -->
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div> <!-- end .board-->
                    </div> <!-- end col -->
                </div>
            </form>
            <!-- MODALS  -->
            <div id="info-alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-body p-4">
                            <div class="text-center">
                                <i class="dripicons-information h1 text-info"></i>
                                <h4 class="mt-2">Conclusão de Prospecção</h4>
                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="projectname">Status</label>
                                            <select name="conclusao_status" id="conclusao_status" class="form-control">
                                                <option value="cancelado"selected>Cancelado</option>
                                                <option value="conquistado">Conquistado</option>
                                                <option value="perdido">Perdido</option> 
                                                <option value="suspenso">Suspenso</option>
                                                <option value="sem_interesse">Sem Interesse</option>
                                            </select>
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-danger my-2 mb-1"  id="cancelar">Cancelar</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-info my-2 mb-1" id="continuar" >Continue</button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <div id="message-alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog  modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-body p-4">
                                <div class="card">
                                    <div class="card-body pb-1">
                                        <h5 class="card-title crtitle "></h5>
                                            <div class="d-flex">
                                            <div class="w-100">
                                                <!-- comment box -->
                                                <div class="border rounded">
                                                    <form name="formConversa" action="#" class="comment-area-box">
                                                        <input type="hidden" id="formConversa_id" value='' />
                                                        <textarea rows="4" id="conteudo" class="form-control border-0 resize-none" placeholder="Digite a resposta ...."></textarea>
                                                        <div class="p-2 bg-light d-flex justify-content-between align-items-center">
                                                            <div>
                                                                <a href="#" class="btn btn-sm px-2 font-16 btn-light"></a>
                                                                <a href="#" class="btn btn-sm px-2 font-16 btn-light"></a>
                                                                <a href="#" class="btn btn-sm px-2 font-16 btn-light"></a>
                                                            </div>
                                                            <button type="submit" class="btn btn-sm btn-success"><i class='uil uil-message me-1'></i>Adicionar conversa</button>
                                                        </div>
                                                    </form>
                                                </div> <!-- end .border-->
                                                <!-- end comment box -->
                                            </div>
                                        </div>
                                       
                                
                                        <hr class="m-0" />
                                
                                        <div class="font-16 text-center text-dark my-3 conversa" ></div>
                                
                                       
                                    </div> <!-- end card-body -->
                                </div> <!-- end card -->
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

        </div> <!-- end col -->
    </div>
    <!-- end row-->
</div> <!-- container -->
@endsection

@section('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
@endsection
@section('js')
<!-- DataTables -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
       
        const cards = document.querySelectorAll('.card')
        const dropzones = document.querySelectorAll('.tasks')
        
        const records = JSON.parse($('form[name="frmTasks"]').find('input#records').val());
        const conversas = JSON.parse($('form[name="frmTasks"]').find('input#conversas').val());
        
        const url  = "{{ url( '/gestao-marketing/prospeccoes') }}/"
        
        
        
        $('#leads-datatable').DataTable({
            language: {
                processing: "Processamento...",
                search: "Pesquisar&nbsp;:",
                lengthMenu: "Mostrar _MENU_ entradas",
                info: "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                infoEmpty: "Exibindo da entrada 0 até 0 de 0 entradas",
                infoFiltered: "(filtro total de _MAX_ entradas)",
                infoPostFix: "",
                loadingRecords: "Carregando ...",
                zeroRecords: "Nenhuma entrada encontrada",
                emptyTable: "Nenhuma entrada disponível na tabela",
                paginate: {
                    first: "Primeiro",
                    previous: "Anterior",
                    next: "Ultimo",
                    last: "Depos"
                },
                aria: {
                    sortAscending: ": ativar para classificar a coluna em ordem crescente",
                    sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                }
            }
        });
        
        $('.chamar_conversa').click(function(event){
            $(".conversa").empty();
            $(".crtitle").empty()
             
            tc_nome = event.currentTarget.attributes[2].value.split(',')[1]
            tc_id   = event.currentTarget.attributes[2].value.split(',')[0]
            
            $('.crtitle').prepend("<p>Conversa com o sr(a)</br>"+tc_nome+"</p>");
             $('#formConversa_id').val(tc_id);
            
            conversas.forEach((record) => {
                if(parseInt(record.prospecto) === parseInt(tc_id)){
                    $(".conversa").prepend("<small class='mb-2'><i class='mdi mdi-format-quote-open font-20'></i>"+ record.conteudo+ "</small><br>");
                }
             });
            $('#message-alert-modal').modal('show');
        })

        $('form[name="formConversa"]').submit(function(event){
            event.preventDefault();
            var mensagem = $('#conteudo').val();
            var dados = {'id':$('#formConversa_id').val(), 'conteudo': mensagem}
            $.ajaxSetup({
               headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
            });
            $.ajax({
                url:"{{ route( 'conversas.store') }}",
                type: "post",
                data: dados,
                dataType: 'json', 
                success: function(response){ }
               
            });
           
            $('#message-alert-modal').modal('hide');
            document.location.reload(true)
        })
        
        
        /** our cards */
        cards.forEach(card => {
            card.addEventListener('dragstart', dragstart)
            card.addEventListener('drag', drag)
            card.addEventListener('dragend', dragend)
        })

        function dragstart() {
            // log('CARD: Start dragging ')
            dropzones.forEach( dropzone => dropzone.classList.add('highlight'))
        
            // this = card
            this.classList.add('is-dragging')
        }

        function drag() {
         // log('CARD: Is dragging ')
        }

        function dragend() {
            // log('CARD: Stop drag! ')
            dropzones.forEach( dropzone => dropzone.classList.remove('highlight'))
            
            const task = $(this.parentNode).find('input#rotulo').val();
            const id = $(this).find('input#id').val();
            // this = card
            this.classList.remove('is-dragging')
            
             atualizarLista(task, id)
             
             
        }

        /** place where we will drop cards */
        dropzones.forEach( dropzone => {
            dropzone.addEventListener('dragenter', dragenter)
            dropzone.addEventListener('dragover', dragover)
            dropzone.addEventListener('dragleave', dragleave)
            dropzone.addEventListener('drop', drop)
        })

        function dragenter() {
            // log('DROPZONE: Enter in zone ')
              this.classList.remove('over')
             
        }

        function dragover() {
            // this = dropzone
            this.classList.add('over')
        
            // get dragging card
            const cardBeingDragged = document.querySelector('.is-dragging')
            this.appendChild(cardBeingDragged)
        }

        function dragleave() {
            // log('DROPZONE: Leave ')
            // this = dropzone
            this.classList.remove('over')
        }

        function drop() {
            // log('DROPZONE: dropped ')
            this.classList.remove('over')
        }
        
        function atualizarLista(status, id) {
            $.ajaxSetup({
               headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
            });
        
            records.forEach((record) => {
                if(record.id === parseInt(id)){
                    switch (status) {
                        case 'prospect':
                            console.log(' Status prospect');
                            record.status = status;
                            $.ajax({
                                url:url + id,
                                type: "PUT",
                                data:record,
                                dataType: 'json', 
                                success: function(response){
                                     document.location.reload(true)
                                }
                            }) 
                            break;
                            
                        case 'qualificacao':
                            console.log(' Status 1º CONTATO');
                            record.status = status;
                            $.ajax({
                                url:url + id,
                                type: "PUT",
                                data:record,
                                dataType: 'json', 
                                success: function(response){
                                    console.log(response)
                                    document.location.reload(true)
                                }
                            }) 
                            break;
                            
                        case 'proposta':
                            console.log(' Status FICHA CADASTRAL');
                            record.status = status;
                            $.ajax({
                                url:url + id,
                                type: "PUT",
                                data:record,
                                dataType: 'json', 
                                success: function(response){
                                    console.log(response)
                                    document.location.reload(true)                                
                                }
                            }) 
                            break;
                            
                        case 'negociacao':
                            console.log(' Status BRIEFING');
                            record.status = status;
                            $.ajax({
                                url:url + id,
                                type: "PUT",
                                data:record,
                                dataType: 'json', 
                                success: function(response){
                                    document.location.reload(true)
                                }
                            }) 
                            break;
                            
                        case 'conclusao':
                            $('#info-alert-modal').modal('show');
                            $('#continuar').click(function(){
                                var select = document.getElementById('conclusao_status');
    	                        var value = select.options[select.selectedIndex].value;

                                $('#info-alert-modal').modal('hide');
                                    record.status = value;
                                    $.ajax({
                                        url:url + id,
                                        type: "PUT",
                                        data:record,
                                        dataType: 'json', 
                                        success: function(response){
                                            console.log(response)
                                            document.location.reload(true)
                                        }
                                    }) 
                                });
                            $('#cancelar').click(function(){
                                $('#info-alert-modal').modal('hide');
                                document.location.reload(true)
                            });
                            break;
                        default:
                            console.log(`Status  ${record.status}.`);
                    }
                    
                }
            })
        }
    });
    

</script>
<script>
    if (message == 'store') {
        Swal.fire(
            'Parabéns!',
            'Registro cadastrado com sucesso!',
            'success'
        )
    }
    if (message == 'update') {
        Swal.fire(
            'Parabéns!',
            'Registro editado com sucesso!',
            'success'
        )
    }

    if (message == 'destroy') {
        Swal.fire(
            'Alerta!',
            'Registro inativado com sucesso!',
            'info'
        )
    }

    if (message == 'active') {
        Swal.fire(
            'Alerta!',
            'Registro ativado com sucesso!',
            'info'
        )
    }

    
</script>


@stop