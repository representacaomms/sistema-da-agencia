@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('prospeccoes.index') }}">Prospecções</a></li>
                            <li class="breadcrumb-item active">Adicionar Prospecção</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Prospecção</h4>

                </div>
            </div>
        </div>

        <form action="{{ route('prospeccoes.store') }}" method="post">
            @csrf
            @method('POST')

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="projectname">Tipo</label>
                                <select name="tipo" id="tipo" class="form-control">
                                    <option value="a"selected>Prospecção Ativa</option>
                                    <option value="p">Prospecção Passiva</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectname">Nome</label>
                                <input type="text"  name="nome" class="form-control"  style="text-transform: capitalize;" placeholder="Nome Completo" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="projectname">Funcionários</label>
                                <select name="funcionario" id="funcionario" class="form-control">
                                    <option value="1"selected>Individual</option>
                                    <option value="2-5">2-5</option>
                                    <option value="6-10">6-10</option>
                                    <option value="11-15">11-15</option>
                                    <option value="15+">Mais de 15</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" >
                                <div class="form-group">
                                    <label for="fantasia">Empresa</label>
                                    <input type="text" name="empresa" class="form-control"  style="text-transform: capitalize;" placeholder="Nome da Empresa" >
                                </div>
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="projectname">Site</label>
                                <input type="text"  name="site" class="form-control"  placeholder="Site">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" >
                                <label id="label_cpf">Status</label>
                                <select name="status"  class="form-control">
                                    <option value="inbound">Inbound</option>
                                    <option value="ativo">Ativo</option>
                                    <option value="inativo">Inativo</option>
                                </select>
                            </div>
                        </div> <!-- end col-->
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label id="label_cpf">Email</label>
                                <input type="email"  name="email" class="form-control" required >
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" >
                                <div class="form-group">
                                    <label id="label_cpf">Celular</label>
                                    <input type="text" data-toggle="input-mask" data-mask-format="(00)0.0000-0000" data-reverse="false" id="celular" name="celular" class="form-control"  >
                                </div>
                            </div>
                        </div> <!-- end col-->

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-8">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="{{ URL::previous()}}">
                                                        Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2 pl-2">
                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit">
                                                            Adicionar<span class="badge badge-primary"></span>
                                                    </button>
                                                </div>
                                            </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>
        </form>
    </div>



@section('js')
    <script>
         
    </script>
@endsection

@endsection
