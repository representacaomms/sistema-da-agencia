@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Usuarios</a></li>
                            <li class="breadcrumb-item active">Perfil</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Perfil Empresa</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        @if ($errors->all())
            @foreach($errors->all() as $error)
                <script>
                    var message = 'error';
                    var content = "<?= $error ?>";
                </script>
            @endforeach
        @endif
        @if ($message = Session::get('success'))
            @if ($message == 'store')
                <script>
                    var message = 'store';
                </script>
            @endif
            @if ($message == 'update')
                <script>
                    var message = 'update';
                </script>
            @endif

            @if ($message == 'destroy')
                <script>
                    var message = 'destroy';
                </script>
            @endif

        @endif

        <div class="row">
            <div class="col-sm-12">
                <!-- Profile -->
                <div class="card bg-primary">
                    <div class="card-body profile-user-box">

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="media">
                                    <span class="float-left m-2 mr-4"><img src="{{ asset('images/company/empresa.jpg') }}" style="height: 100px; width:100px" alt="" class="rounded-circle img-thumbnail"></span>
                                    <div class="media-body">

                                        <h4 class="mt-1 mb-1 text-white">{{ $empresa->nome }}</h4>
                                        <p class="font-13 text-white-50"> {{ $empresa->cpf }}</p>

                                        <ul class="mb-0 list-inline text-light">
                                            <li class="list-inline-item mr-3">
                                                <h5 class="mb-1">$ 25,184</h5>
                                                <p class="mb-0 font-13 text-white-50">Rendimento total</p>
                                            </li>
                                            <li class="list-inline-item">
                                                <h5 class="mb-1">5482</h5>
                                                <p class="mb-0 font-13 text-white-50">Número de pedidos</p>
                                            </li>
                                        </ul>
                                    </div> <!-- end media-body-->
                                </div>
                            </div> <!-- end col-->

                            <div class="col-sm-4">
                                <div class="text-center mt-sm-0 mt-3 text-sm-right">
                                    <a href="{{ route('empresa.edit', $empresa->id) }}" class="btn btn-light" >
                                        <i class="mdi mdi-account-edit mr-1"></i> Editar Perfil
                                    </a>
                                </div>
                            </div> <!-- end col-->
                        </div> <!-- end row -->

                    </div> <!-- end card-body/ profile-user-box-->
                </div><!--end profile/ card -->
            </div> <!-- end col-->
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-md-4">
                <!-- Personal-Information -->
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mt-0 mb-3">Informações da Empresa</h4>
                        <hr>
                        <div class="text-left">
                            <p class="text-muted"><strong>Nome Completo:</strong> <span class="ml-2"> {{ $empresa->nome }} </span></p>

                            <p class="text-muted"><strong>Documento:</strong><span class="ml-2">{{  $empresa->cpf }} </span></p>

                            <p class="text-muted"><strong>Celular:</strong><span class="ml-2">{{ $empresa->celular }}</span></p>

                            <p class="text-muted"><strong>Email:</strong> <span class="ml-2">{{ $empresa->email }}</span></p>

                           @if($empresa->endereco)
                                <p class="text-muted"><strong>CEP:</strong> <span class="ml-2">{{ $empresa->endereco->cep }}</span></p>

                                <p class="text-muted"><strong>Localização:</strong> <span class="ml-2">{{ $empresa->endereco->logradouro}},{{ $empresa->endereco->numero}} - {{ $empresa->endereco->localidade }} / {{ $empresa->endereco->uf }}</span></p>

                           @else
                                <p class="text-muted"><strong>Atencao:</strong> <span class="ml-2">Por Favor, Cadastre o endereço da empresa!</span></p>
                           @endif

                        </div>
                    </div>
                </div>
                <!-- Personal-Information -->





            </div> <!-- end col-->

            <div class="col-md-8">

                <!-- Personal-Information -->
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mt-0 mb-3">Configuração da Empresa</h4>
                        <hr>
                        @if ($configEmpresa)
                            <form action="{{ route('empresa.updateConfig', $empresa->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="text-left">
                                            <p class="text-muted"><strong>Caminho Certificado XML:</strong> <span class="ml-2">
                                                <input type="text" name="caminho_certificado" value="{{ $configEmpresa->caminho_certificado }}" class="form-control"
                                                ></span></p>
                                        </div>
                                    </div>
                                   {{--  <div class="col-sm-6">
                                        <div class="text-left">
                                            <p class="text-muted"><strong>Caminho XML:</strong> <span class="ml-2"> <input type="text" name="caminho_xml" value="{{ $configEmpresa->caminho_xml }}" class="form-control" ></span></p>
                                        </div>
                                    </div> --}}
                                </div>

                              {{--   <div class="row">
                                    <div class="col-sm-6">
                                        <div class="text-left">
                                            <p class="text-muted"><strong>Caminho XML Assinado:</strong> <span class="ml-2"> <input type="text" name="caminho_xml_assinado" class="form-control" value="{{ $configEmpresa->caminho_xml_assinado }}" ></span></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="text-left">
                                            <p class="text-muted"><strong>Caminho XML Protocolado:</strong> <span class="ml-2"> <input type="text" name="caminho_xml_protocolado" class="form-control" value="{{ $configEmpresa->caminho_xml_protocolado }}" ></span></p>
                                        </div>
                                    </div>
                                </div> --}}

                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="text-left">
                                            <p class="text-muted"><strong>Senha Certificado:</strong> <span class="ml-2"> <input type="password" name="senha_cert" class="form-control" value="{{ $configEmpresa->senha_cert }}"></span></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="text-left">
                                            <p class="text-muted"><strong>Serie:</strong> <span class="ml-2"> <input type="text" name="serie" value="{{ $configEmpresa->serie }}" class="form-control" required></span></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="text-left">
                                            <p class="text-muted"><strong>Nro Ultima NotaFiscal Emitida:</strong> <span class="ml-2"> <input type="text" name="ultNotaFiscalEmit" class="form-control"  value="{{ $configEmpresa->ultNotaFiscalEmit }}" required></span></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="text-left">
                                            <p class="text-muted"><strong>Versão:</strong> <span class="ml-2"> <input type="text" name="versao" class="form-control" value="{{ $configEmpresa->versao }}" required></span></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="text-left">
                                            <p class="text-muted"><strong>Regime Tributário:</strong> <span class="ml-2"> <select type="text" name="regime_tributario" class="form-control" required>
                                                @if ($configEmpresa->regime_tributario == "1")
                                                    <option value="1">Simples Nacional</option>
                                                    <option value="2">Simples Nacional com Substituição Tributária</option>
                                                    <option value="3">Lucro Prezumido</option>
                                                @elseif($configEmpresa->regime_tributario == "2")
                                                    <option value="2">Simples Nacional com Substituição Tributária</option>
                                                    <option value="1">Simples Nacional</option>
                                                    <option value="3">Lucro Prezumido</option>
                                                @else
                                                    <option value="3">Lucro Prezumido</option>
                                                    <option value="1">Simples Nacional</option>
                                                    <option value="2">Simples Nacional com Substituição Tributária</option>
                                                @endif
                                            </select></span></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="text-left">
                                            <p class="text-muted"><strong>Atualização:</strong> <span class="ml-2"> <input type="text" name="atualizacao" class="form-control" value="{{ $configEmpresa->atualizacao }}" required></span></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="text-left">
                                            <p class="text-muted"><strong>Schemes:</strong> <span class="ml-2"> <input type="text" name="schemes" class="form-control" value="{{ $configEmpresa->schemes }}" required></span></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="text-left">
                                            <p class="text-muted"><strong>Token IBTP:</strong> <span class="ml-2"> <input type="text" name="token_ibtp" class="form-control" value="{{ $configEmpresa->token_ibtp }}"></span></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="text-left">
                                            <p class="text-muted"><strong>Tipo Ambiente</strong>
                                            <span class="ml-2">
                                                <select type="text" name="ambiente" class="form-control" required>
                                                    @if ($configEmpresa->ambiente == "1")
                                                        <option value="1">Produção</option>
                                                        <option value="2">Homologação</option>
                                                    @else
                                                        <option value="2">Homologação</option>
                                                        <option value="1">Produção</option>
                                                    @endif
                                                </select>
                                            </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="text-left">
                                            <p class="text-muted"><strong>Contribuinte</strong>
                                            <span class="ml-2">
                                                <select type="text" name="contribuinte" class="form-control" required>

                                                    @if ($configEmpresa->contribuinte == "1")
                                                        <option value="1">1-Contribuinte</option>
                                                        <option value="2">2-Contribuinte Isento</option>
                                                        <option value="3">Não Contribuinte</option>
                                                    @elseif($configEmpresa->contribuinte == "2" )
                                                        <option value="2">2-Contribuinte Isento</option>
                                                        <option value="1">1-Contribuinte</option>
                                                        <option value="3">Não Contribuinte</option>
                                                    @endif
                                                        <option value="3">Não Contribuinte</option>
                                                        <option value="1">1-Contribuinte</option>
                                                        <option value="2">2-Contribuinte Isento</option>

                                                </select>
                                            </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row ml-1">
                                    <div class="form-group">
                                        <a class="btn btn-danger" href="{{ URL::previous()}}">
                                                Cancelar<span class="badge badge-primary"></span>
                                        </a>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit">
                                            Atualizar<span class="badge badge-primary"></span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @else
                        <h4 class="header-title mt-0 mb-3 ">É Preciso configurar os parametros da Empresa</h4>
                        <form action="{{ route('empresa.updateConfig', $empresa->id) }}" method="POST">
                            @method('PUT')
                            @csrf
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="text-left">
                                        <p class="text-muted"><strong>Caminho Certificado XML:</strong> <span class="ml-2">
                                            <input type="text" name="caminho_certificado"  class="form-control" ></span></p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    {{-- <div class="text-left">
                                       <p class="text-muted"><strong>Caminho XML:</strong> <span class="ml-2"> <input type="text" name="caminho_xml" class="form-control" ></span></p>
                                    </div> --}}
                                </div>
                            </div>

                            <div class="row">
                                {{-- <div class="col-sm-6">
                                     <div class="text-left">
                                        <p class="text-muted"><strong>Caminho XML Assinado:</strong> <span class="ml-2"> <input type="text" name="caminho_xml_assinado" class="form-control"  ></span></p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="text-left">
                                        <p class="text-muted"><strong>Caminho XML Protocolado:</strong> <span class="ml-2"> <input type="text" name="caminho_xml_protocolado" class="form-control"  ></span></p>
                                    </div>
                                </div> --}}
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="text-left">
                                        <p class="text-muted"><strong>Senha Certificado:</strong> <span class="ml-2"> <input type="password" name="senha_cert" class="form-control" required></span></p>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="text-left">
                                        <p class="text-muted"><strong>Serie:</strong> <span class="ml-2"> <input type="text" name="serie"  class="form-control" required></span></p>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="text-left">
                                        <p class="text-muted"><strong>Nro Ultima NotaFiscal Emitida:</strong> <span class="ml-2"> <input type="text" name="ultNotaFiscalEmit" class="form-control"  required></span></p>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="text-left">
                                        <p class="text-muted"><strong>Versão:</strong> <span class="ml-2"> <input type="text" name="versao" class="form-control" required></span></p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="text-left">
                                        <p class="text-muted"><strong>Regime Tributário:</strong> <span class="ml-2"> <select type="text" name="regime_tributario" class="form-control" required>
                                            <option value="1">Simples Nacional</option>
                                            <option value="2">Simples Nacional com Substituição Tributária</option>
                                            <option value="3">Lucro Prezumido</option>
                                        </select></span></p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="text-left">
                                        <p class="text-muted"><strong>Atualização:</strong> <span class="ml-2"> <input type="text" name="atualizacao" class="form-control"  required></span></p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="text-left">
                                        <p class="text-muted"><strong>Schemes:</strong> <span class="ml-2"> <input type="text" name="schemes" class="form-control"  required></span></p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="text-left">
                                        <p class="text-muted"><strong>Token IBTP:</strong> <span class="ml-2"> <input type="text" name="token_ibtp" class="form-control" ></span></p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="text-left">
                                        <p class="text-muted"><strong>Tipo Ambiente</strong>
                                        <span class="ml-2">
                                            <select type="text" name="ambiente" class="form-control" required>
                                                <option value="1">Produção</option>
                                                <option value="2">Homologação</option>
                                            </select>
                                        </span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="text-left">
                                        <p class="text-muted"><strong>Contribuinte</strong>
                                        <span class="ml-2">
                                            <select type="text" name="contribuinte" class="form-control" required>
                                                <option value="1">1-Contribuinte</option>
                                                <option value="2">2-Contribuinte Isento</option>
                                                <option value="3">Não Contribuinte</option>
                                            </select>
                                        </span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="row ml-1">
                                <div class="form-group">
                                    <a class="btn btn-danger" href="{{ URL::previous()}}">
                                            Cancelar<span class="badge badge-primary"></span>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">
                                        Atualizar<span class="badge badge-primary"></span>
                                    </button>
                                </div>
                            </div>
                        </form>
                        @endif

                    </div>
                </div>
                <!-- Personal-Information -->
            </div>
            <!-- end col -->

        </div>
        <!-- end row -->

    </div>


@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@sweetalert2/theme-bootstrap-4/bootstrap-4.css">
@stop


@section('js')

    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>

    <script type="text/javascript">
        if(message == 'error'){
            Swal.fire(
                'Error!',
                'Erro ao editar registro!',
                'error'
            )
        }
        if(message == 'store'){
            Swal.fire(
                'Parabéns!',
                'Registro cadastrado com sucesso!',
                'success'
            )
        }
        if(message == 'update'){
            Swal.fire(
                'Parabéns!',
                'Registro editado com sucesso!',
                'success'
            )
        }

        if(message == 'destroy'){
            Swal.fire(
                'Parabéns!',
                'Registro excluido com sucesso!',
                'success'
            )
        }

    </script>

@stop
