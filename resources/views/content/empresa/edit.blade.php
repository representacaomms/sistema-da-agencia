@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('empresas.index')}}">Empresa</a></li>
                            <li class="breadcrumb-item active">Editar Empresa</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Empresa</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('empresa.update', $empresa->id) }}" method="POST">           
            @method('PUT')
            @csrf
            <input type="hidden" name="id" value="{{ $empresa->id }}">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="projectname">Tipo</label>
                                <select name="tipo" id="tipo" class="form-control">  
                                    <option value="pf" {{ (old('tipo') == 'pf'  ? 'selected' : ($empresa->tipo == 'pf' ? 'selected' : '') ) }}>Pessoa Fisica</option>                              
                                </select>
                                
                            </div>
                        </div>

                        <div class="col-md-4 fisica ">
                            <div class="form-group" >
                                <label id="label_cpf">CPF</label>
                                <input type="text" data-toggle="input-mask" data-mask-format="000.000.000-00" data-reverse="true"  name="cpf" class="form-control "  placeholder="Documento CPF" value="{{ old('cpf') ?? $empresa->cnpj }}">
                            </div>
                        </div> <!-- end col--> 
                        <div class="col-md-4 fisica ">
                            <div class="form-group" >
                                <label id="label_rg">RG</label>                            
                                <input type="text" data-toggle="input-mask" data-mask-format="00.000.000-a" data-reverse="true"  name="rg" class="form-control" placeholder="Documento RG" value="{{ old('rg') ?? $empresa->rg }}" >
                                            
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-2">
                            <div class="form-group" >
                                <label id="label_cpf">Status</label>
                                <select name="status"  class="form-control">                                
                                    <option value="A" {{ (old('tipo') == 'A'  ? 'selected' : ($empresa->status == 'A' ? 'selected' : '') ) }}>Ativo</option> 
                                    <option value="I" {{ (old('tipo') == 'I'  ? 'selected' : ($empresa->status == 'I' ? 'selected' : '') ) }}>Inativo</option>                               
                               
                                </select>              
                            </div>
                        </div> <!-- end col-->
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectname">Nome</label>
                                <input type="text"  name="nome" class="form-control"  style="text-transform: capitalize;" placeholder="Nome Completo" value="{{ old('nome') ?? $empresa->nome }}">
                            </div>                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="label_cpf">Email</label>
                                <input type="email"  name="email" class="form-control" value="{{ old('email') ?? $empresa->email }}" >  
                            </div>                            
                        </div>
                        <div class="col-md-4" >
                            <div class="form-group" >
                                <div class="form-group">
                                    <label id="label_cpf">Celular</label>
                                    <input type="text" data-toggle="input-mask" data-mask-format="(00) 00000-0000" id="celular" name="celular" class="form-control"  value="{{ old('celular') ?? $empresa->celular }}">
                                </div>                                            
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-4" >
                            <div class="form-group" >
                                <div class="form-group">
                                    <label id="label_cpf">Telefone</label>
                                    <input type="text" data-toggle="input-mask" data-mask-format="(00) 0000-0000" id="telefone" name="telefone" class="form-control"  value="{{ old('telefone') ?? $empresa->telefone }}">
                                </div>                                            
                            </div>
                        </div> <!-- end col-->
                        
                    </div>
                </div>
            </div> 
           
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Endereço</h5>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-xl-3">
                                        <div class="form-group">
                                            <label for="projectname">CEP</label>
                                            <input type="text" data-toggle="input-mask" data-mask-format="00.000-000" name="cep" class="form-control zip_code_search" placeholder="Digite seu CEP" value="{{ $empresa->endereco->cep ?? ''   }}"> 
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-5">
                                        <div class="form-group">
                                            <label for="projectname">Logradouro</label>
                                            <input type="text" name="logradouro" class="form-control logradouro" value="{{ $empresa->endereco->logradouro ?? ''}}"  >
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-4">
                                        <div class="form-group">
                                            <label for="projectname">Complemento</label>
                                            <input type="text" name="complemento" class="form-control complemento" style="text-transform: capitalize;" value="{{  $empresa->endereco->complemento ?? '' }}">
                                        </div>
                                        
                                    </div> <!-- end col-->
                                </div>
                
                                <div class="row">
                                    <div class="col-xl-2">
                                        <div class="form-group">
                                            <label for="projectname">Numero</label>
                                            <input type="text"  name="numero" class="form-control numero" value="{{  $empresa->endereco->numero ?? '' }}">
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-5">
                                        <div class="form-group">
                                            <label for="projectname">Bairro</label>
                                            <input type="text"  name="bairro" class="form-control bairro" value="{{ $empresa->endereco->bairro ?? ''}}" >
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-3">
                                        <div class="form-group">
                                            <label for="projectname">Cidade</label>
                                            <input type="text" name="localidade" class="form-control localidade" value="{{ $empresa->endereco->localidade ?? ''}}"  >
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-2">
                                        <div class="form-group">
                                            <label for="projectname">UF</label>
                                            <input type="text"  name="uf" class="form-control uf"  value="{{ $empresa->endereco->uf ?? ''}}"  >
                                        </div>
                                    </div> <!-- end col-->
                                </div>
                            </div> <!-- end col-->
                        </div> <!-- end card-body -->
                    </div> <!-- end card-->
                </div> <!-- end col-->
               
            </div> 
            
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="{{ URL::previous()}}">
                                                        Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2">
                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit">
                                                        Atualizar<span class="badge badge-primary"></span>
                                                    </button>
                                                </div>
                                            </div> <!-- end col-->                                       
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>                            
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>
          
        </form>
    </div> <!-- container -->

@endsection

@section('js')
    <script>
        var input_cpf = $('input[name="cpf"]').val();
        if(input_cpf){
            $('input[name="cpf"]').attr('readonly', true)
        }
        
       
        $('select[name="tipo"]').change( function() {
            if($('select[name="tipo"]').val() === 'pf'){
                $('.juridica').addClass('d-none');     
                $('.fisica').removeClass('d-none');     
            } else{
                $('.fisica').addClass('d-none');     
                $('.juridica').removeClass('d-none'); 
            }
        })
    </script>
@endsection