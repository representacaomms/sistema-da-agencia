
    @extends('layouts.app')
    @section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <input type="hidden" id="lista_contas" value="{{$lista_contas}}"/>
        <input type="hidden" id="contas_bancaria" value="{{$contas_bancaria}}"/>
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <form class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control form-control-light" id="dash-daterange">
                                    <div class="input-group-append">
                                        <span class="input-group-text bg-primary border-primary text-white">
                                            <i class="mdi mdi-calendar-range font-13"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript: void(0);" class="btn btn-primary ml-2">
                                <i class="mdi mdi-autorenew"></i>
                            </a>
                            <a href="javascript: void(0);" class="btn btn-primary ml-1">
                                <i class="mdi mdi-filter-variant"></i>
                            </a>
                        </form>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-5 col-lg-6">

                <div class="row">
                    <div class="col-sm-6">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-end">
                                    <i class="mdi mdi-account-multiple widget-icon"></i>
                                </div>
                                <h5 class="text-muted fw-normal mt-0" title="Numeros de Clientes">Clientes</h5>
                                <h3 class="mt-3 mb-3">{{$dados['clientes']}}</h3>
                                <p class="mb-0 text-muted">
                                    <span class="{{$dados['clientes_percentual'] < 0 ? 'text-danger' : 'text-success'}} me-2"><i class="mdi {{$dados['clientes_percentual'] < 0 ? 'mdi-arrow-down-bold' : 'mdi-arrow-up-bold'}} "></i> {{abs($dados['clientes_percentual']) }}%</span>
                                    <span class="text-nowrap">Desde o último mês</span>  
                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->

                    <div class="col-sm-6">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-end">
                                    <i class="mdi mdi-cart-plus widget-icon"></i>
                                </div>
                                <h5 class="text-muted fw-normal mt-0" title="Numero de Propostas">Propostas</h5>
                                <h3 class="mt-3 mb-3">{{$dados['propostas_percentual']}}</h3>
                                <p class="mb-0 text-muted">
                                    <span class="{{$dados['propostas_percentual'] < 0 ? 'text-danger' : 'text-success'}} me-2"><i class="mdi {{$dados['propostas_percentual'] < 0 ? 'mdi-arrow-down-bold' : 'mdi-arrow-up-bold'}} "></i> {{abs($dados['propostas_percentual']) }}%</span>
                                    <span class="text-nowrap">Desde o último mês</span>
                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-sm-6">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-end">
                                    <i class="mdi mdi-currency-usd widget-icon"></i>
                                </div>
                                <h5 class="text-muted fw-normal mt-0" title="Receita Média">Receitas</h5>
                                <h3 class="mt-3 mb-3">$6,254</h3>
                                <p class="mb-0 text-muted">
                                    <span class="text-danger me-2"><i class="mdi mdi-arrow-down-bold"></i> 7.00%</span>
                                    <span class="text-nowrap">Desde o último mês</span>
                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->

                    <div class="col-sm-6">
                        <div class="card widget-flat">
                            <div class="card-body">
                                <div class="float-end">
                                    <i class="mdi mdi-pulse widget-icon"></i>
                                </div>
                                <h5 class="text-muted fw-normal mt-0" title="Crescimento">Crescimento</h5>
                                <h3 class="mt-3 mb-3">+ 30.56%</h3>
                                <p class="mb-0 text-muted">
                                    <span class="text-success me-2"><i class="mdi mdi-arrow-up-bold"></i> 4.87%</span>
                                    <span class="text-nowrap">Desde o último mês</span>
                                </p>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col-->
                </div> <!-- end row -->

            </div> <!-- end col -->

            <div class="col-xl-7 col-lg-6">
                <div class="card card-h-100">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-2">
                            <h4 class="header-title">Projeções Vs Reais</h4>
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle arrow-none card-drop" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="mdi mdi-dots-vertical"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-end">
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item">Sales Report</a>
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item">Export Report</a>
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item">Profit</a>
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item">Action</a>
                                </div>
                            </div>
                        </div>

                        <div dir="ltr">
                            <div id="high-performing-product" class="apex-charts" data-colors="#727cf5,#e3eaef"></div>
                            <canvas id="myChart" width="400" height="250"></canvas>
                        </div>
                            
                    </div> <!-- end card-body-->
                </div> <!-- end card-->

            </div> <!-- end col -->
        </div>
        <!-- end row -->
        
        <!-- end row -->
       {{--  <div class="row">
            <div class="col-md-6 col-xxl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-2">
                            <h4 class="header-title">COntas Pagar - Receber</h4>
                            <div class="dropdown">
                                <select name="periodo" id="periodo" class="form-control" onchange="selecionaPeriodoAPagar(value)">
                                    <option value="7" selected>7 dias</option>
                                    <option value="15">15 Dias</option>
                                    <option value="30">30 Dias</option>
                                    <option value="60">2 meses</option>
                                    <option value="90">3 meses</option>
                                    <option value="120">4 meses</option>
                                    <option value="150">5 meses</option>
                                    <option value="180">6 meses</option>
                                    <option value="365">12 meses</option>
                                </select>
                            </div>
                        </div>

                        <div class="border border-light p-3 rounded mb-3">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <p class="font-18 mb-1">Contas a Receber</p>
                                    <h3 class=" my-0" id='saldo_contas_a_receber'></h3>
                                </div>  
                                <div class="avatar-sm">
                                    <span class="avatar-title bg-primary rounded-circle h3 my-0">
                                        <i class="mdi mdi-arrow-up-bold-outline"></i>
                                    </span>
                                </div>                                      
                            </div>
                        </div>

                        <div class="border border-light p-3 rounded mb-3">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <p class="font-18 mb-1">Contas a Pagar </p>
                                    <h3 class="my-0" id='saldo_contas_a_pagar'></h3>
                                </div>  
                                <div class="avatar-sm">
                                    <span class="avatar-title bg-danger rounded-circle h3 my-0">
                                        <i class="mdi mdi-arrow-down-bold-outline"></i>
                                    </span>
                                </div>                                      
                            </div>
                        </div>

                        <div class="border border-light p-3 rounded">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <p class="font-18 mb-1">Saldo</p>
                                    <h3 class="text-primary my-0" id="saldo_a_pagar_text"></h3>
                                </div>  
                                <div class="avatar-sm">
                                    <span class="avatar-title bg-success rounded-circle h3 my-0">
                                        <i class="mdi mdi-swap-horizontal"></i>
                                    </span>
                                </div>                                      
                            </div>
                        </div>
                        <a href="{{route('receitas.index')}}" class="btn btn-outline-primary mt-3 float-right">Ver Receitas
                            <i class="mdi mdi-arrow-right ml-2 "></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xxl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-2">
                            <h4 class="header-title">Contas Pagas - Recebidas</h4>
                            <div class="dropdown">
                                <select name="periodo" id="periodo" class="form-control" onchange="selecionaPeriodoPagar(value)">
                                    <option value="7" selected>7 dias</option>
                                    <option value="15">15 Dias</option>
                                    <option value="30">30 Dias</option>
                                    <option value="60">2 meses</option>
                                    <option value="90">3 meses</option>
                                    <option value="120">4 meses</option>
                                    <option value="150">5 meses</option>
                                    <option value="180">6 meses</option>
                                    <option value="365">12 meses</option>
                                </select>
                            </div>
                        </div>

                        <div class="border border-light p-3 rounded mb-3">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <p class="font-18 mb-1">Contas Recebidas</p>
                                    <h3 class="my-0" id='saldo_contas_recebidas'></h3>
                                </div>  
                                <div class="avatar-sm">
                                    <span class="avatar-title bg-primary rounded-circle h3 my-0">
                                        <i class="mdi mdi-arrow-up-bold-outline"></i>
                                    </span>
                                </div>                                      
                            </div>
                        </div>

                        <div class="border border-light p-3 rounded mb-3">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <p class="font-18 mb-1">Contas Pagas</p>
                                    <h3 class="my-0" id='saldo_contas_pagas'><h3>
                                </div>  
                                <div class="avatar-sm">
                                    <span class="avatar-title bg-danger rounded-circle h3 my-0">
                                        <i class="mdi mdi-arrow-down-bold-outline"></i>
                                    </span>
                                </div>                                      
                            </div>
                        </div>

                        <div class="border border-light p-3 rounded">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <p class="font-18 mb-1">Saldo</p>
                                    <h3 class="text-primary  my-0" id="saldo_pago_text"></h3>
                                </div>  
                                <div class="avatar-sm">
                                    <span class="avatar-title bg-success rounded-circle h3 my-0">
                                        <i class="mdi mdi-swap-horizontal"></i>
                                    </span>
                                </div>                                      
                            </div>
                        </div>

                        <a href="{{route('despesas.index')}}" class="btn btn-outline-danger mt-3 float-right">Ver Despesas
                            <i class="mdi mdi-arrow-right ml-2 "></i>
                        </a>
                    </div>
                </div>
            </div>
           
        </div> --}}
        <!-- end row -->


        {{-- <div class="row">
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-body">
                        <a href="" class="btn btn-sm btn-link float-right mb-3">Export
                            <i class="mdi mdi-download ml-1"></i>
                        </a>
                        <h4 class="header-title mt-2">Top Selling Products</h4>

                        <div class="table-responsive">
                            <table class="table table-centered table-hover mb-0">
                                <tbody>
                                    <tr>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">ASOS Ridley High Waist</h5>
                                            <span class="text-muted font-13">07 April 2018</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">$79.49</h5>
                                            <span class="text-muted font-13">Price</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">82</h5>
                                            <span class="text-muted font-13">Quantity</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">$6,518.18</h5>
                                            <span class="text-muted font-13">Amount</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">Marco Lightweight Shirt</h5>
                                            <span class="text-muted font-13">25 March 2018</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">$128.50</h5>
                                            <span class="text-muted font-13">Price</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">37</h5>
                                            <span class="text-muted font-13">Quantity</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">$4,754.50</h5>
                                            <span class="text-muted font-13">Amount</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">Half Sleeve Shirt</h5>
                                            <span class="text-muted font-13">17 March 2018</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">$39.99</h5>
                                            <span class="text-muted font-13">Price</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">64</h5>
                                            <span class="text-muted font-13">Quantity</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">$2,559.36</h5>
                                            <span class="text-muted font-13">Amount</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">Lightweight Jacket</h5>
                                            <span class="text-muted font-13">12 March 2018</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">$20.00</h5>
                                            <span class="text-muted font-13">Price</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">184</h5>
                                            <span class="text-muted font-13">Quantity</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">$3,680.00</h5>
                                            <span class="text-muted font-13">Amount</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">Marco Shoes</h5>
                                            <span class="text-muted font-13">05 March 2018</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">$28.49</h5>
                                            <span class="text-muted font-13">Price</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">69</h5>
                                            <span class="text-muted font-13">Quantity</span>
                                        </td>
                                        <td>
                                            <h5 class="font-14 mb-1 font-weight-normal">$1,965.81</h5>
                                            <span class="text-muted font-13">Amount</span>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div> <!-- end table-responsive-->
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->

            <div class="col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="dropdown float-right">
                            <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                                <i class="mdi mdi-dots-vertical"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Sales Report</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Export Report</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Profit</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Action</a>
                            </div>
                        </div>
                        <h4 class="header-title">Total Sales</h4>

                        <div class="mb-5 mt-4 chartjs-chart" style="height: 201px; max-width: 180px;">
                            <canvas id="average-sales"></canvas>
                        </div>

                        <div class="chart-widget-list">
                            <p>
                                <i class="mdi mdi-square text-primary"></i> Direct
                                <span class="float-right">$300.56</span>
                            </p>
                            <p>
                                <i class="mdi mdi-square text-danger"></i> Affilliate
                                <span class="float-right">$135.18</span>
                            </p>
                            <p>
                                <i class="mdi mdi-square text-success"></i> Sponsored
                                <span class="float-right">$48.96</span>
                            </p>
                            <p class="mb-0">
                                <i class="mdi mdi-square"></i> E-mail
                                <span class="float-right">$154.02</span>
                            </p>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->

            <div class="col-xl-3">
                <div class="card bg-primary">
                    <div class="card-body">
                        <div class="dropdown float-right">
                            <a href="#" class="dropdown-toggle text-white arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                                <i class="mdi mdi-dots-vertical"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Sales Report</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Export Report</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Profit</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Action</a>
                            </div>
                        </div>
                        <h4 class="header-title text-white mb-4">Average Sale Size</h4>
                        <div class="text-center">
                            <p class="mt-4 mb-2">
                                <span class="badge badge-danger">-23.47 %</span>
                            </p>
                            <h3 class="font-weight-normal text-white mb-2">$154.21</h3>
                            <p class="text-light text-uppercase font-13 font-weight-bold">Per Sale</p>
                            <a href="javascript: void(0);" class="btn btn-outline-light btn-sm mb-1">More
                                <i class="mdi mdi-arrow-right ml-1"></i>
                            </a>

                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div> <!-- end col-->

                <div class="card">
                    <div class="card-body">
                        <div class="dropdown float-right">
                            <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                                <i class="mdi mdi-dots-vertical"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Sales Report</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Export Report</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Profit</a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">Action</a>
                            </div>
                        </div>
                        <h4 class="header-title mb-2">Recent Activity</h4>

                        <div class="slimscroll" style="max-height: 171px;">
                            <div class="timeline-alt pb-0">
                                <div class="timeline-item">
                                    <i class="mdi mdi-upload bg-info-lighten text-info timeline-icon"></i>
                                    <div class="timeline-item-info">
                                        <a href="#" class="text-info font-weight-bold mb-1 d-block">You sold an item</a>
                                        <small>Paul Burgess just purchased “Hyper - Admin Dashboard”!</small>
                                        <p class="mb-0 pb-2">
                                            <small class="text-muted">5 minutes ago</small>
                                        </p>
                                    </div>
                                </div>

                                <div class="timeline-item">
                                    <i class="mdi mdi-airplane bg-primary-lighten text-primary timeline-icon"></i>
                                    <div class="timeline-item-info">
                                        <a href="#" class="text-primary font-weight-bold mb-1 d-block">Product on the Bootstrap Market</a>
                                        <small>Dave Gamache added
                                            <span class="font-weight-bold">Admin Dashboard</span>
                                        </small>
                                        <p class="mb-0 pb-2">
                                            <small class="text-muted">30 minutes ago</small>
                                        </p>
                                    </div>
                                </div>

                                <div class="timeline-item">
                                    <i class="mdi mdi-microphone bg-info-lighten text-info timeline-icon"></i>
                                    <div class="timeline-item-info">
                                        <a href="#" class="text-info font-weight-bold mb-1 d-block">Robert Delaney</a>
                                        <small>Send you message
                                            <span class="font-weight-bold">"Are you there?"</span>
                                        </small>
                                        <p class="mb-0 pb-2">
                                            <small class="text-muted">2 hours ago</small>
                                        </p>
                                    </div>
                                </div>

                                <div class="timeline-item">
                                    <i class="mdi mdi-upload bg-primary-lighten text-primary timeline-icon"></i>
                                    <div class="timeline-item-info">
                                        <a href="#" class="text-primary font-weight-bold mb-1 d-block">Audrey Tobey</a>
                                        <small>Uploaded a photo
                                            <span class="font-weight-bold">"Error.jpg"</span>
                                        </small>
                                        <p class="mb-0 pb-2">
                                            <small class="text-muted">14 hours ago</small>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- end timeline -->
                        </div> <!-- end slimscroll -->
                    </div>
                    <!-- end card-body -->
                </div>
                <!-- end card-->
            </div>
            <!-- end col -->

        </div> --}}
        <!-- end row -->

    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

    <script>
        const labels = ['JAN','FEV','MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'];
      
    const data = {
        labels: labels,
        datasets: [
            {
            label: 'Propostas',
            data: [10, 16, 18, 10, 15, 18, 12],
            backgroundColor: 'rgba(255, 190, 132, 0.2)' ,
            
            stack: 'Stack 0',
        },
        {
            label: 'Vendas',
            data: [2, 4, 4, 3, 6, 11, 2],
            backgroundColor: 'rgba(255, 99, 132, 0.2)' ,
            stack: 'Stack 0',
        }
        ]
    };
      
    const config = {
        type: 'bar',
        data: data,
        options: {
            plugins: {
                title: {
                    display: true,
                    text: 'Chart.js Bar Chart - Stacked'
                },
            },
            responsive: true,
            interaction: {
                intersect: false,
            },
            scales: {
                x: {
                    stacked: true,
                },
                y: {
                    stacked: true
                }
            }
        }
    };
    </script>
    <script>
        const myChart = new Chart(
            document.getElementById('myChart'),
            config
        );
    </script>
    <script>
        let lista_contas = JSON.parse(document.getElementById('lista_contas').value);
        let contas_bancaria = JSON.parse(document.getElementById('contas_bancaria').value);

        const date = moment().format('YYYY-MM-DD');
        let data_final = ''

        // Status = recebido - A Receber - Pago - A Pagar
        const lista_contas_all = filtroAvancado(date, data_final, 'A Receber');
        selecionaPeriodoPagar(7);
        selecionaPeriodoAPagar(7);


        function filtroAvancado(data_inicial, data_final,  status ) {

            return   lista_contas.filter((item) => {
                if (status == 'Pago' || status == 'Recebido') {
                return  item.status == status && item.data_pagto <= data_inicial && item.data_pagto >= data_final
                }else {
                return  item.status == status && item.data_pagto >= data_inicial && item.data_pagto <= data_final
                }
            });
        }

        function converteData(value, params) {
            return params == 'add' ? moment().add(value, 'day').format('YYYY-MM-DD') : moment().subtract(value, 'day').format('YYYY-MM-DD')
        }

        function converteAdd_Sub(status) {
            return  resultado =   status == 'A Pagar' || status == 'A Receber' ?  'add' :  'subtract';
        }

        function selecionaPeriodoPagar(value) {
            
            saldo_contas_pagas.textContent = 'R$ '+filtroDataStatus('Pago', value);
            saldo_contas_recebidas.textContent = 'R$ '+filtroDataStatus('Recebida', value);

            saldo_pago_text.textContent = 'R$ '+ (filtroDataStatus('Recebido', value) - filtroDataStatus('Pago', value)).toFixed(2);
            saldo_pago = (filtroDataStatus('Recebido', value) - filtroDataStatus('Pago', value)).toFixed(2);
            

           if ( Math.sign(saldo_pago)  == -1 ) {
            saldo_pago_text.classList.remove('text-primary');
            saldo_pago_text.classList.add('text-danger');
           }
            
        }

        function selecionaPeriodoAPagar(value) {
            
            saldo_contas_a_pagar.textContent = 'R$ '+filtroDataStatus('A Pagar', value);
            saldo_contas_a_receber.textContent = 'R$ '+filtroDataStatus('A Receber', value);

            saldo_a_pagar_text.textContent = 'R$ '+ (filtroDataStatus('A Receber', value) - filtroDataStatus('A Pagar', value)).toFixed(2);
            saldo_a_pagar = (filtroDataStatus('A Receber', value) - filtroDataStatus('A Pagar', value)).toFixed(2);
            

           if (  Math.sign(saldo_a_pagar)  == -1) {
            saldo_pago_text.classList.remove('text-primary');
            saldo_pago_text.classList.add('text-danger');
           }
            
        }

        function selecionaTipo(status) {
            
        }

        function filtroDataStatus(status, value) {
            let add_sub = '';

            add_sub    = converteAdd_Sub(status)
            data_final = converteData(value, add_sub)
            
            const resultado = filtroAvancado(date, data_final,  status );
            const valor_total =  resultado.map(li => parseFloat(li.valor_parcela)).reduce((sum, val) => sum + val, 0)

                if (valor_total > 0) {
                return valor_total.toFixed(2)
                } else {
                return valor_total
                }
                
        }


    </script>
  @endsection
 


