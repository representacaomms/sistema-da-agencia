@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('contratos.index') }}">Contratoss</a></li>
                            <li class="breadcrumb-item active">Adicionar Contratos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Contratos</h4>

                </div>
            </div>
        </div>

        <form action="{{ route('contratos.store') }}" method="post">
            @csrf
            @method('POST')

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="projectname">Tipo</label>
                                <select name="tipo" id="tipo" class="form-control">
                                    <option value="consultoria"selected>Contratos</option>
                                    <option value="concorrente">Concorrente</option>
                                    <option value="referencia">Referência</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="projectname">Nome</label>
                                <input type="text"  name="nome" class="form-control"  style="text-transform: capitalize;" placeholder="Nome Completo" required>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group" >
                                <div class="form-group">
                                    <label for="email">E-mail</label>
                                    <input type="email" name="email" class="form-control"   placeholder="Email da Empresa" >
                                </div>
                            </div>
                        </div> <!-- end col-->
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="ativo"selected>Ativo</option>
                                    <option value="inativo">Inativo</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="projectname">Site</label>
                                <input type="text"  name="site" class="form-control"  placeholder="Site">
                            </div>
                        </div>
                       
                    
                        <div class="col-md-2">
                            <div class="form-group">
                                <label id="label_cpf">Instagram</label>
                                <input type="text"  name="instagram" class="form-control" required placeholder="Seguidores">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" >
                                <label id="label_cpf">Facebook</label>
                                <input type="text"  name="facebook" class="form-control" required placeholder="Seguidores">
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-2">
                            <div class="form-group" >
                                <label id="label_cpf">Youtube</label>
                                <input type="text"  name="youtube" class="form-control" required placeholder="Inscritos">
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-2">
                            <div class="form-group" >
                                <label id="label_cpf">GMN</label>
                                <input type="text"  name="gmn" class="form-control" required placeholder="Seguidores">
                            </div>
                        </div> <!-- end col-->

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-8">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="{{ URL::previous()}}">
                                                        Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2 pl-2">
                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit">
                                                            Adicionar<span class="badge badge-primary"></span>
                                                    </button>
                                                </div>
                                            </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>
        </form>
    </div>



@section('js')
    <script>
         
    </script>
@endsection

@endsection
