@extends('layouts.app')
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Vendas</a></li>
                            <li class="breadcrumb-item active">Contratos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Contratos</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="container">
                        <div class="card-body">

                            <!-- Invoice Logo-->
                            <div class="clearfix">
                                <div class="float-center mb-1 pt-3">
                                    <img src="https://agenciamms.com/wp-content/uploads/2021/11/cropped-logo.png" alt=""
                                        height="90">
                                    <h6>Agência MMS, Araras/SP</h6>
                                </div>
                                <div class="float-center pt-1">
                                    <h4 class="m-0 d-print-none " style="text-transform: uppercase;">BRIEFING PARA
                                        {{ $dados['consultoria']->nome }} - Nº {{ $nro_doc }}
                                    </h4>
                                </div>
                            </div>

                            <!-- Invoice Detail-->
                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10">
                                    <div class=" mt-3">

                                        <p class="text-left font-16">
                                            No presente <b>documento de consultoria</b> visamos entender necessidades do
                                            projeto para assim elaborar uma solução de acordo com as expectativas aqui
                                            definidas.
                                        </p>
                                        <p class="text-left font-16">
                                            <b>PRESENÇA DIGITAL:</b> Com base nos dados obtidos no digital
                                            tomamos a liberdade de fazer uma rápida pesquisa de mercado, obtivemos os
                                            seguintes dados:
                                        </p>
                                    </div>

                                </div><!-- end col -->
                                <div class="col-sm-1"></div>

                            </div>
                            <!-- end row -->

                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10">
                                    <div class=" mt-3">
                                        <table class="table table-bordered border-primary table-centered mb-0"
                                            id="consultorias-datatable">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Visitas no Site</th>
                                                    <th>Instagram</th>
                                                    <th>Facebook</th>
                                                    <th>Youtube</th>
                                                    <th>GMN</th>
                                                </tr>
                                            </thead>
                                            <h3 class="text-left">Presença Digital:</h3>
                                            <tbody>
                                                <tr>

                                                    <td>
                                                        <p class="m-0 d-inline-block align-middle font-12">
                                                            {{ $dados['consultoria']->site }}
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p class="m-0 d-inline-block align-middle font-12">
                                                            {{ $dados['consultoria']->instagram }}
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p class="m-0 d-inline-block align-middle font-12">
                                                            {{ $dados['consultoria']->facebook }}
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p class="m-0 d-inline-block align-middle font-12">
                                                            {{ $dados['consultoria']->youtube }}
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p class="m-0 d-inline-block align-middle font-12">
                                                            {{ $dados['consultoria']->gmn }}
                                                        </p>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>

                                </div><!-- end col -->
                                <div class="col-sm-1"></div>

                            </div>

                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10">
                                    <div class=" mt-3">
                                        <table class="table table-bordered border-primary table-centered mb-0"
                                            id="consultorias-datatable">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th></th>
                                                    <th>Site</th>
                                                    <th>Instagram</th>
                                                    <th>Facebook</th>
                                                    <th>Youtube</th>
                                                    <th>GMN</th>
                                                </tr>
                                            </thead>
                                            <h3 class="text-left">Mercado:</h3>
                                            <tbody>
                                                @foreach ($dados['concorrente'] as $key => $item)
                                                    <tr>
                                                        <td>
                                                            <p class="m-0 d-inline-block align-middle font-12">
                                                                {{ $item->nome }}
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <p class="m-0 d-inline-block align-middle font-12">
                                                                {{ $item->site }}
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <p class="m-0 d-inline-block align-middle font-12">
                                                                {{ $item->instagram }}
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <p class="m-0 d-inline-block align-middle font-12">
                                                                {{ $item->facebook }}
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <p class="m-0 d-inline-block align-middle font-12">
                                                                {{ $item->youtube }}
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <p class="m-0 d-inline-block align-middle font-12">
                                                                {{ $item->gmn }}
                                                            </p>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                @foreach ($dados['referencia'] as $key => $item)
                                                    <tr class="bg-success">
                                                        <td>
                                                            <p class="m-0 d-inline-block align-middle font-12">
                                                                {{ $item->nome }}

                                                            </p>
                                                        </td>
                                                        <td>
                                                            <p class="m-0 d-inline-block align-middle font-12">
                                                                {{ $item->site }}
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <p class="m-0 d-inline-block align-middle font-12">
                                                                {{ $item->instagram }}
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <p class="m-0 d-inline-block align-middle font-12">
                                                                {{ $item->facebook }}
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <p class="m-0 d-inline-block align-middle font-12">
                                                                {{ $item->youtube }}
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <p class="m-0 d-inline-block align-middle font-12">
                                                                {{ $item->gmn }}
                                                            </p>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>

                                </div><!-- end col -->
                                <div class="col-sm-1"></div>

                            </div>
                           
                            <div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-10 pt-2">

                                    <h3 class="text-left">Observação:</h3>
                                    <div id="snow-editor" style="height: 300px"></div>
                                </div>
                                <div class="col-md-1"></div>
                                
                            </div>
                             <!-- end row -->
                             <div class="d-print-none mt-4" style="page-break-inside:avoid">
                                <div class="text-right">
                                    <a href="javascript:window.print()" class="btn btn-primary"><i
                                            class="mdi mdi-printer"></i> Imprimir</a>
                                    <a href="{{ route('consultorias.index') }}" class="btn btn-info">Fechar</a>

                                </div>
                            </div>

                            <!-- end buttons -->

                        </div> <!-- end card-body-->
                    </div>
                </div> <!-- end card -->
            </div> <!-- end col-->
        </div>
        <!-- end row -->

    </div> <!-- container -->

@endsection
@section('css')
    <!-- Include stylesheet -->
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection
@section('js')
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <!-- Initialize Quill editor -->
    <script>
        var ColorClass = Quill.import('attributors/class/color');
        var SizeStyle = Quill.import('attributors/style/size');
        Quill.register(ColorClass, true);
        Quill.register(SizeStyle, true);
        var quill = new Quill('#snow-editor', {
            modules: {
                toolbar: true // Snow includes toolbar by default
            },
            theme: 'snow'
        });
    </script>
@endsection
