@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('funcoes.index') }}">Funções</a></li>
                            <li class="breadcrumb-item active">Adicionar Permissão</li>
                        </ol>
                    </div>
                    <h3 class="page-title">Adicionar Permissão</h3>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('funcoes.permissoesSync', $funcao->id) }}" method="POST" novalidate>
            @method('PUT')
            @csrf
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Permissões para: {{ $funcao->name }}</h4>
                                    @foreach ($permissoes as $key => $permissao)
                                            <div class="col-6">
                                                <div class="custom-control custom-checkbox">
                                                    <p class="m-0 d-inline-block align-middle font-16">
                                                        <input type="checkbox" class="custom-control-input"
                                                            name="{{ $permissao->id }}" id="{{ $permissao->id }}"
                                                            {{ $permissao->can == 1 ? 'checked' : '' }}>
                                                        <label class="custom-control-label"
                                                            for="{{ $permissao->id }}">{{ $permissao->name }}</label>
                                                    </p>
                                                </div>
                                            </div>
                                    @endforeach

                            <div class="row mt-2">
                                <div class="col-xl-1">
                                    <div class="form-group">
                                        <a class="btn btn-danger" href="{{ URL::previous() }}">
                                            Cancelar<span class="badge badge-primary"></span>
                                        </a>
                                    </div>
                                </div> <!-- end col-->
                                <div class="col-xl-1 ml-2">
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit">
                                            Sincronizar<span class="badge badge-primary"></span>
                                        </button>
                                    </div>
                                </div> <!-- end col-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div> <!-- container -->

@endsection

@section('js')
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function() {

            $('#permissoes-datatable').DataTable({
                language: {
                    processing: "Processamento...",
                    search: "Pesquisar&nbsp;:",
                    lengthMenu: "Mostrar _MENU_ entradas",
                    info: "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty: "Exibindo da entrada 0 até 0 de 0 entradas",
                    infoFiltered: "(filtro total de _MAX_ entradas)",
                    infoPostFix: "",
                    loadingRecords: "Carregando ...",
                    zeroRecords: "Nenhuma entrada encontrada",
                    emptyTable: "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first: "Primeiro",
                        previous: "Anterior",
                        next: "Ultimo",
                        last: "Depos"
                    },
                    aria: {
                        sortAscending: ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        });
    </script>
    <script type="text/javascript">
        if (message == 'store') {
            Swal.fire(
                'Parabéns!',
                'Registro cadastrado com sucesso!',
                'success'
            )
        }
        if (message == 'update') {
            Swal.fire(
                'Parabéns!',
                'Registro editado com sucesso!',
                'success'
            )
        }

        if (message == 'destroy') {
            Swal.fire(
                'Alerta!',
                'Registro inativado com sucesso!',
                'info'
            )
        }

        if (message == 'active') {
            Swal.fire(
                'Alerta!',
                'Registro ativado com sucesso!',
                'info'
            )
        }
    </script>

@stop
