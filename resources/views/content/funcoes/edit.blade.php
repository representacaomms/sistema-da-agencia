@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('clientes.index')}}">Funções</a></li>
                            <li class="breadcrumb-item active">Editar Função</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Função</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('funcoes.update', $record->id) }}" method="POST" novalidate>
            @method('PUT')
            @csrf
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectname">Nome da Função</label>
                                <input type="text"  name="name" class="form-control"  style="text-transform: capitalize;" placeholder="Nome da função" value="{{old('name') ?? $record->name}}" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="{{ URL::previous()}}">
                                                        Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2 ml-3">
                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit">
                                                            Atualizar<span class="badge badge-primary"></span>
                                                    </button>
                                                </div>
                                            </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>

        </form>
    </div> <!-- container -->

@endsection


