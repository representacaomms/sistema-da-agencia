@extends('layouts.app')
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('produtos.index') }}">Produtos</a></li>
                            <li class="breadcrumb-item active">Adicionar Item</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Item</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('item-produto.store') }}" method="POST">
            <input type="hidden" name="id_produto" value="{{ $id }}">
            @method('POST')
            @csrf
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Nome</label>
                        <div class="col-9">
                            <input type="text" class="form-control" style="text-transform: capitalize;" id="nome"
                                name="nome" placeholder="Nome" required>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Descrição</label>
                        <div class="col-9">
                            <textarea class="form-control" id="descricao" name="descricao" placeholder="Descrição"
                                required></textarea>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Valor</label>
                        <div class="col-9">
                            <input type="text" class="form-control" id="valor" name="valor" placeholder="R$" required />
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Situacao</label>
                        <div class="col-9">

                            <select name="situacao" id="situacao" class="form-control">
                                <option value="Ativo">Ativo</option>
                                <option value="Inativo">Inativo</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="{{ URL::previous() }}">
                                                    Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit">
                                                    Adicionar<span class="badge badge-primary"></span>
                                                </button>
                                            </div>
                                        </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>

        </form>
    </div> <!-- container -->


@endsection
