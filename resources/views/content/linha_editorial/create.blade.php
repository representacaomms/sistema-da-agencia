@extends('layouts.app')
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('linhas-editoriais.index') }}">Linha Editorial</a></li>
                            <li class="breadcrumb-item active">Adicionar Linha Editorial</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Linha Editorial</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('linhas-editoriais.store') }}" method="POST">
            @method('POST')
            @csrf
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">DADOS LINHA EDITORIAL</h5>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">CLiente</label>
                        <div class="col-9">
                            <div class="form-group id_cliente">
                                <label for="projectname">Cliente</label>
                                <select class="form-control select2" data-toggle="select2" name="id_cliente"
                                    id="id_cliente">
                                    <option value="0">Selecione o Cliente ...</option>
                                    @foreach ($pessoas as $item)
                                        <option value="{{ $item->id }}" style="text-transform: capitalize;">
                                            {{ $item->fantasia }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Produto/Serviço/Marca (PSM)</label>
                        <div class="col-9">
                            <textarea class="form-control" id="psm" name="psm" placeholder="Produto/Serviço/Marca (PSM)"
                                required></textarea>
                        </div>
                    </div>                 

                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">BIG IDEA</h5>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Persona</label>
                        <div class="col-9">
                            <input type="text" class="form-control" style="text-transform: capitalize;" id="publico_alvo"
                                name="publico_alvo" placeholder="Publico Alvo" required>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Mundo Ideal</label>
                        <div class="col-9">
                            <textarea class="form-control" id="mundo_ideal" name="mundo_ideal" placeholder="Mundo Ideal"
                                required></textarea>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Promessa</label>
                        <div class="col-9">
                            <textarea class="form-control" id="promessa" name="promessa" placeholder="Promessa"
                                required></textarea>
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Motivo Racional</label>
                        <div class="col-9">
                            <textarea class="form-control" id="motivo_racional" name="motivo_racional" placeholder="Motivo Racional"
                                required></textarea>
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Vantagem Competitiva</label>
                        <div class="col-9">
                            <textarea class="form-control" id="vantagem_competitiva" name="vantagem_competitiva" placeholder="Vantagem Competitiva"
                                required></textarea>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Posição Unica Vendas (USP)</label>
                        <div class="col-9">
                            <textarea class="form-control" id="posicao_unica_venda" name="posicao_unica_venda" placeholder="Posição Unica Vendas (USP)"
                                required></textarea>
                        </div>
                    </div>

                   

                </div>
            </div>

            <div class="card">  
                <div class="card-body">
                    <h5 class="card-title">CLIENTE IDEAL</h5>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Persona</label>
                        <div class="col-9">
                            <input type="text" class="form-control" style="text-transform: capitalize;" id="persona"
                                name="persona" placeholder="Persona" required>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">O que ele Busca</label>
                        <div class="col-9">
                            <textarea class="form-control" id="o_que_ele_busca" name="o_que_ele_busca" placeholder="O que ele Busca"
                                required></textarea>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">O que ele Teme</label>
                        <div class="col-9">
                            <textarea class="form-control" id="o_que_ele_teme" name="o_que_ele_teme" placeholder="O que ele Teme"
                                required></textarea>
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">O que impede Ele</label>
                        <div class="col-9">
                            <textarea class="form-control" id="o_que_impede_ele" name="o_que_impede_ele" placeholder="O que impede Ele"
                                required></textarea>
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Como Sair de A e chegar a B</label>
                        <div class="col-9">
                            <textarea class="form-control" id="a_b" name="a_b" placeholder="Como Sair de A e chegar a B"
                                required></textarea>
                        </div>
                    </div>

                </div>
            </div>

            <div class="card">  
                <div class="card-body">
                    <h5 class="card-title">LINHA EDITORIAL</h5>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Linha Principal</label>
                        <div class="col-9">
                            <input type="text" class="form-control" style="text-transform: capitalize;" id="linha_principal"
                                name="linha_principal" placeholder="Linha Principal" required>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Auxiliar 1</label>
                        <div class="col-9">
                            <textarea class="form-control" id="auxiliar1" name="auxiliar1" placeholder="Auxiliar 1"
                                required></textarea>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Auxiliar 2 ( HUMANIZAÇÃO )</label>
                        <div class="col-9">
                            <textarea class="form-control" id="auxiliar2" name="auxiliar2" placeholder="Auxiliar 2"
                                required></textarea>
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Auxiliar 3</label>
                        <div class="col-9">
                            <textarea class="form-control" id="auxiliar3" name="auxiliar3" placeholder="Auxiliar 3"
                                ></textarea>
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Auxiliar 4</label>
                        <div class="col-9">
                            <textarea class="form-control" id="auxiliar4" name="auxiliar4" placeholder="Auxiliar 4"
                                ></textarea>
                        </div>
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="{{ URL::previous() }}">
                                                    Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2 ml-3">
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit">
                                                    Adicionar<span class="badge badge-primary"></span>
                                                </button>
                                            </div>
                                        </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>

        </form>
    </div> <!-- container -->


@endsection
