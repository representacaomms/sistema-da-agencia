@extends('layouts.app')
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('produtos.index') }}">Produtos</a></li>
                            <li class="breadcrumb-item active">Editar Produtos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Produto</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('produtos.update', $produto->id) }}" method="POST">
            @method('PUT')
            @csrf
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Descricao</label>
                        <div class="col-9">
                            <input type="text" class="form-control" style="text-transform: capitalize;" id="descricao"
                                name="descricao" placeholder="Nome" value="{{ $produto->descricao }}" required>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Benefícios</label>
                        <div class="col-9">
                            <textarea class="form-control" id="beneficios" name="beneficios" placeholder="Beneficios"
                                required>{!! $produto->beneficios !!}</textarea>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Causas</label>
                        <div class="col-9">
                            <textarea class="form-control" id="causas" name="causas" placeholder="Causas"
                                required>{!! $produto->causa !!}</textarea>
                        </div>
                    </div>



                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Situacao </label>
                        <div class="col-9">
                            <select name="situacao" id="situacao" class="form-control">
                                @if ($produto->situacao == 'Ativo')
                                    <option value="Ativo">Ativo</option>
                                    <option value="Inativo">Inativo</option>
                                @else
                                    <option value="Inativo">Inativo</option>
                                    <option value="Ativo">Ativo</option>
                                @endif
                            </select>
                        </div>
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="{{ URL::previous() }}">
                                                    Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit">
                                                    Atualizar<span class="badge badge-primary"></span>
                                                </button>
                                            </div>
                                        </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>

        </form>
    </div> <!-- container -->


@endsection
