@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Usuarios</a></li>
                            <li class="breadcrumb-item active">Listar Usuarios</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Listar Usuarios</h4>
                </div>
            </div>
        </div>
       
      <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                                <a href="{{ route('usuarios.create') }}" class="btn btn-danger mb-2"><i class="mdi mdi-plus-circle mr-2"></i> Adicionar Usuarios</a>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-success mb-2 mr-1"><i class="mdi mdi-settings"></i></button>
                                    <button type="button" class="btn btn-light mb-2 mr-1">Importar</button>
                                    <button type="button" class="btn btn-light mb-2">Exportar</button>
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                             @if ($message = Session::get('success'))
                                @if ($message == 'store')
                                    <script>
                                        var message = 'store';
                                    </script>
                                @endif
                                @if ($message == 'update')
                                    <script>
                                        var message = 'update';
                                    </script>
                                @endif

                                @if ($message == 'destroy')
                                    <script>
                                        var message = 'destroy';
                                    </script>
                                @endif

                                @if ($message == 'active')
                                    <script>
                                        var message = 'active';
                                    </script>
                                @endif
                                
                            @endif
                         
                            <table class="table table-centered w-100 dt-responsive " id="clientes-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th class="all">Usuarios</th>
                                        <th>Contato</th>
                                        <th>Status</th>
                                        <th style="width: 100px;">Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($records as $key=>$item)
                                        <tr>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-12">
                                                    <a href="{{ route('usuarios.edit', $item->id) }}" class="text-body"style="text-transform: uppercase;">{{ $item->name}}</a>
                                                </p>
                                            </td>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-10">
                                                    <a class="text-body">{{$item->celular}}</a>
                                                    <br/>
                                                    <a class="text-body">{{$item->telefone}}</a>
                                                    <br/>
                                                    <a class="text-body">{{$item->email}}</a>                                                    
                                                </p>
                                            </td>

                                            <td>
                                                @if ($item->status == 'A')
                                                <span class="badge badge-success">Ativo</span>
                                                @else
                                                <span class="badge badge-danger">Inativo</span>
                                                @endif                                               
                                            </td>
                                            <td class="d-flex">
                                                <a class="mr-3 btn btn-sm btn-outline-success" href="{{route('usuarios.edit', $item->id)}}">Editar</a>
                                                <a class="mr-3 btn btn-sm btn-outline-info" href="{{route('usuarios.funcoes', $item->id)}}">Perfis</a>
                                                <form action="{{route('usuarios.destroy', ['usuario' => $item->id])}}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <input class="btn btn-sm btn-outline-danger" type="submit" value="Remover">
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div> <!-- container -->
@endsection
@section('css') 
@stop
@section('js')
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function() {
          
            $('#clientes-datatable').DataTable({
                language: {
                    processing:     "Processamento...",
                    search:         "Pesquisar&nbsp;:",
                    lengthMenu:    "Mostrar _MENU_ entradas",
                    info:           "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty:      "Exibindo da entrada 0 até 0 de 0 entradas",
                    infoFiltered:   "(filtro total de _MAX_ entradas)",
                    infoPostFix:    "",
                    loadingRecords: "Carregando ...",
                    zeroRecords:    "Nenhuma entrada encontrada",
                    emptyTable:     "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first:      "Primeiro",
                        previous:   "Anterior",
                        next:       "Ultimo",
                        last:       "Depos"
                    },
                    aria: {
                        sortAscending:  ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        } );
    </script>
    <script type="text/javascript">
    
        if(message == 'store'){
            Swal.fire(
                'Parabéns!',
                'Registro cadastrado com sucesso!',
                'success'
            )
        }
        if(message == 'update'){
            Swal.fire(
                'Parabéns!',
                'Registro editado com sucesso!',
                'success'
            )
        }   
        
        if(message == 'destroy'){
            Swal.fire(
                'Alerta!',
                'Registro inativado com sucesso!',
                'info'
            )
        } 

        if(message == 'active'){
            Swal.fire(
                'Alerta!',
                'Registro ativado com sucesso!',
                'info'
            )
        }   
        
    </script> 

@stop