@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('funcoes.index')}}">Funções</a></li>
                            <li class="breadcrumb-item active">Adicionar Permissão</li>
                        </ol>
                    </div>
                    <h3 class="page-title">Adicionar Permissão</h3>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('usuarios.funcoesSync', $usuario->id) }}" method="POST" novalidate>
            @method('PUT')
            @csrf
             <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Funcão de: {{$usuario->name}}</h4>
                            <hr class="mb-4">
                            @foreach ($funcoes as $funcao)
                            <div class="all mt-1" style="font-size:16px;">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="{{$funcao->id}}" id="{{$funcao->id}}" {{($funcao->can == 1 ? 'checked' : '')}}>
                                    <label class="custom-control-label" for="{{$funcao->id}}">#{{$funcao->id}} - {{$funcao->name}}</label>
                                </div>
                            </div>
                            
                            @endforeach
                            <hr>
                            <div class="row mt-5">
                                <div class="col-xl-1">
                                    <div class="form-group">
                                        <a class="btn btn-danger" href="{{ URL::previous()}}">
                                                Cancelar<span class="badge badge-primary"></span>
                                        </a>
                                    </div>
                                </div> <!-- end col-->
                                <div class="col-xl-1">
                                        <div class="form-group">
                                            <button class="btn btn-primary" type="submit">
                                                    Sincronizar<span class="badge badge-primary"></span>
                                            </button>
                                        </div>
                                    </div> <!-- end col-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div> <!-- container -->

@endsection

