@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('clientes.index')}}">Usuários</a></li>
                            <li class="breadcrumb-item active">Adicionar Usuário</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Usuário</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('usuarios.store') }}" method="POST" novalidate>
            @method('POST')
            @csrf
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectname">Nome</label>
                                <input type="text"  name="name" class="form-control"  style="text-transform: capitalize;" placeholder="Nome Completo" value="{{old('name')}}" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label id="label_cpf">Email</label>
                                <input type="email"  name="email" class="form-control" placeholder="Insira o email válido" required >
                            </div>
                        </div>
                        <div class="col-md-3" >
                            <div class="form-group" >
                                <div class="form-group">
                                    <label id="label_cpf">Senha</label>
                                    <input type="password" name="password" class="form-control" placeholder="Insira a senha"  >
                                </div>
                            </div>
                        </div> <!-- end col-->
                        

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="{{ URL::previous()}}">
                                                        Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2 ml-3">
                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit">
                                                            Adicionar<span class="badge badge-primary"></span>
                                                    </button>
                                                </div>
                                            </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>

        </form>
    </div> <!-- container -->

@endsection

@section('js')
    <script>
        $('select[name="tipo"]').change( function() {
            if($('select[name="tipo"]').val() === 'pf'){
                $('.juridica').addClass('d-none');
                $('.fisica').removeClass('d-none');
            } else{
                $('.fisica').addClass('d-none');
                $('.juridica').removeClass('d-none');
            }
        })
    </script>
@endsection
