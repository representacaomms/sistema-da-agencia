@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Regras Impostos</a></li>
                            <li class="breadcrumb-item active">Adicionar Impostos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Impostos</h4>
                </div>
            </div>
        </div>
       <div class="card">
           
           <div class="card-body">
               <form class="p-2" method="POST" action="{{ route('impostos.store') }}"> 
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Descrição </label>
                                    <input class="form-control form-control-light" name="descricao" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>CFOP</label>
                                    <input class="form-control form-control-light" name="cfop">                           
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status"  class="form-control form-control-light" >
                                        <option value="Ativo">Ativo</option>
                                        <option value="Inativo">Inativo</option>
                                    </select>
                                                                       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               <div class="card">
                   <div class="card-body">
                       <h5 class="card-title">IMPOSTO CST</h5>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>CST ICMS</label>
                                    <input class="form-control form-control-light" name="cst_icms"> 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>CST PIS</label>
                                    <input class="form-control form-control-light" name="cst_pis"> 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>CST COFINS</label>
                                    <input class="form-control form-control-light" name="cst_cofins">  
                                </div>                            
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="task-priority2">CST IPI</label>
                                    <input class="form-control form-control-light" name="cst_ipi"> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">ALIQUOTA IMPOSTO</h5>
                         <div class="row">
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label>Aliquota ICMS</label>
                                    <input class="form-control form-control-light" name="alicota_icms"> 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Aliquota PIS</label>
                                    <input class="form-control form-control-light" name="alicota_pis"> 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Aliquota COFINS</label>
                                    <input class="form-control form-control-light" name="alicota_cofins">  
                                </div>                            
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="task-priority2">Aliquota IPI</label>
                                    <input class="form-control form-control-light" name="alicota_ipi"> 
                                </div>
                            </div>
                         </div>
                    </div>
                </div>
    
                <div class="text-right mt-5">
                    <a href="{{ URL::previous()}}" class="btn btn-light" data-dismiss="modal">Sair</a>
                    <button type="submit" class="btn btn-primary">Adicionar</button>
                </div>
            </form>
           </div>
       </div>
    </div> <!-- container -->

@endsection