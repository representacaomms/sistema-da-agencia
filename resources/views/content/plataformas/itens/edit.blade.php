@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('plataformas.index')}}">Plataformas</a></li>
                            <li class="breadcrumb-item active">Editar Plataforma</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Plataforma</h4>
                    @if ($message = Session::get('success'))
                        @if ($message == 'store')
                            <script>
                                var message = 'store';
                            </script>
                        @endif
                        @if ($message == 'update')
                            <script>
                                var message = 'update';
                            </script>
                        @endif

                        @if ($message == 'destroy')
                            <script>
                                var message = 'destroy';
                            </script>
                        @endif

                        @if ($message == 'active')
                            <script>
                                var message = 'active';
                            </script>
                        @endif
                    
                    @endif
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('plataforma-itens.update',$plataformaItens->id ) }}"   method="POST" novalidate>
            @method('PUT')
            @csrf
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    <hr>
                        
                    <div class="row mt-3 "'>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="projectname">Plataforma</label>
                                <input type="text"   name="plataforma" class="form-control " value="{{$plataforma->nome}}" readonly>
                                <input type="hidden"   name="id_plataforma" class="form-control " value="{{$plataforma->id}}">
                            </div>
                        </div>

                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="projectname">Adiconar Termos de consultoria </label>
                                <input type="text"   name="termo_consultoria" class="form-control " value="{{$plataformaItens->descricao}}"  required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="projectname">Tipo</label>
                                <input type="text"   name="tipo" class="form-control " value="{{$plataformaItens->tipo}}"  required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="projectname">Label </label>
                                <input type="text"   name="label" class="form-control " value="{{$plataformaItens->label}}"  required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="projectname">Input </label>
                                <input type="text"   name="input" class="form-control " value="{{$plataformaItens->input}}"  required>
                            </div>
                        </div>
                      
                    </div>
                   
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="{{ URL::previous()}}">
                                                        Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2">
                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit">
                                                            Atualizar<span class="badge badge-primary"></span>
                                                    </button>
                                                </div>
                                            </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>

        </form>
    </div> <!-- container -->

@endsection
@section('css')
    .d-none{
        display: none;
    }
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        var oldValue = $('select[name="plataforma"]').val();
        $('select[name="plataforma"]').change( function() {
           
            switch ($('select[name="plataforma"]').val()) {

                case 'instagram':
                    addClass('instagram');
                    oldValue = 'instagram';              
                    break;
                case 'facebook':
                    addClass('facebook') ;
                    oldValue = 'facebook';         
                    break;
                case 'youtube':
                    addClass('youtube')  
                    oldValue = 'youtube';                
                    break;
                case 'site':
                    addClass('site')     
                    oldValue = 'site';              
                    break;
                case 'gmn':
                    addClass('gmn')   
                    oldValue = 'gmn';                 
                    break;
            }
            
        })

        function addClass(params) {
            $('#' + params).removeClass('d-none');
            $('#' + oldValue).addClass('d-none') 
        }
       
    </script>

<script type="text/javascript">
        
    if(message == 'store'){
        Swal.fire(
            'Parabéns!',
            'Registro cadastrado com sucesso!',
            'success'
        )
    }
    if(message == 'update'){
        Swal.fire(
            'Parabéns!',
            'Registro editado com sucesso!',
            'success'
        )
    }   
    
    if(message == 'destroy'){
        Swal.fire(
            'Alerta!',
            'Registro inativado com sucesso!',
            'info'
        )
    } 

    if(message == 'active'){
        Swal.fire(
            'Alerta!',
            'Registro ativado com sucesso!',
            'info'
        )
    }   
    
</script> 
@endsection

