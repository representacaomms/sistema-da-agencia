@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('plataformas.index')}}">Plataformas</a></li>
                            <li class="breadcrumb-item active">Adicionar Plataforma</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Plataforma</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('plataformas.store') }}" method="POST" novalidate>
            @method('POST')
            @csrf
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="projectname">Plataforma</label>
                                <input type="text"   name="plataforma" class="form-control " required>
                            </div>
                        </div>

                    </div>    

                        
                    
                   
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="{{ URL::previous()}}">
                                                        Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2">
                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit">
                                                            Adicionar<span class="badge badge-primary"></span>
                                                    </button>
                                                </div>
                                            </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>

        </form>
    </div> <!-- container -->

@endsection

@section('js')
    <script>
        

       
    </script>
@endsection
