@extends('layouts.app')
@section('content')
<!-- Start Content-->
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Consultorias</a></li>
                        <li class="breadcrumb-item active">Montar Consultoria</li>
                    </ol>
                </div>
                <h4 class="page-title">Montar Consultoria  - 1ª Etapa</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('consultorias.montar') }}" method="post">
                        @csrf
                        @method('POST')
            
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Selecione o dados para consultoria</h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group id_cliente">
                                            <label for="projectname">Cliente</label>
                                            <select class="form-control select2" data-toggle="select2" name="id_cliente"
                                                id="id_cliente" required>
                                                <option value="0">Selecione o Cliente ...</option>
                                                @foreach ($records as $item)
                                                    <option value="{{ $item->id }}" style="text-transform: capitalize;">
                                                        {{ $item->nome }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group id_cliente">
                                            <label for="projectname">Concorrentes</label>
                                            <select multiple class="form-control select2" data-toggle="select2" name="concorrentes[]" 
                                                id="concorrentes" required>
                                                <option value="0">Selecione o Concorrente ...</option>
                                                @foreach ($records as $item)
                                                    <option value="{{ $item->id }}" style="text-transform: capitalize;">
                                                        {{ $item->nome }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group id_cliente">
                                            <label for="projectname">Referência</label>
                                            <select class="form-control select2" data-toggle="select2" name="id_referencia"
                                                id="id_cliente" required>
                                                <option value="0">Selecione a Referência ...</option>
                                                @foreach ($records as $item)
                                                    <option value="{{ $item->id }}" style="text-transform: capitalize;">
                                                        {{ $item->nome }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-xl-2">
                                                        <div class="form-group">
                                                            <a class="btn btn-danger" href="{{ URL::previous()}}">
                                                                    Cancelar<span class="badge badge-primary"></span>
                                                            </a>
                                                        </div>
                                                    </div> <!-- end col-->
                                                    <div class="col-xl-2 pl-2">
                                                            <div class="form-group">
                                                                <button class="btn btn-primary" type="submit">
                                                                        Montar<span class="badge badge-primary"></span>
                                                                </button>
                                                            </div>
                                                        </div> <!-- end col-->
                                                </div>
                                            </div> <!-- end card-body -->
                                        </div>
                                    </div>
                                </div> <!-- end card-->
                            </div> <!-- end col-->
                        </div>
                    </form>
                  
                </div> <!-- end card-body-->
            </div> <!-- end card-->
          

        </div> <!-- end col -->
    </div>
    <!-- end row-->
</div> <!-- container -->
@endsection

@section('css')
@endsection
@section('js')
@stop