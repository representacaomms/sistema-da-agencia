@extends('layouts.app')
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('planos-gestao.index') }}">Planos </a></li>
                            <li class="breadcrumb-item active">Adicionar Planos </li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Planos de Gestão</h4>
                    <input type="hidden" id="base_custos" value="{{ $base_custos }}">
                    <input type="hidden" id="salarios" value="{{ $salarios }}">
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('planos-gestao.store') }}" method="POST">
            @method('POST')
            @csrf
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-2 col-form-label">Nome</label>
                        <div class="col-10">
                            <input type="text" class="form-control" style="text-transform: capitalize;" id="descricao"
                                name="descricao" placeholder="Nome" required>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-2 col-form-label">Escopo</label>
                        <div class="col-10">
                            <textarea class="form-control" id="escopo" name="escopo" placeholder="Escopo"
                                required></textarea>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-2 col-form-label">Horas de Gestão</label>
                        <div class="col-10">
                            <input class="form-control" id="hrs" name="hrs" placeholder="Horas" type="number" min="1" max="40" value="1"
                                required>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-2 col-form-label">Canais de Atendimento</label>
                        <div class="col-10">
                            <select name="ca" id="ca" class="select2 form-control select2-multiple" data-toggle="select2" multiple="multiple" required>
                                <option value="email">E-mail</option>
                                <option value="whats">WhastApp</option>
                                <option value="cel">Telefone</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-2 col-form-label">Infraestrutura</label>
                        <div class="col-10">
                            <select class="form-control" id="infraestrutura" name="infraestrutura" placeholder="Infraestrutura" required>
                                <option value="S">Sim</option>
                                <option value="N">Não</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-2 col-form-label">Google Ads</label>
                        <div class="col-10">
                            <input class="form-control" id="ga" name="ga" placeholder="Google Ads"
                                required>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-2 col-form-label">Facebook Ads</label>
                        <div class="col-10">
                            <input class="form-control" id="fa" name="fa" placeholder="Facebook Ads"
                                required>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-2 col-form-label">E-mail Marketing</label>
                        <div class="col-10">
                            <input class="form-control" id="em" name="em" placeholder="E-mail Marketing"
                                required>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-2 col-form-label">Relatório Mensal</label>
                        <div class="col-10">
                            <select name="rm" id="rm" class="form-control" required>
                                <option value="S">Sim</option>
                                <option value="N">Não</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-2 col-form-label">Adicionais</label>
                        <div class="col-10">
                            <textarea class="form-control" id="adicionais" name="adicionais" placeholder="Adicionais"
                                ></textarea>
                        </div>
                    </div>


                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-2 col-form-label">Valor Gestão </label>
                        <div class="col-5 row">
                            <label for="inputEmail3" class="col-3 col-form-label">Valor do Plano </label>
                            <div class="col-9">
                                <input class="form-control" id="vp" name="vp" placeholder="R$ 0,00"
                                required>
                            </div>
                        </div>

                        <div class="col-5 row">
                            <label for="inputEmail3" class="col-3 col-form-label">Plano Individual </label>
                            <div class="col-9">
                                <input class="form-control" id="vi" name="vi" placeholder="R$ 0,00"
                                required>
                            </div>
                        </div>
                    </div>



                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-2 col-form-label">Situacao</label>
                        <div class="col-10">

                            <select name="situacao" id="situacao" class="form-control">
                                <option value="Ativo">Ativo</option>
                                <option value="Inativo">Inativo</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="{{ URL::previous() }}">
                                                    Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit">
                                                    Adicionar<span class="badge badge-primary"></span>
                                                </button>
                                            </div>
                                        </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>

        </form>
    </div> <!-- container -->

    @section('js')
        <script>
            var arrayProdutos = [];
            $(document).ready(function() {

                var base_custos = $('#base_custos').val();
                var salarios = $('#salarios').val();
                var soma_salarios_base = 0
                var custos_totais = 0
                var preco_plano_hr = 0
                var qtde_horas = $('#hrs').val()
                var custo_operacional = 0
                var margem_lucro = 0
                var valor_plano = 0
                var plano_individual = 0
               
                base_custos = JSON.parse(base_custos);
                salarios = JSON.parse(salarios);
                salarios.forEach(element => {
                    soma_salarios_base += parseFloat(element.base)
                });
                console.log('DEBUG: ', salarios);
                atualizaDados()


                $('#hrs').change(function(event) {
                    event.preventDefault();
                    qtde_horas = $('#hrs').val()
                    atualizaDados()
                });

                function atualizaDados() {
                    preco_plano_hr =  parseInt(qtde_horas) + (qtde_horas * (soma_salarios_base / salarios.length ))
                    custo_operacional = base_custos.valor_despesas / base_custos.clientes
                    margem_lucro =  preco_plano_hr  / (1- parseFloat(base_custos.margem_lucro/100))
                    valor_plano = (parseFloat(preco_plano_hr) + parseFloat(custo_operacional) + parseFloat(margem_lucro))/ (1 - (parseInt(base_custos.impostos)/100) )
                    plano_individual = parseFloat(valor_plano) * (1 + (base_custos.onboarding/100))

                    $('#vp').val(valor_plano.toFixed(2))
                    $('#vi').val(plano_individual.toFixed(2))

                }
                

                
            });

        </script>
    @endsection
@endsection
