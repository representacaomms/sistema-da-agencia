@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('estoques.index')}}">Compra</a></li>
                            <li class="breadcrumb-item active">Adicionar Compra</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Compra</h4>                   
                </div>
            </div>
        </div>
        
        <form name="formOrcamento" action="{{ route('compras.store')}}" method="POST">
            <input type="hidden" name="relPodutos" id="relPodutos">                        
            
            @method('POST')
            @csrf
            <div class="row">               
                <div class="col-xl-12">
                    <!-- tasks panel -->
                    <div class="collapse show" id="todayTasks">
                        <div class="card mb-0">
                            <div class="card-body">
                                <a class="text-dark" data-toggle="collapse" href="#todayTasks" aria-expanded="false" aria-controls="todayTasks">
                                    <h4 class="m-0 pb-2">
                                        <i class="uil uil-angle-down font-18"></i>Detalhes Compra <span class="text-muted"></span>
                                    </h4>
                                </a>
                                <hr>
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="form-group cliente">
                                            <label >Fornecedor</label>
                                            <select class="form-control select2" data-toggle="select2" name="fornecedor" id="cliente" >
                                                @foreach ($fornecedor as $item)
                                                <option value="{{ $item->id}} " style="text-transform: capitalize;" >{{$item->nome}}</option>
                                                @endforeach                                            
                                            </select> 
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-3"></div> <!-- end col-->
                                    <div class="col-xl-4">
                                        <div class="form-group">
                                            <label for="projectname">Vendedor</label>
                                            <input type="text" name="vendedor" id="vendedor" class="form-control" value="{{ Auth::user()->name }}" readonly>
                                            <input type="hidden" name="id_vendedor" id="id_vendedor" class="form-control" value="{{ Auth::user()->id }}">
                                        </div>
                                    </div> <!-- end col-->
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">                       
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="projectname">Produto</label>
                                                    <select class="form-control select2" data-toggle="select2" id="produto" >
                                                        <option value="0">Selecione um produto</option>
                                                        @foreach ($produtos as $item)
                                                            <option value="{{ $item->id}}" >{{$item->descricao}}</option>
                                                        @endforeach                                            
                                                    </select> 
                                                </div>
                                            </div> <!-- end col-->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="projectname">Valor Unitario  &nbsp;</label>                                           
                                                    <input type="text" id="valor" value="0.00"  data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true"  class="form-control">
                                                </div>
                                            </div> <!-- end col-->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="projectname">Quantidade  &nbsp;</label>                                           
                                                    <input type="number" id="qtde" value="1"  class="form-control">
                                                </div>
                                            </div> <!-- end col-->
                                           
                                           
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="projectname">&nbsp;</label>
                                                    <button type="button" class="btn btn-outline-primary form-control" id="addProd"> Adicionar </button> 
                                                </div>                                                                                        
                                            </div> <!-- end col-->                                           
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-borderless table-centered mb-0 no-wrap"  id="Table" style=" max-height: 150px;" >
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th>Descricao</th>
                                                        <th>Qtde</th>
                                                        <th>Valor Unitário</th>
                                                        <th>Valor Total</th>
                                                        <th style="width: 50px"></th> 
                                                    </tr>
                                                </thead>
                                                <tbody id="Tbody" style="height-max:400px"></tbody> 
                                            </table>
                                        </div> <!-- end table-responsive-->
                                        <!-- action buttons-->
                                        <div class="row mt-4">
                                            <div class="col-sm-6">
                                                <a href="apps-ecommerce-products.html" class="btn text-muted d-none d-sm-inline-block btn-link font-weight-semibold">
                                                    <i class="mdi mdi-arrow-left"></i> Voltar  </a>
                                            </div> <!-- end col -->
                                            <div class="col-sm-6">
                                                <div class="text-sm-right">
                                                    <button type="submit" class="btn btn-danger">
                                                        <i class="mdi mdi-cart-plus mr-1"></i> Concluir </button>
                                                </div>
                                            </div> <!-- end col -->
                                        </div> <!-- end row-->
                                    </div>
                                </div>
                                <!-- end row -->        
                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end .collapse-->
                    <br>
                </div>
            </div>
        </form>
    </div> <!-- container --> 
    
    @section('js')
    <script>
        var arrayProdutos =[];
        $(document).ready(function() {
            var valor_total = '0.00';

            $('#scroll-vertical-datatable').DataTable();
           
            $('#qtde').change( function (event){
                event.preventDefault();
                var qtde        =  $('#qtde').val();
                var valor_unit  =  $('#valor').val();
                valor_total = parseFloat(valor_unit) * parseInt(qtde);
                valor_total = valor_total.toFixed(2);

            });

            $('#produto').change(function(event){
                event.preventDefault();               
                var selected =  $('#produto option:selected'); 
                if(selected.val() == "0"){
                    $('#qtde').val("1");
                }             
                $.ajax({
                    url: "http://35.222.87.218/gestao/produtos/"+selected.val(),
                    type: "get",
                    dataType: 'json',
                    success: function (response) {                                     
                       
                    }
                });
               
            });

            $("#addProd").click(function(){
               
               $('#desconto').val(0.00);               
               var valor        = $("#valor").val();
               var quantidade   = $("#qtde").val();
               var produto      = $('#produto option:selected');

               
               if(produto.val() == "0"){
                  return;
               } 

               arrayProdutos.push(
                   {
                       produto:produto.text(),
                       quantidade:quantidade,
                       valor: valor,
                       valor_total:valor_total,
                   }
               );   

               var id = arrayProdutos.length;
               var markup       = "<tr><td data-prod="+produto.text()+">"+produto.text()+"</td><td data-qtd="+quantidade+" style='width: 90px;'>" + quantidade + "</td><td data-valor="+valor+">" + valor + "</td><td data-total="+valor_total+">" + valor_total + "</td><td><button type='button' class='btn btn-warning  remover'>Excluir</button></td></tr>";
               $("table #Tbody").append(markup);             
               $("#valor").val("0,00");
               $("#valor_total").val("0,00");
               $("#qtde").val("1"); 
               $('#relPodutos').val(JSON.stringify(arrayProdutos));               

           });

            $('#Table').on('click', '.remover', function (e) {
                var produto = $(this).closest('tr').find('td[data-prod]').data('prod');
                
                var indice = $("td").attr("idlinha");
               
                arrayProdutos.splice(indice);   
                $(this).closest('tr').remove();
                $('#relPodutos').val(JSON.stringify(arrayProdutos));
               
            });
        } );
    </script>
    @endsection

@endsection
