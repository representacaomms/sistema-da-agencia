@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Compras</a></li>
                            <li class="breadcrumb-item active">Importação XML</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Produtos Importado do XML</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-5"></div>
                            <div class="col-sm-7">
                                <div class="text-sm-right">
                                   {{--  <form action="{{ route('compras.addProduto') }}" method="post">
                                        @csrf
                                        @foreach ($produtos as $key=>$item)
                                            <input type="hidden" name="xProd"  value="{{ $item->xProd }}">
                                            <input type="hidden" name="uCom"   value="{{ $item->uCom }}">
                                            <input type="hidden" name="qCom"   value="{{ $item->qCom }}">
                                            <input type="hidden" name="vUnCom" value="{{ $item->vUnCom }}">
                                            <input type="hidden" name="vProd"  value="{{ $item->vProd }}">
                                            <input type="hidden" name="NCM"    value="{{ $item->NCM }}">
                                            <input type="hidden" name="cEAN"   value="{{ $item->cEAN }}">
                                        @endforeach
                                        <button type="submit" class="btn btn-light mb-2 mr-1">Importar</button>
                                    </form> --}}

                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                @if ($message == 'invoice')
                                    <script>
                                        var message = 'invoice';
                                    </script>
                                @endif
                            @endif
                            <table class="table table-centered w-100 dt-responsive " id="estoque-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Descrição</th>
                                        <th>Unidade</th>
                                        <th>Quantidade</th>
                                        <th>Valor Unit</th>
                                        <th>Valor Total </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($produtos as $key=>$item)
                                        <tr>
                                            <td>{{$item->xProd}}</td>
                                            <td>{{$item->uCom}}</td>
                                            <td>{{$item->qCom}}</td>
                                            <td>{{$item->vUnCom}}</td>
                                            <td>{{$item->vProd}}</td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
            <!-- end row -->
    </div> <!-- container -->


@endsection
@section('js')
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function() {

            $('#estoque-datatable').DataTable({
                language: {
                    processing:     "Processamento...",
                    search:         "Pesquisar&nbsp;:",
                    lengthMenu:    "Mostrar _MENU_ entradas",
                    info:           "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty:      "Exibindo da entrada 0 até 0 de 0 entradas",
                    infoFiltered:   "(filtro total de _MAX_ entradas)",
                    infoPostFix:    "",
                    loadingRecords: "Carregando ...",
                    zeroRecords:    "Nenhuma entrada encontrada",
                    emptyTable:     "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first:      "Primeiro",
                        previous:   "Anterior",
                        next:       "Ultimo",
                        last:       "Depos"
                    },
                    aria: {
                        sortAscending:  ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        } );
    </script>
    <script type="text/javascript">

        if(message == 'store'){
            Swal.fire(
                'Parabéns!',
                'Registro cadastrado com sucesso!',
                'success'
            )
        }
        if(message == 'update'){
            Swal.fire(
                'Parabéns!',
                'Registro editado com sucesso!',
                'success'
            )
        }

        if(message == 'destroy'){
            Swal.fire(
                'Alerta!',
                'Registro inativado com sucesso!',
                'info'
            )
        }

        if(message == 'active'){
            Swal.fire(
                'Alerta!',
                'Registro ativado com sucesso!',
                'info'
            )
        }

    </script>
    <script type="text/javascript">

        if(message == 'invoice'){
            Swal.fire(
                'Parabéns!',
                'Pedido FATURADO com sucesso!',
                'success'
            )
        }

    </script>
@stop
