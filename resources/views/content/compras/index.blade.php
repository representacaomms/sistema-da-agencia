@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Compras</a></li>
                            <li class="breadcrumb-item active">Listar Compra</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Listar Compra</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-5">
                                <form action="{{ route('compras.upload') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <button type="submit"  class="btn btn-warning mb-2">
                                        <input type="file" name="arquivo" id="arquivo">
                                        <i class="mdi mdi-plus-circle mr-2"></i> Importar XML / Adicionar Compra</button>
                                </form>
                            </div>

                            {{-- <div class="col-sm-7">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-success mb-2 mr-1"><i class="mdi mdi-settings"></i></button>
                                    <button type="button" class="btn btn-light mb-2 mr-1">Importar</button>
                                    <button type="button" class="btn btn-light mb-2">Exportar</button>
                                </div>
                            </div><!-- end col--> --}}
                        </div>

                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                @if ($message == 'store')
                                    <script>
                                        var message = 'store';
                                    </script>
                                @endif
                                @if ($message == 'update')
                                    <script>
                                        var message = 'update';
                                    </script>
                                @endif

                                @if ($message == 'destroy')
                                    <script>
                                        var message = 'destroy';
                                    </script>
                                @endif

                            @endif

                            <table class="table table-centered w-100 dt-responsive " id="estoque-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th class="all" style="width: 20px;">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                <label class="custom-control-label" for="customCheck1">&nbsp;</label>
                                            </div>
                                        </th>
                                        <th class="all">ID Nota</th>
                                        <th>Fornecedor</th>
                                        <th>Status</th>
                                        <th>Atualização</th>
                                        <th style="width: 75px;">Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($compras as $key=>$item)
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                                                    <label class="custom-control-label" for="customCheck2">&nbsp;</label>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16">
                                                    <a href="apps-ecommerce-products-details.html" class="text-body">{{$item->id_cNF}}</a>
                                                    <br/>
                                                </p>
                                            </td>
                                            <td> {{$item->fornecedor ? $item->fornecedor->nome : 'Sem Fornecedor'}} </td>


                                            <td>
                                                    @if ($item->status == 'Concluida')
                                                    <span class="badge badge-success"><p class="m-0 d-inline-block align-middle font-16">{{$item->status}}</p></span>
                                                    @else
                                                    <span class="badge badge-danger"><p class="m-0 d-inline-block align-middle font-16">{{$item->status}}</p></span>
                                                    @endif

                                            </td>
                                            <td>
                                                {{$item->updated_at->format('d/m/Y')}}
                                             </td>


                                            <td>
                                                <a href="{{ route('compras.show', $item->id) }}"  class="action-icon"> <i class="mdi mdi-checkbox-marked-circle"></i></a>

                                            </td>
                                        </tr>

                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
            <!-- end row -->
    </div> <!-- container -->


@endsection
@section('js')
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function() {

            $('#estoque-datatable').DataTable({
                language: {
                    processing:     "Processamento...",
                    search:         "Pesquisar&nbsp;:",
                    lengthMenu:    "Mostrar _MENU_ entradas",
                    info:           "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty:      "Exibindo da entrada 0 até 0 de 0 entradas",
                    infoFiltered:   "(filtro total de _MAX_ entradas)",
                    infoPostFix:    "",
                    loadingRecords: "Carregando ...",
                    zeroRecords:    "Nenhuma entrada encontrada",
                    emptyTable:     "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first:      "Primeiro",
                        previous:   "Anterior",
                        next:       "Ultimo",
                        last:       "Depos"
                    },
                    aria: {
                        sortAscending:  ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        } );
    </script>
    <script type="text/javascript">

        if(message == 'store'){
            Swal.fire(
                'Parabéns!',
                'Registro cadastrado com sucesso!',
                'success'
            )
        }
        if(message == 'update'){
            Swal.fire(
                'Parabéns!',
                'Registro editado com sucesso!',
                'success'
            )
        }

        if(message == 'destroy'){
            Swal.fire(
                'Alerta!',
                'Já existe importação com essa NOTA FISCAL!',
                'info'
            )
        }

        if(message == 'active'){
            Swal.fire(
                'Alerta!',
                'Registro ativado com sucesso!',
                'info'
            )
        }

    </script>
    <script type="text/javascript">

        if(message == 'invoice'){
            Swal.fire(
                'Parabéns!',
                'Pedido FATURADO com sucesso!',
                'success'
            )
        }

    </script>
@stop
