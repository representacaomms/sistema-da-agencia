@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('leads.index') }}">Leads</a></li>
                            <li class="breadcrumb-item active">Editar Lead</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Lead</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('leads.update', $lead->id) }}" method="POST">
            @method('PUT')
            @csrf
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Dados Gerais</h5>
                    <input type="hidden" name="rotulo" value="{{$lead->rotulo}}"/>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="projectname">Tipo</label>
                                <select name="tipo" id="tipo" class="form-control">
                                    <option value="pj"
                                        {{ old('tipo') == 'pj' ? 'selected' : ($lead->tipo == 'pj' ? 'selected' : '') }}>
                                        Pessoa Juridica</option>
                                </select>

                            </div>
                        </div>

                        <div class="col-md-4 fisica ">
                            <div class="form-group">
                                <label id="label_cpf">CNPJ</label>
                                <input type="text" data-toggle="input-mask" data-mask-format="00.000.000/0000-00"
                                    data-reverse="true" name="cpf" class="form-control " placeholder="Documento CPF"
                                    value="{{ old('cpf') ?? $lead->cpf }}">
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-4 fisica ">
                            <div class="form-group">
                                <label id="label_rg">Insc Est</label>
                                <input type="text" data-toggle="input-mask" data-mask-format="000.000.000.000"
                                    data-reverse="true" name="rg" class="form-control" placeholder="Documento RG"
                                    value="{{ old('rg') ?? $lead->rg }}">

                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-2">
                            <div class="form-group">
                                <label id="label_cpf">Status</label>
                                <select name="status" class="form-control">
                                    <option value="inbound"
                                        {{ old('tipo') == 'inbound' ? 'selected' : ($lead->status == 'inbound' ? 'selected' : '') }}>
                                        Inbound</option>
                                    <option value="ficha_cadastral"
                                        {{ old('tipo') == 'ficha_cadastral' ? 'selected' : ($lead->status == 'ficha_cadastral' ? 'selected' : '') }}>
                                        Ficha Cadastral</option>
                                    <option value="briefing"
                                        {{ old('tipo') == 'briefing' ? 'selected' : ($lead->status == 'briefing' ? 'selected' : '') }}>
                                        Briefing</option>
                                    <option value="consultoria"
                                        {{ old('tipo') == 'consultoria' ? 'selected' : ($lead->status == 'consultoria' ? 'selected' : '') }}>
                                        Consultoria</option>
                                    <option value="proposta"
                                        {{ old('tipo') == 'proposta' ? 'selected' : ($lead->status == 'proposta' ? 'selected' : '') }}>
                                        Proposta</option>
                                    <option value="lead"
                                        {{ old('tipo') == 'lead' ? 'selected' : ($lead->status == 'lead' ? 'selected' : '') }}>
                                        Lead</option>
                                        

                                </select>
                            </div>
                        </div> <!-- end col-->
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectname">Nome</label>
                                <input type="text" name="nome" class="form-control" style="text-transform: capitalize;"
                                    placeholder="Nome Completo" value="{{ old('nome') ?? $lead->nome }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="projectname">Fantasia</label>
                                <input type="text" name="fantasia" class="form-control" style="text-transform: capitalize;"
                                    placeholder="Nome Completo" value="{{ old('fantasia') ?? $lead->fantasia }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="label_cpf">Email</label>
                                <input type="email" name="email" class="form-control"
                                    value="{{ old('email') ?? $lead->email }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="form-group">
                                    <label id="label_cpf">Celular</label>
                                    <input type="text" data-toggle="input-mask" data-mask-format="(00) 00000-0000"
                                        id="celular" name="celular" class="form-control"
                                        value="{{ old('celular') ?? $lead->celular }}">
                                </div>
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="form-group">
                                    <label id="label_cpf">Telefone</label>
                                    <input type="text" data-toggle="input-mask" data-mask-format="(00) 0000-0000"
                                        id="telefone" name="telefone" class="form-control"
                                        value="{{ old('telefone') ?? $lead->telefone }}">
                                </div>
                            </div>
                        </div> <!-- end col-->

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Endereço</h5>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-xl-3">
                                        <div class="form-group">
                                            <label for="projectname">CEP</label>
                                            <input type="text" data-toggle="input-mask" data-mask-format="00.000-000"
                                                name="cep" class="form-control zip_code_search" placeholder="Digite seu CEP"
                                                value="{{ $lead->endereco ? $lead->endereco->cep : '' }}">
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-5">
                                        <div class="form-group">
                                            <label for="projectname">Logradouro</label>
                                            <input type="text" name="logradouro" class="form-control logradouro"
                                                value="{{ $lead->endereco ? $lead->endereco->logradouro : '' }}">
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-4">
                                        <div class="form-group">
                                            <label for="projectname">Complemento</label>
                                            <input type="text" name="complemento" class="form-control complemento"
                                                style="text-transform: capitalize;"
                                                value="{{ $lead->endereco ? $lead->endereco->complemento : '' }}">
                                        </div>

                                    </div> <!-- end col-->
                                </div>

                                <div class="row">
                                    <div class="col-xl-2">
                                        <div class="form-group">
                                            <label for="projectname">Numero</label>
                                            <input type="text" name="numero" class="form-control numero"
                                                value="{{ $lead->endereco ? $lead->endereco->numero : '' }}">
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-2">
                                        <div class="form-group">
                                            <label for="projectname">IBGE</label>
                                            <input type="text" name="ibge" class="form-control ibge"
                                                value="{{ $lead->endereco ? $lead->endereco->ibge : '' }}">
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-3">
                                        <div class="form-group">
                                            <label for="projectname">Bairro</label>
                                            <input type="text" name="bairro" class="form-control bairro"
                                                value="{{ $lead->endereco ? $lead->endereco->bairro : '' }}">
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-3">
                                        <div class="form-group">
                                            <label for="projectname">Cidade</label>
                                            <input type="text" name="localidade" class="form-control localidade"
                                                value="{{ $lead->endereco ? $lead->endereco->localidade : '' }}">
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-2">
                                        <div class="form-group">
                                            <label for="projectname">UF</label>
                                            <input type="text" name="uf" class="form-control uf"
                                                value="{{ $lead->endereco ? $lead->endereco->uf : '' }}">
                                        </div>
                                    </div> <!-- end col-->
                                </div>
                            </div> <!-- end col-->
                        </div> <!-- end card-body -->
                    </div> <!-- end card-->
                </div> <!-- end col-->

            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="{{ URL::previous() }}">
                                                    Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit">
                                                    Atualizar<span class="badge badge-primary"></span>
                                                </button>
                                            </div>
                                        </div> <!-- end col-->
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>

        </form>
    </div> <!-- container -->

@endsection

@section('js')
    <script>
        $('select[name="tipo"]').change(function() {
            if ($('select[name="tipo"]').val() === 'pf') {
                $('.juridica').addClass('d-none');
                $('.fisica').removeClass('d-none');
            } else {
                $('.fisica').addClass('d-none');
                $('.juridica').removeClass('d-none');
            }
        })

    </script>
@endsection
