@extends('layouts.app')
@section('content')
 <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('gestao-anuncios.index') }}">Receitas</a></li>
                            <li class="breadcrumb-item active">Recibo</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Recibo de Pagamento</h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <!-- Invoice Logo-->
                        <div class="clearfix">
                            <div class="float-right mb-3">
                                <img src="{{ url('https://agenciamms.com/wp-content/uploads/2021/11/cropped-logo.png') }}" alt="" height="70">
                            </div>
                            <div class="float-left">
                                <h1 class="m-5" style="width: 300px">{{$record->cliente->nome}}</h1>
                            </div>
                        </div>
                        <h2 class="text-muted text-center font-24">Recibo de Pagamento</h2>
                       
                            

                        <div class="d-print-none mt-4">
                            <div class="text-right">
                                <a href="javascript:window.print()" class="btn btn-primary imprimir"><i class="mdi mdi-printer"></i> Imprimir</a>
                                <a href="{{ route('gestao-anuncios.index') }}" class="btn btn-info">Fechar</a>
                            </div>
                        </div>   
                        <!-- end buttons -->

                    </div> <!-- end card-body-->
                </div> <!-- end card -->
            </div> <!-- end col-->
        </div>
        <!-- end row -->
        
    </div> <!-- container -->
@endsection
@section('js')

<script>
    $('.imprimir').click( function() {
        $('.recibo').removeClass('d-none');
        setInterval(function(){
            $('.recibo').addClass('d-none');
            }, 3000);
    });
</script>
    
@endsection