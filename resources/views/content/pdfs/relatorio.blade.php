@extends('layouts.app')
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Despesas</a></li>
                            <li class="breadcrumb-item active">Relatório</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Relatórior</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="container">
                        <div class="card-body">

                            <!-- Invoice Logo-->
                            <div class="clearfix">
                                <div class="float-left mb-3">
                                    <img src="https://agenciamms.com.br/wp-content/uploads/2021/01/Logo-transparente.png"
                                        alt="" height="100">
                                </div>
                                <div class="float-right">
                                    <h4 class="m-0 d-print-none">RELATÓRIO DE DESPESAS
                                    </h4>
                                </div>
                            </div>

                            <!-- Invoice Detail-->
                            <div class="row">
                                <div class="col-sm-6">
                                   

                                </div><!-- end col -->
                                <div class="col-sm-4 offset-sm-2">
                                    
                                </div><!-- end col -->
                            </div>
                            <!-- end row -->

                            
                            <div class="row pt-3" style="page-break-inside:avoid">

                                <div class="col-md-12 text-left">
                                    <h4><b>Despesas do mês DE  {{$mes}}</b></h4>
                                    <h5>Total das despesas: <b>R$ {{$total_divida_mes}}</b></h5>
                                    <div class="table-responsive">
                                        <table class="table mt-4">
                                            <thead>
                                                <tr>
                                                    <th>#Descrição</th>
                                                    <th>Total Parcelas</th>
                                                    <th>Total Lançamentos</th>
                                                    <th>Valor Unit</th>
                                                    <th>Data Venc</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($despesas as $key => $item)
                                                    <tr>
                                                        <td>{{ $item->descricao}}</td>
                                                        <td>
                                                            <b>{{ $item->parcela }} parcelas</b>
                                                        </td>

                                                        <td> R$  {{ $item->valor_lancamento }}</td>
                                                        <td>
                                                            @foreach ($item->itens as $lanc)
                                                            <a class="text-body"> Parcela: {{$lanc->parcela}}</a>
                                                            <br/>
                                                            <a class="text-body">R$ {{$lanc->valor_parcela}}</a>
                                                            @endforeach
                                                        </td>

                                                        <td>  
                                                            @foreach ($item->itens as $lanc)
                                                                <a class="text-body">{{date('d/m/Y', strtotime($lanc->data_pagto))}}</a>
                                                            @endforeach</td>
                                                    </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div> <!-- end table-responsive-->
                                </div>
                            </div>

                            <div class="row pt-5"></div>

                            
                            <div class="d-print-none mt-4" style="page-break-inside:avoid">
                                <div class="text-right">
                                    <a href="javascript:window.print()" class="btn btn-primary"><i
                                            class="mdi mdi-printer"></i> Imprimir</a>
                                    <a href="{{ route('despesas.index') }}" class="btn btn-info">Fechar</a>
                                   
                                </div>
                            </div>
                            <!-- end buttons -->

                        </div> <!-- end card-body-->
                    </div>
                </div> <!-- end card -->
            </div> <!-- end col-->
        </div>
        <!-- end row -->

    </div> <!-- container -->

@endsection
