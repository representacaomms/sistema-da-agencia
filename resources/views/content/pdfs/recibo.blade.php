@extends('layouts.app')
@section('content')
 <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('receitas.index') }}">Receitas</a></li>
                            <li class="breadcrumb-item active">Recibo</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Recibo de Pagamento</h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <!-- Invoice Logo-->
                        <div class="clearfix">
                            <div class="float-left mb-3">
                                <img src="{{ url('/images/logo.png') }}" alt="" height="70">
                            </div>
                            <div class="float-right">
                                <h4 class="m-0 d-print-none">RECIBO DE PAGAMENTO</h4>
                            </div>
                        </div>
                        <h2 class="text-muted text-center font-24">Recibo de Pagamento</h2>
                        <!-- Invoice Detail-->
                        <div class="row">
                            
                            <div class="col-sm-6">
                                <div class="float-left mt-3">
                                    <p class="text-muted font-13">
                                        Por meio deste documento, declaro, para todos os fins e a quem possa interessar, que recebi da pessoa {{   $lancamento->cliente->tipo == 'pf' ? 'Fisica' : 'Juridica'  }},{{ $lancamento->cliente->nome }}, portador do documento {{   $lancamento->cliente->tipo == 'pf' ? 'CPF: ' : 'CNPJ: '  }} nº: {{ $lancamento->cliente->cpf }} com sede em {{ $lancamento->cliente->endereco->localidade }}, a quantia de R$ {{ $receita->valor_amoretizado }} <b>( {{ $receita->valor_amoretizado_extenso }} )</b>, referente à seguinte situação:
                                        <p class="text-muted font-13">
                                           Pagamento da parcela {{ $receita->parcela }} de {{ $lancamento->parcela }}
                                        </p>
                                        <p class="text-muted font-13"> Por ser verdade, assino a presente.</p>
                                    </p>                                    
                                </div>

                            </div><!-- end col -->
                            <div class="col-sm-4 offset-sm-2">
                                <div class="mt-3 float-sm-right">
                                    <p class="font-13"><strong>Data Recibo: </strong> &nbsp;&nbsp;&nbsp; {{ $receita->created_at->format('d/m/Y') }}</p>
                                    <p class="font-13"><strong>ID Recibo: </strong> <span class="float-right">#{{$lancamento->id }}</span></p>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->

                        <div class="row mt-4">
                            <div class="col-sm-4">
                                <h6>Endereço</h6>
                                <address>
                                   {{$lancamento->cliente->endereco->logradouro}},{{$lancamento->cliente->endereco->numero }} {{ $lancamento->cliente->endereco->complemento }}<br>                                    
                                     {{ $lancamento->cliente->endereco->bairro }}<br>                                    
                                   {{ $lancamento->cliente->endereco->localidade }},{{ $lancamento->cliente->endereco->uf }} &nbsp; {{$lancamento->cliente->endereco->cep }}  <br>                                   
                                    <abbr title="Phone">Celular:</abbr>                                    
                                    {{ $lancamento->cliente->celular }}
                                    
                                </address>
                            </div> <!-- end col-->
                        </div>    
                        <!-- end row -->        

                        <div class="row">
                            <div class="col-sm-7 mt-5">
                                ____________________________________________________________________________________<br>
                                <h3>{{ $empresa->nome }} -- {{   $empresa->tipo == 'pf' ? 'CPF: ' : 'CNPJ: '  }} nº: {{ $empresa->cpf }} </h3> 
                               {{--  <div class="clearfix pt-3">
                                    <h6 class="text-muted">Notas:</h6>
                                    <small>
                                        Todas as contas devem ser pagas dentro de 7 dias após o recebimento da fatura. Se a conta não for paga dentro de 7 dias, os serviços poderá ser suspenso sem
                                        aviso previo.
                                    </small>
                                </div> --}}
                            </div> <!-- end col -->
                            
                        </div>
                        <!-- end row-->

                        <div class="d-print-none mt-4">
                            <div class="text-right">
                                <a href="javascript:window.print()" class="btn btn-primary imprimir"><i class="mdi mdi-printer"></i> Imprimir</a>
                                <a href="{{ route('receitas.index') }}" class="btn btn-info">Fechar</a>
                            </div>
                        </div>   
                        <!-- end buttons -->

                    </div> <!-- end card-body-->
                </div> <!-- end card -->
            </div> <!-- end col-->
        </div>
        <!-- end row -->
        <hr>
        <div class="row mt-5 recibo d-none">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <!-- Invoice Logo-->
                        <div class="clearfix">
                            <div class="float-left mb-3">
                                <img src="{{ url('/images/logo.png') }}" alt="" height="70">
                            </div>
                            <div class="float-right">
                                <h4 class="m-0 d-print-none">RECIBO DE PAGAMENTO</h4>
                            </div>
                        </div>
                        <h2 class="text-muted text-center font-24">Recibo de Pagamento</h2>
                        <!-- Invoice Detail-->
                        <div class="row">
                            
                            <div class="col-sm-6">
                                <div class="float-left mt-3">
                                    <p class="text-muted font-13">
                                        Por meio deste documento, declaro, para todos os fins e a quem possa interessar, que recebi da pessoa {{   $lancamento->cliente->tipo == 'pf' ? 'Fisica' : 'Juridica'  }},{{ $lancamento->cliente->nome }}, portador do documento {{   $lancamento->cliente->tipo == 'pf' ? 'CPF: ' : 'CNPJ: '  }} nº: {{ $lancamento->cliente->cpf }} com sede em {{ $lancamento->cliente->endereco->localidade }}, a quantia de R$ {{ $receita->valor_amoretizado }} <b>( {{ $receita->valor_amoretizado_extenso }} )</b>, referente à seguinte situação:
                                        <p class="text-muted font-13">
                                           Pagamento da parcela {{ $receita->parcela }} de {{ $lancamento->parcela }}
                                        </p>
                                        <p class="text-muted font-13"> Por ser verdade, assino a presente.</p>
                                    </p>                                    
                                </div>

                            </div><!-- end col -->
                            <div class="col-sm-4 offset-sm-2">
                                <div class="mt-3 float-sm-right">
                                    <p class="font-13"><strong>Data Recibo: </strong> &nbsp;&nbsp;&nbsp; {{ $receita->created_at->format('d/m/Y') }}</p>
                                    <p class="font-13"><strong>ID Recibo: </strong> <span class="float-right">#{{$lancamento->id }}</span></p>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->

                        <div class="row mt-4">
                            <div class="col-sm-4">
                                <h6>Endereço</h6>
                                <address>
                                   {{$lancamento->cliente->endereco->logradouro}},{{$lancamento->cliente->endereco->numero }} {{ $lancamento->cliente->endereco->complemento }}<br>                                    
                                     {{ $lancamento->cliente->endereco->bairro }}<br>                                    
                                   {{ $lancamento->cliente->endereco->localidade }},{{ $lancamento->cliente->endereco->uf }} &nbsp; {{$lancamento->cliente->endereco->cep }}  <br>                                   
                                    <abbr title="Phone">Celular:</abbr>                                    
                                    {{ $lancamento->cliente->celular }}
                                    
                                </address>
                            </div> <!-- end col-->
                        </div>    
                        <!-- end row -->        

                        <div class="row">
                            <div class="col-sm-7 mt-5">
                                ____________________________________________________________________________________<br>
                                <h3>{{ $empresa->nome }} -- {{   $empresa->tipo == 'pf' ? 'CPF: ' : 'CNPJ: '  }} nº: {{ $empresa->cpf }} </h3> 
                               {{--  <div class="clearfix pt-3">
                                    <h6 class="text-muted">Notas:</h6>
                                    <small>
                                        Todas as contas devem ser pagas dentro de 7 dias após o recebimento da fatura. Se a conta não for paga dentro de 7 dias, os serviços poderá ser suspenso sem
                                        aviso previo.
                                    </small>
                                </div> --}}
                            </div> <!-- end col -->
                            
                        </div>
                        <!-- end row-->

                    </div> <!-- end card-body-->
                </div> <!-- end card -->
            </div> <!-- end col-->
        </div>
        <!-- end row -->
        
    </div> <!-- container -->
@endsection
@section('js')

<script>
    $('.imprimir').click( function() {
        $('.recibo').removeClass('d-none');
        setInterval(function(){
            $('.recibo').addClass('d-none');
            }, 3000);
    });
</script>
    
@endsection