@extends('layouts.app')
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Vendas</a></li>
                            <li class="breadcrumb-item active">Proposta</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Proposta</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="container">
                        <div class="card-body">

                            <!-- Invoice Logo-->
                            <div class="clearfix">
                                <div class="float-left mb-3">
                                    <img src="https://agenciamms.com.br/wp-content/uploads/2021/01/Logo-transparente.png"
                                        alt="" height="100">
                                </div>
                                <div class="float-right">
                                    <h4 class="m-0 d-print-none">PROPOSTA DE PRESTAÇÃO DE SERVIÇOS
                                    </h4>
                                </div>
                            </div>

                            <!-- Invoice Detail-->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="float-left mt-3">

                                        <p class="text-left font-13">

                                            Este documento visa formalizar a proposta de prestação dos serviços para o
                                            projeto
                                            de desenvolvimento e marketing digital para empresa {{ $cliente->fantasia }}
                                            com
                                            base no
                                            documento
                                            de briefing Nº
                                            {{ $proposta->created_at->format('Y') }}.{{ str_pad($proposta->nro_proposta, 3, 0, STR_PAD_LEFT) }}.
                                            Sendo assim, descrevemos aqui as informações acerca da
                                            metodologia, cronograma de atividades, recursos necessários e da execução do
                                            projeto, assim como, os prazos previamente definidos.</p>
                                    </div>

                                </div><!-- end col -->
                                <div class="col-sm-4 offset-sm-2">
                                    <div class="mt-3 float-sm-right">
                                        <p class="font-13"><strong>Data Proposta: </strong> &nbsp;&nbsp;&nbsp;
                                            {{ $proposta->created_at->format('d/m/Y') }} </p>
                                        <p class="font-13"><strong>Status Proposta: </strong>
                                            @if ($proposta->status == 'Aguardando Aprovacao')
                                                &nbsp;&nbsp;&nbsp; <span class="badge badge-warning float-right">Aguardando
                                                    Aprovação</span>
                                            @elseif($proposta->status == 'Aprovado')
                                                &nbsp;&nbsp;&nbsp; <span
                                                    class="badge badge-primary float-right">Aprovada</span>
                                            @elseif($proposta->status == 'Faturado')
                                                &nbsp;&nbsp;&nbsp; <span
                                                    class="badge badge-success float-right">Faturado</span>
                                            @else
                                                &nbsp;&nbsp;&nbsp; <span
                                                    class="badge badge-danger float-right">Orçamento</span>
                                            @endif

                                        </p>
                                        <p class="font-13"><strong>ID Proposta: </strong> <span
                                                class="float-right">#
                                                {{ $proposta->created_at->format('Y') }}.{{ str_pad($proposta->nro_proposta, 3, 0, STR_PAD_LEFT) }}</span>
                                        </p>
                                    </div>
                                </div><!-- end col -->
                            </div>
                            <!-- end row -->

                            <div class="row mt-4 pb-5">
                                <div class="col-sm-5 text-left">
                                    <h6>Plano Selecionado</h6>
                                    <h4>{{ $plano->nome }}</h4>
                                    <address>
                                        {{-- {{$endereco->logradouro}},{{$endereco->numero }} {{ $endereco->complemento }}<br>                                    
                                         {{ $endereco->bairro }}<br>                                    
                                       {{ $endereco->localidade }},{{ $endereco->uf }} &nbsp; {{$endereco->cep }}  <br>                                   
                                        <abbr title="Phone">Telefone:</abbr>                                    
                                        @if (isset($contato->tipo) && $contato->tipo == 'telefone')
                                            {{ $contato->contato }}
                                        @endif --}}
                                    </address>
                                </div> <!-- end col-->

                                {{-- <div class="col-sm-4">
                                    <h6>Endereço Entrega</h6>
                                    <address>
                                        Cooper Hobson<br>
                                        795 Folsom Ave, Suite 600<br>
                                        San Francisco, CA 94107<br>
                                        <abbr title="Phone">P:</abbr> (123) 456-7890 
                                    </address>
                                </div> <!-- end col--> --}}

                                <div class="col-sm-4">
                                    <div class="text-sm-right">
                                        {{-- <img src="assets/images/barcode.png" alt="barcode-image" class="img-fluid mr-2" /> --}}
                                    </div>
                                </div> <!-- end col-->
                            </div>
                            <!-- end row -->

                            <div class="row text-left" style="page-break-inside:avoid">
                                <div class="col-12">
                                    <h4>PRODUTOS E SERVIÇOS:</h4>
                                    <h5>De acordo com as expectativas alinhadas no briefing os produtos e serviços aqui tem
                                        como
                                        principais objetivos ajudar a<br />
                                        <p> <b>AUMENTAR AS VENDAS, GERAR LEADS
                                                QUALIFICADOS .</b></p>
                                    </h5>
                                    <h4 class="text-center pt-3">Para isso determinamos os seguintes produtos e serviços:
                                    </h4>

                                </div> <!-- end col -->
                            </div>
                            <!-- end row -->
                            <div class="row pt-3" style="page-break-inside:avoid">
                                <div class="col-md-12">
                                    @foreach ($servicos as $item)
                                        <dl class="row mb-0 text-left">
                                            <dt class="col-sm-3">{{ $item->servico->descricao }}</dt>
                                            <dd class="col-sm-9">{{ $item->servico->escopo }}</dd>
                                        </dl>
                                        <hr>
                                    @endforeach
                                </div>
                            </div>

                            <div class="row pt-3" style="page-break-inside:avoid">
                                <div class="col-md-12">
                                    <h4>Etapas do Projeto</h4>
                                    <h5 class="text-left">As seguintes etapas do projeto definem seu cronograma assim
                                        como prazos de entrega,
                                        é
                                        importante ressaltar que o prazo é estipulado em dias úteis para uma aprovação.
                                        Sendo
                                        adicionado o mesmo prazo para cada alteração caso seja solicitada com o limite de 3
                                        alterações no total.
                                    </h5>
                                    <h5 class="text-left">
                                        Ex: Se o prazo é estipulado em 5 dias e é solicitada 1 alteração, deve-se estender
                                        mais
                                        5 dias de prazo para a entrega da etapa em questão.</h5>

                                    <h5 class="text-left">As etapas serão passadas por email que ficará como oficial o
                                        relacionamento entre as
                                        partes.</h5>
                                    <h5 class="pt-3 text-left">Por fim, o investimento mensal em campanhas de tráfego no
                                        Facebook Ads para Facebook
                                        e
                                        Instagram e no Google Ads para Google e YouTube.:
                                    </h5>
                                </div>
                            </div>

                            <div class="row pt-3" style="page-break-inside:avoid">
                                <div class="col-md-12 text-left">
                                    <h4>BUDGET DE CAMPANHAS:</h4>
                                    <h5><b>IMPORTANTE:</b>O budget de até R$ 3.000,00 (três mil reais) será
                                        100%
                                        investido em campanhas distribuídos entre as redes de tráfego como isenção. A partir
                                        deste valor 20% do budget é destinado a agência para custos de gestão da campanha.
                                    </h5>
                                    <ul>
                                        <li>01 - Budget Google - R$ 900,00 - mensal</li>
                                        <li>01 - Budget Facebook - R$ 900,00 - mensal</li>
                                    </ul>


                                </div>
                            </div>

                            <div class="row pt-3 " style="page-break-inside:avoid">
                                <div class="col-md-12 text-left">
                                    <h4>LICENÇAS E FERRAMENTAS DE TERCEIROS:</h4>
                                    <h5>Além dos valores estipulados algumas ferramentas são essenciais para a execução do
                                        projeto, a agência fica encarregada apenas da gestão e bom uso das mesmas, mas nunca
                                        dos
                                        valores, pagamentos ou atualizações de planos:
                                    </h5>
                                    <ul>
                                        <li>01 - Elementor Pro - USD 49,00/Anual</li>
                                        <li>01 - ConvertKit - USD 29,00/Mensal</li>
                                    </ul>


                                </div>
                            </div>

                            <div class="row pt-3" style="page-break-inside:avoid">

                                <div class="col-md-12 text-left">
                                    <h4><b>INVESTIMENTO DO PROJETO</b></h4>
                                    <h5>Para a execução do projeto assim como a busca pelos resultados já estipulados nesta
                                        proposta, levando em consideração um contrato de 12 meses para cálculo de descontos,
                                        os
                                        seguintes itens e valores devem ser considerados:</h5>
                                    <div class="table-responsive">
                                        <table class="table mt-4">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Item</th>
                                                    <th>Quantidade</th>
                                                    <th>Valor Unit</th>
                                                    <th class="text-right">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($servicos as $key => $item)
                                                    <tr>
                                                        <td>{{ $key + 1 }}</td>
                                                        <td>
                                                            <b>{{ $item->servico->descricao }}</b>

                                                        </td>
                                                        <td> {{ $item->qtde }}</td>
                                                        <td> R$ {{ number_format($item->servico->valor_individual, 2, ',', '.') }}
                                                        </td>
                                                        <td class="text-right"> R$
                                                            {{ number_format($item->servico->valor_individual * $item->qtde , 2, ',', '.') }}</td>
                                                    </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div> <!-- end table-responsive-->
                                </div>
                            </div>

                            <div class="row pt-3 text-left" style="page-break-inside:avoid">
                                <div class="col-sm-7 forma-pagto">
                                    <div class="clearfix ">
                                        <h6 class="text-muted">FORMAS DE PAGAMENTO:</h6>
                                        <li id="pri_parcelado"></li>
                                        <li id="seg_parcelado" ></li>
                                        <li id="ter_parcelado" ></li>
                                    </div> 
                                </div> <!-- end col -->
                                <div class="col-sm-5">
                                   
                                    <div class=" mt-3 mt-sm-0">
                                        <hr>
                                        <p><b>Projeto: &nbsp;</b> <span class="float-right" id="projeto" ></span>
                                        </p>
                                        <p><b>Desconto: &nbsp;</b> <span class="float-right" id="desconto"></span>
                                        </p>
                                        <p><b>Sub-Total: &nbsp;</b> <span class="float-right" id="subTotal-projeto"></span>
                                    </p>
                                    </div>
                                    <div class=" pt-3 mt-sm-0">
                                        <hr>
                                        <p><b>Gestão: &nbsp;</b> <span  class="float-right" id="plano"></span>
                                        </p>
                                        <p><b>Desconto: &nbsp;</b> <span class="float-right" id="desconto_plano"></span>
                                        </p>
                                        <p><b>Sub-Total: &nbsp;</b> <span class="float-right" id="subTotal-plano"> </span>
                                        </p>
                                        <h4>&nbsp;&nbsp;&nbsp;{{-- {{ 'R$ '.number_format($total, 2,',','.') }} --}}</h4>
                                    </div>
                                    <div class="clearfix"></div>
                                </div> <!-- end col -->
                            </div>
                            <!-- end row-->
                            <div class="row" style="page-break-inside:avoid">
                                <div class="col-md-12">
                                    <h4>ANOTAÇÕES:</h4>
                                    <h5> As anotações abaixo feitas servem para ajustes a presente proposta e devem ser
                                        consideradas para a confecção do contrato e execução do projeto.</h5>

                                </div>
                            </div>
                            <div class="row pt-4">
                                <div class="col-md-12">
                                    <div id="snow-editor" style="height: 300px"></div>
                                  
                                </div>
                            </div>

                            <div class="row mt-3" style="page-break-inside:avoid">
                                <div class="col-md-12">
                                    <h4>CONDIÇÕES GERAIS::</h4>
                                    <h5 class="text-left"> Na expectativa de oferecer a melhor solução em busca de
                                        AUMENTAR AS VENDAS, OBTER
                                        MAIS LEADS QUALIFICADOS e certos da melhor oferta para os serviços necessários ao
                                        resultado, aproveitamos a oportunidade para apresentar protestos de consideração e
                                        apreço.</h5>
                                    <h5 class="text-left">
                                        <ul>
                                            <li>Validade da proposta: 10 (dez dias) corridos.

                                            </li>
                                        </ul>
                                    </h5>
                                    <h5 class="pt-2 text-left">
                                        Estando de acordo, assinam as partes autorizando confecção do contrato para
                                        pagamento da primeira parcela e início do projeto;
                                    </h5>
                                </div>
                            </div>
                            <div class="row pt-5"></div>

                            <div class="row pt-5" style="page-break-inside:avoid">
                                <div class="col-md-6">
                                    <p>____________________________________________
                                    <p>
                                    <p><b>AgenciaMMS</b> - Marcelo Andrade da Silva</p>
                                </div>

                                <div class="col-md-6">
                                    <p>____________________________________________</p>
                                    <p style="text-transform: capitalize;">
                                        <b>{{ $cliente->fantasia ?? $cliente->nome }}</b>
                                    </p>
                                </div>
                            </div>
                            <div class="d-print-none mt-4" style="page-break-inside:avoid">
                                <div class="text-right">
                                    <a href="javascript:window.print()" class="btn btn-primary"><i
                                            class="mdi mdi-printer"></i> Imprimir</a>
                                    <a href="{{ route('propostas.index') }}" class="btn btn-info">Fechar</a>
                                    @if ($proposta->situacao === 'Aprovado')
                                        <a href="{{ route('propostas.invoice', $proposta->id) }}"
                                            class="btn btn-success"> <i class="mdi mdi-coin"></i>Faturar</a>
                                    @endif
                                </div>
                            </div>
                            <!-- end buttons -->

                        </div> <!-- end card-body-->
                    </div>
                </div> <!-- end card -->
            </div> <!-- end col-->
        </div>
        <!-- end row -->
        <input type="hidden" id="servicos" value="{{ $servicos }}">
        <input type="hidden" id="planos" value="{{ $plano }}">

    </div> <!-- container -->
@endsection
@section('css')
    <!-- Include stylesheet -->
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection
@section('js')
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <!-- Initialize Quill editor -->
    <script>
        var ColorClass = Quill.import('attributors/class/color');
        var SizeStyle = Quill.import('attributors/style/size');
        Quill.register(ColorClass, true);
        Quill.register(SizeStyle, true);
        var quill = new Quill('#snow-editor', {
            modules: {
                toolbar: true // Snow includes toolbar by default
            },
            theme: 'snow'
        });
    </script>
    <script>
        $(document).ready(function() {

        var servicos = $('#servicos').val();
        var plano = $('#planos').val();
        var list_total_individual = [];
        var list_total_valor = [];
        var valor_individual;

        servicos = JSON.parse(servicos);
        plano = JSON.parse(plano);

        servicos.forEach(element => {
            valor_individual =  parseFloat(element.servico.valor_individual) * parseInt(element.qtde);
            esforco_valor    =  parseFloat(element.servico.esforco_valor) * parseInt(element.qtde);
            list_total_individual.push(valor_individual);
            list_total_valor.push(esforco_valor);
        });

        /* CALCULO DO PROJETO */
         var total_individual = list_total_individual.reduce(( acumulador, valorAtual ) => acumulador + valorAtual );
         var total_valor      = list_total_valor.reduce(( acumulador, valorAtual ) => acumulador + valorAtual );
         var desconto         = total_individual -  total_valor;

         $("#projeto").append(total_individual.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
         $("#desconto").append(desconto.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
         $("#subTotal-projeto").append(total_valor.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));


        /* CALCULO DO PLANO */
         var total_plano = plano.plano_individual;
         var subTotal_plano = plano.valor_plano;
         var desconto_plano = total_plano - subTotal_plano;

         $("#plano").append('R$ '+ total_plano + ' a.m. ( ' + (total_plano * 12).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })+ ' )');
         $("#desconto_plano").append( 'R$ '+ desconto_plano.toFixed(2) + ' a.m. ( ' + (desconto_plano * 12).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })+ ' )');
         $("#subTotal-plano").append('R$ '+ subTotal_plano +' a.m. ( ' + (subTotal_plano * 12).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })+ ' )');



        $('#pri_parcelado').append('(&nbsp;&nbsp;&nbsp; ) À vista com desconto adicional de 10% no projeto + plano mensal de 12 meses. <br> De <i class="text-taxado"> R$ '+ total_valor +'</i> por <b> R$ ' + (total_valor * 0.90).toFixed(2) + ' + '+ subTotal_plano + ' a.m </b>')

        $('#seg_parcelado').append('(&nbsp;&nbsp;&nbsp; )  Projeto em 1 + 3 e fee mensal + plano mensal de 12 meses. <br> R$ '+ total_valor +'<b> em 1 + 3 de  R$ ' + (total_valor / 4).toFixed(2) + ' + '+ subTotal_plano + ' a.m. </b>')

        $('#ter_parcelado').append('(&nbsp;&nbsp;&nbsp; )  Valor mensal com plano em contrato de 12 meses. <br> <b> R$ '+((total_valor/12) + parseFloat(subTotal_plano)).toFixed(2) + '</b> ( R$ ' + (total_valor).toFixed(2) + ' do projeto + '+ (subTotal_plano * 12).toFixed(2) + ' da gestão )')

        });
    </script>
@stop

