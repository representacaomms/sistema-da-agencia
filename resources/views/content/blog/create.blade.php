@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('blogs.index')}}">Blogs</a></li>
                            <li class="breadcrumb-item active">Adicionar Blog</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Blog</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('blogs.store') }}" method="POST" enctype="multipart/form-data">           
            @method('POST')
            @csrf
            <div class="card">
                <div class="card-body">                    
                    <div class="row">
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="projectname">Publicar?</label>
                                <select name="publicar"  class="form-control">                                
                                    <option value="nao">Não</option>
                                    <option value="sim">Sim</option>
                                </select>
                                
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group" >
                                <label id="label_cpf">Categoria</label>
                                <input type="text"  name="categoria" class="form-control "  placeholder="Categoria do BLOG" required>
                            </div>
                        </div> <!-- end col--> 
                        <div class="col-md-4">
                            <div class="form-group" >
                                <label id="label_rg">Titulo</label>                            
                                <input type="text" name="titulo" class="form-control" placeholder="Titulo do BLOG" required >
                                            
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-4">
                            <div class="form-group" >
                                <label id="label_rg">Imagem</label>                            
                                <input type="file" name="imagem" class="form-control" placeholder="Inserir a imagem" required >
                                            
                            </div>
                        </div> <!-- end col-->
                       
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="header-title">Conteudo</h4>
                            <p class="text-muted font-14 mb-3">Descreva o conteudo do Blog.</p>
                            <!-- basic summernote-->
                            <textarea name="conteudo" class="form-control" required></textarea>
                        </div>
                    </div>
                    
                </div>
            </div> 
           
           
            
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2">
                                            <div class="form-group">
                                                <a class="btn btn-danger" href="{{ URL::previous()}}">
                                                        Cancelar<span class="badge badge-primary"></span>
                                                </a>
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2">
                                                <div class="form-group">
                                                    <button class="btn btn-primary" type="submit">
                                                            Adicionar<span class="badge badge-primary"></span>
                                                    </button>
                                                </div>
                                            </div> <!-- end col-->                                       
                                    </div>
                                </div> <!-- end card-body -->
                            </div>
                        </div>                            
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div>
          
        </form>
    </div> <!-- container -->

@endsection
@section('css')
<link href="{{ asset('css/vendor/summernote-bs4.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('js')
<script src="{{ asset('js/vendor/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('js/pages/demo.summernote.js') }}"></script>
@endsection
