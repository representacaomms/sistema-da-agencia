@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('planos.index') }}">Planos</a></li>
                            <li class="breadcrumb-item active">Adicionar Plano</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Plano</h4>
                    <input type="hidden" id="operador" value="{{ $operador }}">
                    <input type="hidden" id="base_custos" value="{{ $base_custos }}">
                    <input type="hidden" id="gestor" value="{{ $gestor }}">
                    <input type="hidden" id="servicos_agenciado" value="{{ $servicos_agenciado }}">
                    <input type="hidden" id="tempo" value="12">

                </div>
            </div>
        </div>

        <form name="formOrcamento" action="{{ route('planos.store') }}" method="POST">
            <input type="hidden" name="relProdutos" id="relProdutos">
            @method('POST')
            @csrf
            <div class="row">
                <div class="col-xl-12">
                    <!-- tasks panel -->
                    <div class="collapse show" id="todayTasks">
                        <div class="card mb-0">
                            <div class="card-body">
                                <a class="text-dark" data-toggle="collapse" href="#todayTasks" aria-expanded="false"
                                    aria-controls="todayTasks">
                                    <h4 class="m-0 pb-2">
                                        <i class="uil uil-angle-down font-18"></i>Detalhes Planos <span
                                            class="text-muted"></span>
                                    </h4>
                                </a>
                                <hr>
                                <div class="row">
                                    <div class="col-xl-5">
                                        <div class="form-group cliente">
                                            <label for="projectname">Nome</label>
                                            <input type="text" name="nome" class="form-control">
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-3">
                                        <div class="form-group">
                                            <label for="projectname">Categoria</label>
                                            <input type="text" name="categoria" class="form-control">
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-2">
                                        <div class="form-group cliente">

                                            <label for="projectname">Hora Gerenciar</label>
                                            <input type="number" name="hora_gerenciar" id="hora_gerenciar"
                                                class="form-control" min="1" max="40">
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-2">

                                        <label for="projectname">Status</label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="ativo">Ativo</option>
                                            <option value="inativo">Inativo</option>
                                        </select>
                                    </div> <!-- end col-->
                                </div>
                                <!-- end row -->
                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end .collapse-->
                    <br>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="projectname">Serviços</label>
                                        <select class="form-control select2" data-toggle="select2" id="produto">
                                            <option value="0">Selecione um serviço</option>
                                            @foreach ($servicos_agenciado as $item)
                                                <option value="{{ $item->id }}">{{ $item->descricao }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> <!-- end col-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="projectname">&nbsp;</label>
                                        <input type="number" name="qtde" id="qtde" class="form-control" min="1">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="projectname">&nbsp;</label>
                                        <button type="button" class="btn btn-outline-primary form-control" id="addProd">
                                            <i class="mdi mdi-plus mr-1"></i></button>
                                    </div>
                                </div> <!-- end col-->
                            </div>
                            <div class="table-responsive limitar-table">
                                <table class="table table-borderless table-centered mb-0 no-wrap" id="Table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Descricao</th>
                                            <th>Qtde</th>
                                            <th>SubTotal</th>
                                            <th style="width: 50px"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="Tbody"></tbody>
                                </table>
                            </div> <!-- end table-responsive-->

                            <!-- action buttons-->
                            <div class="row mt-4">
                                <div class="col-sm-6">
                                    <a href="{{ URL::previous()}}"
                                        class="btn text-muted d-none d-sm-inline-block btn-link font-weight-semibold">
                                        <i class="mdi mdi-arrow-left"></i> Voltar </a>
                                </div> <!-- end col -->
                                <div class="col-sm-6">
                                    <div class="text-sm-right">
                                        <button type="submit" class="btn btn-danger">
                                            <i class="mdi mdi-cart-plus mr-1"></i> Concluir </button>
                                    </div>
                                </div> <!-- end col -->
                            </div> <!-- end row-->
                        </div>
                        <!-- end col -->

                        <div class="col-lg-7">
                           <div class="row">
                               <div class="col-lg-6">
                                <div class="border p-3 mt-4 mt-lg-0 rounded" >
                                    <h4 class="header-title mb-3">Resumo do Plano</h4>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group" >
                                                <label id="label_cpf">Preço Plano (+1hora)</label>
                                                <input type="text" name="preco_plano_hora" data-toggle="input-mask"
                                                data-mask-format="###0.00" data-reverse="true" id="preco_plano_hora"
                                                class="form-control">
                                            </div>
                                            <div class="form-group" >
                                                <label id="label_cpf">Custo Operacional:</label>
                                                <input type="text" name="custo_operacional" data-toggle="input-mask"
                                                data-mask-format="###0.00" data-reverse="true" id="custo_operacional"
                                                class="form-control">
                                            </div>
                                            <div class="form-group" >
                                                <label id="label_cpf">Margem Lucro</label>
                                                <input type="text" name="margem_lucro" id="margem_lucro"
                                                class="form-control">
                                            </div>
                                            <div class="form-group" >
                                                <label id="label_cpf">Imposto do Plano</label>
                                                <input type="text" name="impostos" data-toggle="input-mask"
                                                data-mask-format="###0.00" data-reverse="true" id="impostos"
                                                class="form-control">
                                            </div>
                                            <div class="form-group" >
                                                <label id="label_cpf">Valor Plano</label>
                                                <input type="text" name="valor_plano" data-toggle="input-mask"
                                                data-mask-format="###0.00" data-reverse="true" id="valor_plano"
                                                class="form-control">
                                            </div>
                                            <div class="form-group" >
                                                <label id="label_cpf">Plano Individual</label>
                                                <input type="text" name="valor_plano_individual" data-toggle="input-mask"
                                                data-mask-format="###0.00" data-reverse="true" id="valor_plano_individual"
                                                class="form-control">
                                            </div>
                                        </div>
                                    </div>
    
                    
                                </div>
                               </div>
                               <div class="col-lg-6">
                                <div class="border p-3 mt-4 mt-lg-0 rounded" >
                                    <h4 class="header-title mb-3">Resumo do Projeto</h4>
                                   <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" >
                                            <label id="label_cpf">Valor Projeto</label>
                                            <input type="text" name="valor_projeto" data-toggle="input-mask"
                                            data-mask-format="###0.00" data-reverse="true" id="valor_projeto"
                                            class="form-control">
                                        </div>
                                        <div class="form-group" >
                                            <label id="label_cpf">Total Projeto</label>
                                            <input type="text" name="total_projeto" data-toggle="input-mask"
                                                            data-mask-format="###0.00" data-reverse="true" id="total_projeto"
                                                            class="form-control">
                                        </div>
                                        <div class="form-group" >
                                            <label id="label_cpf">Valor a Vista</label>
                                            <input type="text" name="valor_vista" data-toggle="input-mask"
                                                            data-mask-format="###0.00" data-reverse="true" id="valor_vista"
                                                            class="form-control">
                                        </div>
                                        <div class="form-group" >
                                            <label id="label_cpf">Fee Mensal: (12meses)</label>
                                            <input type="text" name="fee_mensal" data-toggle="input-mask"
                                                            data-mask-format="###0.00" data-reverse="true" id="fee_mensal"
                                                            class="form-control">
                                        </div>
                                        <div class="form-group" >
                                            <label id="label_cpf">Plano</label>
                                            <input type="text" name="plano" data-toggle="input-mask"
                                                            data-mask-format="###0.00" data-reverse="true" id="plano"
                                                            class="form-control">
                                        </div>
                                        <div class="form-group" >
                                            <label id="label_cpf">Plano</label>
                                            <input type="text" name="fee_plano" data-toggle="input-mask"
                                                            data-mask-format="###0.00" data-reverse="true" id="fee_plano"
                                                            class="form-control">
                                        </div>
                                    </div> <!-- end col-->
                                   </div>
                                
                                </div>
                               </div>
                           </div>

                        </div> <!-- end col -->

                    </div> <!-- end row -->
                </div>
            </div>
        </form>
    </div> <!-- container -->

@section('js')
    <script>
        var arrayProdutos = [];
        var tempo = $('#tempo').val();
        $('#hora_gerenciar').val(1);
        $('#qtde').val(1);
        var qtde = $('#qtde').val();
        $('#valor_plano').val('0.00')
        var valor_plano = $('#valor_plano').val();
        var valor_projeto  = $('#valor_projeto').val('0.00');
        var valor_vista  = $('#valor_vista').val('0.00');
        var fee_mensal  = $('#fee_mensal').val('0.00');
        var plano  = $('#plano').val('0.00');
        var fee_plano  = $('#fee_plano').val('0.00');
       
        
        $(document).ready(function() {

            var operador = $('#operador').val();
            var base_custos = $('#base_custos').val();
            var gestor = $('#gestor').val();
            base_custos = JSON.parse(base_custos);

            var subValor_projeto = 0;
            var total_projeto = 0;
            var totais_impostos = 1-((parseInt(base_custos.impostos) + parseInt(base_custos.margem_negocio)+ parseInt(base_custos.margem_lucro))/100);
            var valor_vista = 0;
            var fee_mensal = 0;
            var plano = 0;
            var fee_plano = 0;

            $('#total_projeto').val('0.00');
            $('#preco_plano_hora').val();
            $('#custo_operacional').val();
            $('#impostos').val();

            $("#hora_gerenciar").change(function() {
                var hora = $('#hora_gerenciar').val();
                var preco_plano_hora = ((parseInt(hora) * parseFloat(operador)) +  parseFloat(operador)).toFixed(2);
                var custo_operacional = (base_custos.valor_despesas / base_custos.clientes).toFixed(2)
                var margem_lucro = ((parseFloat(preco_plano_hora)/(1-(parseInt(base_custos.margem_lucro)/100)))-parseFloat(preco_plano_hora)).toFixed(2);
                var total_operacao = (parseFloat(preco_plano_hora)+ parseFloat(custo_operacional) + parseFloat(margem_lucro)).toFixed(2);
                var imposto_decimal = 1-(parseInt(base_custos.impostos)/100)
                var imposto_plano =  ((total_operacao / imposto_decimal ) - total_operacao).toFixed(2);
                valor_plano = (parseFloat(total_operacao) + parseFloat(imposto_plano)).toFixed(2);
                var valor_plano_individual = (parseFloat(valor_plano)/ (1- 0.30)).toFixed(2);

                $('#preco_plano_hora').val(preco_plano_hora);
                $('#custo_operacional').val(custo_operacional);
                $('#margem_lucro').val(margem_lucro);
                $('#imposto_plano').val(imposto_plano);
                $('#valor_plano').val(valor_plano);
                $('#valor_plano_individual').val(valor_plano_individual); 
                $('#plano').val(valor_plano); 
                $('#fee_plano').val((parseFloat(valor_plano)).toFixed(2)); 


            });

            $("#qtde").change(function() {
                qtde = $('#qtde').val();
            });


            $("#addProd").click(function() {
                var id = $('#produto').val();
                if (id > 0) {
                    var servicos_agenciado = $('#servicos_agenciado').val();
                    var produto = '';
                    var valor = '';

                    servicos_agenciado = JSON.parse(servicos_agenciado);
                    servicos_agenciado.forEach(element => {
                        if (element.id == id) {
                            produto = element.descricao
                            valor = element.esforco_valor
                            qtde = qtde
                            sub_total = parseFloat(valor) * parseInt(qtde)
                        }
                    });
                    arrayProdutos.push({
                        produto: produto,
                        valor: valor,
                        id: id,
                        qtde: qtde,
                        sub_total: sub_total
                    });



                    var id = arrayProdutos.length;
                    var markup = "<tr><td idlinha=" + 
                        produto +  ">" + produto + "</td><td data-qtde=" + qtde + ">" +
                        qtde + "</td>><td data-sub_total=" + sub_total + ">" +
                        sub_total +
                        "</td><td><button type='button' class='btn btn-warning  remover'>Excluir</button></td></tr>";
                    $("table #Tbody").append(markup);
                    $('#relProdutos').val(JSON.stringify(arrayProdutos));

                    valor_projeto = subValor_projeto + sub_total;
                    subValor_projeto  = valor_projeto;                    
                    total_projeto = (subValor_projeto / totais_impostos).toFixed(2);
                    valor_vista =  (parseFloat(total_projeto) * ( 1 - (parseInt(base_custos.margem_negocio)/100))).toFixed(2);
                    fee_mensal = (total_projeto / 12).toFixed(2);
                    fee_plano = (parseFloat(valor_plano) + parseFloat(fee_mensal)).toFixed(2);

                    $('#valor_projeto').val(subValor_projeto);
                    $('#total_projeto').val(total_projeto);
                    $('#valor_vista').val(valor_vista);
                    $('#fee_mensal').val(fee_mensal);
                    $('#fee_plano').val(fee_plano);

                    
                }
            });

            $('#Table').on('click', '.remover', function(e) {
                $('#desconto').val(0.00);
                var produto = $(this).closest('tr').find('td[data-prod]').data('prod');
                var sub_total = $(this).closest('tr').find('td[data-sub_total]').data('sub_total');
                var indice = $("td").attr("idlinha");

                arrayProdutos.splice(indice);
                $(this).closest('tr').remove();

                valor_projeto = subValor_projeto - sub_total;
                subValor_projeto  = valor_projeto;                    
                total_projeto = (subValor_projeto / totais_impostos).toFixed(2);
                valor_vista =  (parseFloat(total_projeto) * ( 1 - (parseInt(base_custos.margem_negocio)/100))).toFixed(2);
                fee_mensal = (total_projeto / 12).toFixed(2);
                fee_plano = (parseFloat(valor_plano) + parseFloat(fee_mensal)).toFixed(2);

                $('#valor_projeto').val(subValor_projeto);
                $('#total_projeto').val(total_projeto);
                $('#valor_vista').val(valor_vista);
                $('#fee_mensal').val(fee_mensal);
                $('#fee_plano').val(fee_plano);

            });

            function atualizaDados(){
                var hora = $('#hora_gerenciar').val();
                var preco_plano_hora = ((parseInt(hora) * parseFloat(operador)) +  parseFloat(operador)).toFixed(2);
                var custo_operacional = (base_custos.valor_despesas / base_custos.clientes).toFixed(2)
                var margem_lucro = ((parseFloat(preco_plano_hora)/(1-(parseInt(base_custos.margem_lucro)/100)))-parseFloat(preco_plano_hora)).toFixed(2);
                var total_operacao = (parseFloat(preco_plano_hora)+ parseFloat(custo_operacional) + parseFloat(margem_lucro)).toFixed(2);
                var imposto_decimal = 1-(parseInt(base_custos.impostos)/100)
                var imposto_plano =  ((total_operacao / imposto_decimal ) - total_operacao).toFixed(2);
                valor_plano = (parseFloat(total_operacao) + parseFloat(imposto_plano)).toFixed(2);
                var valor_plano_individual = (parseFloat(valor_plano)/ (1- 0.30)).toFixed(2)

                $('#preco_plano_hora').val(preco_plano_hora);
                $('#custo_operacional').val(custo_operacional);
                $('#margem_lucro').val(margem_lucro);
                $('#imposto_plano').val(imposto_plano);
                $('#valor_plano').val(valor_plano);
                 $('#valor_plano_individual').val(valor_plano_individual); 
            }

        });

    </script>
@endsection

@endsection
