@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('planos.index')}}">Plano</a></li>
                            <li class="breadcrumb-item active">Editar Plano</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Plano</h4>
                </div>
            </div>
        </div>
       <div class="card">
           <div class="card-body">
            <form>
                <input type="hidden" name="relProdutos" id="relProdutos">
                @csrf
                <div class="row">
                    <div class="col-xl-12">
                        <!-- tasks panel -->
                        <div class="collapse show" id="todayTasks">
                            <div class="card mb-0">
                                <div class="card-body">
                                    <a class="text-dark" data-toggle="collapse" href="#todayTasks" aria-expanded="false" aria-controls="todayTasks">
                                        <h4 class="m-0 pb-2">
                                            <i class="uil uil-angle-down font-18"></i>Detalhes Planos <span class="text-muted"></span>
                                        </h4>
                                    </a>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xl-4">
                                            <div class="form-group cliente">
                                                <h4 class="mb-0 mt-2">Nome</h4>
                                                <input type="text" name="nome" class="form-control" value="{{ $plano->nome }}">
                                            </div>
                                        </div> <!-- end col-->
                                        <div class="col-xl-2">

                                        </div> <!-- end col-->
                                        <div class="col-xl-4">

                                        </div> <!-- end col-->
                                        <div class="col-xl-2">
                                            <h4 class="mb-0 mt-2">Status</h4>
                                            <select name="status" id="status" class="form-control">
                                                <option value="{{$plano->status}}" {{$plano->status == 'ativo' ? 'selected' :''}} >Ativo</option>
                                                <option value="{{$plano->status}}" {{$plano->status == 'inativo' ? 'selected' :''}}>Inativo</option>
                                            </select>
                                        </div> <!-- end col-->
                                    </div>
                                    <!-- end row -->
                                </div>
                            </div> <!-- end card -->
                        </div> <!-- end .collapse-->
                        <br>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-8">

                                <div class="table-responsive">
                                    <table class="table table-borderless table-centered mb-0 no-wrap"  id="Table" style=" max-height: 150px;" >
                                        <thead class="thead-light">
                                            <tr>
                                                <th>#</th>
                                                <th>Descricao</th>
                                                <th style="width: 50px"></th>
                                            </tr>
                                        </thead>
                                        <tbody style="height-max:400px">
                                            @foreach ($itens as $item)
                                            <tr>
                                            <td>{{ $item->id}}</td>
                                                <td>{{ $item->servico->nome}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div> <!-- end table-responsive-->

                                <!-- action buttons-->
                                <div class="row mt-4">
                                    <div class="col-sm-6">
                                        <a href="{{ URL::previous() }}" class="btn text-muted d-none d-sm-inline-block btn-link font-weight-semibold">
                                            <i class="mdi mdi-arrow-left"></i> Voltar  </a>
                                    </div> <!-- end col -->
                                    <div class="col-sm-6">
                                        <div class="text-sm-right">

                                        </div>
                                    </div> <!-- end col -->
                                </div> <!-- end row-->
                            </div>
                            <!-- end col -->

                            <div class="col-lg-4">
                                <div class="border p-3 mt-4 mt-lg-0 rounded">
                                    <h4 class="header-title mb-3">Resumo da Proposta</h4>

                                    <div class="table-responsive">
                                        <table class="table mb-0">
                                            <tbody>
                                                <tr>
                                                    <td>Plano Base</td>
                                                    <td>
                                                    <input type="text" name="plano_base" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" id="plano_base" class="form-control" value="{{ $plano->plano_base }}">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Gerenciamento: </td>
                                                    <td>
                                                    <input type="text" name="gerenciamento" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" id="gerenciamento" class="form-control" value="{{ $plano->gerenciamento }}">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Margem Lucro</td>
                                                    <td>
                                                        <input type="text" name="margem_lucro"  id="margem_lucro" class="form-control" value="{{ $plano->margem_lucro }}">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Impostos</td>
                                                    <td>
                                                        <input type="text" name="impostos" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" id="impostos" class="form-control" value="{{ $plano->impostos }}">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Valor do Plano</td>
                                                    <td>
                                                        <input type="text" name="valor_plano" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" id="valor_plano" class="form-control" value="{{ $plano->valor_plano }}">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Plano Individual</td>
                                                    <td>
                                                        <input type="text" name="valor_plano_individual" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" id="valor_plano_individual" class="form-control" value="{{ $plano->plano_individual }}">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end table-responsive -->
                                </div>

                            </div> <!-- end col -->

                        </div> <!-- end row -->
                    </div>
                </div>
            </form>
           </div>
       </div>
    </div> <!-- container -->
@endsection
