@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('despesas.index')}}">Despesa</a></li>
                            <li class="breadcrumb-item active">Receber Pagamento</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Pagar Despesa</h4>
                </div>
            </div>
        </div>
       <div class="card">
           
           <div class="card-body">
               <form class="p-2" method="POST" action="{{ route('lancamentos.update',$despesa->id) }}"> 
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Nome </label>
                            <select class="form-control select2" data-toggle="select2" name="id_cli" disabled>
                                <option value="{{ $lancamento->cliente->id}}" style="text-transform: capitalize;" >{{$lancamento->cliente->nome}} --- {{$lancamento->cliente->cpf}}</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="task-priority2">Tipo</label>
                            <input type="text" class="form-control form-control-light"  name="tipo"  value="{{ $lancamento->tipo }}" readonly>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="task-priority2">Descrição</label>
                            <input type="text" class="form-control form-control-light"  name="descricao"  value="{{ $lancamento->descricao }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Parcela</label>
                            <input type="text" class="form-control form-control-light"  name="parcela"  value="{{ $despesa->parcela }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Valor Parcela</label>
                            <input type="text" class="form-control form-control-light"  name="valor_parcela"  value="{{ $despesa->valor_parcela }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="task-priority2">Valor a Pagar</label>
                            <input type="text" class="form-control form-control-light" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" name="valor_a_pagar" >
                        </div>
                    </div>
                </div>
    
                <div class="text-right mt-5">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Sair</button>
                    <button type="submit" class="btn btn-primary">Baixar</button>
                </div>
            </form>
           </div>
       </div>
    </div> <!-- container -->

@endsection

@section('js')
<script>
   
    $(document).ready(function() {
       
       
        $('#valor_parcela_create').focus( function (event){
            event.preventDefault();            
            var parcela     =  $('#parcela_create').val();
            var valor_lanc  =  $('#valor_lancamento_create').val();

            if(!parcela || parcela == 0){
                $('#valor_parcela_create').val(parseInt(valor_lanc).toFixed(2));
                $('#parcela_create').val('A Vista');
            } else {
                var valor_parcela = parseInt(valor_lanc) / parseInt(parcela);
                $('#valor_parcela_create').val(valor_parcela.toFixed(2));
            }
        });  

        
        
          
    } );
</script>
@endsection