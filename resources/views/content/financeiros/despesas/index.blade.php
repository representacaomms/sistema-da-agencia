@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('despesas.index') }}">Despesas</a></li>
                            <li class="breadcrumb-item active">Listar Despesas</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Listar Despesas</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                                <a href="{{ route('despesas.create') }}" class="btn btn-danger mb-2"><i class="mdi mdi-plus-circle mr-2"></i> Adicionar Despesas</a>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-success mb-2 mr-1"><i class="mdi mdi-settings"></i></button>
                                    <button type="button" class="btn btn-light mb-2 mr-1">Importar</button>
                                    <button type="button" data-toggle="modal" id="rel-modal" class="btn btn-light mb-2">Exportar</button>
                                </div>

                        
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                            @if ($message == 'store')
                                <script>
                                    var message = 'store';
                                </script>
                            @endif
                            @if ($message == 'update')
                                <script>
                                    var message = 'update';
                                </script>
                            @endif

                            @if ($message == 'destroy')
                                <script>
                                    var message = 'destroy';
                                </script>
                            @endif

                            @if ($message == 'active')
                                <script>
                                    var message = 'active';
                                </script>
                            @endif
                            
                        @endif
                            <table class="table table-centered w-100 dt-responsive " id="despesas-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Cliente</th>                                                                                
                                        <th>Descrição</th>       
                                        <th >Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($despesas))
                                        @foreach ($despesas as $key=>$item)
                                              @if ($item->status == 'A')
                                              <tr>
                                                <td><p class="m-0 d-inline-block align-middle font-16"><a  class="text-body" style="text-transform: Capitalize">{{$item->cliente->tipo == 'pj' ? $item->cliente->fantasia : $item->cliente->nome}}</a></p>
                                                
                                                    @if ($item->status_parc == 'Vencida')
                                                    <p><span class="badge badge-danger">Uma ou mais parcelas estão atrasada.</span></p>
                                                    @endif
                                                    </td>                                            
                                                <td><p class="m-0 d-inline-block align-middle font-16"><a  class="text-body">{{$item->descricao}}</a></p></td>                                          
                                                
                                                <td> 
                                                    <a href="{{ route('despesas.detal', $item->id) }}"  class="action-icon"> <i class="mdi mdi-eye"></i></a>
                                                </td>
                                            </tr> 
                                              @endif
                                        @endforeach
                                    @endif

                                </tbody>
                            </table>
                        </div>

                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
        <!-- end row -->
<!-- Standard modal -->
<div id="standard-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
    <form class="p-2" method="POST" action="{{ route('despesas.relatorio') }}">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="standard-modalLabel">Dados do Relatório</h4>
                
                </div>
                <div class="modal-body">
                    
                    <div class="mb-3">
                        <label for="username" class="form-label">Name</label>
                        <select class="form-control" type="text" name="cliente" required=""> 
                            <option value="">Selecione o Nome ....</option>
                            @foreach ($clientes  as $item)
                           
                            <option value="{{$item->id}}">{{$item->nome }}</option>
                           
                            @endforeach
                        </select>
                    </div>
                    

                    <div class="mb-3">
                        <label for="descricao" class="form-label">Descrição</label>
                        <input class="form-control" type="text" name="descricao" required>
                    </div>

                    <div class="mb-3">
                        <label for="data" class="form-label">Data</label>
                        <select name="data" id="data" class="form-control">
                            <option value="">Selecione o mês ...</option>
                            <option value="mes_atual">Atual</option>
                            <option value="mes_anterior">Anteriro</option>
                            <option value="mes_proximo">Proximo</option>
                        </select>
                    </div>

                    {{-- <div class="mb-3">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="customCheck1">
                            <label class="form-check-label" for="customCheck1">I accept <a href="#">Terms and Conditions</a></label>
                        </div> 
                    </div> --}}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
   </form>
</div><!-- /.modal -->


@endsection
@section('js')
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function() {
            jQuery.noConflict(); 
            $('#rel-modal').click( function () {
            $('#standard-modal').modal('show');

            })
            $('#despesas-datatable').DataTable({
                language: {
                    processing:     "Processamento...",
                    search:         "Pesquisar&nbsp;:",
                    lengthMenu:    "Mostrar _MENU_ entradas",
                    info:           "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty:      "Exibindo da entrada 0 até 0 de 0 entradas",
                    infoFiltered:   "(filtro total de _MAX_ entradas)",
                    infoPostFix:    "",
                    loadingRecords: "Carregando ...",
                    zeroRecords:    "Nenhuma entrada encontrada",
                    emptyTable:     "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first:      "Primeiro",
                        previous:   "Anterior",
                        next:       "Ultimo",
                        last:       "Depos"
                    },
                    aria: {
                        sortAscending:  ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        } );
    
        if(message == 'store'){
            Swal.fire(
                'Parabéns!',
                'Despesa cadastrada com sucesso!',
                'success'
            )
        }
        if(message == 'update'){
            Swal.fire(
                'Parabéns!',
                'Despesa editada com sucesso!',
                'success'
            )
        }   
        
        if(message == 'destroy'){
            Swal.fire(
                'Alerta!',
                'Despesa inativada com sucesso!',
                'info'
            )
        } 

        if(message == 'active'){
            Swal.fire(
                'Alerta!',
                'Despesa ativada com sucesso!',
                'info'
            )
        }   
        
    </script> 

@stop