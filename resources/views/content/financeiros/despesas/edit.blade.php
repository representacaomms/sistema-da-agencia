@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('despesas.index')}}">Despesa</a></li>
                            <li class="breadcrumb-item active">Editar Despesa</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Despesa</h4>
                </div>
            </div>
        </div>
       <div class="card">
           
           <div class="card-body">
               <form class="p-2" method="POST" action="{{ route('lancamentos.update',$despesa->id) }}"> 
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Nome </label>
                            <select class="form-control select2" data-toggle="select2" name="id_cli" disabled>
                                <option value="{{ $despesa->cliente->id}}" style="text-transform: capitalize;" >{{$despesa->cliente->nome}} -- {{ $despesa->cliente->cpf }}</option>
                                @foreach ($pessoas as $item)
                                <option value="{{ $item->id}} " style="text-transform: capitalize;" >{{$item->nome}} -- {{ $item->cpf }}</option>
                                @endforeach                                            
                            </select> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Plano de Conta</label>
                            <select class="form-control select2" data-toggle="select2" name="id_plano_contas" >
                                <option value="{{ $despesa->plano_conta->id}}" style="text-transform: capitalize;" >{{$despesa->plano_conta->nome}}</option>
                                @foreach ($contas as $item)
                                <option value="{{ $item->id}} " style="text-transform: capitalize;" >{{$item->nome}}</option>
                                @endforeach                                            
                            </select> 
                        </div>
                    </div>
    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Centro de Custo</label>
                            <select class="form-control select2" data-toggle="select2" name="id_centro_custo"  >
                                <option value="{{ $despesa->centro_custo->id}}" style="text-transform: capitalize;" >{{$despesa->centro_custo->nome}}</option>
                                @foreach ($centro_custos as $item)
                                <option value="{{ $item->id}} " style="text-transform: capitalize;" >{{$item->nome}}</option>
                                @endforeach                                            
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Categoria</label>
                            <select class="form-control select2" data-toggle="select2" id="categoria_create" name="id_categoria" >
                                <option value="{{ $despesa->categoria->id}}" style="text-transform: capitalize;" >{{$despesa->categoria->nome}}</option>
                                @foreach ($categorias as $item)
                                <option value="{{ $item->id}} " style="text-transform: capitalize;" >{{$item->nome}}</option>
                                @endforeach                                            
                            </select> 
                        </div>
                    </div>
    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Sub-Categoria</label>
                            <select class="form-control select2" data-toggle="select2" name="id_subcategoria" > 
                                <option value="{{ $despesa->subcategoria->id}}" style="text-transform: capitalize;" >{{$despesa->subcategoria->nome}}</option>
                                @foreach ($subcategorias as $sub)
                                <option value="{{ $sub->id}} " style="text-transform: capitalize;" >{{$sub->nome}}</option>
                                @endforeach                                                                           
                            </select> 
                        </div>                            
                    </div>
                    
                   
                </div>
                <div class="row">
                   {{--  <div class="col-md-3">
                        <div class="form-group">
                            <label for="task-title">Vencimento</label>
                            <input type="date" class="form-control form-control-light" name="vencimento">
                        </div>
                    </div> --}}
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Tipo</label>
                            <input class="form-control form-control-light" name="tipo" value="Despesa" readonly>                                    
                                
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Valor do Lançamento</label>
                            <input type="text" class="form-control form-control-light" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" name="valor_lancamento" value="{{ $despesa->valor_lancamento  }}" readonly>
                            
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Valor  Quitado</label>
                            <input type="text" class="form-control form-control-light" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" name="valor_amoretizado"  value="{{ $despesa->itens->valor_amoretizado }}" readonly>
                            
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Valor a Receber</label>
                            <input type="text" class="form-control form-control-light" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true"  id="valor_lancamento_create" value="{{ number_format($despesa->valor_lancamento - $despesa->valor_amoretizado,2,'.', '') }}" readonly>
                            
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Parcelas</label>
                            <input type="text" class="form-control form-control-light" name="parcela" id="parcela_create" value="{{ $despesa->total_parcela }}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Valor Parcela</label>
                            <input type="text" class="form-control form-control-light" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" id="valor_parcela_create" name="valor_parcela" value="{{ $despesa->valor_parcela }}" readonly>
                        </div>
                    </div>
                </div>  
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="task-priority2">Descrição</label>
                            <input type="text" class="form-control form-control-light"  name="descricao"  value="{{ $despesa->descricao }}">
                        </div>
                    </div>
                </div>
    
                <div class="text-right mt-5">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Sair</button>
                    <button type="submit" class="btn btn-primary">Atualizar</button>
                </div>
            </form>
           </div>
       </div>
    </div> <!-- container -->

@endsection

@section('js')
<script>
   
    $(document).ready(function() {
       
       
        $('#valor_parcela_create').focus( function (event){
            event.preventDefault();            
            var parcela     =  $('#parcela_create').val();
            var valor_lanc  =  $('#valor_lancamento_create').val();

            if(!parcela || parcela == 0){
                $('#valor_parcela_create').val(parseInt(valor_lanc).toFixed(2));
                $('#parcela_create').val('A Vista');
            } else {
                var valor_parcela = parseInt(valor_lanc) / parseInt(parcela);
                $('#valor_parcela_create').val(valor_parcela.toFixed(2));
            }
        });  

        
        
          
    } );
</script>
@endsection