@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('servicos-agenciado.index') }}">Serviços
                                    Agenciado</a></li>
                            <li class="breadcrumb-item active">Adicionar Serviços Agenciado</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Serviços Agenciado</h4>
                    <input type="hidden" id="base_custos" value="{{ $base_custos }}">
                    <input type="hidden" id="salarios" value="{{ $salarios }}">
                </div>
            </div>
        </div>
        <div class="card">

            <div class="card-body">
                <form class="p-2" method="POST" action="{{ route('servicos-agenciado.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nome </label>
                                <input type="text" name="descricao" id="descricao" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="task-priority2">Função</label>
                                <select name="funcao" id="funcao" class="form-control" required>
                                    @foreach ($salarios as $item)
                                        <option value="{{ $item->id }}">{{ $item->nome }}</option>
                                    @endforeach
                                </select>
                                   
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="task-priority2">Categoria</label>
                                <select name="categoria" id="categoria" class="form-control" required>
                                    <option value="estrutura">Estrutura</option>
                                    <option value="desenvolvimento">Desenvolvimento</option>
                                    <option value="marketing">Marketing</option>
                                    <option value="gestao">Gestão</option>
                                </select>
                              

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="task-priority2">Base</label>
                                <input type="text" name="base" id="base" class="form-control" readonly required>

                            </div>
                        </div>
                       
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="task-priority2">Esforço Hrs</label>
                                <input type="text" name="esforco_hrs" id="esforco_hrs" class="form-control" required>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="task-priority2">Esforço R$</label>
                                <input type="text" name="esforco_valor" id="esforco_valor" class="form-control" required>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="task-priority2">Individual</label>
                                <input type="text" name="valor_individual" id="valor_individual" class="form-control"
                                    required>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="task-priority2">Status</label>
                                <select name="situacao" id="situacao" class="form-control" required>
                                    <option value="ativo">Ativo</option>
                                    <option value="inativo">Inativo</option>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Escopo</label>
                                <textarea name="escopo" id="escopo" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="text-right mt-5">
                        <a href="{{ URL::previous() }}" class="btn btn-light" data-dismiss="modal">Sair</a>
                        <button type="submit" class="btn btn-primary">Adicionar</button>
                    </div>
                </form>
            </div>
        </div>
    </div> <!-- container -->

@section('js')
    <script>
        var arrayProdutos = [];
        $(document).ready(function() {

            var base_custos = $('#base_custos').val();
            var salarios = $('#salarios').val();
            salarios = JSON.parse(salarios);
            base_custos = JSON.parse(base_custos);

            var custos_totais = ((parseInt(base_custos.impostos) + parseInt(base_custos.margem_lucro)+ parseInt(base_custos.margem_negocio)) /100)
            var base = $('#base').val();
            var funcao = $('#funcao').val();
            $('#esforco_hrs').val(0);
            $('#esforco_valor').val(0);
            $('#valor_individual').val(0);

          

            loopDados(funcao, 'inicio');

            $('#funcao').change(function(event){
                event.preventDefault();
                const select_funcao = document.getElementById('funcao');
	            const value = select_funcao.options[select_funcao.selectedIndex].value;

                loopDados(value, 'funcao');
            })

            $('#esforco_hrs').blur(function(event) {
                event.preventDefault();
               atualizaDados()
            });

            function atualizaDados() {
                var esforco_hrs = $('#esforco_hrs').val();
                var valor_hora = (parseFloat(funcao) / 20) / 6.4;
                var esforco_valor = esforco_hrs * parseFloat(base);
                var valor_individual  =(((esforco_valor*(1+(parseInt(base_custos.margem_lucro)/100)))/(1-(parseInt(base_custos.margem_negocio)/100)))*(100 + parseInt(base_custos.onboarding)))/(1-parseInt(base_custos.impostos/100))/100
                $('#esforco_valor').val(esforco_valor.toFixed(2));
                $('#valor_individual').val(valor_individual.toFixed(2));
            }
            function loopDados(dados, value){
                salarios.forEach(element => {
                    if(element.id === parseInt(dados) ){
                        base = element.base;
                        $('#base').val(parseFloat(base).toFixed(2));  
                        if (value == 'funcao') {
                            atualizaDados()
                        }
                                        
                    } 
                });
            }

            
        });

    </script>
@endsection

@endsection
