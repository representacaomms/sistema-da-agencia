@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('custos.index')}}">Custo</a></li>
                            <li class="breadcrumb-item active">Editar Custo</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Custo</h4>
                </div>
            </div>
        </div>
       <div class="card">
           <div class="card-body">
               <form class="p-2" method="POST" action="{{ route('custos.update',$custo->id) }}">
                @method('PUT')
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Descrição </label>
                            <input type="text" name="nome" id="nome" class="form-control"  value="{{ $custo->nome }}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                        <label>Tipo {{ $custo->tipo }} </label>
                            <select class="form-control form-control-light" name="tipo" readonly>
                                <option value= "{{ $custo->tipo}}" {{ $custo->tipo == 'insumos' ? 'selected' : '' }}>Insumos</option>
                                <option value= "{{ $custo->tipo}}" {{ $custo->tipo == 'operacional' ? 'selected' : '' }}>Operacional</option>
                                <option value= "{{ $custo->tipo}}" {{ $custo->tipo == 'licencas' ? 'selected' : '' }}>Licenças</option>
                                <option value= "{{ $custo->tipo}}" {{ $custo->tipo == 'budget' ? 'selected' : '' }}>Budget</option>
                                <option value= "{{ $custo->tipo}}" {{ $custo->tipo == 'fixos' ? 'selected' : '' }}>Fixos</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Valor</label>
                            <input type="text" class="form-control form-control-light" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true"  name="valor"  value="{{ number_format($custo->valor,2) }}">
                        </div>
                    </div>
                </div>
                <div class="text-right mt-5">
                    <a href="{{ URL::previous()}}" class="btn btn-light" data-dismiss="modal">Sair</a>
                    <button type="submit" class="btn btn-primary">Atualizar</button>
                </div>
            </form>
           </div>
       </div>
    </div> <!-- container -->
@endsection
