@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('custos.index') }}">Custos</a></li>
                            <li class="breadcrumb-item active">Listar Custos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Listar Custos</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                                <a href="{{ route('custos.create') }}" class="btn btn-danger mb-2"><i class="mdi mdi-plus-circle mr-2"></i> Adicionar Custos</a>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-success mb-2 mr-1" data-toggle="modal" data-target="#right-modal"><i class="mdi mdi-settings"></i></button>
                                    <button type="button" class="btn btn-light mb-2 mr-1">Importar</button>
                                    <button type="button" class="btn btn-light mb-2">Exportar</button>
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                            @if ($message == 'store')
                                <script>
                                    var message = 'store';
                                </script>
                            @endif
                            @if ($message == 'update')
                                <script>
                                    var message = 'update';
                                </script>
                            @endif

                            @if ($message == 'destroy')
                                <script>
                                    var message = 'destroy';
                                </script>
                            @endif

                            @if ($message == 'active')
                                <script>
                                    var message = 'active';
                                </script>
                            @endif

                            @endif
                            <table class="table table-centered w-100 dt-responsive " id="custos-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Tipo</th>
                                        <th>Descrição</th>
                                        <th>Valor</th>
                                        <th >Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($custos as $key=>$item)
                                        <tr>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16">
                                                    @if ($item->tipo =='insumos')
                                                        <span class=" font-16 badge badge-info">Insumos</span>
                                                    @elseif($item->tipo =='operacional')
                                                        <span class=" font-16 badge badge-warning">Operacional</span>
                                                    @elseif($item->tipo =='licencas')
                                                        <span class=" font-16 badge badge-secondary">Licenças</span>
                                                    @elseif($item->tipo =='budget')
                                                        <span class=" font-16 badge badge-primary">Budget</span>
                                                    @else
                                                        <span class="font-16 badge badge-success">Fixos</span>
                                                    @endif

                                                </p>
                                            </td>
                                            <td><p class="m-0 d-inline-block align-middle font-16"><a  class="text-body">{{$item->nome}}</a></p></td>
                                            <td><p class="m-0 d-inline-block align-middle font-16"><a  class="text-body">R$ {{number_format($item->valor,2)}}</a></p></td>

                                            <td>
                                                <a href="{{ route('custos.edit', $item->id) }}" class="btn btn-warning mb-2">Editar</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div id="right-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-right">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="standard-modalLabel">Base dos Custos</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <form class="pl-3 pr-3" action="{{ route('base-custos.store') }}" method="POST">
                                            @csrf
                                            <div class="form-group">
                                                <label for="username">Clientes</label>
                                                <input class="form-control" type="text" name="clientes" value="{{ $clientes <= 10 ? 10 : $clientes   }}">
                                            </div>

                                            <div class="form-group">
                                                <label for="emailaddress">Despesas</label>
                                                <input class="form-control" type="text" name="despesas" data-toggle="input-mask"
                                                data-mask-format="###0.00" data-reverse="true"  value="{{ number_format($despesas_fixas, 2)}}">
                                            </div>

                                            <div class="form-group">
                                                <label for="password">Margem Negócio</label>
                                                <input class="form-control" type="text" required id="margem_negocio" name="margem_negocio" value="{{ $base_custos ? $base_custos->margem_negocio : '' }}">
                                            </div>
                                           <div class="form-group">
                                                <label for="password">Margem Lucro</label>
                                                <input class="form-control" type="text" required id="margem_lucro" name="margem_lucro" value="{{ $base_custos ? $base_custos->margem_lucro : '' }}">
                                            </div>

                                            <div class="form-group">
                                                <label for="password">Impostos</label>
                                                <input class="form-control" type="text" required id="impostos" name="impostos" value="{{ $base_custos ? $base_custos->impostos : '' }}">
                                            </div>

                                            <div class="form-group">
                                                <label for="password">Onboarding</label>
                                                <input class="form-control" type="text" required id="onboarding" name="onboarding" value="{{ $base_custos ? $base_custos->onboarding : '' }}">
                                            </div>


                                            

                                            <div class="form-group text-center">
                                                <button class="btn btn-primary" type="submit">Atualizar</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal -->
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
            <!-- end row -->

@endsection
@section('js')
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function() {

            $('#custos-datatable').DataTable({
                language: {
                    processing:     "Processamento...",
                    search:         "Pesquisar&nbsp;:",
                    lengthMenu:    "Mostrar _MENU_ entradas",
                    info:           "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty:      "Exibindo da entrada 0 até 0 de 0 entradas",
                    infoFiltered:   "(filtro total de _MAX_ entradas)",
                    infoPostFix:    "",
                    loadingRecords: "Carregando ...",
                    zeroRecords:    "Nenhuma entrada encontrada",
                    emptyTable:     "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first:      "Primeiro",
                        previous:   "Anterior",
                        next:       "Ultimo",
                        last:       "Depos"
                    },
                    aria: {
                        sortAscending:  ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        } );
    </script>
    <script type="text/javascript">

        if(message == 'store'){
            Swal.fire(
                'Parabéns!',
                'Despesa cadastrada com sucesso!',
                'success'
            )
        }
        if(message == 'update'){
            Swal.fire(
                'Parabéns!',
                'Despesa editada com sucesso!',
                'success'
            )
        }

        if(message == 'destroy'){
            Swal.fire(
                'Alerta!',
                'Despesa inativada com sucesso!',
                'info'
            )
        }

        if(message == 'active'){
            Swal.fire(
                'Alerta!',
                'Despesa ativada com sucesso!',
                'info'
            )
        }

    </script>

@stop
