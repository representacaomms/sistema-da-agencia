@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('receitas.index')}}">Receita</a></li>
                            <li class="breadcrumb-item active">Receber Pagamento</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Receber Pagamento</h4>
                </div>
            </div>
        </div>
       <div class="card">
           
           <div class="card-body">
               <form class="p-2" method="POST" action="{{ route('lancamentos.update',$receita->id) }}"> 
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Conta Bancária </label>
                            <select class="form-control select2" data-toggle="select2" name="conta_bancaria" required>
                                <option value="" style="text-transform: capitalize;" >Selecione uma conta bancária</option>
                               @foreach ($contas as $item)
                               @foreach ($bancos as $banco)
                                    @if ($banco['id']  == $item->banco)
                                    <option value="{{ $item->id}}" style="text-transform: capitalize;" >{{$banco['nome']}} ( {{$item->conta}} ) </option>
                                    @endif
                                @endforeach
                                    
                               @endforeach
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Nome </label>
                            <select class="form-control select2" data-toggle="select2" name="id_cli" disabled>
                                <option value="{{ $lancamento->cliente->id}}" style="text-transform: capitalize;" >{{$lancamento->cliente->nome}} --- {{$lancamento->cliente->cpf}}</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Tipo</label>
                            <input type="text" class="form-control form-control-light"  name="tipo"  value="{{ $lancamento->tipo }}" readonly>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="task-priority2">Descrição</label>
                            <input type="text" class="form-control form-control-light"  name="descricao"  value="{{ $lancamento->descricao }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Parcela</label>
                            <input type="text" class="form-control form-control-light"  name="parcela"  value="{{ $receita->parcela }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="task-priority2">Valor Parcela</label>
                            <input type="text" class="form-control form-control-light"  name="valor_parcela"  value="{{ $receita->valor_parcela }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="task-priority2">Valor a Pagar</label>
                            <input type="text" class="form-control form-control-light" data-toggle="input-mask" data-mask-format="###0.00" data-reverse="true" name="valor_a_pagar" >
                        </div>
                    </div>
                </div>
    
                <div class="text-right mt-5">
                    <a href="{{ URL::previous() }}" class="btn btn-light" >Sair</a>
                    <button type="submit" class="btn btn-primary">Baixar</button>
                </div>
            </form>
           </div>
       </div>
    </div> <!-- container -->

@endsection

@section('js')
<script>
   
    $(document).ready(function() {
       
       
        $('#valor_parcela_create').focus( function (event){
            event.preventDefault();            
            var parcela     =  $('#parcela_create').val();
            var valor_lanc  =  $('#valor_lancamento_create').val();

            if(!parcela || parcela == 0){
                $('#valor_parcela_create').val(parseInt(valor_lanc).toFixed(2));
                $('#parcela_create').val('A Vista');
            } else {
                var valor_parcela = parseInt(valor_lanc) / parseInt(parcela);
                $('#valor_parcela_create').val(valor_parcela.toFixed(2));
            }
        });  

        
        
          
    } );
</script>
@endsection