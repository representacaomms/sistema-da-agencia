@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('receitas.index') }}">Receitas</a></li>
                            <li class="breadcrumb-item active">Detalhes Receitas</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Detalhes  Receitas</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                               <a href="{{ route('receitas.index') }}" class="btn btn-danger mb-2"><i
                                        class="mdi mdi-arrow-left-bold mr-2"></i> Voltar</a>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-success mb-2 mr-1"><i class="mdi mdi-settings"></i></button>
                                    <button type="button" class="btn btn-light mb-2 mr-1">Importar</button>
                                    <button type="button" class="btn btn-light mb-2">Exportar</button>
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                            @if ($message == 'store')
                                <script>
                                    var message = 'store';
                                </script>
                            @endif
                            @if ($message == 'update')
                                <script>
                                    var message = 'update';
                                </script>
                            @endif

                            @if ($message == 'destroy')
                                <script>
                                    var message = 'destroy';
                                </script>
                            @endif

                            @if ($message == 'active')
                                <script>
                                    var message = 'active';
                                </script>
                            @endif
                            
                        @endif
                            <table class="table table-centered w-100 dt-responsive " id="receitas-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Descrição</th>                           
                                        <th>Parcela</th>                                                                                
                                        <th>Valor</th>                                                                                
                                        <th >Data</th>       
                                        <th >Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($receitas))
                                        @foreach ($receitas as $key=>$item)
                                            
                                                <tr>                                  <td><p class="m-0 d-inline-block align-middle font-16"><a  class="text-body">{{$descricao}}</a></p></td> 
                                                    <td>
                                                        <p class="m-0 d-inline-block align-middle font-16"><a  class="text-body">Parcela: {{$item->parcela}} de {{ $parcelas}}</a></br>
                                                            @if ($item->status =='A Receber')
                                                            <span class="badge badge-info">A Receber</span>
                                                            @elseif ($item->status =='Pendente')
                                                            <span class="badge badge-warning">Pendente</span>
                                                            @else
                                                            <span class="badge badge-success">Quitado</span>
                                                            @endif
                                                        </p>
                                                    </td>                                          
                                                    <td><p class="m-0 d-inline-block align-middle font-16"><a  class="text-body">R$ {{$item->valor_parcela}}</a></p></td>                                          
                                                    <td>{{date('d/m/Y', strtotime($item->data_pagto))}} </td>
                                                    <td> 
                                                    @if ($item->status != "Recebido")
                                                    @if ($item->data_pagto < $hoje )
                                                    <a href="{{ route('receitas.show', $item->id) }}"  class="action-icon"> <i class="mdi mdi-coin"></i></a> 
                                                    <a target="_blank" href="https://api.whatsapp.com/send?phone={{$telefone}}&text=Ol%C3%A1%2C%20{{$cliente}} consta%20uma%20parcela%20em%20aberto%20vencida%20no%20dia {{date('d/m/Y', strtotime($item->data_pagto))}} %20segue%20o%20pix:%20agenciamms20%40gmail.com"  class="action-icon"> <span class="badge badge-danger">Vencida</span> </a>
                                                    @else
                                                    <a href="{{ route('receitas.show', $item->id) }}"  class="action-icon"> <i class="mdi mdi-coin"></i></a>                                                   
                                                    @endif
                                                    @endif
                                                   
                                                    </td>
                                                </tr> 
                                        @endforeach
                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
            <!-- end row -->

@endsection
@section('js')
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function() {
          
            $('#receitas-datatable').DataTable({
                language: {
                    processing:     "Processamento...",
                    search:         "Pesquisar&nbsp;:",
                    lengthMenu:    "Mostrar _MENU_ entradas",
                    info:           "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty:      "Exibindo da entrada 0 até 0 de 0 entradas",
                    infoFiltered:   "(filtro total de _MAX_ entradas)",
                    infoPostFix:    "",
                    loadingRecords: "Carregando ...",
                    zeroRecords:    "Nenhuma entrada encontrada",
                    emptyTable:     "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first:      "Primeiro",
                        previous:   "Anterior",
                        next:       "Ultimo",
                        last:       "Depos"
                    },
                    aria: {
                        sortAscending:  ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        } );
    </script>
    <script type="text/javascript">
    
        if(message == 'store'){
            Swal.fire(
                'Parabéns!',
                'Receita cadastrada com sucesso!',
                'success'
            )
        }
        if(message == 'update'){
            Swal.fire(
                'Parabéns!',
                'Receita editada com sucesso!',
                'success'
            )
        }   
        
        if(message == 'destroy'){
            Swal.fire(
                'Alerta!',
                'Receita inativada com sucesso!',
                'info'
            )
        } 

        if(message == 'active'){
            Swal.fire(
                'Alerta!',
                'Receita ativada com sucesso!',
                'info'
            )
        }   
        
    </script> 

@stop