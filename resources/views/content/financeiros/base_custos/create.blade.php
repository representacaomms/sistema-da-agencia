@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('servicos-agenciado.index')}}">Serviços Agenciado</a></li>
                            <li class="breadcrumb-item active">Adicionar Serviços Agenciado</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Adicionar Serviços Agenciado</h4>
                </div>
            </div>
        </div>
       <div class="card">

           <div class="card-body">
               <form class="p-2" method="POST" action="{{ route('servicos-agenciado.store') }}">
                @csrf
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Nome </label>
                            <input type="text" name="nome" id="nome" class="form-control" required>
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label>Especificação</label>
                            <input type="text" name="especificacao" id="especificacao" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Adicionais</label>
                        <input type="text" name="adicionais" id="adicionais" class="form-control" required>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="task-priority2">Escopo</label>
                            <input type="text" name="escopo" id="escopo" class="form-control" required>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="task-priority2">Status</label>
                           <select name="status" id="status" class="form-control" required>
                               <option value="ativo">Ativo</option>
                               <option value="inativo">Inativo</option>
                           </select>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Calcular Serviços Adicionais</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="task-priority2">Escopo</label>
                            <input type="text" name="escopo" id="escopo" class="form-control" required>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="task-priority2">Escopo</label>
                            <input type="text" name="escopo" id="escopo" class="form-control" required>

                        </div>
                    </div>
                </div>

                <div class="text-right mt-5">
                    <a href="{{ URL::previous()}}" class="btn btn-light" data-dismiss="modal">Sair</a>
                    <button type="submit" class="btn btn-primary">Adicionar</button>
                </div>
            </form>
           </div>
       </div>
    </div> <!-- container -->

@endsection
