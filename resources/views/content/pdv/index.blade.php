@extends('layouts.thema')
@section('content')
   <!-- Start Content-->
     <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('propostas.index') }}">Vendas</a></li>
                            <li class="breadcrumb-item active">PDV</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <pdv-component :configEmpresa="{{ json_encode($configEmpresa) }}"></pdv-component>

      
    </div> <!-- container -->

@endsection
