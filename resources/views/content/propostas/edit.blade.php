@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('propostas.index')}}">Propostas</a></li>
                            <li class="breadcrumb-item active">Editar Propostas</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Propostas</h4>                   
                </div>
            </div>
        </div>
        
        <form name="formOrcamento" action="{{ route('propostas.update', $proposta->id)}}" method="POST">
            
            @method('PUT')
            @csrf
            <div class="row">
                <div class="col-xl-12">
                    <!-- tasks panel -->
                    <div class="collapse show" id="todayTasks">
                        <div class="card mb-0">
                            <div class="card-body">
                                <a class="text-dark" data-toggle="collapse" href="#todayTasks" aria-expanded="false"
                                    aria-controls="todayTasks">
                                    <label class="m-0 pb-2">
                                        <i class="uil uil-angle-down font-18"></i>Dados Propostas <span
                                            class="text-muted"></span>
                                    </label>
                                </a>
                                <hr>
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="form-group id_cliente">
                                            <label for="projectname">Cliente</label>
                                             <input type="text" name="cliente" id="cliente" class="form-control"
                                                value="{{ $proposta->dados->nome }}" readonly>
                                        </div>
                                    </div> <!-- end col-->

                                    <div class="col-xl-4">
                                        <div class="form-group">
                                            <label for="projectname">Vendedor</label>
                                            <input type="text" name="vendedor" id="vendedor" class="form-control"
                                                value="{{ Auth::user()->name }}" readonly>
                                            <input type="hidden" name="id_vendedor" id="id_vendedor" class="form-control"
                                                value="{{ Auth::user()->id }}">
                                        </div>
                                    </div> <!-- end col-->

                                    <div class="col-xl-4">
                                        <div class="form-group">
                                            <label for="projectname">Situação</label>
                                            <select name="status" id="status" class="form-control">
                                                <option value="">{{$proposta->situacao}}</option>
                                                <option value="Aprovado" {{$proposta->situacao == 'Aprovado' ? 'selected': ''}}>Aprovado</option>
                                                <option value="Reprovado" {{$proposta->situacao == 'Reprovado' ? 'selected': ''}}>Reprovado</option>
                                                <option value="Faturado" {{$proposta->situacao == 'Faturado' ? 'selected': ''}}>Faturado</option>
                                                <option value="Pendente" {{$proposta->situacao == 'Pendente' ? 'selected': ''}}>Pendente</option>
                                            </select>
                                           
                                        </div>
                                    </div> <!-- end col-->
                                </div>
                                <!-- end row -->
                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end .collapse-->
                    <br>
                </div>
            </div>

            <div class="card">

                <div class="card-body">

                   
                    
                    <div class="row mb-2">
                        <div class="col-sm-4">
                            <h4 class="header-title mb-3"> Detalhes da Proposta </h4>  
                        </div>
                        <div class="col-sm-8">
                            <div class="text-sm-right">
                                <input type="submit" value="Atualizar" class="btn btn-primary btn-sm " >
                            </div>
                        </div><!-- end col-->
                    </div>

                    <div class="row">
                        @foreach ($planos as $item)

                            <div class="col-md-4">
                                <!-- Portlet card -->
                                <div class="card mb-md-0 mb-3">
                                    <div class="{{ $item->id == $proposta->plano ? 'card-body border-danger border' : 'card-body border-primary border' }}">
                                            <h5 class="card-title mb-0 text-center">{{ $item->nome }}
                                            </h5>
                                            <hr>
                                            <div class="card-body">
                                                @foreach ($item->itens as $iten)
                                                    <h5 class="card-title">{{ $iten->servico->descricao }}</h5>
                                                    <p class="card-text">{{ $iten->servico->escopo }}</p>
                                                    <hr>
                                                @endforeach
                                                <input type="hidden" name="plano" value="{{ $item->id }}">
                                                <input type="hidden" name="cliente" value="{{$cliente->nome}}">
                                                <input type="hidden" name="vendedor" value="{{ Auth::user()->name }}">
                                                @if ($item->id != $proposta->plano)
                                                    <input type="submit" value="Selecionar" class="btn btn-primary btn-sm " >
                                                @endif
                                                  
                                            </div>

                                    </div>
                                </div> <!-- end card-->

                            </div><!-- end col -->
                        @endforeach
                    </div>

                    <!-- Bool Switch-->

                </div>
            </div>
        </form>
    </div> <!-- container --> 
@endsection
@section('js')
<script>
        $(document).ready(function() {

            $('#id_cliente').change(function() {
                var id_cliente = ($(this).val());
                $('.cliente').val(id_cliente);
            });



        });

    </script>
@endsection