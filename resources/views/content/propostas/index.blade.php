@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Propostas</a></li>
                            <li class="breadcrumb-item active">Listar Propostas</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Listar Propostas</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                                <a href="{{ route('propostas.create') }}" class="btn btn-danger mb-2"><i
                                        class="mdi mdi-plus-circle mr-2"></i> Adicionar Proposta</a>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-success mb-2 mr-1"><i
                                            class="mdi mdi-settings"></i></button>
                                    <button type="button" class="btn btn-light mb-2 mr-1">Importar</button>
                                    <button type="button" class="btn btn-light mb-2">Exportar</button>
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                @if ($message == 'invoice')
                                    <script>
                                        var message = 'invoice';
                                    </script>
                                @endif
                            @endif
                            <table class="table table-centered w-100 dt-responsive " id="propostas-datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th class="all" style="width: 20px;">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                <label class="custom-control-label" for="customCheck1">&nbsp;</label>
                                            </div>
                                        </th>
                                        <th class="all">ID</th>
                                        <th>Data</th>
                                        <th>Propostas</th>
                                        <th>Valor</th>
                                        <th>Vendedor</th>
                                        <th style="width: 75px;">Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($propostas as $key => $item)
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                                                    <label class="custom-control-label" for="customCheck2">&nbsp;</label>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="m-0 d-inline-block align-middle font-16">
                                                    <a href="apps-ecommerce-products-details.html"
                                                        class="text-body">{{ $item->id }}</a>
                                                    <br />
                                                    @if ($item->situacao == 'orcamento')
                                                        <span class="badge badge-info">Orçamentos</span>
                                                    @else
                                                        <span class="badge badge-success">Pedido</span>
                                                    @endif


                                                </p>
                                            </td>
                                            <td>
                                                {{ $item->created_at->format('d/m/Y') }}
                                            </td>
                                            <td> {{ $item->dados['nome'] }} </td>
                                            <td>
                                                {{ 'R$ ' . number_format($item->valor, 2, ',', '.') }}
                                            </td>
                                           
                                            <td>
                                                {{ $item->vendedor }}
                                            </td>


                                            <td>
                                                <a href="{{ route('propostas.edit', $item->id) }}" class="action-icon">
                                                    <i class="mdi mdi-square-edit-outline"></i></a>
                                                <a href="{{ route('propostas.show', $item->id) }}" class="action-icon">
                                                    <i class="mdi mdi-checkbox-marked-circle"></i></a>
                                                <a href="{{ route('propostas.destroy', $item->id) }}" class="action-icon">
                                                    <i class="mdi mdi-alert-circle"></i></a>

                                            </td>
                                        </tr>

                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div> <!-- container -->


@endsection
@section('js')
    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function() {

            $('#propostas-datatable').DataTable({
                language: {
                    processing: "Processamento...",
                    search: "Pesquisar&nbsp;:",
                    lengthMenu: "Mostrar _MENU_ entradas",
                    info: "Exibindo _START_ até _END_ de _TOTAL_ entradas",
                    infoEmpty: "Exibindo da entrada 0 até 0 de 0 entradas",
                    infoFiltered: "(filtro total de _MAX_ entradas)",
                    infoPostFix: "",
                    loadingRecords: "Carregando ...",
                    zeroRecords: "Nenhuma entrada encontrada",
                    emptyTable: "Nenhuma entrada disponível na tabela",
                    paginate: {
                        first: "Primeiro",
                        previous: "Anterior",
                        next: "Ultimo",
                        last: "Depos"
                    },
                    aria: {
                        sortAscending: ": ativar para classificar a coluna em ordem crescente",
                        sortDescending: ": ativar para classificar a coluna em ordem decrescente"
                    }
                }
            });
        });
    </script>
    <script type="text/javascript">
        if (message == 'store') {
            Swal.fire(
                'Parabéns!',
                'Registro cadastrado com sucesso!',
                'success'
            )
        }
        if (message == 'update') {
            Swal.fire(
                'Parabéns!',
                'Registro editado com sucesso!',
                'success'
            )
        }

        if (message == 'destroy') {
            Swal.fire(
                'Alerta!',
                'Registro inativado com sucesso!',
                'info'
            )
        }

        if (message == 'active') {
            Swal.fire(
                'Alerta!',
                'Registro ativado com sucesso!',
                'info'
            )
        }
    </script>
    <script type="text/javascript">
        if (message == 'invoice') {
            Swal.fire(
                'Parabéns!',
                'Pedido FATURADO com sucesso!',
                'success'
            )
        }
    </script>
@stop
