@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('notafiscal.index')}}">Nota Fiscal</a></li>
                            <li class="breadcrumb-item active">Adicionar Nota Fiscal</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Gerar Nota Fiscal</h4>
                </div>
            </div>
        </div>
        @if (session('error'))
        <input type="hidden" name="message" id="message" value="{{ session('error') }}">
            <script>
                var message = document.getElementById('message').value;
                var tipo = 'Ops!';
                var slug = 'error'
            </script>
        @endif

        @if ($message)
        <input type="hidden" name="message" id="message" value="{{ $message }}">
            <script>
                var message = document.getElementById('message').value;;
                var tipo = 'Informação!';
                var slug = 'info'
            </script>
        @endif
        <form name="formOrcamento" action="{{ route('notafiscal.store')}}" method="POST">
            <input type="hidden" name="relPodutos" id="relPodutos">

            @method('POST')
            @csrf
            <div class="row">
                <div class="col-xl-12">
                    <!-- tasks panel -->
                    <div class="collapse show" id="todayTasks">
                        <div class="card mb-0">
                            <div class="card-body">
                                <a class="text-dark" data-toggle="collapse" href="#todayTasks" aria-expanded="false" aria-controls="todayTasks">
                                    <h4 class="m-0 pb-2">
                                        <i class="uil uil-angle-down font-18"></i>Detalhes Nota Fiscal <span class="text-muted"></span>
                                    </h4>
                                </a>
                                <hr>
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="form-group cliente">
                                            <label class="mb-0 mt-2">Natureza da Operação</label>
                                            <select class="form-control select2" data-toggle="select2" name="natureza_operacao"  >
                                                <option value="Venda" style="text-transform: capitalize;" >Venda</option>
                                            </select>
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-3"></div>
                                    <div class="col-xl-2">
                                        <div class="form-group">
                                            <label class="mb-0 mt-2" for="projectname">Forma Pagto</label>
                                            <select class="form-control" name="indPag"  >
                                                <option value="0" style="text-transform: capitalize;" >A Vista</option>
                                                <option value="1" style="text-transform: capitalize;" selected >A Prazo</option>
                                                <option value="2" style="text-transform: capitalize;" >Outros</option>

                                            </select>
                                        </div></div> <!-- end col-->
                                    <div class="col-xl-2">
                                        <div class="form-group">
                                            <label class="mb-0 mt-2" for="projectname">Finalidade</label>
                                            <select class="form-control" name="finNFe"  >
                                                <option value="1" style="text-transform: capitalize;" selected >Normal</option>
                                                <option value="2" style="text-transform: capitalize;" >Complementar</option>
                                                <option value="3" style="text-transform: capitalize;" >Ajuste</option>
                                                <option value="4" style="text-transform: capitalize;" >Devolução/Retorno</option>

                                            </select>
                                        </div>
                                    </div> <!-- end col-->
                                   {{--  <div class="col-xl-1">
                                        <div class="form-group">
                                            <label class="mb-0 mt-2" for="projectname">Tipo</label>
                                            <select class="form-control" name="tipo"  >
                                                <option value="0" style="text-transform: capitalize;" >Saida</option>
                                                <option value="0" style="text-transform: capitalize;" >Entrada</option>

                                            </select>

                                        </div>
                                    </div> <!-- end col-->--}}
                                </div>

                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="form-group cliente">
                                            <label class="mb-0 mt-2">Cliente</label>
                                            <select class="form-control select2" data-toggle="select2" name="cliente" id="cliente" >
                                                @foreach ($clientes as $item)
                                                <option value="{{ $item->id}} " style="text-transform: capitalize;" >{{$item->nome}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-3"></div> <!-- end col-->
                                    <div class="col-xl-4">
                                        <div class="form-group">
                                            <label class="mb-0 mt-2" for="projectname">Vendedor</label>
                                            <input type="text" name="vendedor" id="vendedor" class="form-control" value="{{ Auth::user()->name }}" readonly>
                                            <input type="hidden" name="id_vendedor" id="id_vendedor" class="form-control" value="{{ Auth::user()->id }}">
                                        </div>
                                    </div> <!-- end col-->
                                </div>
                                <!-- end row -->
                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end .collapse-->
                    <br>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="projectname">Produto</label>
                                        <select class="form-control select2" data-toggle="select2" id="produto" >
                                            <option value="0">Selecione um produto</option>
                                            @foreach ($produtos as $item)
                                                <option value="{{ $item->id}}" >{{$item->descricao}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> <!-- end col-->
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label for="projectname">Qtde  &nbsp;</label>
                                        <input type="number" id="qtde" value="1"  class="form-control">
                                    </div>
                                </div> <!-- end col-->
                                <div id='teste'>

                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="projectname">Valor Unitário</label>
                                        <input type="text"  id="valor" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="projectname">Valor Total</label>
                                        <input type="text"  id="valor_total"  data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="projectname">&nbsp;</label>
                                        <button type="button" class="btn btn-outline-primary form-control" id="addProd"> Adicionar </button>
                                    </div>
                                </div> <!-- end col-->
                            </div>
                            <div class="table-responsive">
                                <table class="table table-borderless table-centered mb-0 no-wrap"  id="Table" style=" max-height: 150px;" >
                                    <thead class="thead-light">
                                        <tr>
                                            <th>#</th>
                                            <th>Descricao</th>
                                            <th>Qtde</th>
                                            <th>Valor</th>
                                            <th>Valor Total</th>
                                            <th style="width: 50px"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="Tbody" style="height-max:400px"></tbody>
                                </table>
                            </div> <!-- end table-responsive-->

                            <!-- Add note input-->
                            <div class="mt-3">
                                <label for="example-textarea">Observações da Nota Fiscal:</label>
                                <textarea class="form-control" name="detalhes_notafiscal" id="example-textarea" rows="3" placeholder="Esta informação será impressa na observações da nota fiscal."></textarea>
                            </div>

                        </div>
                        <!-- end col -->

                        <div class="col-lg-3">
                            <div class="border p-3 mt-4 mt-lg-0 rounded">
                                <h4 class="header-title mb-3">Totais da Nota Fiscal</h4>

                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <tbody>
                                            <tr>
                                                <td>Valor Frete:</td>
                                                <td>
                                                    <input type="text" name="frete" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" id="frete" class="form-control">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Valor Desconto: </td>
                                                <td>
                                                    <input type="text" name="desconto" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" id="desconto" class="form-control">

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Valor Despesa:</td>
                                                <td>
                                                    <input type="text" name="despesa" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" id="despesa" class="form-control">

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Valor Total da NotaFiscal: </td>
                                                <td>
                                                    <input type="text" id="valor_total_nota" name="vlr_total_notafiscal" data-toggle="input-mask" data-mask-format="#.##0.00" data-reverse="true" id="vlr_total_notafiscal" class="form-control">

                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <!-- end table-responsive -->
                            </div>

                            <!-- action buttons-->
                            <div class="row mt-4">
                                <div class="col-sm-6">
                                    <a href="#" class="btn text-muted d-none d-sm-inline-block btn-link font-weight-semibold">
                                        <i class="mdi mdi-arrow-left"></i> Voltar  </a>
                                </div> <!-- end col -->
                                <div class="col-sm-6">
                                    <div class="text-sm-right">
                                        <button type="submit" class="btn btn-danger">
                                            <i class="mdi mdi-cart-plus mr-1"></i> Emitir Nota Fiscal </button>
                                    </div>
                                </div> <!-- end col -->
                            </div> <!-- end row-->

                        </div> <!-- end col -->

                    </div> <!-- end row -->
                </div>
            </div>
        </form>
    </div> <!-- container -->

    @section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script type="text/javascript">
        if(message){
            Swal.fire(
                tipo,
                message+'!',
                slug
            )
        }

    </script>
    <script>
        var arrayProdutos =[];
        $(document).ready(function() {

            $('#scroll-vertical-datatable').DataTable();
            $('#valor').val(0.00);
            $('#valor_total').val(0.00);
            $('#valor_total_nota').val('0.00');
            $('#desconto').val('0.00');
            $('#frete').val('0.00');
            $('#despesa').val('0.00');


            $('#qtde').change( function (event){
                event.preventDefault();
                var qtde        =  $('#qtde').val();
                var valor_unit  =  $('#valor').val();
                var valor_total = parseFloat(valor_unit) * parseInt(qtde);
                var valor_total_nota = $('#valor_total_nota').val();

                $('#valor_total').val(valor_total.toFixed(2));
            });

            $('#produto').change(function(event){
                event.preventDefault();
                var selected =  $('#produto option:selected');
                if(selected.val() == "0"){
                    $('#valor').val(0.00);
                    $('#valor_total').val(0.00);
                    $('#qtde').val("1");
                }
                $.ajax({
                    url: "https://lojistas.solutiomms.com/gestao/produtos/"+selected.val(),
                    type: "get",
                    dataType: 'json',
                    success: function (response) {
                        $('#valor').val(response.preco_venda);
                        $('#valor_total').val(response.preco_venda);
                    }
                });

            });

            $("#addProd").click(function(){

                $('#desconto').val('0.00');
                var valor        = $("#valor").val();
                var valor_total  = $("#valor_total").val();
                var quantidade   = $("#qtde").val();
                var produto      = $('#produto option:selected');


                if(produto.val() == "0"){
                   return;
                }

                arrayProdutos.push(
                    {
                        produto:produto.text(),
                        quantidade:quantidade,
                        valor: valor,
                        valor_total:valor_total,
                    }
                );

                var id = arrayProdutos.length;
                var markup       = "<tr><td idlinha="+id+">"+id+"</td><td data-prod="+produto.text()+">"+produto.text()+"</td><td data-qtd="+quantidade+" style='width: 90px;'>" + quantidade + "</td><td data-valor="+valor+">" + valor + "</td><td data-total="+valor_total+">" + valor_total + "</td><td><button type='button' class='btn btn-warning  remover'>Excluir</button></td></tr>";
                $("table #Tbody").append(markup);

                var valor_total_nota =   parseFloat($('#valor_total_nota').val());


                if(valor_total_nota == 0){
                    $('#valor_total_nota').val(valor_total);
                } else {
                    const total = parseFloat(valor_total_nota) + parseFloat(valor_total);
                    $('#valor_total_nota').val(total.toFixed(2));
                }

                $("#valor").val("0.00");
                $("#valor_total").val("0.00");
                $("#qtde").val("1");
                $('#relPodutos').val(JSON.stringify(arrayProdutos));

            });

            $('#Table').on('click', '.remover', function (e) {
                $('#desconto').val(0.00);
                var valor_total_nota =   parseFloat($('#valor_total_nota').val());
                var valor_total = $(this).closest('tr').find('td[data-total]').data('total');
                var produto = $(this).closest('tr').find('td[data-prod]').data('prod');
                if(valor_total_nota > 0){
                    const total = parseFloat(valor_total_nota) - parseFloat(valor_total);
                    $('#valor_total_nota').val(total.toFixed(2));

                }

                var indice = $("td").attr("idlinha");

                arrayProdutos.splice(indice);
                $(this).closest('tr').remove();
                $('#relPodutos').val(JSON.stringify(arrayProdutos));

            });

            $('#desconto').change( function() {
                var desconto = parseFloat($(this).val());
                var subtotal = parseFloat($('#subtotal_th').text());

                var total = subtotal - desconto;
                $('#total_th').text(total.toFixed(2));

            });

        } );
    </script>


    @endsection

@endsection
