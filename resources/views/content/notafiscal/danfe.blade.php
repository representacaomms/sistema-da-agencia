@extends('layouts.app')
@section('content')
 <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('notafiscal.index')}}">Nota Fiscal</a></li>
                            <li class="breadcrumb-item active">Danfe Notafiscal</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Danfe Notafiscal</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">


                    <iframe src="data:application/pdf;base64,{{$pdf}}" style="border: none;"></iframe>

                        <div class="d-print-none mt-4">
                            <div class="text-right">
                                <a href="javascript:window.print()" class="btn btn-primary"><i class="mdi mdi-printer"></i> Imprimir</a>
                                <a href="{{ route('propostas.index') }}" class="btn btn-info">Fechar</a>

                            </div>
                        </div>

                    </div> <!-- end card-body-->
                </div> <!-- end card -->
            </div> <!-- end col-->
        </div>
        <!-- end row -->

    </div> <!-- container -->
@endsection
