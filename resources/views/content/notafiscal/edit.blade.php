@extends('layouts.app')
@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('propostas.index')}}">Propostas</a></li>
                            <li class="breadcrumb-item active">Editar Propostas</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Editar Propostas {{ $proposta->subtotal }}</h4>                   
                </div>
            </div>
        </div>
        
        <form name="formOrcamento" action="{{ route('propostas.update', $proposta->id)}}" method="POST">
            <input type="hidden" name="tipo" value="orcamento">                           
            <input type="hidden" name="status" value="Aguardando Aprovacao">                           
            <input type="hidden" name="relPodutos" id="relPodutos">                        
            <input type="hidden" name="id_vendedor" value="{{ Auth::user()->id }}">                        
            
            @method('PUT')
            @csrf
            <div class="row">               
                <div class="col-xl-12">
                    <!-- tasks panel -->
                    <div class="collapse show" id="todayTasks">
                        <div class="card mb-0">
                            <div class="card-body">
                                <a class="text-dark" data-toggle="collapse" href="#todayTasks" aria-expanded="false" aria-controls="todayTasks">
                                    <h4 class="m-0 pb-2">
                                        <i class="uil uil-angle-down font-18"></i>Detalhes Propostas <span class="text-muted"></span>
                                    </h4>
                                </a>
                                <hr>
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="form-group cliente">
                                            <h4 class="mb-0 mt-2">Cliente</h4>
                                            <input type="text" name="cliente" id="cliente" class="form-control" value="{{ $proposta->cliente->nome }}" readonly>
                                            <input type="hidden" name="id_cli" id="id_cli" class="form-control" value="{{ $proposta->id_cli}}" readonly>
                                            
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-3"></div> <!-- end col-->
                                    <div class="col-xl-3">
                                        <div class="form-group">
                                            <label for="projectname">Vendedor</label>
                                            <input type="text" name="vendedor" id="vendedor" class="form-control" value="{{ Auth::user()->name }}" readonly>
                                        </div>
                                    </div> <!-- end col-->
                                    <div class="col-xl-2">
                                        <div class="form-group">
                                            <label for="projectname">Status</label>
                                            <select class="form-control" id="status" name="status" >
                                                <option value="{{ $proposta->status }}">{{ $proposta->status }}</option>
                                                <option value="Aprovado">Aprovado</option>
                                                <option value="Reprovado">Reprovado</option>                                                                                          
                                            </select> 
                                            
                                        </div>
                                    </div> <!-- end col-->
                                </div>
                                <!-- end row -->        
                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end .collapse-->
                    <br>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8">                       
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="projectname">Produto</label>
                                        <select class="form-control select2" data-toggle="select2" id="produto" >
                                            <option value="0">Selecione um produto</option>
                                            @foreach ($produtos as $item)
                                                <option value="{{ $item->id}}" >{{$item->descricao}}</option>
                                            @endforeach                                            
                                        </select> 
                                    </div>
                                </div> <!-- end col-->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="projectname">Quantidade  &nbsp;</label>                                           
                                        <input type="number" id="qtde" value="1"  class="form-control">
                                    </div>
                                </div> <!-- end col-->
                                <div id='teste'>  

                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="projectname">Valor Unitário</label>
                                        <input type="text"  id="valor" class="form-control" readonly>
                                    </div>
                                </div> 
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="projectname">Valor Total</label>
                                        <input type="text"  id="valor_total" class="form-control" readonly>                                           
                                    </div>
                                </div>  
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="projectname">&nbsp;</label>
                                        <button type="button" class="btn btn-outline-primary form-control" id="addProd"> Adicionar</button> 
                                    </div>                                                                                        
                                </div> <!-- end col-->                                           
                            </div>
                            <div class="table-responsive">
                                <table class="table table-borderless table-centered mb-0 no-wrap"  id="Table" style=" max-height: 150px;" >
                                    <thead class="thead-light">
                                        <tr>
                                            <th>#</th>
                                            <th>Descricao</th>
                                            <th>Qtde</th>
                                            <th>Valor</th>
                                            <th>Valor Total</th>
                                            <th style="width: 50px"></th> 
                                        </tr>
                                    </thead>
                                    <tbody id="Tbody" style="height-max:400px">

                                        @foreach ($itensProposta as $key=>$item)
                                            <tr>
                                                <td data-idlinha="{{$key + 1}}">{{$key + 1}}</td>
                                                <td data-prod="{{$item->produto}}">{{$item->produto}}</td>
                                                <td data-qtd="{{$item->quantidade}}">{{$item->quantidade}}</td>
                                                <td data-valor="{{$item->valor}}">{{$item->valor}}</td>
                                                <td data-total="{{$item->valor_total}}">{{$item->valor_total}}</td>                                                
                                                <td><button type='button' class='btn btn-warning  remover'>Excluir</button></td>
                                            </tr>
                                        @endforeach
                                    
                                    </tbody> 
                                </table>
                            </div> <!-- end table-responsive-->

                            <!-- Add note input-->
                            <div class="mt-3">
                                <label for="example-textarea">Adicione uma Observação:</label>
                                <textarea class="form-control" id="example-textarea" rows="3" placeholder="Escreva um pouco .."></textarea>
                            </div>

                            <!-- action buttons-->
                            <div class="row mt-4">
                                <div class="col-sm-6">
                                    <a href="{{ URL::previous()}}" class="btn text-muted d-none d-sm-inline-block btn-link font-weight-semibold">
                                        <i class="mdi mdi-arrow-left"></i> Voltar  </a>
                                </div> <!-- end col -->
                                <div class="col-sm-6">
                                    <div class="text-sm-right">
                                        <button type="submit" class="btn btn-danger">
                                            <i class="mdi mdi-cart-plus mr-1"></i> Atualizar </button>
                                    </div>
                                </div> <!-- end col -->
                            </div> <!-- end row-->
                        </div>
                        <!-- end col -->

                        <div class="col-lg-4">
                            <div class="border p-3 mt-4 mt-lg-0 rounded">
                                <h4 class="header-title mb-3">Resumo da Proposta</h4>

                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <tbody>
                                            <tr>
                                                <td>Sub Total:</td>
                                                <td id="subtotal_th"></td>
                                            </tr>
                                            <tr>
                                                <td>Desconto: </td>
                                                <td>
                                                    <input type="text" data-toggle="input-mask" data-mask-format="#.##0,00" data-reverse="true"  name="desconto" id="desconto" class="form-control">
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Forma de Pagamento:</td>
                                                <td><select name="fr_pagamento" id="fr_pagamento" class="form-control" required>
                                                    @if ($proposta->fr_pagamento)
                                                        <option value="{{ $proposta->fr_pagamento }}">{{ $proposta->fr_pagamento }}</option>
                                                        <option value="a Vista">Dinheiro</option>
                                                        <option value="a Vista">Cartão Credito / Débito</option>
                                                        <option value="a Vista">Boleto</option>
                                                    @else
                                                        <option value="">Selecione ...</option>
                                                        <option value="a Vista">Dinheiro</option>
                                                        <option value="a Vista">Cartão Credito / Débito</option>
                                                        <option value="a Vista">Boleto</option>
                                                    @endif
                                                   
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Condições de Pagamento: </td>
                                                <td><select name="cnd_pagamento" id="cnd_pagamento" class="form-control" required>
                                                    @if ($proposta->cnd_pagamento)
                                                        <option value="{{ $proposta->cnd_pagamento }}">{{ $proposta->cnd_pagamento }}</option>
                                                        <option value="1x">A Vista</option>
                                                        <option value="2x">2x</option>
                                                        <option value="3x">3x</option>
                                                        <option value="4x">4x</option>
                                                        <option value="5x">5x</option>
                                                        <option value="6x">6x</option>
                                                    @else
                                                        <option value="">Selecione ...</option>
                                                        <option value="1x">A Vista</option>
                                                        <option value="2x">2x</option>
                                                        <option value="3x">3x</option>
                                                        <option value="4x">4x</option>
                                                        <option value="5x">5x</option>
                                                        <option value="6x">6x</option>
                                                    @endif
                                                   
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Total:</th>
                                                <th id="total_th"></th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- end table-responsive -->
                            </div>                      

                        </div> <!-- end col -->

                    </div> <!-- end row -->
                </div>
            </div> 
        </form>
    </div> <!-- container --> 
@endsection
@section('js')
<script>
    
    var arrayProdutos = []
    var loop = {{$arrayProdutos}}

    for (var i = 0; i < loop; i++) {

        const id = $("#Tbody td" ).closest('tr').find('td[data-id]').data('id');   
        const produto = $("#Tbody td" ).closest('tr').find('td[data-prod]').data('prod');   
        const quantidade = $("#Tbody td" ).closest('tr').find('td[data-qtd]').data('qtd');   
        const valor = $("#Tbody td" ).closest('tr').find('td[data-valor]').data('valor');   
        const valor_total = $("#Tbody td" ).closest('tr').find('td[data-total]').data('total');   

            arrayProdutos.push({                        
                produto:produto,
                quantidade:quantidade,
                valor: valor,
                valor_total:valor_total,
            });
        
        // more statements
    }

    console.log(arrayProdutos)
    

    $(document).ready(function() {
       
        $('#scroll-vertical-datatable').DataTable();
        $('#valor').val("0.00");
        $('#valor_total').val("0.00");
        $('#subtotal_th').text(({{ $proposta->subtotal }}).toFixed(2));
        $('#desconto').val(({{ $proposta->desconto }}).toFixed(2));
        $('#total_th').text(({{ $proposta->total }}).toFixed(2));
       
        $('#qtde').change( function (event){
            event.preventDefault();
            var qtde        =  $('#qtde').val();
            var valor_unit  =  $('#valor').val();
            var valor_total = parseInt(valor_unit) * parseInt(qtde);
            $('#valor_total').val(valor_total.toFixed(2));
        });

        $('#produto').change(function(event){
            event.preventDefault();               
            var selected =  $('#produto option:selected'); 
            if(selected.val() == "0"){
                $('#valor').val("0,00");
                $('#valor_total').val("0,00");
                $('#qtde').val("1");
            }             
            $.ajax({
                url: "/solutiomms/public/gestao-vendas/produtos/"+selected.val(),
                type: "get",
                dataType: 'json',
                success: function (response) {                                     
                    $('#valor').val((response.preco_venda).toFixed(2));
                    $('#valor_total').val((response.preco_venda).toFixed(2));
                }
            });
        });

        $("#addProd").click(function(){
           
            $('#desconto').val("0.00");               
            var valor        = $("#valor").val();
            var valor_total  = $("#valor_total").val();
            var quantidade   = $("#qtde").val();
            var produto      = $('#produto option:selected');

            
            if(produto.val() == "0"){
               return;
            } 

            arrayProdutos.push(
                {
                    produto:produto.text(),
                    quantidade:quantidade,
                    valor: valor,
                    valor_total:valor_total,
                }
            );   

            var id = arrayProdutos.length;
            var markup       = "<tr><td idlinha="+id+">"+id+"</td><td data-prod="+produto.text()+">"+produto.text()+"</td><td data-qtd="+quantidade+" style='width: 90px;'>" + quantidade + "</td><td data-valor="+valor+">" + valor + "</td><td data-total="+valor_total+">" + valor_total + "</td><td><button type='button' class='btn btn-warning  remover'>Excluir</button></td></tr>";
            $("table #Tbody").append(markup);             
          
            var subtotal =   parseInt($('#subtotal_th').text());
            if(subtotal == 0){
                $('#subtotal_th').text(valor_total);
                $('#total_th').text(valor_total);
            } else {
                const total = subtotal + parseInt(valor_total);
                $('#subtotal_th').text(total.toFixed(2));
                $('#total_th').text(total.toFixed(2));
            }

            $("#valor").val("0,00");
            $("#valor_total").val("0,00");
            $("#qtde").val("1"); 
            $('#relPodutos').val(JSON.stringify(arrayProdutos)); 

                       

        });

        $('#Table').on('click', '.remover', function (e) {
            $('#desconto').val("0.00");
            var subtotal =   parseInt($('#subtotal_th').text());
           
            var valor_total = $(this).closest('tr').find('td[data-total]').data('total');
            var produto = $(this).closest('tr').find('td[data-prod]').data('prod');               
            if(subtotal > 0){
                const total = (subtotal - parseInt(valor_total)).toFixed(2);
                $('#subtotal_th').text(total);
                $('#total_th').text(total);
            }  

            
           var linha = $(this).closest('tr');
           
           var indice = linha[0]['cells'][0]['dataset']['idlinha'];
            arrayProdutos.splice(indice);   
            $(this).closest('tr').remove();
            $('#relPodutos').val(JSON.stringify(arrayProdutos));

        });

        $('#desconto').change( function() {
            var desconto = parseInt($(this).val());
            var subtotal = parseInt($('#subtotal_th').text());

            var total = subtotal - desconto;
            $('#total_th').text(total.toFixed(2));
            
        });
          
    } );
</script>
@endsection