<!-- Top modal -->
<div id="modal-plano_conta" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-top">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="topModalLabel">Cadastro de Plano Contas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="{{ route('plano-contas.store') }}" method="post" class="form-horizontal">
                    @csrf
                <div class="modal-body">                
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Descricao</label>
                        <div class="col-9">
                            <input type="text" class="form-control" style="text-transform: capitalize;" id="nome" name="nome" placeholder="Nome" required>
                        </div>
                    </div>                                                
                </div>
                <div class="modal-footer">
                    <button  type="button" class="btn btn-secondary" data-dismiss="modal" >Cancelar</button>
                    <button  type="submit" class="btn btn-primary">Cadastrar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
