<!-- Top modal -->
<div id="modal-produto{{ $item->id}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-top">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="topModalLabel">Editar Produto</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="{{ route('produtos.update',$item->id) }}" method="post" class="form-horizontal">
                    @csrf
                <div class="modal-body">                
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Descricao</label>
                        <div class="col-9">
                            <input type="text" class="form-control" style="text-transform: capitalize;" id="descricao" name="descricao" placeholder="Nome" readonly value="{{ $item->descricao }}">
                        </div>
                    </div>   
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Custo</label>
                        <div class="col-9">
                            <input type="text" class="form-control"  id="preco_custo" name="preco_custo" placeholder="Preço Custo" value="{{ $item->preco_custo }}">
                            <script type="text/javascript">$("#preco_custo").mask('##0.00', {reverse: true});</script>

                        </div>
                    </div>   
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Venda</label>
                        <div class="col-9">
                            <input type="text" class="form-control" id="preco_venda" name="preco_venda" placeholder="Preço Venda" required value="{{ $item->preco_venda }}">
                            <script type="text/javascript">$("#preco_venda").mask('##0.00', {reverse: true});</script>

                        </div>
                    </div>   
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Fornecedor</label>
                        <div class="col-9">
                            <select name="id_fornecedor" id="id_fornecedor" class="form-control">
                                <option value="{{$item->id_fornecedor}}">{{$item->fornecedor->nome}}</option>
                                    @foreach ($records as $it)
                                        <option value="{{$it->id }}">{{$it->nome}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>   
                                               
                </div>
                <div class="modal-footer">
                    <button  type="button" class="btn btn-secondary" data-dismiss="modal" >Cancelar</button>
                    <button  type="submit" class="btn btn-primary">Cadastrar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
