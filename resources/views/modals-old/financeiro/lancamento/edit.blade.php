<!--  Add new task modal -->
<div class="modal fade task-modal-content" id="modal-lancamento-edit{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="NewTaskModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="NewTaskModalLabel">Editar Lançamento</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="p-2" method="POST" action="{{ route('lancamentos.update', $item->id) }}"> 
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Nome </label>
                                <select class="form-control select2" data-toggle="select2" name="id_cli"  >
                                    <option value="{{ $item->id_cli}}"> {{ $item->cliente->nome   }}</option>
                                   
                                    @foreach ($pessoas as $i)
                                    <option value="{{ $i->id}} " style="text-transform: capitalize;" >{{$i->nome}}</option>
                                    @endforeach                                            
                                </select> 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Plano de Conta</label>
                                <select class="form-control select2" data-toggle="select2" name="status" >
                                    @if ($item->tipo == 'Despesa')
                                        <option value="">{{ $item->status}}</option>
                                        <option>A Pagar</option>
                                        <option >Pago</option>
                                    @else
                                        <option value="">{{ $item->status}}</option>
                                        <option>A Receber</option>
                                        <option >Recebido</option>
                                    @endif
                                                                                                                  
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Plano de Conta</label>
                                <select class="form-control select2" data-toggle="select2" name="id_plano_contas" >
                                    <option value="{{ $item->id_plano_contas }}">{{ $item->plano_conta->nome }}</option>
                                    @foreach ($contas as $i)
                                    <option value="{{ $i->id}} " style="text-transform: capitalize;" >{{$i->nome}}</option>
                                    @endforeach                                            
                                </select> 
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Centro de Custo</label>
                                <select class="form-control select2" data-toggle="select2" name="id_centro_custo"  >
                                    <option value="{{ $item->id_centro_custo }}">{{ $item->centro_custo->nome  }}</option>
                                    @foreach ($centro_custos as $i)
                                    <option value="{{ $i->id}} " style="text-transform: capitalize;" >{{$i->nome}}</option>
                                    @endforeach                                            
                                </select> 
                            </div>
                        </div>

                        
                       
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Categoria</label>
                                <select class="form-control select2" data-toggle="select2" name="id_categoria" id="categoria">
                                    <option value="{{ $item->id_categoria }}">{{ $item->categoria->nome }}</option>
                                    @foreach ($categorias as $i)
                                        @if($i->id !== $item->id_categoria)
                                            <option value="{{ $i->id}} " style="text-transform: capitalize;" >{{$i->nome}}</option>
                                        @endif
                                    
                                    @endforeach                                            
                                </select> 
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Sub-Categoria</label>
                                <select class="form-control select2" data-toggle="select2" name="id_subcategoria" id="subcategoria" >     
                                    <option value="{{ $item->id_subcategoria }}">{{ $item->subcategoria->nome }}</option>                                                                                                 
                                </select> 
                            </div>                            
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Tipo</label>
                                <select class="form-control form-control-light" name="tipo">  
                                    @if ($item->tipo == 'Despesa')
                                    <option>{{ $item->tipo }}</option>
                                    <option>Receita</option>
                                    @else
                                    <option>{{ $item->tipo }}</option>
                                    <option>Despesa</option>
                                    @endif                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    

                    <div class="row">
                       {{--  <div class="col-md-3">
                            <div class="form-group">
                                <label for="task-title">Vencimento</label>
                                <input type="date" class="form-control form-control-light" name="vencimento">
                            </div>
                        </div> --}}

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="task-priority2">Valor do Lançamento</label>
                                <input type="text" class="form-control form-control-light" name="valor_lancamento" id="valor_lancamento" value="{{$item->valor_lancamento }}">
                                <script type="text/javascript">$("#valor_lancamento").mask('##0.00', {reverse: true});</script>
                            </div>
                        </div>

                        <div class="col-md-2">
                            
                        </div>


                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="task-priority2">Parcela</label>
                                <input type="text" class="form-control form-control-light" name="parcela" id="parcela" value="{{$item->parcela }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="task-priority2">Valor Parcela</label>
                                <input type="text" class="form-control form-control-light" id="valor_parcela" name="valor_parcela" value="{{$item->valor_parcela }}">
                            </div>
                        </div>
                    </div>  
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="task-priority2">Descrição</label>
                                <input type="text" class="form-control form-control-light"  name="descricao" value="{{$item->descricao }}">
                            </div>
                        </div>
                    </div>

                    <div class="text-right mt-5">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Sair</button>
                        <button type="submit" class="btn btn-primary">Adicionar</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
   
    $(document).ready(function() {            
        $('#valor_parcela').focus( function (event){
            event.preventDefault();            
            var parcela     =  $('#parcela').val();
            var valor_lanc  =  $('#valor_lancamento').val();

            if(!parcela || parcela == 0){
                $('#valor_parcela').val(parseInt(valor_lanc).toFixed(2));
                $('#parcela').val('A Vista');
            } else {
                var valor_parcela = parseInt(valor_lanc) / parseInt(parcela);
                $('#valor_parcela').val(valor_parcela.toFixed(2));
            }
        });  

        $('#categoria').change(function(event){
            event.preventDefault();               
            var selected =  $('#categoria option:selected'); 
            var option = '<option>Selecione ... </option>';               
            $('#subcategoria').removeAttr('disabled');                         
            $.ajax({
                url: "http://localhost/solutiomms/public/financeiro/subcategorias/"+selected.val(),
                type: "get",
                dataType: 'json',
                success: function (response) {  
                    response.forEach(element => {
                        option += '<option value="'+element.id+'" style="text-transform: capitalize;">'+element.nome+'</option>';
                        
                    }); 
                    $('#subcategoria').html(option).show();                                  
                    
                }
            });
        });
        
          
    } );
</script>