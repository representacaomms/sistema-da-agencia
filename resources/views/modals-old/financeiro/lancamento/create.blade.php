<!--  Add new task modal -->
<div class="modal fade task-modal-content" id="modal-lancamento-create" tabindex="-1" role="dialog" aria-labelledby="NewTaskModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="NewTaskModalLabel">Adicionar Lançamento</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="p-2" method="POST" action="{{ route('lancamentos.store') }}"> 
                    @csrf
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Nome </label>
                                <select class="form-control select2" data-toggle="select2" name="id_cli"  >
                                    <option value="0">Selecione ....</option>
                                    @foreach ($pessoas as $item)
                                    <option value="{{ $item->id}} " style="text-transform: capitalize;" >{{$item->nome}}</option>
                                    @endforeach                                            
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Plano de Conta</label>
                                <select class="form-control select2" data-toggle="select2" name="id_plano_contas" >
                                    <option value="0">Selecione ....</option>
                                    @foreach ($contas as $item)
                                    <option value="{{ $item->id}} " style="text-transform: capitalize;" >{{$item->nome}}</option>
                                    @endforeach                                            
                                </select> 
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Centro de Custo</label>
                                <select class="form-control select2" data-toggle="select2" name="id_centro_custo"  >
                                    <option value="0">Selecione ....</option>
                                    @foreach ($centro_custos as $item)
                                    <option value="{{ $item->id}} " style="text-transform: capitalize;" >{{$item->nome}}</option>
                                    @endforeach                                            
                                </select> 
                            </div>
                        </div>

                        
                       
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Categoria</label>
                                <select class="form-control select2" data-toggle="select2" id="categoria_create" name="id_categoria" >
                                    <option value="0">Selecione ....</option>
                                    @foreach ($categorias as $item)
                                    <option value="{{ $item->id}} " style="text-transform: capitalize;" >{{$item->nome}}</option>
                                    @endforeach                                            
                                </select> 
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Sub-Categoria</label>
                                <select class="form-control select2" data-toggle="select2" name="id_subcategoria" id="subcategoria_create" disabled>                                                                         
                                </select> 
                            </div>                            
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Tipo</label>
                                <select class="form-control form-control-light" name="tipo">                                    
                                    <option>Despesa</option>
                                    <option>Receita</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="task-title">Vencimento</label>
                                <input type="date" class="form-control form-control-light" name="vencimento">
                            </div>
                        </div> 

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="task-priority2">Valor do Lançamento</label>
                                <input type="text" class="form-control form-control-light" name="valor_lancamento" id="valor_lancamento_create">
                                <script type="text/javascript">$("#valor_lancamento_create").mask('##0.00', {reverse: true});</script>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="task-priority2">Parcela</label>
                                <input type="text" class="form-control form-control-light" name="parcela" id="parcela_create">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="task-priority2">Valor Parcela</label>
                                <input type="text" class="form-control form-control-light" id="valor_parcela_create" name="valor_parcela">
                            </div>
                        </div>
                    </div>  
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="task-priority2">Descrição</label>
                                <input type="text" class="form-control form-control-light"  name="descricao">
                            </div>
                        </div>
                    </div>

                    <div class="text-right mt-5">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Sair</button>
                        <button type="submit" class="btn btn-primary">Adicionar</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
   
    $(document).ready(function() {
       
       
        $('#valor_parcela_create').focus( function (event){
            event.preventDefault();            
            var parcela     =  $('#parcela_create').val();
            var valor_lanc  =  $('#valor_lancamento_create').val();

            if(!parcela || parcela == 0){
                $('#valor_parcela_create').val(parseInt(valor_lanc).toFixed(2));
                $('#parcela_create').val('A Vista');
            } else {
                var valor_parcela = parseInt(valor_lanc) / parseInt(parcela);
                $('#valor_parcela_create').val(valor_parcela.toFixed(2));
            }
        });  

        $('#categoria_create').change(function(event){           
            event.preventDefault();               
            var selected =  $('#categoria_create option:selected'); 
            var option = '<option>Selecione ... </option>';               
            $('#subcategoria_create').removeAttr('disabled');                         
            $.ajax({
                url: "http://localhost/solutiomms/public/financeiro/subcategorias/"+selected.val(),
                type: "get",
                dataType: 'json',
                success: function (response) {  
                    console.log(response)
                    response.forEach(element => {
                        option += '<option value="'+element.id+'" style="text-transform: capitalize;">'+element.nome+'</option>';
                        
                    }); 
                    $('#subcategoria_create').html(option).show();                                  
                    
                }
            });
        });
        
          
    } );
</script>