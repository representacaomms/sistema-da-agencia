<!--  Add new task modal -->
<div class="modal fade task-modal-content" id="modal-categoria-create" tabindex="-1" role="dialog" aria-labelledby="NewTaskModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-top">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="topModalLabel">Cadastro de Categoria</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="{{ route('categorias.store') }}" method="post" class="form-horizontal">
                    @csrf
                <div class="modal-body">                
                    <div class="form-group row mb-3">
                        <label for="inputEmail3" class="col-3 col-form-label">Descricao</label>
                        <div class="col-9">
                            <input type="text" class="form-control" style="text-transform: capitalize;" id="nome" name="nome" placeholder="Nome" required>
                        </div>
                    </div>                                                
                </div>
                <div class="modal-footer">
                    <button  type="button" class="btn btn-secondary" data-dismiss="modal" >Cancelar</button>
                    <button  type="submit" class="btn btn-primary">Cadastrar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->