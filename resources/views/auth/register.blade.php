
<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="utf-8" />
        <title>Registrar-se | Solutiomms - Sistema de Gestão de empresas</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Sistema para empresas como nota fiscal" name="description" />
        <meta content="Coderthemes" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App css -->
        <link href="{{ asset('css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/app.min.css') }}" rel="stylesheet" type="text/css" id="light-style" />

    </head>

    <body class="authentication-bg">
        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                        <div class="col-lg-5">
                            <div class="card">
                                <!-- Logo-->
                                <div class="card-header pt-4 pb-4 text-center bg-primary">
                                    <a href="index.html">
                                        <span><img src="{{ url('images/logo.png') }}" alt="" height="80"></span>
                                    </a>
                                </div>

                                <div class="card-body p-4">
                                    
                                    <div class="text-center w-75 m-auto">
                                        <h4 class="text-dark-50 text-center mt-0 font-weight-bold">Cadastre-se</h4>
                                        <p class="text-muted mb-4"> Não possui uma conta? Crie sua conta, leva menos de um minuto </p>
                                    </div>

                                    <form method="POST" action="{{ route('register') }}">
                                        @csrf

                                        <div class="form-group">
                                            <label for="fullname">Nome</label>
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="emailaddress">Email</label>
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="emailaddress">Senha</label>
                                            <input id="senha" type="password" class="form-control @error('senha') is-invalid @enderror" name="password" value="{{ old('senha') }}" required autocomplete="password">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="password">Celular</label>
                                            <input  type="text" data-toggle="input-mask" data-mask-format="(00) 0.0000-0000" class="form-control @error('celular') is-invalid @enderror" name="celular" required autocomplete="celular">

                                            @error('celular')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div> 
                                        <div class="form-group">
                                            <label for="password">CNPJ</label>
                                            <input  type="text" data-toggle="input-mask" data-mask-format=" 00.000.000/0000-00" class="form-control @error('cpf') is-invalid @enderror" name="cpf" required >

                                            @error('cpf')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div> 

                                        {{-- <div class="form-group">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="checkbox-signup">
                                                <label class="custom-control-label" for="checkbox-signup">Eu Aceito <a href="#" class="text-muted">Termos e Condições</a></label>
                                            </div>
                                        </div> --}}

                                        <div class="form-group mb-0 text-center">
                                            <button class="btn btn-primary" type="submit"> Registrar-se</button>
                                        </div>

                                    </form>
                                </div> <!-- end card-body -->
                            </div>
                            <!-- end card -->

                            <div class="row mt-3">
                                <div class="col-12 text-center">
                                    <p class="text-muted">Já tem uma conta? <a href="pages-login.html" class="text-muted ml-1"><b>Conecte-se</b></a></p>
                                </div> <!-- end col-->
                            </div>
                            <!-- end row -->

                        </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->

        <footer class="footer footer-alt">
            2018 - 2020 © Hyper - Coderthemes.com
        </footer>

        <!-- bundle -->
        <script src="{{ asset('js/vendor.min.js') }}"></script>
        <script src="{{ asset('js/app.min.js') }}"></script>

        
    </body>
        

</html>