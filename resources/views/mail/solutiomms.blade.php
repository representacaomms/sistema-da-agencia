@component('mail::message')
<h1> Seja bem Vindo!</h1>

<h3>Olá, {{ $user->name }} tudo bem?</h3>
<h4>Meu nome é Marcelo Andrade da Silva, <br>fundador da plataforma <b>SOLUTIOMMS</b> <br>
Agradecemos pela oportunidade e confiança que nos proporcionou.</h4>

<p>Como presente, vamos liberar para você o acesso gratuito!</p>
<p>Esses são os seus dados de acesso no nosso sistema!</p>
<b>Login:</b> {{ $user->email }} <br>
<b>senha:</b> {{ $user->acesso }}
@component('mail::button', ['url' => 'http://solutiomms.solutiomms.com'])
    Acessar Sistema
@endcomponent

<p>Desfrute a vontade !!</p>  
@endcomponent

