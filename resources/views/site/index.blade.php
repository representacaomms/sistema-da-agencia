@extends('site.template.app')
@section('content')

<main>

    <div class="main_cta">
        <article class="main_cta_content">
            <div class="main_cta_content_spacer">
                <header>
                    <h1>Aqui você encontra tudo o que é necessário para deixar sua empresa bem posicionada no mercado.</h1>
                </header>

                <p>Tenha melhores resultados para sua empresa.</p>
                <p><a href="#" class="btn">Saiba Mais</a></p>
            </div>
        </article>
    </div>

    <section class="main_blog">
        <header class="main_blog_header">
            <h1 class="icon-blog">Nossos Últimos Artigos</h1>
            <p>Aqui você encontra os artigos necessários para auxiliar na sua caminhada ao melhor resultado.</p>
        </header>
        @foreach ($blogs as $blog)
        <article>
            <a href="#">
                <img src="{{ env('APP_URL') }}/storage/{{  $blog->img }}" title="{{ $blog->titulo}}">
            </a>
            <p><a href="#" class="category">{{$blog->categoria}} </a></p>
            <h2><a href="#" class="title">{{ $blog->titulo}} </a></h2>
        </article>
        @endforeach
        

        
    </section>

    <article class="main_optin">
        <div class="main_optin_content">
            <header>
                <h1>Quer receber todas as novidades em seu e-mail?</h1>
                <p>Informe seu nome e e-mail no campo ao lado e clique em Ok!</p>
            </header>

            <form>
                <input type="text" placeholder="Seu nome">
                <input type="email" placeholder="Seu e-mail">
                <button type="submit">Ok!</button>
            </form>
        </div>
    </article>

    <section class="main_course">
        <header class="main_course_header">
            <img src="{{ asset('site/_img/logo.png') }}"  alt="Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
            <h1 class="icon-books">Sistema de Gestão Comercial</h1>
            <p>Sistema de gestão com facilidade para gerenciar a sua empresa, com tecnologias atualizadas trazendo ao usuário melhor experiência e agilidade no dia a dia !</p>
        </header>

        <div class="main_course_content">
            <article>
                <header>
                    <h2>Gerenciamento de Clientes</h2>
                    <p>Aqui você encontra tudo o que precisa para ter o melhor relacionamento comercial.</p>
                </header>
            </article>

            <article>
                <header>
                    <h2>Controle de Vendas</h2>
                    <p>Aqui você tem acesso em tudo que precisa para ter controle das suas vendas </p>
                </header>
            </article>

            <article>
                <header>
                    <h2>Controle Financeiro</h2>
                    <p>Controle Financeiro agil e fácil para você manter a vida financeira da sua empresa saudavel.</p>
                </header>
            </article>

            <article>
                <header>
                    <h2>Emissão de Nota Fiscal</h2>
                    <p>Emissor de nota fiscal com facilidade na geração e agilidade no controle.</p>
                </header>
            </article>
        </div>

        {{-- <div class="main_course_fullwidth">
            <div class="main_course_rating_content">
                <article class="main_course_rating_content_title">
                    <header>
                        <h2>Curso considerado 5 estrelas por nossos 100 alunos matriculados</h2>
                    </header>

                    <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                    <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                    <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                    <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                    <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                </article>

                <section class="main_course_rating_content_comment">
                    <header>
                        <h2>Veja o que estão falando sobre o curso</h2>
                    </header>

                    <article>
                        <header>
                            <h3>Gustavo Web</h3>
                            <p>10/01/2018</p>
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                        </header>

                        <p>O conteúdo é fantástico, eu não tinha conhecimento nenhum e agora estou desenvolvendo minhas páginas em HTML5 sem problemas</p>
                    </article>

                    <article>
                        <header>
                            <h3>Gustavo Web</h3>
                            <p>10/01/2018</p>
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                        </header>

                        <p>O conteúdo é fantástico, eu não tinha conhecimento nenhum e agora estou desenvolvendo minhas páginas em HTML5 sem problemas</p>
                    </article>

                    <article>
                        <header>
                            <h3>Gustavo Web</h3>
                            <p>10/01/2018</p>
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                        </header>

                        <p>O conteúdo é fantástico, eu não tinha conhecimento nenhum e agora estou desenvolvendo minhas páginas em HTML5 sem problemas</p>
                    </article>

                    <article>
                        <header>
                            <h3>Gustavo Web</h3>
                            <p>10/01/2018</p>
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                        </header>

                        <p>O conteúdo é fantástico, eu não tinha conhecimento nenhum e agora estou desenvolvendo minhas páginas em HTML5 sem problemas</p>
                    </article>

                    <article>
                        <header>
                            <h3>Gustavo Web</h3>
                            <p>10/01/2018</p>
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                        </header>

                        <p>O conteúdo é fantástico, eu não tinha conhecimento nenhum e agora estou desenvolvendo minhas páginas em HTML5 sem problemas</p>
                    </article>

                    <article>
                        <header>
                            <h3>Gustavo Web</h3>
                            <p>10/01/2018</p>
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                            <img src="{{ asset('site/_img/star.png') }}" alt="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Review do Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
                        </header>

                        <p>O conteúdo é fantástico, eu não tinha conhecimento nenhum e agora estou desenvolvendo minhas páginas em HTML5 sem problemas</p>
                    </article>
                </section>
            </div>
        </div> --}}
    </section>

    <div class="main_school">
        <section class="main_school_content">
            <header class="main_school_header">
                <h1>Bem vindo a Solutiomms </h1>
                <p>A sua empresa de controle de gestão</p>
            </header>

            <div class="main_school_content_left">
                <article class="main_school_content_left_content">
                    <header>
                        <p><span class="icon-facebook"><a href="#">Facebook</a></span> <span class="icon-google-plus2"><a href="#">Google+</a></span> <span class="icon-twitter"><a href="#">Twitter</a></span></p>
                        <h2>Tudo o que você precisa para ter controle de sua empresa em um só lugar</h2>
                    </header>

                    <p> A Solutiomms empresa de gestão trabalha desde então fornecendo soluções inteligente para nossos clientes, soluções simples do dia a dia que trazem um maior resultado para nossos clientres. </p>

                    <p>Que tal ter um sistema onde além de controle de gestão , ter todo o  resultado do marketing da sua empresa em um só lugar?</p>
                </article>

                <section class="main_school_list">
                    <header>
                        <h2>Veja um pouco do que podemos te oferecer:</h2>
                    </header>

                    <article>
                        <header>
                            <h3 class="icon-checkmark">Relatório de tráfego no seu site</h3>
                        </header>
                    </article>

                    <article>
                        <header>
                            <h3 class="icon-checkmark">Relatório de campanha de marketing</h3>
                        </header>
                    </article>

                    <article>
                        <header>
                            <h3 class="icon-checkmark">Relatórios de Propostas (vendas / orçamentos)</h3>
                        </header>
                    </article>

                    <article>
                        <header>
                            <h3 class="icon-checkmark">Gestão de Clientes / Fornecedores </h3>
                        </header>
                    </article>

                    <article>
                        <header>
                            <h3 class="icon-checkmark">Controle Financeiro </h3>
                        </header>
                    </article>

                    <article>
                        <header>
                            <h3 class="icon-checkmark">Emissão de Documentos Fiscais</h3>
                        </header>
                    </article>

                    <article>
                        <header>
                            <h3 class="icon-checkmark">Loja Virtual</h3>
                        </header>
                    </article>
                </section>
            </div>

            <div class="main_school_content_right">
                <img src="{{ asset('site/_img/Solutiomms.png') }}" alt="Solutiomms - Sistema de Gestão" title="Solutiomms - Sistema de Gestão">
            </div>

            <article class="main_school_address">
                <header>
                    <h2 class="icon-map2">Nos Encontre</h2>
                </header>

                <p>Rua Augusto Metzker, 110 - Parque das Arvores - Araras/SP - +55 19 9.7135-1777</p>
            </article>
        </section>
    </div>

   {{--  <section class="main_tutor">
        <div class="main_tutor_content">
            <header>
                <h1>Conheça o Gustavo Web, seu tutor nesse Curso</h1>
                <p>Eu vou te ajudar a criar sua webpage em HTML5 e CSS3</p>
            </header>

            <div class="main_tutor_content_img">
                <img src="{{ asset('site/_img/gustavoweb.jpg') }}" alt="Imagem do Gustavo Web" title="Imagem do Gustavo Web">
            </div>

            <article class="main_tutor_content_history">
                <header>
                    <h2>Formado em Ciência da Computação e Apaixonado por Web</h2>
                </header>

                <p>Como muitos, comecei na programação por conta dos jogos! Com o tempo, o amor pela programação foi crescendo a ponto de tomar como profissão e me especializar na área. Hoje, com a bagagem que tenho, compartilho meu conhecimento com todos os alunos da UpInside Treinamentos</p>
            </article>

            <section class="main_tutor_social_network">
                <header>
                    <h2>Me siga nas redes sociais</h2>
                </header>

                <article>
                    <header>
                        <h3><a href="#" class="icon-facebook">Meu Facebook</a></h3>
                    </header>
                </article>

                <article>
                    <header>
                        <h3><a href="#" class="icon-facebook2">Minha FanPage</a></h3>
                    </header>
                </article>

                <article>
                    <header>
                        <h3><a href="#" class="icon-google-plus2">Meu Google+</a></h3>
                    </header>
                </article>

                <article>
                    <header>
                        <h3><a href="#" class="icon-instagram">Meu Instagram</a></h3>
                    </header>
                </article>
            </section>
        </div>
    </section> --}}

</main>

@endsection