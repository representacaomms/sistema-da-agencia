<!DOCTYPE html>
<html lang="pt-br" itemscope itemtype="https://schema.org/WebSite">
    <head>
        <title>Solutiomms - Sistema de Gestão e Automação para empresas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="description" content="Solutiomms - Sistema de Gestão e Automação para empresas">
        <meta name="robots" content="index, follow">

        <link rel="canonical" href="https://www.solutiomms.com/">

        <meta itemprop="name" content="Solutiomms - Sistema de Gestão">
        <meta itemprop="description" content="Solutiomms - Sistema de Gestão e Automação para empresas">
        <meta itemprop="image" content=" {{ env('APP_URL') }}/site/_img/post.jpg">
        <meta itemprop="url" content="{{ env('APP_URL') }}/">

        <meta property="og:url" content="{{ env('APP_URL') }}/" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Solutiomms - Sistema de Gestão" />
        <meta property="og:description" content="Solutiomms - Sistema de Gestão e Automação para empresas" />
        <meta property="og:image" content="{{ env('APP_URL') }}/site/_img/post.jpg" />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="630" />
        {{-- <meta property="fb:app_id" content="816037398891575" /> --}}
        <meta property="og:locale" content="pt_BR" />{{-- 
        <meta property="article:author" content="https://www.facebook.com/guhweb" /> --}}
        <meta property="article:publisher" content="https://www.facebook.com/Solutiomms-110440683810111" />

        {{-- <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:domain" content="{{ env('APP_URL') }}">
        <meta name="twitter:creator" content="@guuweb">
        <meta name="twitter:title" content="Solutiomms - Sistema de Gestão">
        <meta name="twitter:description" content="Solutiomms - Sistema de Gestão e Automação para empresas">
        <meta name="twitter:image" content="{{ env('APP_URL') }}/site/_img/post.jpg">
        <meta name="twitter:url" content="{{ env('APP_URL') }}/"> --}}

        <link href="{{ asset('site/_cdn/fonticon.css') }}" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
        <link href="{{ asset('site/_cdn/boot.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('site/_cdn/style.css') }}" rel="stylesheet" type="text/css"/>
    </head>
    <body>

        @include('site.template.header')

        @yield('content')

        @include('site.template.footer')

        <script src="_cdn/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="_cdn/main.js" type="text/javascript"></script>
        @php
            require '/data/www/lojistas/public/site/_cdn/wc_track.php'
        @endphp

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155841383-2"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-155841383-2');
        </script>

        

    </body>
    <!--  LINKS DA CAMPANHA GOOGLE-->
   {{--  https://solutiomms.com/?utm_source=Facebook&utm_medium=banner&utm_campaign=capt-lead --}}
</html>
