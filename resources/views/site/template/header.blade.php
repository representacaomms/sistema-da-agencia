<header class="main_header">
    <div class="main_header_content">
        <a href="#" class="logo">
            <img src="{{ asset('site/_img/logo-preto.png') }}" alt="Bem vindo ao sistema de gestão e automação - SOLUTIOMMS" title="Bem vindo ao sistema de gestão e automação - SOLUTIOMMS">
        </a>

        <nav class="main_header_content_menu">
            <ul>
                <li><a href="{{ route('site') }}">Home</a></li>
                <li><a href="#">A Empresa</a></li>
                <li><a href="{{ route('blog') }}">Blog</a></li>
                <li><a href="#">Loja Virtual</a></li>
                <li><a href="#">Contato</a></li>
            <li><a href="{{route('home') }}">Area restrita</a></li>
            </ul>
        </nav>

        <nav class="main_header_content_menu_mobile">
            <ul>
                <li><span class="main_header_content_menu_mobile_obj icon-menu icon-notext"></span>
                    <ul class="main_header_content_menu_mobile_sub ds_none">
                        <li><a href="#">Home</a></li>
                        <li><a href="{{ route('blog') }}">Blog</a></li>
                        <li><a href="#">A Escola</a></li>
                        <li><a href="#">Contato</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</header>