<section class="main_optin_footer">
    <div class="main_optin_footer_content">
        <header>
            <h1>Quer receber nosso conteúdo exclusivo? Assina nossa lista VIP :)</h1>
        </header>

        <article>
            <header>
                <h2><a href="#" class="btn">Entrar para a Lista VIP</a></h2>
            </header>
        </article>
    </div>
</section>

<section class="main_footer">
    <header>
        <h1>Quer saber mais?</h1>
    </header>

    <article class="main_footer_our_pages">
        <header>
            <h2>Nossas Páginas</h2>
        </header>

        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">A Empresa</a></li>
            <li><a href="#">Loja Virtual</a></li>
            <li><a href="#">Contato</a></li>
        </ul>
    </article>

    <article class="main_footer_links">
        <header>
            <h2>Links Úteis</h2>
        </header>

        <ul>
            <li><a href="#">Política de Privacidade</a></li>
            <li><a href="#">Aviso Legal</a></li>
            <li><a href="#">Termos de Uso</a></li>
        </ul>
    </article>

    <article class="main_footer_about">
        <header>
            <h2>Sobre a Empresa</h2>
        </header>

        <p>A Solutiomms empresa atuante no ramo de desenvolvimento ofere soluções que trazem os resultados necessário para a sua empresa crescer!</p>
    </article>
</section>

<footer class="main_footer_rights">
    <p>Todos os Direitos Reservados a Solutiomms Sistema de Gestão ®</p>
</footer>