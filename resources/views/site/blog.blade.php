@extends('site.template.app')
@section('content')

    <section class="main_blog">
        <header class="main_blog_header">
            <h1 class="icon-blog">Nossos Últimos Artigos</h1>
            <p>Aqui você encontra os artigos necessários para auxiliar na sua caminhada ao melhor resultado.</p>
        </header>
        @foreach ($blogs as $blog)
        <article>
            <a href="#">
                <img src="{{ env('APP_URL') }}/storage/{{  $blog->img }}" title="{{ $blog->titulo}}">
            </a>
            <p><a href="#" class="category">{{$blog->categoria}} </a></p>
            <h2><a href="#" class="title">{{ $blog->titulo}} </a></h2>
        </article>
        @endforeach
    </section>

@endsection
