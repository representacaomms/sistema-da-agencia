<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu ">
    <div class="slimscroll-menu" id="left-side-menu-container">

        <!-- LOGO -->
        <a href="{{ route('home') }}" class="logo text-center">
            <span class="logo-lg">
                <img src="https://agenciamms.com/wp-content/uploads/2021/11/cropped-logo.png" alt="" height="72">
            </span>
            <span class="logo-sm">
                <img src="https://agenciamms.com/wp-content/uploads/2021/11/cropped-logo.png" alt="" height="25">
            </span>
        </a>
        <div class="h-100" id="leftside-menu-container" data-simplebar>
            <!--- Sidemenu -->
            <ul class="metismenu side-nav">
                <li class="side-nav-title side-nav-item">Navegação</li>
    
                <li class="side-nav-item">
                    <a href="javascript: void(0);" class="side-nav-link">
                        <i class="dripicons-meter"></i>
                        <span class="badge badge-success float-right"></span>
                        <span> Paineis</span>
                    </a>
                    <ul class="side-nav-second-level" aria-expanded="false">
                        @can('Painel CRM')
                        <li>
                            <a href="{{ route('home') }}">CRM</a>
                        </li>
                        @endcan
                        {{-- <li>  
                                <a href="#">CRM</a>  
                            </li>  
                            <li>  
                                <a href="#">Projects</a>  
                            </li> --}}
                    </ul>
                </li>
                <li class="side-nav-title side-nav-item">Configuração</li>
                {{-- <li class="side-nav-item">  
                        <a href="javascript: void(0);" class="side-nav-link">  
                            <i class="dripicons-gear"></i>  
                            <span> Empresa </span>  
                            <span class="menu-arrow"></span>  
                        </a>  
                        <ul class="side-nav-second-level" aria-expanded="false">  
                            <li><a href="{{ route('empresa.show', Auth::user()->id_empresa) }}">Empresa</a></li>  
                        </ul>  
                        <ul class="side-nav-second-level" aria-expanded="false">  
                            <li><a href="{{ route('impostos.show', Auth::user()->id_empresa) }}">Impostos</a></li>  
                        </ul>  
                        <ul class="side-nav-second-level" aria-expanded="false">  
                            <li class="side-nav-item">  
                                <a href="{{ route('estoques.index') }}">Estoque</a>  
                            </li>  
                        </ul>  
                    </li> --}}
                    @can('Gestão Financeira')
                    <li class="side-nav-item">
                        <a href="javascript: void(0);" class="side-nav-link">
                            <i class="dripicons-battery-full"></i>
                            <span> Financeiro </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <ul class="side-nav-second-level" aria-expanded="false">
                            <li><a href="{{ route('cadastros.diversos') }}">Diversos</a></li>
                        </ul>
                        <ul class="side-nav-second-level" aria-expanded="false">
                            <li><a href="{{ route('custos.index') }}">Custos</a></li>
                        </ul>
                        <ul class="side-nav-second-level" aria-expanded="false">
                            <li><a href="{{ route('custos-salarios.index') }}">Salários</a></li>
                        </ul>
        
                        <ul class="side-nav-second-level" aria-expanded="false">
                            <li><a href="{{ route('receitas.index') }}">Receitas</a></li>
                        </ul>
                        <ul class="side-nav-second-level" aria-expanded="false">
                            <li><a href="{{ route('despesas.index') }}">Despesas</a></li>
                        </ul>
                        <ul class="side-nav-second-level" aria-expanded="false">
                            <li><a href="{{ route('contas-bancarias.index') }}">Contas Bancária</a></li>
                        </ul>
                    </li>
                  <li class="side-nav-item">
                        <a href="javascript: void(0);" class="side-nav-link">
                            <i class="dripicons-battery-full"></i>
                            <span> Consultoria </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <ul class="side-nav-second-level" aria-expanded="false">
                            <li><a href="{{ route('plataformas.index') }}">Plataformas</a></li>
                        </ul>
                       
                    </li> 
                    @endcan
                {{-- <li class="side-nav-title side-nav-item mt-2">Site</li>  
                    <li class="side-nav-item">  
                        <a href="javascript: void(0);" class="side-nav-link">  
                            <i class="dripicons-gear"></i>  
                            <span> Blogs </span>  
                            <span class="menu-arrow"></span>  
                        </a>  
                        <ul class="side-nav-second-level" aria-expanded="false">  
                            <li><a href="{{ route('blogs.index') }}">listar Blogs</a></li>  
                        </ul>  
                    </li> --}}
                <li class="side-nav-title side-nav-item">Gestão</li>
                @can('Gestão Planos')
                <li class="side-nav-item">
                    <a href="javascript: void(0);" class="side-nav-link">
                        <i class="dripicons-article"></i>
                        <span>Planos</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="side-nav-second-level" aria-expanded="false">
                        @can('Listar Planos')
                        <li class="side-nav-item">
                            <a href="{{ route('planos-gestao.index') }}">Planos Gestão</a>
                        </li>
                        @endcan
                        @can('Listar Serviços')
                        <li class="side-nav-item">
                            <a href="{{ route('servicos-agenciado.index') }}">Serviços</a>
                        </li>
                        @endcan
                        @can('Listar Serviços')
                            <li class="side-nav-item">  
                                <a href="{{ route('produtos.index') }}">Produtos</a>  
                            </li>  
                        @endcan




                        
                    </ul>
                </li>
                {{-- <li class="side-nav-item">
                    <a href="javascript: void(0);" class="side-nav-link">
                        <i class="dripicons-article"></i>
                        <span>Anúncios</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="side-nav-second-level" aria-expanded="false">
                        
                        <li class="side-nav-item">
                            <a href="{{ route('gestao-anuncios.index') }}">Listar Anúncios</a>
                        </li>
    
                    </ul>
                </li> --}}
                @endcan
                {{-- <li class="side-nav-item">  
                        <a href="javascript: void(0);" class="side-nav-link">  
                            <i class="dripicons-clockwise"></i>  
                            <span>Movimentação</span>  
                            <span class="menu-arrow"></span>  
                        </a>  
                        <ul class="side-nav-second-level" aria-expanded="false">  
                            <li class="side-nav-item">  
                                <a href="{{ route('notafiscal.index') }}">Nota Fiscal</a>  
                            </li>  
                            <li class="side-nav-item">  
                                <a href="{{ route('compras.index') }}">Compras</a>  
                            </li>  
                            <li class="side-nav-item">  
                                <a href="{{ route('estoques.index') }}">Estoques</a>  
                            </li>  
                        </ul>  
                    </li> --}}
                    @can('Gestão Pessoas')
                    <li class="side-nav-item">
                        <a href="javascript: void(0);" class="side-nav-link">
                            <i class="dripicons-user-group"></i>
                            <span> Pessoas</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <ul class="side-nav-second-level" aria-expanded="false">
                            @can('Listar Clientes')
                                <li class="side-nav-item">
                                    <a href="{{ route('clientes.index') }}">Clientes</a>
                                </li>
                            @endcan
                            @can('Listar Fornecedores')
                            <li class="side-nav-item">
                                <a href="{{ route('fornecedores.index') }}">Fornecedores</a>
                            </li>
                            @endcan
                        </ul>
                    </li>
                        
                    @endcan
                    @can('Gestão Acessos')
                  {{--   <li class="side-nav-item">
                        <a href="javascript: void(0);" class="side-nav-link">
                            <i class="dripicons-clockwise"></i>
                            <span> Acessos</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <ul class="side-nav-second-level" aria-expanded="false">
    
                            <li class="side-nav-item">
                                <a href="{{ route('usuarios.index') }}">Usuários</a>
                            </li>
                            <li class="side-nav-item">
                                <a href="{{ route('funcoes.index') }}">Funções</a>
                            </li>
                            <li class="side-nav-item">
                                <a href="{{ route('permissoes.index') }}">Permissões</a>
                            </li>
    
                        </ul>
                    </li> --}}
                    @endcan
                    @can('Gestão Vendas')
                    <li class="side-nav-item">
                        <a href="javascript: void(0);" class="side-nav-link">
                            <i class="dripicons-cart"></i>
                            <span> Vendas </span>
                            <span class="menu-arrow"></span>
                        </a>
                        {{-- <ul class="side-nav-second-level" aria-expanded="false">  
                            <li class="side-nav-item">  
                            <a href="{{ route('pdv.create') }}">PDV</a>  
                            </li>  
                         </ul> --}}
                         @can('Listar Propostas')
                         <ul class="side-nav-second-level" aria-expanded="false">
                             <li class="side-nav-item">
                                 <a href="{{ route('propostas.index') }}">Propostas</a>
                             </li>
                         </ul>
                         @endcan
    
                        
    
                    </li>
                    @endcan

                <li class="side-nav-title side-nav-item mt-1">Processos</li>
    
                <li class="side-nav-item">
                    <a href="#" class="side-nav-link">
                        <i class="dripicons-copy"></i>
                        <span> Prospecção </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="side-nav-second-level" aria-expanded="false">  
                        <li class="side-nav-item">  
                            <a href="{{ route('consultorias.index') }}">Consultoria</a>  
                        </li>  
                        <li class="side-nav-item">  
                            <a href="{{ route('contratos.index') }}">Contratos</a>  
                        </li>  
                        <li class="side-nav-item">  
                            <a href="{{ route('apresentacao.index') }}">Apresentações</a>  
                        </li>  
                     </ul>
                   
                </li> 
    
            </ul>
        </div>

        

        <div class="clearfix">v1.0.0</div>
    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
