
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8" />
    <title>Painel | Solutiomms - Sistema de Gestão</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Soluções me Tecnologia para empresas." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('/images/favicon.ico')}}">

    <!-- third party css -->
    <link href="{{ asset('/css/style.css')}}" rel="stylesheet" type="text/css" />    
    <link href="{{ asset('/css/vendor/jquery-jvectormap-1.2.2.css')}}" rel="stylesheet" type="text/css" />    
    <link href="{{ asset('/css/vendor/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->


    <!-- App css -->
    <link href="{{ asset('/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/app.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/style.css')}}" rel="stylesheet" type="text/css" />

    <!-- third party css -->
    <link href="{{ asset('css/vendor/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/vendor/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />

    <!-- SELECT 2 -->
    <link href="{{ asset('css/vendor/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
    @yield('css')
</head>

 <body>

    <div id="app">
        <!-- Begin page -->
        <div class="wrapper">
            @yield('content')
        
        </div>
        <div class="rightbar-overlay"></div>
        <!-- /Right-bar -->
    </div>


    <!-- bundle -->
    <script src="{{ asset('js/vendor.min.js')}}"></script>
    <script src="{{ asset('js/app.min.js')}}"></script>
    <script src="{{ asset('js/app.js')}}"></script>
   
    <!-- third party js -->
    <script src="{{ asset('js/vendor/Chart.bundle.min.js')}}"></script>
    <script src="{{ asset('js/vendor/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{ asset('js/vendor/jquery-jvectormap-world-mill-en.js')}}"></script>
    <!-- third party js ends -->
    <!-- third party js -->
    <script src="{{ asset('js/vendor/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('js/vendor/dataTables.bootstrap4.js')}}"></script>
    <script src="{{ asset('js/vendor/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('js/vendor/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('js/vendor/dataTables.checkboxes.min.js')}}"></script>
    <script src="{{ asset('js/vendor/fullcalendar.min.js')}}"></script>
    <script src="{{ asset('js/vendor/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('js/vendor/dragula.min.js')}}"></script>
    <script src="{{ asset('js/ui/component.dragula.js')}}"></script>
    <!-- third party js ends -->

    <!-- Datatables js -->
    <script src="{{ asset('js/vendor/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/vendor/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/vendor/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/vendor/responsive.bootstrap4.min.js') }}"></script>  

    <!-- Typehead -->
    <script src="{{ asset('js/vendor/handlebars.min.js')}}"></script>
    <script src="{{ asset('js/vendor/typeahead.bundle.min.js')}}"></script>

    <!-- Scripst -->
    <script src="{{ asset('js/scripts_pdv.js')}}"></script>
    <script>
        $('.dropdown-toggle').dropdown();
   </script>
    @yield('js')
</body>
</html>
