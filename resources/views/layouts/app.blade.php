
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8" />
    <title>Painel | Agencia MMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Soluções me Tecnologia para empresas." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App favicon -->
    <link rel="shortcut icon" href="https://agenciamms.com/wp-content/uploads/2021/11/cropped-Favicon-192x192.jpeg" sizes="192x192">

    <!-- third party css -->
    <link href="{{ asset('/css/vendor/jquery-jvectormap-1.2.2.css')}}" rel="stylesheet" type="text/css" />    
    <link href="{{ asset('/css/vendor/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- third party css end -->


    <!-- App css -->
    <link href="{{ asset('/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/app.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/gestao.css')}}" rel="stylesheet" type="text/css" />

    <!-- third party css -->
    <link href="{{ asset('css/vendor/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/vendor/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />

    <!-- SELECT 2 -->
    <link href="{{ asset('css/vendor/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />

  
    @yield('css')
    @livewireStyles
</head>

<body class="loading hide-menu" data-layout="full" data-layout-config='{ "leftSidebarCondensed":true, "leftSidebarScrollable":true}'>     <div class="wrapper">
            @include('layouts.sidebar')
       

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">
                @include('layouts.topbar')
                <!--@include('layouts.navbar') -->
                @yield('content')
            </div>
            <!-- content -->
            <!-- Footer Start -->
           @include('layouts.footer')
            <!-- end Footer -->

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->

   

    @include('layouts.right_bar')

    <div class="rightbar-overlay"></div>
    <!-- /Right-bar -->

   
    <!-- bundle -->
    <script src="{{ asset('js/vendor.min.js')}}"></script>
    <script src="{{ asset('js/app.min.js')}}"></script>
   
    <!-- third party js -->
    <script src="{{ asset('js/vendor/Chart.bundle.min.js')}}"></script>
    <script src="{{ asset('js/vendor/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{ asset('js/vendor/jquery-jvectormap-world-mill-en.js')}}"></script>
    <!-- third party js ends -->
    <!-- third party js -->
    <script src="{{ asset('js/vendor/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('js/vendor/dataTables.bootstrap4.js')}}"></script>
    <script src="{{ asset('js/vendor/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('js/vendor/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('js/vendor/dataTables.checkboxes.min.js')}}"></script>
    <script src="{{ asset('js/vendor/fullcalendar.min.js')}}"></script>
    <script src="{{ asset('js/vendor/jquery-ui.min.js')}}"></script>
    <!-- third party js ends -->

    <!-- Datatables js -->
    <script src="{{ asset('js/vendor/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/vendor/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/vendor/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/vendor/responsive.bootstrap4.min.js') }}"></script>  
    
    
    <!-- Typehead -->
    <script src="{{ asset('js/vendor/handlebars.min.js')}}"></script>
    <script src="{{ asset('js/vendor/typeahead.bundle.min.js')}}"></script>

    <!-- Scripst -->
    <script src="{{ asset('js/scripts.js')}}"></script>
    <script>
        $('.dropdown-toggle').dropdown();
/* 
        $('.button-menu-mobile').trigger('click'); */
   </script>
    @yield('js')
      @livewireScripts
</body>
</html>
