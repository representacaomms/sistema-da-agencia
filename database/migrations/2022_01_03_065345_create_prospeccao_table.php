<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProspeccaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospeccao', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_empresa');
            $table->string('tipo')->nullable();
            $table->string('status')->nullable();
            $table->string('nome');
            $table->string('email');
            $table->string('telefone');
            $table->string('empresa')->nullable();
            $table->string('site')->nullable();
            $table->string('funcionarios')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prospeccao');
    }
}
