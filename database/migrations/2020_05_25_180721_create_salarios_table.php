<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_empresa');
            $table->string('nome');
            $table->decimal('salario', 8, 2);
            $table->decimal('ferias', 8, 2);
            $table->decimal('adicional_ferias', 8, 2);
            $table->decimal('fgts_ferias_adicional', 8, 2);
            $table->decimal('salario_13', 8, 2);
            $table->decimal('fgts_salario_13', 8, 2);
            $table->decimal('fgts_salario', 8, 2);
            $table->decimal('aviso_previo', 8, 2);
            $table->decimal('fgts_aviso_previo', 8, 2);
            $table->decimal('multa_fgts', 8, 2);
            $table->decimal('total_provisoes', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salarios');
    }
}
