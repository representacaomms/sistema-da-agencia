<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCamposTablePlataformaItens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plataforma_itens', function (Blueprint $table) {
            $table->string('tipo')->nullable()->after('acao');
            $table->string('label')->nullable()->after('tipo');
            $table->string('input')->nullable()->after('label');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plataforma_itens', function (Blueprint $table) {
            $table->dropColumn('tipo');
            $table->dropColumn('label');
            $table->dropColumn('input');
        });
    }
}
