<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanoGestaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plano_gestaos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('hrs');
            $table->string('ca');
            $table->string('infra');
            $table->string('ga');
            $table->string('fa');
            $table->string('em');
            $table->string('rm');
            $table->float('vp',9, 2);
            $table->float('vi',9, 2);
            $table->longText('adicionais');
            $table->longText('escopo');
            $table->string('status');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plano_gestaos');
    }
}
