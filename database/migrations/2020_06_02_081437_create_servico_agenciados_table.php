<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicoAgenciadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servico_agenciados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_empresa');
            $table->string('nome');
            $table->string('especificacao');
            $table->string('adicionais')->nullable();
            $table->string('escopo');
            $table->string('status');
            $table->string('categoria');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servico_agenciados');
    }
}
