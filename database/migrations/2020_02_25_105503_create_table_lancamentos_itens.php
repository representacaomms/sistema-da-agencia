<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableLancamentosItens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lancamentos_itens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_lancamento');
            $table->string('parcela')->nullable();
            $table->string('valor_amoretizado')->nullable();
            $table->string('valor_parcela');                        
            $table->dateTime('data_pagto');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lancamentos_itens');
    }
}
