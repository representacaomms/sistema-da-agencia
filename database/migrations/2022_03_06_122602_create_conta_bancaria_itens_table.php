<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContaBancariaItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conta_bancaria_itens', function (Blueprint $table) {
            $table->id();
            $table->string('id_conta_bancaria');
            $table->string('data_pagto');
            $table->string('id_fatura');
            $table->string('valor');
            $table->string('tipo');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conta_bancaria_itens');
    }
}
