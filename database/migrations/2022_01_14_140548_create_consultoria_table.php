<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultoria', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_empresa');
            $table->string('cnpj');
            $table->string('nome');
            $table->string('email');
            $table->string('telefone');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultoria');
    }
}
