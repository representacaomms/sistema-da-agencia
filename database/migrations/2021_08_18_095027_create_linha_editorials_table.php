<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinhaEditorialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linhas_editoriais', function (Blueprint $table) {
            $table->id();
            $table->string('id_cliente');
            $table->string('PSM');
            $table->string('publico_alvo');
            $table->string('mundo_ideal');
            $table->string('promessa');
            $table->string('motivo_racional');
            $table->string('vantagem_competitiva');
            $table->string('posicao_unica_vendas');
            $table->string('persona');
            $table->string('o_que_ele_busca');
            $table->string('o_que_ele_teme');
            $table->string('o_que_impede_ele');
            $table->string('a_b');
            $table->string('linha_principal');
            $table->string('auxiliar1');
            $table->string('auxiliar2');
            $table->string('auxiliar3');
            $table->string('auxiliar4');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linha_editorials');
    }
}
