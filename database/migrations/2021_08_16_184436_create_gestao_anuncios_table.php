<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGestaoAnunciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gestao_anuncios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_cliente');
            $table->string('plataforma');
            $table->string('data_lancamento');            
            $table->string('nome_campanha');
            $table->string('tipo_campanha');
            $table->string('custo_total');
            $table->string('impressoes');
            $table->string('cliques');
            $table->string('conversoes');
            $table->string('cpm_medio');
            $table->string('ctr');
            $table->string('taxa_conversao');
            $table->string('custo_conversao');
            $table->string('pagina_destino');
            $table->string('custo_lead');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gestao_anuncios');
    }
}
