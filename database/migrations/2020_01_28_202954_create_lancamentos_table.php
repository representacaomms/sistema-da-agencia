<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLancamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lancamentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_cli');
            $table->string('id_empresa');
            $table->string('descricao');
            $table->string('id_plano_contas');
            $table->string('id_centro_custo');
            $table->string('id_categoria');
            $table->string('id_subcategoria');
            $table->string('tipo');
            $table->string('valor_lancamento');
            $table->string('parcela');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lancamentos');
    }
}
