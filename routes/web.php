<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ClientesController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\CustoController;
use App\Http\Controllers\SalarioController;
use App\Http\Controllers\PlanosController;
use App\Http\Controllers\ServicoAgenciadoController;
use App\Http\Controllers\BaseCustoController;
use App\Http\Controllers\CadastroDiversosFinancController;
use App\Http\Controllers\ContaController;
use App\Http\Controllers\CentroCustoController;
use App\Http\Controllers\CategoriaLancController;
use App\Http\Controllers\SubCategoriaLancController;
use App\Http\Controllers\LancamentoController;
use App\Http\Controllers\ReceitasController;
use App\Http\Controllers\DespesasController;
use App\Http\Controllers\FornecedoresController;
use App\Http\Controllers\PropostasController;
use App\Http\Controllers\LeadsController;
use App\Http\Controllers\GestaoAnuncioController;
use App\Http\Controllers\LinhaEditorialController;
use App\Http\Controllers\ProspeccaoController;
use App\Http\Controllers\ConversaController;
use App\Http\Controllers\ConsultoriaController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ContratoController;
use App\Http\Controllers\ApresentacaoController;
use App\Http\Controllers\ContaBancariaController;
use App\Http\Controllers\PlataformaController;
use App\Http\Controllers\PlataformaItensController;
use App\Http\Controllers\GoogleAdsController;
use App\Http\Controllers\ProdutoController;
use App\Http\Controllers\PlanoGestaoController;
use App\Http\Livewire\Leads;



Auth::routes();
Route::get('/', [HomeController::class, 'index'])->name('home');



Route::group(['prefix' => 'pdf'], function () {
    Route::get('resumo/{id}', [PdfController::class, 'resumo'])->name('pdf.resumo');
    Route::get('recibo/{id}', [PdfController::class, 'recibo'])->name('pdf.recibo');
});



Route::group(['prefix' => 'gestao-financeira'], function () {
    Route::resource('custos', CustoController::class);
    Route::resource('custos-salarios', SalarioController::class);
    Route::resource('planos', PlanosController::class);
    Route::resource('servicos-agenciado', ServicoAgenciadoController::class);
    Route::resource('base-custos', BaseCustoController::class);
    Route::get('cadastros-diversos', [CadastroDiversosFinancController::class, 'index'])->name('cadastros.diversos');
    Route::resource('plano-contas', ContaController::class);
    Route::resource('centro-custos', CentroCustoController::class);
    Route::resource('categorias', CategoriaLancController::class);
    Route::resource('subcategorias', SubCategoriaLancController::class);
    Route::resource('lancamentos', LancamentoController::class);

    Route::resource('receitas', ReceitasController::class);
    Route::get('receitas/detal/{id}', [ReceitasController::class, 'detal'])->name('receitas.detal');

    Route::resource('despesas', DespesasController::class);
    Route::get('despesas/detal/{id}', [DespesasController::class, 'detal'])->name('despesas.detal');
    Route::post('despesas/relatorio', [DespesasController::class, 'relatorio'])->name('despesas.relatorio');


    Route::resource('contas-bancarias', ContaBancariaController::class);
    Route::resource('propostas', PropostasController::class);
    Route::get('propostas/invoice/{id}', [PropostasController::class, 'invoice'])->name('propostas.invoice');

    Route::resource('produtos', ProdutoController::class);

    
});

Route::group(['prefix' => 'gestao-pessoas'], function () {
    Route::get('clientes/{id}/active', [ClientesController::class, 'active'])->name('clientes.active');
    Route::resource('clientes', ClientesController::class);

    Route::get('fornecedores/{id}/active', [FornecedoresController::class, 'active'])->name('fornecedores.active');
    Route::resource('fornecedores', FornecedoresController::class);

    Route::get('leads/{id}/active', [LeadsController::class, 'active'])->name('leads.active');
    Route::resource('leads-controller', LeadsController::class);

    Route::get('usuarios/{id}/funcoes',[UserController::class, 'funcoes'])->name('usuarios.funcoes');
    Route::put('usuarios/{id}/funcoes/sync',[UserController::class, 'funcoesSync'])->name('usuarios.funcoesSync');
    Route::resource('usuarios', UserController::class);

    Route::get('funcoes/{id}/permissoes',[RoleController::class, 'permissoes'])->name('funcoes.permissoes');
    Route::put('funcoes/{id}/permissoes/sync',[RoleController::class, 'permissoesSync'])->name('funcoes.permissoesSync');
    Route::resource('funcoes', RoleController::class);

    Route::resource('permissoes', PermissionController::class);
    Route::get('leads', Leads::class);

});

Route::group(['prefix' => 'gestao-marketing'], function () {
    Route::get('consultorias/consultoria', [ConsultoriaController::class, 'consultoria'])->name('consultorias.consultoria');
    Route::post('consultorias/montar', [ConsultoriaController::class, 'montar'])->name('consultorias.montar');
    Route::resource('consultorias', ConsultoriaController::class);
    Route::resource('contratos', ContratoController::class);
    Route::resource('apresentacao', ApresentacaoController::class);

    Route::get('gestao-anuncios/{id}/relatorio', [GestaoAnuncioController::class, 'relatorio'])->name('gestao-anuncios.relatorio');
    Route::resource('gestao-anuncios', GestaoAnuncioController::class);
});

Route::group(['prefix' => 'configuracao'], function () {
    Route::resource('plataformas', PlataformaController::class);
    Route::resource('plataforma-itens', PlataformaItensController::class);
    Route::resource('planos-gestao', PlanoGestaoController::class);
});
